<?php

function getUrlEntity($object, $id = null)
{
	$link = "/" . trans('routes.products') . "/";

	if( get_class($object) == "App\Models\Category" ) {
		$link = "/" . trans('routes.categories') . "/";
	}

	$entityId = isset($object->pid) ? $object->pid : $object->id;
	return ($link . $object->slug . "/" . $entityId);
}
