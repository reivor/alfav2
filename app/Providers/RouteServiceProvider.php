<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
		
		$this->checkStoreConfig();
		$storeId = config('store.id');
		
		switch( $storeId ) {
			case 'co':
				$route_file = 'Http/routes_co.php';
				break;
			case 'ec':
				$route_file = 'Http/routes_ec.php';
				break;
			case 'us':
				$route_file = 'Http/routes_us.php';
				break;
			default:
				$route_file = 'Http/routes_co.php';
				break;
		}
		
		$router->group(['namespace' => 'App\Http\Controllers\Store', 'middleware' => ['web', 'locale', 'store']], function() use ($route_file) {
			require app_path($route_file);
		});
    }
	
	private function checkStoreConfig()
	{
		
		if( !isset($_SERVER['HTTP_HOST']) ) {
			// cli
			return;
		}
		
		switch($_SERVER['HTTP_HOST']) {
			case env('CO_STORE_URL'):
				config([
					'store.id' => env('CO_STORE_ID', 'co'),
					'store.locale' => env('CO_STORE_LOCALE', 'es'),
					'store.timezone' => env('CO_STORE_TIMEZONE', 'America/Bogota')
				]);
				break;
			case env('EC_STORE_URL'):
				config([
					'store.id' => env('EC_STORE_ID', 'ec'),
					'store.locale' => env('EC_STORE_LOCALE', 'es'),
					'store.timezone' => env('EC_STORE_TIMEZONE', 'America/Bogota')
				]);
				break;
			case env('US_STORE_URL'):
				config([
					'store.id' => env('US_STORE_ID', 'us'),
					'store.locale' => env('US_STORE_LOCALE', 'en'),
					'store.timezone' => env('US_STORE_TIMEZONE', 'America/Bogota')
				]);
				break;
			default:
				config([
					'store.id' => env('CO_STORE_ID', 'co'),
					'store.locale' => env('CO_STORE_LOCALE', 'es'),
					'store.timezone' => env('CO_STORE_TIMEZONE', 'America/Bogota')
				]);
		}
		
	}
	
}
