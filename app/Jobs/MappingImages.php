<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use App\Models\Product;
use App\Models\ProductImages;

class MappingImages extends Job implements ShouldQueue
{
    public function handle()
    {
        $directories = Storage::disk('images')->directories();
        config(['database.default' => 'co']);
        $products_co = Product::get();
        $this->saveImages($products_co, $directories);
        config(['database.default' => 'ec']);
        $products_ec = Product::get();
        $this->saveImages($products_ec, $directories);
        config(['database.default' => 'us']);
        $products_us = Product::get();
        $this->saveImages($products_us, $directories);
    }

    private function saveImages($products,$directories)
    {
        foreach ($products as $product):
            if(array_search($product->code,$directories)!== false){
                $files = Storage::disk('images')->files($product->code);
                foreach ($files as $file):
                    $trash = explode('/',$file);
                    $name = explode('.',$trash[1]);
                    if(!empty($name[0]) && !empty($name[1])){
                        if(!ProductImages::where('filename', $trash[1] )->first()){
                            $image = new ProductImages();
                            $image->filename = $trash[1];
                            $image->path = public_path('images/products').'/'.$product->code.'/'.$trash[1];
                            $image->url = '/images/products/'.$product->code.'/'.$trash[1];
                            $image->product_id = $product->id;
                            $image->save();
                        }
                    }
                endforeach;
            }
        endforeach;
    }
	
}
