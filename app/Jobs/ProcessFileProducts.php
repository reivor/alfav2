<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Models\ProductImages;
use App\Models\RelatedProduct;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\Models\Product;
use App\Models\Reference;
use Illuminate\Support\Facades\DB;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use App\Models\ProductUpload;

class ProcessFileProducts extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

	private $file;
	
    public function __construct($file)
	{
        $this->file = $file;
    }

    public function handle()
	{
		
		
		try {
			
			// excel reader
			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($this->file);
			$file_log = 'log_'.date("YmdHis").'.xlsx';

			// excel writer
			$writer = WriterFactory::create(Type::XLSX);
			$writer->openToFile(public_path().'/files/products/'.$file_log);
			$singleRow[0] = 'Fila';
			$singleRow[1] = 'Tipo';
			$singleRow[2] = 'Mensaje';
			$writer->addRow($singleRow);

			// marcar upload como en proceso...
			$upload = ProductUpload::where('file', $this->file)->first();
			$upload->status = ProductUpload::STATUS_PROCESANDO;
			$upload->fecha_process_start = date("Y-m-d H:i:s");
			$upload->save();
		}
		catch(\Exception $e) {
			Log::error($e->getMessage());
		}

		try {
			
			DB::connection('co')->beginTransaction();
			DB::connection('ec')->beginTransaction();
			DB::connection('us')->beginTransaction();

			foreach ($reader->getSheetIterator() as $sheet) {
				$rows = $sheet->getRowIterator();
				$j = 1;
				$total_error = 0;
				$total_warnings = 0;
				$total_success = 0;

				//primera pasada, se graban los productos, las referencias y las categorias
				foreach ($rows as $index => $row) {
					
					if( $index == 1) {
						$first_row = $row;
						$j++;
						continue;
					}
					
					if(count($row) == 23) {
							$colombia = $row[3];
							$ecuador = $row[4];
							$usa = $row[5];

							try{
								if($colombia == 'X' || $colombia == 'x'){
									config(['database.default' => 'co']);
									$validate = $this->getValidateFromExcelRow($row,$j,$writer,$first_row);
									$total_error = $total_error + $validate[0];
									$total_warnings = $total_warnings + $validate[1];
									$reference = $this->getReferenceFromExcelRow($row);
									$product = $this->getProductFromExcelRow($row);
									$this->saveProducts($reference,$product,$row);
								}
								if($ecuador == 'X' || $ecuador == 'x'){
									config(['database.default' => 'ec']);
									$validate = $this->getValidateFromExcelRow($row,$j,$writer,$first_row);
									$total_error = $total_error + $validate[0];
									$total_warnings = $total_warnings + $validate[1];
									$reference = $this->getReferenceFromExcelRow($row);
									$product = $this->getProductFromExcelRow($row);
									$this->saveProducts($reference,$product,$row);
								}
								if($usa == 'X' || $usa == 'x'){
									config(['database.default' => 'us']);
									$validate = $this->getValidateFromExcelRow($row,$j,$writer,$first_row);
									$total_error = $total_error + $validate[0];
									$total_warnings = $total_warnings + $validate[1];
									$reference = $this->getReferenceFromExcelRow($row);
									$product = $this->getProductFromExcelRow($row);
									$this->saveProducts($reference,$product,$row);
								}
								$total_success = $total_success + 1;
							}
							catch(\Exception $e) {
								Log::error($e->getMessage());
								$total_error = $total_error + 1;
								$singleRow[0] = $index;
								$singleRow[1] = 'ERROR';
								$singleRow[2] = 'Error Guardando';
								$writer->addRow($singleRow); // add a row at a time
							}
					}
					else{
						$total_error = $total_error + 1;
						$singleRow[0] = $index;
						$singleRow[1] = 'ERROR';
						$singleRow[2] = 'Formato de fila inválido';
						$writer->addRow($singleRow); // add a row at a time
					}
					$j++;
				}
				if($total_error == 0){
					$j = 1;
					//segunda pasada, se graba las relaciones con los productos
					foreach ($rows as $index => $row) {
						if( $index == 1 ) {
							$first_row = $row;
							$j++;
							continue;
						}
						$colombia=$row[3];
						$ecuador=$row[4];
						$usa=$row[5];

						$related = explode(';',$row[18]);
						if(!empty($related[0])){
							try{
								if($colombia == 'X' || $colombia == 'x'){
									config(['database.default' => 'co']);
									$product_id = Product::where('code', $row[0])->first();
									RelatedProduct::where('product_id', $product_id->id )->delete();
									$this->saveRelatedProducts($related,$product_id);
								}
								if($ecuador == 'X' || $ecuador == 'x'){
									config(['database.default' => 'ec']);
									$product_id = Product::where('code', $row[0])->first();
									RelatedProduct::where('product_id', $product_id->id )->delete();
									$this->saveRelatedProducts($related,$product_id);
								}
								if($usa == 'X' || $usa == 'x'){
									config(['database.default' => 'us']);
									$product_id = Product::where('code', $row[0])->first();
									RelatedProduct::where('product_id', $product_id->id )->delete();
									$this->saveRelatedProducts($related,$product_id);
								}
								$total_success = $total_success + 1;
							}catch(\Exception $e) {
								$total_error = $total_error + 1;
								$singleRow[0] = $index;
								$singleRow[1] = 'ERROR';
								$singleRow[2] = 'El campo Productos Relacionados contiene uno o más productos que no existen';
								$writer->addRow($singleRow); // add a row at a time
							}
						}
						$j++;
					}
				}
				$writer->close();
				break;
			}
			$reader->close();
			if($total_error == 0){
				DB::connection('co')->commit();
				DB::connection('ec')->commit();
				DB::connection('us')->commit();
			}
			else{
				DB::connection('co')->rollBack();
				DB::connection('ec')->rollBack();
				DB::connection('us')->rollBack();
			}
		}
		catch(\Exception $e) {
			Log::error($e->getMessage());
			DB::connection('co')->rollBack();
			DB::connection('ec')->rollBack();
			DB::connection('us')->rollBack();
		}

		try{
			config(['database.default' => 'common']);
			$upload->status = ProductUpload::STATUS_PROCESADO;
			$upload->total_success = $total_success;
			$upload->total_error = $total_error;
			$upload->total_warnings = $total_warnings;
			$upload->file_log = $file_log;
			$upload->fecha_process_end = date("Y-m-d H:i:s");
			$upload->save();
		}
		catch(\Exception $e){
			echo $e->getMessage()."\n";
			Log::error($e->getMessage());
		}

		return (true);
    }
	
	private function getProductFromExcelRow($row)
	{
		$product = new Product();
		$product = $product->firstOrNew(['code' => $row[0]]);
		$product->code = $row[0];
		$product->color_code = $row[21];
		$product->name = $row[2];
		$product->dimensions = $row[6];
		$product->color = $row[8];
		$product->tags = str_replace(";", ",", $row[19]);

		$product->description = $row[9];
		/*$product->enforcement = $row[10];
		$product->use = $row[11];
		$product->ambience = $row[12];
		$product->data_sheet = '/files/products/data-sheets/' . $row[13];
		$product->user_manual = '/files/products/user-manuals/' . $row[14];
		$product->maintenance_manual = '/files/products/maintenance-manuals/' . $row[15];
		$product->guarantee = '/files/products/warraties/' . $row[16];
		$product->quality_seal = str_replace(";", ",", $row[17]);
		$product->is_new = $row[20];*/
		
		return $product;
	}
	
	private function getReferenceFromExcelRow($row)
	{
		$reference = new Reference();
		$reference = $reference->firstOrNew(['name' => $row[1]]);
		$reference->description = $row[9];
		$reference->enforcement = $row[10];
		$reference->use = $row[11];
		$reference->ambience = $row[12];
		$reference->data_sheet = '/files/products/data-sheets/' . $row[13];
		$reference->user_manual = '/files/products/user-manuals/' . $row[14];
		$reference->maintenance_manual = '/files/products/maintenance-manuals/' . $row[15];
		$reference->guarantee = '/files/products/warraties/' . $row[16];
		$reference->quality_seal = $row[17];
		$reference->is_new = $row[20];

		return $reference;
	}

	private function getValidateFromExcelRow($row,$i,$writer,$first_row)
	{
		$error = 0;
		$warnings = 0;
		
		for($j = 0; $j < 3; $j++){
			if(empty($row[$j])){
				$error = 1;
				$singleRow[0] = $i;
				$singleRow[1] = 'ERROR';
				$singleRow[2] = 'El campo '.$first_row[$j].' se encuentra vacío';
				$writer->addRow($singleRow); // add a row at a time
			}
		}
		
		for($j = 6; $j < 23; $j++){
			if(empty($row[$j])){
				$warnings = 1;
				$singleRow[0] = $i;
				$singleRow[1] = 'WARNING';
				$singleRow[2] = 'El campo '.$first_row[$j].' se encuentra vacío';
				$writer->addRow($singleRow); // add a row at a time
			}
		}
		
		$categories = explode(';',$row[22]);
		$product_id = new Product();
		$product_id = $product_id->firstOrNew(['code' => $row[0]]);
		$product_category = new ProductCategory();
		$product_category->where('product_id', $product_id->id )->delete();
		
		foreach($categories as $code) {
			$model = new Category();
		
			if( !$model->where('code', trim($code))->first() ){
				print("#$i Category-".$code."\n");
				$error = 1;
				$singleRow[0] = $i;
				$singleRow[1] = 'ERROR';
				$singleRow[2] = 'El campo Categorías contiene una o más categorías que no existen';
				$writer->addRow($singleRow); // add a row at a time
			}
		}
		

		return [$error,$warnings];
	}

	private function saveProducts($reference,$product,$row)
	{
		if($reference->save()){
			$product->reference_id=$reference->id;
			if($product->save()){
				$categories = explode(';',$row[22]);
				$product_id = new Product();
				$product_id = $product_id->firstOrNew(['code' => $row[0]]);
				if(!empty($categories[0])){
					$category = new Category();
					for($i = 0; $i < count($categories); $i++){
						if( $category_id = $category->where('code', trim($categories[$i]))->first() ){
							$prodc = new ProductCategory();
							$prodc->product_id = $product_id->id;
							$prodc->category_id = $category_id->id;
							$prodc->save();
						}
					}
				}
			}
			else{
				throw new \Exception('Error Producto.');
			}
		}
		else{
			throw new \Exception('Error Reference');
		}
	}

	private function saveRelatedProducts($related,$product_id)
	{
		$product2 = new Product();
		for($i = 0; $i < count($related); $i++){
			if( $product2_id = $product2->where('code',$related[$i])->first() ){
				$prod = new RelatedProduct();
				$prod->product_id = $product_id->id;
				$prod->product2_id = $product2_id->id;
				$prod->save();
			}
			else{
				throw new \Exception('Error Related.');
			}
		}
	}
}
