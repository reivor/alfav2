<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Customer;

class CustomersController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderBy('id', 'asc')
                ->simplePaginate(8);
        return view('admin.customers.index', ['customers' => $customers]);
    }

    /**
     * Display a list of subscribers.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscribers()
    {
        $customers = Customer::where('subscribe','=','1')->orderBy('id', 'asc')
                ->simplePaginate(8);
        return view('admin.customers.index', ['customers' => $customers]);
    }

    public function unsubscribe($id){
        $customers = Customer::find($id);
        $customers->subscribe = 0;
        $customers->save();
        return redirect('admin/customers/show/'.$id)->with('success', trans('cms.customer_newsletter_unsusbcribed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        if($customer){
            return view('admin.customers.show', ['customer' => $customer]);
        }else{
            return redirect('admin/customers');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
