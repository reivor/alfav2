<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\User;

class UsersController extends AdminController
{
	
	public function getIndex()
	{
		$users = User::orderBy('id', 'asc')
		->simplePaginate(8);
		return view('admin.users.index', ['users' => $users]);
	}
	
	public function getCreate()
	{
		$user = new User();
		return view('admin.users.form', [
			'user' => $user, 
			'action' => trans('cms.create_user')
			]);
	}
	


	public function postCreate(Request $request)
	{
		$user = new User();
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->avatar = User::DEFAULT_PICTURE;
		$user->role = $request->input('role');
		$user->status = User::STATUS_PENDING;
		$token = uniqid('UA', true);
		$user->activation_token = $token;
		$user->save();
		
		// send welcome email
		Mail::send('admin.emails.user_invited', ['user' => $user, 'token' => $token], function($mail) use ($user) {
			$mail->to($user->email)
			->subject('¡Bienvenido a Alfa!');
		});
		
		return redirect('admin/users')
		->with('success', trans('cms.message_user_created'));
	}
	
	public function getEdit($id)
	{
		$user = User::find($id);
		return view('admin.users.form', [
			'user' => $user,
			'action' => trans('cms.edit_user')
			]);
	}
	
	public function postEdit(Request $request)
	{
		$user = User::firstOrNew(['id' => $request->input('id')]);
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->role = $request->input('role');
		
		$user->save();
		
		return redirect('admin/users')
		->with('success', trans('cms.message_user_created'));
	}
	
	public function getView($id)
	{
		$user = User::find($id);
		return view('admin.users.view', ['user' => $user]);
	}
	
	public function getProfile()
	{
		$userId = Auth::user()->id;
		$user = User::find($userId);
		return view('admin.users.profile', ['user' => $user]);
	}
	
	public function getEditProfile()
	{
		$userId = Auth::user()->id;
		$user = User::find($userId);
		return view('admin.users.edit_profile', ['user' => $user]);
	}
	
	public function postEditProfile(Request $request)
	{
		$user = User::find($request->input('id'));
		$user->name = $request->input('name');
		
		// upload profile picture
		if( $request->hasFile('picture') && $request->file('picture')->isValid() ) {
			$file = $request->file('picture');
			$filePath = public_path('images/users');
			$fileName = 'UA_' . uniqid() . '_' . str_pad($user->id, 6, '0', STR_PAD_LEFT) . '.' . $file->getClientOriginalExtension();
			$file->move($filePath, $fileName);
			$user->avatar = '/images/users/' . $fileName;
		}
		
		$user->save();
		return redirect('admin/profile')
		->with('success', trans('cms.message_profile_updated'));
	}

	public function getChangePassword()
	{
		$userId = Auth::user()->id;
		$user = User::find($userId);
		return view('admin.users.change_password', ['user' => $user]);
	}

	public function postChangePassword(Request $request)
	{
		$user = User::find($request->input('id'));
		$user->password = bcrypt($request->input('password'));
		$user->save();
		return redirect('admin/profile')
		->with('success', trans('cms.confirm_password'));
	}

	public function getActivated($id){
		$user = User::find($id);
		$user->status = 'active';
		$user->save();

		// send active email
		Mail::send('admin.emails.user_activated', ['user' => $user], function($mail) use ($user) {
			$mail->to($user->email)
			->subject('¡Su cuenta ya se encuentra activa en Alfa!');
		});

		return view('admin.users.view', ['user' => $user]);
	}

	public function getDesactivated($id){
		$user = User::find($id);
		$user->status = 'blocked';
		$user->save();
		return view('admin.users.view', ['user' => $user]);
	}

	public function getDelete($id){
		$user = User::find($id);
		$user->delete();
		return redirect('admin/users')
		->with('success', trans('cms.message_user_deleted'));
	}


	
}