<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Requests;

class CountriesController extends AdminController
{
    /**
     * Shows the countries list.
     * @return View
     */
    public function getIndex()
    {
        $countries = Country::get();
        return view('admin.countries.index', ['countries' => $countries]);
    }

    /**
     * Show the form for creating a new country.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        $country = new Country();
        return view('admin.countries.form', [
            'country' => $country,
            'action' => trans('cms.create_country')
        ]);
    }

    public function postCreate(Request $request)
    {
        $country = new Country();
        $country->name = $request->input('name');
        $country->code = $request->input('code');
        $country->save();

        return redirect('admin/countries')
            ->with('success', trans('cms.message_country_created'));
    }

    public function getEdit($id)
    {
        $country = Country::find($id);
        return view('admin.countries.form', [
            'country' => $country,
            'action' => trans('cms.edit_country')
        ]);
    }

    public function postEdit(Request $request, $id)
    {
        $store = Country::find($id);
        $store->name = $request->input('name');
        $store->code = $request->input('code');
        $store->save();

        return redirect('admin/countries')
            ->with('success', trans('cms.message_country_updated'));
    }

    public function getDelete($id)
    {
        $store = Country::find($id);
        $store->delete();

        return redirect('admin/countries')
            ->with('success', trans('cms.message_country_deleted'));
    }

}
