<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Models\Department;
use App\Models\City;
use Illuminate\Http\Request;
use App\Models\Country;

class CitiesController extends AdminController
{
    /**
     * Shows the cities list.
     * @return View
     */
    public function getIndex($id)
    {
        $cities = Department::find($id)->cities;
        $department = Department::find($id);
        return view('admin.cities.index', [
			'cities' => $cities, 
			'department_id' => $id, 
			'department' => $department
		]);
    }

    /**
     * Show the form for creating a new city.
     *
     * @return \Illuminate\Http\Response
     */

    public function getEdit($id)
    {
        $cities = Department::find($id)->cities;
        return view('admin.cities.form', [
			'cities' => $cities, 
			'department_id' => $id, 
			'action' => trans('cms.edit_city')
		]);
    }

    public function postEdit(Request $request)
    {

        DB::beginTransaction();
		
        try{

			$countryCode = $this->getCountryCode($request);
            $pais = Country::where('code', $countryCode)->first();

            if(isset($request->city) && !empty($request->city)){
                foreach($request->city as $id => $cities) {
                    $city = City::find($id);
                    $city->name = $cities['name'];
                    $city->lat = $cities['lat'];
                    $city->lon = $cities['lon'];
                    $city->save();
				}
            }

            if(isset($request->citynew) && !empty($request->citynew)){
                foreach($request->citynew as $newcities) {
                    $city = new City();
                    $city->name = $newcities['name'];
                    $city->lat = $newcities['lat'];
                    $city->lon = $newcities['lon'];
                    $city->country_id = $pais->id;
                    $city->department_id = $request->department_id;
                    $city->save();
				}
            }
			
            DB::commit();
            return redirect('admin/cities/index/' . $request->department_id)
                ->with('success', trans('cms.message_city_updated'));
        }
		catch(\Exception $e) {
            DB::rollBack();
            return redirect('admin/cities/index/' . $request->department_id)
                ->with('error', trans('cms.message_city_error'));
        }
    }

    public function postDrop(Request $request)
    {
        if(!empty($request->id)){
            City::where('id', $request->id )->delete();
        }
    }
	
}
