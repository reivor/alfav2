<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
/*use App\Models\BlogArticle;
use App\Models\BlogCategory;
use App\Models\BlogComment;
use App\Models\BlogPoint;
use App\Models\Category;
*/
use App\Models\Page;
use Illuminate\Support\Facades\Log;
use DB;
class PageController extends AdminController{
	const ALLOWED_TAGS = '<u><b><span><i><u><style><strike><ul><li><ol><div><img><br><p><a><iframe><hr>';
	// CMS ARTICULOS BLOG ------------------------------------------------------
	public function getPages()
	{
		$pages = Page::orderBy('date_publishing', 'desc')->get();
		return view('admin.pages.indexPages', compact('pages'));
	}

	public function getPage($id=NULL)
	{
		$status=[Page::STATUS_ACTIVE, Page::STATUS_DISABLED];
		$page = Page::firstOrNew(['id'=> $id]);
		$blogCategories = BlogCategory::all();
		return view('admin.pages.form',  compact('pages')); //compact('article','blogCategories','status'));
	}
	

public function getForm($id=NULL)
{
 $status=[Page::STATUS_ACTIVE, Page::STATUS_DISABLED];
$page = Page::firstOrNew(['id'=> $id]);
	//	var_dump($id);
		return view('admin.pages.form', compact('page','status'));  //,  compact('pages'));


}


	public function postDeletePage(Request $request) //borrado de paginas *rb
	{
		try {
			DB::beginTransaction();
			Page::where(['id'=> $request->id])->delete();
			DB::commit();
		} catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
		}
	}

	public function postSavePage(Request $request){		 //guardar pagina *rb
		$message=trans('cms.message_pagina_created');
		$status="success";
		$redirect='/admin/page/pages'; //--admin/blog/articles
		
		 $page = Page::firstOrNew(['id' => $request->input('id')]);
		 $page->title = $request->input('title');
		 $page->status=$request->input('status');
	//	$page->blog_category_id=$request->input('category');
		 $page->url = $request->input('url');
		 $page->body = $request->input('body');
	
	/*	if( isset($request->tags[0]) ) {
			$page->tags=$request->tags[0];
		}
*/

		$date = \DateTime::createFromFormat('d/m/Y', $request->input('date-publishing'));
		$page->date_publishing=$date->format('Y-m-d');
		$page->save();
		
 if( $request->file('image_header') ){
			//Guardar imagen header
			try {
				DB::beginTransaction();
				$currentFile=$request->file('image_header');
				if($currentFile->isValid()) {
					$filePath = public_path('images/pages');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($page->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$page->image_header = '/images/pages/' . $fileName;
					$page->save();
				}
				else{
					throw new \Exception ("Artículo - Imagen Header no válida");
				}
				DB::commit();
			}
			catch(\Exception $e) {
				Log::error($e->getMessage());
				DB::rollBack();
				$status='error';
				$message=trans('cms.error_page_created');	
				//$redirect='admin/blog/article/'.$request->input('id');
				$redirect='admin/page/pages/'.$request->input('id');
				
			}
		}


/*
		if( $request->input('id')=="" || $request->input('id')==NULL ) {
			$redirect='admin/blog/points/'.$article->id;
		}
*/
		//Images-----
		/*
		if( $request->file('image_header') ){
			//Guardar imagen header
			try {
				DB::beginTransaction();
				$currentFile=$request->file('image_header');
				if($currentFile->isValid()) {
					$filePath = public_path('images/articles');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($article->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$article->image_header = '/images/articles/' . $fileName;
					$article->save();
				}
				else{
					throw new \Exception ("Artículo - Imagen Header no válida");
				}
				DB::commit();
			}
			catch(\Exception $e) {
				Log::error($e->getMessage());
				DB::rollBack();
				$status='error';
				$message=trans('cms.error_article_created');	
				$redirect='admin/blog/article/'.$request->input('id');
			}
		}

		*/

/*		
		if($request->file('image_related')) {
			// Guardar imagen related
			try {
				DB::beginTransaction();
				$currentFile=$request->file('image_related');
				if( $currentFile->isValid() ) {
					$filePath = public_path('images/articles');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($article->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$article->image_related = '/images/articles/' . $fileName;
					$article->save();
				}
				else {
					throw new \Exception ("Artículo - Imagen Relacionada no válida");
				}
 
				DB::commit();
			}
			catch(\Exception $e) {
				Log::error($e->getMessage());
				DB::rollBack();
				$status='error';
				$message=trans('cms.error_article_created');	
				$redirect='admin/blog/article/'.$request->input('id');
			}
		}
		*/

		/*
		
		if($request->file('image_default')){//Guardar imagen
			try{
				DB::beginTransaction();
				$currentFile=$request->file('image_default');
				if($currentFile->isValid()) {
					$filePath = public_path('images/articles');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($article->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$article->image_default = '/images/articles/' . $fileName;
					$article->save();
				}
				else{
					throw new \Exception ("Artículo - Imagen Default no válida");
				}
				DB::commit();
			}
			catch(\Exception $e){
				Log::error($e->getMessage());
				DB::rollBack();
				$status='error';
				$message=trans('cms.error_article_created');	
				$redirect='admin/blog/article/'.$request->input('id');
			}
		}

		*/
		
		/*
		if($request->file('image_home')){//Guardar imagen
			try{
				DB::beginTransaction();
				$currentFile=$request->file('image_home');
				if($currentFile->isValid()) {
					$filePath = public_path('images/articles');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($article->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$article->image_home = '/images/articles/' . $fileName;
					$article->save();
				}
				else{
					throw new \Exception ("Artículo - Imagen Home no válida");
				}
				DB::commit();
			}
			catch(\Exception $e){
				Log::error($e->getMessage());
				DB::rollBack();
				$status='error';
				$message=trans('cms.error_article_created');	
				$redirect='admin/blog/article/'.$request->input('id');
			}
		}

		*/
		
		return redirect($redirect)
			   ->with($status, $message);
	}
	// FIN CMS ARTICULOS BLOG --------------------------------------------------
	
	// CMS COORDENADAS BLOG ----------------------------------------------------
	
	/*
	public function getPoints($id){
		$products=[];
		$article = BlogArticle::where('id',$id)->first();
		$categories=Category::buildCategoriesTree();
		$currentPoints = BlogPoint::where('blog_article_id',$id)->get();
		$numberPoints = count($currentPoints);
		
		foreach ($currentPoints as $point) {
			$products[$point->product->id]=$point->product->name;
		}

		return view('admin.blog.articlePoints', compact('article','currentPoints','numberPoints','categories','products'));
	}

*/

/*
	public function postSavePoint(Request $request){
		$message=trans('cms.message_blog_point_created');
		$status="success";
		$redirect='admin/blog/articles';
		
		$idArticle=$request->idArticle;
		
		try{
			DB::beginTransaction();
			
			BlogPoint::where('blog_article_id', $idArticle )->delete();
			
			$points=json_decode($request->points);

			foreach ($points as $currentPoint) {
				$percentages=json_decode($currentPoint->percentages);
				
				$point = new BlogPoint();
				$point->nearest_point=$currentPoint->bestPoint;
				$point->blog_article_id=$idArticle;

				$point->top=(isset($percentages->top)) ? $percentages->top : null;
				$point->right=(isset($percentages->right)) ? $percentages->right : null;
				$point->bottom=(isset($percentages->bottom)) ? $percentages->bottom : null;
				$point->left=(isset($percentages->left)) ? $percentages->left : null;

				$point->offset_x=$currentPoint->offsetX;
				$point->offset_y=$currentPoint->offsetY;
				$point->product_id=$currentPoint->product;
				$point->save();
				DB::commit();
			}
		}catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			$message=trans('cms.error_blog_point_created');
			$status="error";
			$redirect='admin/blog/articles';
		}
		
		return redirect($redirect)->with($status, $message);
	}
*/

	// FIN CMS COORDENADAS BLOG ------------------------------------------------
	
	// CMS CATEGORIAS BLOG -----------------------------------------------------
	/*
	public function getCategories(){
		$blog_categories= BlogCategory::all();
		return view('admin.blog.indexCategory', compact('blog_categories'));
	}
	
	public function getCategory($id=NULL){
		$status=[BlogCategory::STATUS_ACTIVE, BlogCategory::STATUS_DISABLED];
		$category = BlogCategory::firstOrNew(['id'=> $id]);
		return view('admin.blog.category', compact('category','status'));
	}
	
	public function postDeleteCategory(Request $request){
		try{
			DB::beginTransaction();
			BlogCategory::where(['id'=> $request->id])->delete();
			DB::commit();
		}catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
		}
	}
	*/


	/*
	public function postSaveCategory(Request $request){
		$message=trans('cms.message_blog_category_created');
		$status="success";
		$redirect='admin/blog/categories';
		
		try{
			DB::beginTransaction();
			$category = BlogCategory::firstOrNew(['id' => $request->input('id')]);
			$category->name=$request->input('name');
			$category->status=$request->input('status');
			$category->color=$request->input('color');
			$category->save();
			DB::commit();
		}catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			$status='error';
			$message=trans('cms.error_blog_category_created');	
			$redirect='admin/blog/categories/'.$request->input('id');
		}

		return redirect($redirect)
			   ->with($status, $message);
	}
	*/
	
	// FIN CMS CATEGORIAS BLOG -------------------------------------------------
	
	// CMS COMENTARIOS BLOG ----------------------------------------------------
	public function getComments(){ //Trae listado de articulos con al menos un comentario 
		$articles = BlogArticle::orderBy('date_publishing', 'desc')
				->selectRaw('pages.*, 
								(SELECT count(*) 
								FROM title 
								WHERE id>0) 
							as count')
				->having('count', '>', 1)
				->simplePaginate(10);
		
		$recentComments = BlogComment::orderBy('created_at', 'desc')
					->limit(20)
					->get();
		$stateActive=BlogComment::STATUS_ACTIVE;
		$stateRejected=BlogComment::STATUS_REJECTED;
		return view('admin.blog.indexComments', compact('articles','recentComments','stateActive','stateRejected'));
	}

	
	public function getArticleComments($idArticle){
		$comments = BlogComment::getParentComments($idArticle)
					->orderBy('created_at', 'desc')
					->simplePaginate(10);
		$article= BlogArticle::where('id',$idArticle)->first();
		$stateActive=BlogComment::STATUS_ACTIVE;
		$stateRejected=BlogComment::STATUS_REJECTED;
		return view('admin.blog.articleComments', compact('comments','article'));
	}
	
	public function postChangeStatusComment(Request $request){
		$comment= BlogComment::where('id',$request->id)->first();
		
		if($comment!=NULL){
			$comment->status=$request->status;
			$comment->save();
		}
		
		return $comment->status;
	}
	// FIN CMS COMENTARIOS BLOG ------------------------------------------------
	/*public function postLikeArticles(Request $request){
		$clue="%".$request->clue."%";
		$currentId=$request->currentId;
		
		$relatedArticles=Article::where('title', 'like', $clue)
				  ->where('id', '!=', $currentId)
				  ->get();
		
		return json_encode($relatedArticles);
	}*/
}