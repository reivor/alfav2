<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use App\Models\Product;

class ServicesController extends AdminController{
	
	public function postReleated(Request $request){//Obtiene los productos relacionados a la categoria seleccionada
		$products =  Product::getProductsByCategory($request->selectedCategory)->get();
		return json_encode($products);
	}
	
	
	
}
