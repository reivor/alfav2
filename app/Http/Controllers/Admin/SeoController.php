<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\PageMeta;
use Illuminate\Support\Facades\DB;

class SeoController extends AdminController
{

	public function getIndex()
	{
		return view('admin.seo.index');
	}

	public function getRobots()
	{
		try {
			$file = file_get_contents(public_path("robots.txt"));
		}
		catch(\ErrorException $e) {
			// file not found?
			$file = '';
		}
		
		return view('admin.seo.robots', [
			'content' => $file
		]);
	}

	public function postRobots(Request $request)
	{
		$file = fopen("robots.txt", "w");
		fputs($file, $request->input('file_robots'));
		fclose($file);
		$request->session()->flash('success', 'Cambios Realizados');
		return redirect('/admin/seo');
	}

	public function getCategories()
	{
		$categories = Category::buildCategoriesTree();
		
		$sitemapcategories = DB::table('sitemap_categories')->select('category_id')->get();
		
		$selectedCategories = [];
		foreach( $sitemapcategories as $sitemapcategory ) {
			$selectedCategories[] = (int) $sitemapcategory->category_id;
		}
		
		return view('admin.seo.categories.select_categories', [
			'categories' => $categories,
			'selectedCategories' => json_encode($selectedCategories),
			'selectedCategoriesAux' => $selectedCategories
		]);
	}

	public function postCategories(Request $request)
	{

		DB::table('sitemap_categories')->truncate();
		foreach( json_decode($request->selectedCategories) as $category ) {
			DB::table('sitemap_categories')->insert(['category_id' => $category]);
		}

		return redirect('/admin/seo');
	}

	public function getProducts()
	{

		$categories = Category::buildCategoriesTree();
		
		$sitemapcategories = DB::table('sitemap_products')->select('category_id')->get();
		
		$selectedCategories = [];
		foreach( $sitemapcategories as $sitemapcategory ) {
			$selectedCategories[] = (int) $sitemapcategory->category_id;
		}
		
		return view('admin.seo.products.select_products', [
			'categories' => $categories,
			'selectedCategories' => $selectedCategories,
			'selectedCategoriesAux' => json_encode($selectedCategories)
		]);
	}

	public function postProducts(Request $request)
	{
		DB::table('sitemap_products')->truncate();

		foreach( json_decode($request->selectedCategories) as $category ) {
			DB::table('sitemap_products')->insert(['category_id' => $category]);
		}

		return redirect('/admin/seo');
	}

	public function getSelectPage()
	{
		return view('admin.seo.meta.select_page');
	}

	public function getMetaTag($page)
	{

		$title = PageMeta::firstOrCreate(['page' => $page, 'type' => 'title']);
		$keyword = PageMeta::firstOrCreate(['page' => $page, 'type' => 'meta', 'key' => 'keyword']);
		$description = PageMeta::firstOrCreate(['page' => $page, 'type' => 'meta', 'key' => 'description']);
		$og_title = PageMeta::firstOrCreate(['page' => $page, 'type' => 'og', 'key' => 'title']);
		$og_type = PageMeta::firstOrCreate(['page' => $page, 'type' => 'og', 'key' => 'type']);
		$og_url = PageMeta::firstOrCreate(['page' => $page, 'type' => 'og', 'key' => 'url']);
		$og_image = PageMeta::firstOrCreate(['page' => $page, 'type' => 'og', 'key' => 'image']);

		return view('admin.seo.meta.meta_page', [
			'page' => $page,
			'title' => $title,
			'keyword' => $keyword,
			'description' => $description,
			'og_title' => $og_title,
			'og_type' => $og_type,
			'og_url' => $og_url,
			'og_image' => $og_image,
		]);
	}

	public function postMetaTag(Request $request)
	{

		$page = $request->input('page');
		
		$title = PageMeta::firstOrCreate(['page' => $page, 'type' => 'title']);
		$title->value = $request->input('title');
		$title->key = 'title';
		$title->save();

		$keywords = PageMeta::firstOrCreate(['page' => $page, 'type' => 'meta', 'key' => 'keyword']);
		$keywords->value = $request->input('keyword');
		$keywords->save();

		$description = PageMeta::firstOrCreate(['page' => $page, 'type' => 'meta', 'key' => 'description']);
		$description->value = $request->input('description');
		$description->save();

		/* Open Graph */
		foreach( $request->input('og') as $key => $og ) {
			$metaOg = PageMeta::firstOrCreate(['page' => $page, 'type' => 'og', 'key' => $key]);
			$metaOg->value = $og;
			$metaOg->save();
		}
		
		return view('admin.seo.meta.select_page');
	}

}
