<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Models\Country;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentsController extends AdminController
{
    /**
     * Shows the departments list.
     * @return View
     */
    public function getIndex(Request $request)
    {
		$countryCode = $this->getCountryCode($request);
        $departments = Department::getDepartmentByCountry($countryCode)->get();
        return view('admin.departments.index', ['departments' => $departments]);
    }

    /**
     * Show the form for creating a new department.
     * @return \Illuminate\Http\Response
     */
    public function getEdit(Request $request)
	{
		$countryCode = $this->getCountryCode($request);
        $departments = Department::getDepartmentByCountry($countryCode)->get();
        return view('admin.departments.form', [
			'departments' => $departments, 
			'action' => trans('cms.edit_department')
		]);
    }

    public function postEdit(Request $request)
    {
		$countryCode = $this->getCountryCode($request);
        $pais = Country::where('code',$countryCode)->first();
        DB::beginTransaction();
		
        try {
			
            if(isset($request->department) && !empty($request->department)){
                foreach($request->department as $id => $departments) {
                    $department = Department::find($id);
                    $department->name = $departments;
                    $department->save();
				}
            }

            if(isset($request->name) && !empty($request->name)){
                foreach($request->name as $newdepartments) {
                    $department = new Department();
                    $department->name = $newdepartments;
                    $department->country_id = $pais->id;
                    $department->save();
				}
            }

            DB::commit();
            return redirect('admin/departments/index')
                ->with('success', trans('cms.message_department_updated'));

        }
		catch(\Exception $e){
            DB::rollBack();
            return redirect('admin/departments/index')
                ->with('error', trans('cms.message_department_error'));
        }
    }

    public function postDrop(Request $request)
    {
        if(!empty($request->id)){
            Department::where('id', $request->id )->delete();
        }
    }
}
