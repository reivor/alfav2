<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Models\Setting;

class SettingsController extends AdminController {

	public function getIndex() {
		$settings = Setting::get();
		$settings_array=[];
		
		foreach ($settings as $key => $value) {
			# code...
			$settings_array[$value->name] = $value->value;
		}
		
	
		

		return view('admin.settings.index',['settings'=>$settings_array]);
	}

	public function postIndex(Request $request) {

		foreach ($request->input() as $key => $value) {
			if ($key != '_token') {
	 			$setting = Setting::firstOrCreate(['name' => $key]);
				$setting->name = $key;
				$setting->value = $value;
				$setting->save();
			}
		}

		$settings = Setting::get();
		
		foreach ($settings as $key => $value) {
			# code...
			$settings_array[$value->name] = $value->value;
		}

		return view('admin.settings.index',['settings'=>$settings_array]);
	}

}
