<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\QuoteOrder;
use App\Models\Customer;

class HomeController extends AdminController {

	/**
	 * Shows the dashboard page.
	 * @return View
	 */
	public function dashboard()
	{
		$subscribers_count = Customer::where('subscribe','=','1')->count();
		$quotes_count = QuoteOrder::where('status','=','open')->count();
		return view('admin.home.dashboard')
		->with('subscribers_count', $subscribers_count)
		->with('quotes_count', $quotes_count);
	}

}
