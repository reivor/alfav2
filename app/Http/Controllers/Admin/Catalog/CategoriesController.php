<?php 

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\BannerCategory;
use Illuminate\Support\Facades\Log;
use DB;

class CategoriesController extends AdminController{


	public function getIndex(){
		$categories = Category::buildCategoriesTree();
		return view('admin.catalog.categories.index', ['categories' => $categories]);
	}
	
	public function getCreate($parent=NULL){
		$category = new Category();
		$status = [Category::STATUS_ACTIVE, Category::STATUS_BLOCKED];
		
		if($parent!=NULL){
			$parentCategory = Category::find($parent);
			$category->parentCategory=$parentCategory;
		}

		$idAmbience=Category::AMBIENTES_CATEGORY;

		return view('admin.catalog.categories.create', [
			'category' => $category, 
			'bannersSearch' => array(), 
			'bannersHome' => array(), 
			'bannersDetail' => array(), 
			'idAmbience' => $idAmbience,
			'status' => $status]);
	}
	
	public function getEdit($id){
		$category=Category::find($id);

		if($category != null){
			$childCategories=Category::getCategoriesTree($category);
			
			$products=Product::getTreeProducts($category->id)->get();
			$banners=BannerCategory::where('category_id', '=' , $category->id)->get();
			$bannersSearch=array();
			$bannersDetail=array();
			
			foreach ($banners as $banner) {
				if($banner->location== BannerCategory::LOCATION_SEARCH){
					$bannersSearch[$banner->position]=$banner;
				}else if($banner->location== BannerCategory::LOCATION_DETAIL){
					$bannersDetail[$banner->position]=$banner;
				}
			}
			
			$idAmbience=Category::AMBIENTES_CATEGORY;
			$status = [Category::STATUS_ACTIVE, Category::STATUS_BLOCKED];

			return view('admin.catalog.categories.create', [
				'category' => $category, 
				'categories' => $childCategories->categories, 
				'products' => $products, 
				'bannersSearch' => $bannersSearch, 
				'bannersDetail' => $bannersDetail, 
				'idAmbience' => $idAmbience,
				'status' => $status]);
		}
		
		return redirect('admin/catalog/categories');
		

	}
	
	/**
	 * Check if exists a category with the name provided, who also has the same parent.
	 * (there shouldn't be two categories with equal names within the same parent)
	 * @param Request $request
	 * @return json
	 */
	public function getCheck(Request $request)
	{
		$name = $request->input('name');
		$parent = $request->input('parent');
		$current = $request->input('current');
		
		if( !$parent ) {
			$criteria = ['name' => $name, 'level' => 1];
		}
		else {
			$criteria = ['name' => $name, 'parent' => $parent];
		}
		
		$category = Category::where($criteria)->first();
		
		if( $category && $category->id != $current ) {
			// category exists within the same parent
			$response = ['valid' => false];
		}
		else {
			$response = ['valid' => true];
		}
		
		return response()->json($response);
	}
	
	public function getDelete($id){
		$category = Category::find($id);
		$category->delete();
		
		return redirect('admin/catalog/categories')
			->with('success', trans('cms.message_category_deleted'));
	}
	
	public function postSave(Request $request){
		
		$success=true;
		$message=trans('cms.message_category_created');
		$status="success";
		$redirect='admin/catalog/categories';
		
		
		//Category------------------------------------------------------------------
		$category = Category::firstOrNew(['id' => $request->input('id')]);

		if($request->input('parent')==NULL){//Categoria padre
			$category->level=1;
		}else{//Categoria hijo
			$parentCategory = Category::find($request->input('parent'));
			if($parentCategory->level < Category::MAX_LEVEL){//Validacion de subcategorias hasta nivel 4
				$category->parent = $parentCategory->id;
				$category->level = ($parentCategory->level)+1;
			}else{
				$success!=$success;
			}
		}
		
		if($success){
			$category->name = $request->input('name');
			$sameName=Category::where('id', '!=' , $request->input('id'))
						->where('parent', '=', $category->parent)
						->where('name', '=', $category->name)
						->count();

			if($sameName>0){//Nombre repetido en el mismo nivel
				$status='error';
				$message=trans('cms.message_category_created_error_name');
				$redirect='admin/catalog/categories/edit/'.$request->input('id');
			}else{
				if($request->input('display_name_first')!=NULL){
					$category->display_name_first = $request->input('display_name_first');
				}
				
				if($request->input('display_name_second')!=NULL){
					$category->display_name_second = $request->input('display_name_second');
				}

				$category->description = $request->input('description');
				$category->tags = $request->input('tags');
				$category->save();
				
				//Image category-------------------------------------------------------
				if($request->file('banner')){//Guardar imagen
					try{
						DB::beginTransaction();
						$currentFile=$request->file('banner');
						if( $currentFile->isValid() ) {
							$filePath = public_path('images/categories');
							$fileName = 'SO_' . uniqid() . '_' . str_pad($category->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
							$category->image_name = $fileName;
							$currentFile->move($filePath, $fileName);
							$category->image_url = '/images/categories/' . $fileName;
							$category->save();
						}else{
							throw new \Exception ("Imagen Categoria no válido");
						}
						DB::commit();
					}catch(\Exception $e){
						Log::error($e->getMessage());
						DB::rollBack();
						$status='error';
						$message=trans('cms.message_category_created_error');	
						$redirect='admin/catalog/categories/edit/'.$request->input('id');
					}
				}

				
				//Image mobile category-------------------------------------------------------
				if($request->file('banner-mobile')){//Guardar imagen
					try{
						DB::beginTransaction();
						$currentFile=$request->file('banner-mobile');
						if( $currentFile->isValid() ) {
							$filePath = public_path('images/categories');
							$fileName = 'SO_' . uniqid() . '_' . str_pad($category->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
							$category->image_name = $fileName;
							$currentFile->move($filePath, $fileName);
							$category->image_mobile_url = '/images/categories/' . $fileName;
							$category->save();
						}else{
							throw new \Exception ("Imagen Categoria no válido");
						}
						DB::commit();
					}catch(\Exception $e){
						Log::error($e->getMessage());
						DB::rollBack();
						$status='error';
						$message=trans('cms.message_category_created_error');	
						$redirect='admin/catalog/categories/edit/'.$request->input('id');
					}
				}

				//Header category-------------------------------------------------------
				if($request->file('header')){//Guardar imagen
					try{
						DB::beginTransaction();
						$currentFile=$request->file('header');
						if( $currentFile->isValid() ) {
							$filePath = public_path('images/categories');
							$fileName = 'SO_' . uniqid() . '_' . str_pad($category->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
							$category->header_name = $fileName;
							$currentFile->move($filePath, $fileName);
							$category->header_url = '/images/categories/' . $fileName;
							$category->save();
						}else{
							throw new \Exception ("Header Categoria no válido");
						}
						DB::commit();
					}catch(\Exception $e){
						Log::error($e->getMessage());
						DB::rollBack();
						$status='error';
						$message=trans('cms.message_category_created_error');	
						$redirect='admin/catalog/categories/edit/'.$request->input('id');
					}
				}

				//Banners --------------------------------------------------------------

				$home=BannerCategory::LOCATION_HOME; //Home
				$search=BannerCategory::LOCATION_SEARCH; //Search
				$detail=BannerCategory::LOCATION_DETAIL; //Detail
				
				try{
					$this->processBanners($detail, $category->id, $request);
					$this->processBanners($search, $category->id, $request);
				}catch(\Exception $e){
					$status='error';
					$message=trans('cms.message_category_created_error');	
					$redirect='admin/catalog/categories/edit/'.$request->input('id');
				}

			}
		}else{
			$status='error';
			$message=trans('cms.message_category_created_error');	
			$redirect='admin/catalog/categories/edit/'.$request->input('id');
		}

		return redirect($redirect)
			->with($status, $message);
	}

	public function processBanners($location, $idCategory, $request){
		
		if($request->input('type-'.$location)){
			$typeArray=$request->input('type-'.$location);
			$entityArray=$request->input('entity-'.$location);
			$externalArray=$request->input('external-'.$location);
			$statusArray=$request->input('status-'.$location);

			$imageGet=$request->input('image-get-'.$location);
			$fileArray=array();

			if($request->hasFile('image-'.$location)){
				$fileArray=$request->file('image-'.$location);
			}

			try{
				DB::beginTransaction();

				foreach ($typeArray as $i => $type) {
					BannerCategory::where('category_id', '=' , $idCategory)
									->where('location', '=', $location)
									->where('position', '=', $i)
									->delete();
					
					if($type!="" && $type!=null){
						$bannerCategory = new BannerCategory();
						$bannerCategory->type=$type;
						$bannerCategory->location=$location;
						$bannerCategory->status = $statusArray[$i];
						
						if($bannerCategory->type==BannerCategory::TYPE_EXTERNAL){
							$bannerCategory->external_url=$externalArray[$i];
						}else{
							$bannerCategory->entity_id=$entityArray[$i];
						}
						
						$bannerCategory->position=$i;
						$bannerCategory->category_id=$idCategory;
						$bannerCategory->save();
						
						if(isset($fileArray[$i])){
							if( $fileArray[$i]->isValid() ) {
								$currentFile=$fileArray[$i];
								$filePath = public_path('images/bannersCategories');
								$fileName = 'SO_' . uniqid() . '_' . str_pad($bannerCategory->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
								$currentFile->move($filePath, $fileName);
								$bannerCategory->image_url = '/images/bannersCategories/' . $fileName;
								$bannerCategory->save();
							}else{
								throw new \Exception ("Banner ".$location." no válido");
							}
						}else{
							if(isset($imageGet[$i])){
								$bannerCategory->image_url=$imageGet[$i];
								$bannerCategory->save();
							}
						}
					}
				}
				DB::commit();	
			}catch(\Exception $e){
				Log::error($e->getMessage());
				DB::rollBack();
				throw $e;
			}		
		}	
	}
}
