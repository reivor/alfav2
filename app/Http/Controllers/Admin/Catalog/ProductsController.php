<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Controllers\Admin\AdminController;
use App\Models\ProductUpload;
use Illuminate\Http\Request;
use App\Jobs\ProcessFileProducts;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Models\Reference;
use App\Models\ProductImages;
use App\Models\RelatedProduct;
use App\Models\ProductCategory;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use DB;


class ProductsController extends AdminController {
	
	public function getIndex()
	{
        $products = Reference::getReferenceList()
			->paginate(16);
		
		$categories = Category::buildCategoriesTree();
		
        return view('admin.catalog.products.index', [
			'products' => $products,
			'categories' => $categories,
			'pagination' => []
		]);
    }
	
	public function getView($id)
	{
		$product = Reference::find($id);
		return view('admin.catalog.products.view', [
			'product' => $product
		]);
	}
	
	public function getCreate($idReference = null)
	{
		$product = new Product();
		$reference = Reference::firstOrNew(['id' => $idReference]);
		$product->reference = $reference;
		
		$categories = Category::buildCategoriesTree();
		$relatedProducts = Product::select('id', 'name')
							->orderBy('name')
							->get();

		return view('admin.catalog.products.form', [
			'product' => $product,
			'categories' => $categories,
			'relatedProducts' => $relatedProducts,
			'reference' => $idReference
		]);
	}
	
	public function getEdit($id)
	{
		$product = Product::where('id', $id)->first();
		$idReference = null;
		
		if($product){
			$idReference = $product->reference_id;
			$categories = Category::buildCategoriesTree();
			$relatedProducts = Product::select('id', 'name')
								->orderBy('name')
								->get();

			return view('admin.catalog.products.form', [
				'product' => $product,
				'categories' => $categories,
				'relatedProducts' => $relatedProducts,
				'reference' => $idReference
			]);
			
		}
		else{
			return redirect('admin/catalog/products');
		}
	}
	
	public function checkCode(Request $request, $idProduct = null)
	{
		if( $idProduct ) {
			$product = Product::where('code', '=', $request->input('code_variation'))
						->where('id', '<>', $idProduct)
						->first();
		}
		else {
			$product = Product::where('code', '=', $request->input('code_variation'))->first();
		}
		
		$response = ['valid' => ($product==null) ? true : false];
		return response()->json($response);
	}
	
	
	public function postSaveProduct(Request $request)
	{
		$message = "";
		$status = "success";
		
		// PRODUCT - REFERENCE --------------------------------------------------------
		$reference = Reference::firstOrNew(['id' => $request->input('id_reference')]);
		$reference->name = $request->input('name_product');
		$reference->ambience=$request->input('ambience');
		$reference->enforcement=$request->input('enforcement');
		$reference->use=$request->input('use');

		if(isset($request->quality_seal)){
			$quality = implode(",", $request->quality_seal);
			$reference->quality_seal=$quality;
		}

		$reference->is_new = isset($request->is_new);
		$reference->save();
		
		$redirect='admin/catalog/products/view/'.$reference->id;

		// Save Archivos --------------------------------------------
		if($request->file('data_sheet')){//Guardar Ficha Tecnica
			try{
				$currentFile=$request->file('data_sheet');
				if($currentFile->isValid()) {
					$filePath = public_path('files/products/data-sheets');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($reference->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$reference->data_sheet = $fileName;
					$reference->save();
				}
				else{
					throw new \Exception ("Referencia ".$reference->id." - Ficha Tecnica no válida");
				}
			}
			catch(\Exception $e){
				Log::error($e->getMessage());
				$status='error';
				$message.=trans('cms.error_product_data_sheet')."<br>";	
			}
		}
		
		if($request->file('user_manual')){//Guardar Manual de Usuario
			try{
				$currentFile=$request->file('user_manual');
				if($currentFile->isValid()) {
					$filePath = public_path('files/products/user-manuals');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($reference->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$reference->user_manual = $fileName;
					$reference->save();
				}
				else{
					throw new \Exception ("Referencia ".$reference->id." - Manual de Usuario no valido");
				}
			}catch(\Exception $e){
				Log::error($e->getMessage());
				$status='error';
				$message.=trans('cms.error_product_user_manual')."<br>";	
			}
		}
		
		if($request->file('maintenance_manual')){//Guardar Manual de Mantenimiento
			try{
				$currentFile=$request->file('maintenance_manual');
				if($currentFile->isValid()) {
					$filePath = public_path('files/products/maintenance-manuals');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($reference->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$reference->maintenance_manual = $fileName;
					$reference->save();
				}
				else{
					throw new \Exception ("Referencia ".$reference->id." - Manual de Mantenimiento no valido");
				}
			}
			catch(\Exception $e){
				Log::error($e->getMessage());
				$status='error';
				$message.=trans('cms.error_product_maintenance_manual')."<br>";	
			}
		}
		
		if($request->file('guarantee')){//Guardar Garantia
			try{
				$currentFile=$request->file('guarantee');
				if($currentFile->isValid()) {
					$filePath = public_path('files/products/warraties');
					$fileName = 'SO_' . uniqid() . '_' . str_pad($reference->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
					$currentFile->move($filePath, $fileName);
					$reference->guarantee = $fileName;
					$reference->save();
				}
				else{
					throw new \Exception ("Referencia ".$reference->id." - Garantia no valida");
				}
			}
			catch(\Exception $e){
				Log::error($e->getMessage());
				$status='error';
				$message.=trans('cms.error_product_guarantee')."<br>";	
			}
		}
		// FIN Save Archivos -----------------------------------------


		// VARIATION ------------------------------------------------------
		$product = Product::firstOrNew(['id' => $request->input('id_product')]);
		$product->name=$request->input('name_variation');
		$product->code=$request->input('code_variation');
		$product->color=$request->input('color_variation');
		$product->color_code=$request->input('color_code_variation');
		$product->dimensions=$request->input('dimensions_variation');
		$product->description=$request->input('description_product');
		
		if(isset($request->tags_variation[0])){
			$product->tags=$request->tags_variation[0];
		}
		
		$product->status=$request->input('status_variation');
		$product->reference_id = $reference->id;
		$product->save();
		
		// Categorias --------------------------------------------------
		if(isset($request->categories)){
			foreach ($request->categories as $key => $category) {
				$productCategory = new ProductCategory();
				$productCategory->product_id = $product->id;
				$productCategory->category_id = $category;
				$productCategory->save();
			}
		}
		
		// Productos relacionados --------------------------------------
		if(isset($request->related_product)){
			foreach ($request->related_product as $key => $related) {
				$relatedProduct = new RelatedProduct();
				$relatedProduct->product_id = $product->id;
				$relatedProduct->product2_id = $related;
				$relatedProduct->save();
			}
		}

		//Save Imagenes ----------------------------------------------
		if($request->file('images_variation') && $request->hasFile('images_variation')){//Guardar imagen
			try{
				$images=$request->file('images_variation');
				
				foreach ($images as $currentFile) {
					if($currentFile->isValid()) {
						$filePath = public_path('images/products/'.$product->code);
						if (!file_exists($filePath)) {
                            mkdir($filePath, 0777, true);
                        }
						
						$fileName = 'SO_' . uniqid() . '_' . str_pad($product->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
						$currentFile->move($filePath, $fileName);
						
						$imageProduct = new ProductImages();
						$imageProduct->product_id = $product->id;
						$imageProduct->url = '/images/products/'.$product->code.'/'. $fileName;
						$imageProduct->path = $filePath.'/'.$fileName;
						$imageProduct->filename = $fileName;
						$imageProduct->save();
					}else{
						throw new \Exception ("Producto ".$product->id." - Imagen no válida");
					}
				}
				
			}catch(\Exception $e){
				Log::error($e->getMessage());
				$status='error';
				$message.=trans('cms.error_product_image');	
			}
		}
		//FIN Save Imagenes ------------------------------------------
		
		if($status=="success"){
			$message=trans('cms.message_product_created');
		}
		
		return redirect($redirect)
			   ->with($status, $message);
	}

	public function getUpload()
	{
		return view('admin.catalog.products.upload');
	}
	
	public function postUpload(Request $request)
	{
		
		if( !$request->file('file_products') ) {
			// file not present
			return redirect('admin/catalog/products/upload')
				->with('error', trans('cms.error_upload_file_products'));
		}
		
		$fileProducts = $request->file('file_products');
		$ext = strtolower($fileProducts->getClientOriginalExtension());
		$validator = Validator::make(
			 ['ext' => $ext],
			 ['ext' => 'in:xls,xlsx']
		);

		if( $validator->fails() ) {
			// invalid file extension
			return redirect('admin/catalog/products/upload')
				->with('error', trans('cms.error_upload_file_products'));
		}

		$pathName = 'files/products';
		$filePath = date("YmdHis").'.'.$ext;
		$fileProducts->move(public_path($pathName), $filePath);
		
		// save product upload
		$upload = new ProductUpload();
		$upload->user_id = Auth::user()->id;
		$upload->file = public_path($pathName . '/' . $filePath);
		$finfo = pathinfo($upload->file);
		$fileLog = $finfo['filename'] . '_log.xlsx';
		$upload->file_log = public_path($pathName . '/' . $fileLog);
		$upload->status = ProductUpload::STATUS_EN_COLA;
		$upload->fecha_upload = date("Y-m-d H:i:s");
		$upload->original_file = $fileProducts->getClientOriginalName();
		$upload->save();

		// dispatch job
		//$inputFileName = public_path().$pathName.'/'.$filePath;
		//$job = (new ProcessFileProducts(public_path($pathName . '/' . $filePath)))
		//	->onQueue('uploads');
		//$this->dispatch($job);

		return redirect('admin/catalog/products/upload-log')
			->with('success', trans('cms.success_upload_file_products'));
	}

	public function getUploadLog()
	{
		$upload_log = ProductUpload::orderBy('fecha_upload', 'desc')->take(5)->get();
		return view('admin.catalog.products.uploadlog', ['uploads' => $upload_log]);
	}
	
	public function getDeleteProduct($idReference)
	{		
		try{
			DB::beginTransaction();
			Reference::where('id', $idReference)->delete();
			Product::where('reference_id', $idReference)->delete();
			DB::commit();
			return redirect('admin/catalog/products')
				->with('success', trans('cms.message_product_deleted'));
		}
		catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			return redirect('admin/catalog/products/view/'.$idReference)
				->with('error', trans('cms.error_product_delete'));
		}
    }
	
	public function getDeleteVariation($idProduct){
		$status = 'success';
		$message=trans('cms.message_product_deleted');
		$product=Product::where('id', $idProduct)->first();
		$redirect='admin/catalog/products/view/'.$product->reference_id;
		
		try{
			DB::beginTransaction();
			$product->delete();
			DB::commit();
		}catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			$status='error';
			$message=trans('cms.error_product_delete');	
		}

        return redirect($redirect)
			->with($status, $message);
    }
	
	
	public function getCategory(Request $request){
        $cat = $request->input('category');
		$catName=null;
		$categories = Category::buildCategoriesTree();
		
		if($cat==0){
			return redirect('admin/catalog/products');
		}else{
			$catName = Category::where('id', $cat)->first();

			$references = Reference::join('products', 'references.id', '=', 'products.reference_id')
			->join('products_categories', 'products_categories.product_id', '=', 'products.id')
			->select('references.*')
			->groupBy('products.reference_id')
			->where('products_categories.category_id', '=', $cat)
			->orderBy('references.name')
			->paginate(16);
			
			return view('admin.catalog.products.index', [
				'products' => $references,
				'categories' => $categories,
				'pagination' => ['category' => $cat]
			])->with('catName', $catName);
		} 
    }
	
	public function getSearch(Request $request){
        $search = $request->input('search');
		
		$categories = Category::buildCategoriesTree();
		
		$references = Reference::where('name','like','%'.$search.'%')
        ->orderBy('name')
        ->paginate(16);
		
        return view('admin.catalog.products.index', [
			'products' => $references,
			'categories' => $categories,
			'pagination' => ['search' => $search]
		])->with('search', $search);
    }
	
	

	//JORGE---------------------------------------------
    /*public function create(){
        return view('admin.catalog.products.create');
    }

    public function store(Request $request){
    }


    public function show($id){
        $products = Product::find($id);
        return view('admin.catalog.products.show', ['products' => $products]);
    }


    public function edit($id){
        $products = Product::find($id);
		return view('admin.appearance.products.edit', ['products' => $products]);
    }


    public function update(Request $request, $id){
    }

    

	public function getTags(){
		$tags = Tag::get();
		return view('admin.catalog.products.tags', ['tags' => $tags]);
	}

	public function productbytags(Request $request, $name){
		$search = $request->input('search');
		$products = Product::where('name','like','%'.$search.'%')->where('tags','like','%'.$name.'%')
			->orderBy('name')
			->groupBy('reference_id')
			->paginate(10);
		return view('admin.catalog.products.productbytags', compact('products'));
	}

	public function borrartags($name){
		$products = Product::where('tags','like','%'.$name.'%')->get();
		foreach ($products as $product):
			Product::deleteTags($product->tags,$name,$product->id);
		endforeach;

		Tag::where('name', $name )->delete();
		return redirect('admin/catalog/products/tags')
			->with('success', trans('cms.message_tag_deleted'));
	}*/
}