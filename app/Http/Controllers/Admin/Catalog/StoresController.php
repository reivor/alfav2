<?php 

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\Store;

class StoresController extends AdminController
{

	/**
	 * Shows the stores list.
	 * @return View
	 */
	public function getIndex()
	{
		$stores = Store::getActiveStores()
			->paginate(20);
		return view('admin.catalog.stores.index', ['stores' => $stores]);
	}

    public function getCreate(Request $request){
		
    	if(!$request->cookie('cms_country')){
    		$value = config('store.id');
    	} else{
    		$value = $request->cookie('cms_country');
    	}

		
		$store = new Store();
		
		switch ($value){
			case "co":
				$store->latitude = 4.6482976;
				$store->longitude = -74.107807;
			break;
			case "us":
				$store->latitude = 	25.761681;
				$store->longitude = -80.191788;
			break;
			case "ec":
				$store->latitude = -0.180653;
				$store->longitude = -78.467834;
			break;
		}
		
		$departments = Department::getDepartmentByCountry($value)->get();

        return view('admin.catalog.stores.form', [
			'store' => $store,
			'departments' => $departments,
			'action' => trans('cms.create_store')
		]);
    }
	
	public function postCreate(Request $request)
	{
		$store = new Store();
		$store->name = $request->input('name');
		$store->email = $request->input('email');
		$store->location = $request->input('location');
		$store->department_id = $request->input('department');
		$store->city_id = $request->input('cities');
		$store->opening_hours = $request->input('opening_hours');
		$store->latitude = $request->input('latitude');
		$store->longitude = $request->input('longitude');
		$store->phone_local = $request->input('phone_local');
		$store->phone_mobile = $request->input('phone_mobile');
		$store->save();
		
		// upload picture
		if( $request->hasFile('picture') && $request->file('picture')->isValid() ) {
			$file = $request->file('picture');
			$filePath = public_path('images/stores');
			$fileName = 'SO_' . uniqid() . '_' . str_pad($store->id, 6, '0', STR_PAD_LEFT) . '.' . $file->getClientOriginalExtension();
			$file->move($filePath, $fileName);
			$store->picture = '/images/stores/' . $fileName;
			$store->save();
		}
		
		return redirect('admin/catalog/stores')
			->with('success', trans('cms.message_store_created'));
	}
	
	public function getEdit($id)
	{
		$store = Store::find($id);
		if($store != null){
			$departments = Department::getDepartmentByCountry(config('store.id'))->get();
			$cities = '';
			if(!empty($store->department_id)){
				$cities = Department::find($store->department_id)->cities;
			}
			return view('admin.catalog.stores.form', [
				'store' => $store,
				'departments' => $departments,
				'cities' => $cities,
				'action' => trans('cms.edit_store')
			]);
		}else{
			return redirect('admin/catalog/stores');
		}

		
	}
	
	public function postEdit(Request $request, $id)
	{
		$store = Store::find($id);
		$store->name = $request->input('name');
		$store->email = $request->input('email');
		$store->location = $request->input('location');
		$store->department_id = $request->input('department');
		$store->city_id = $request->input('cities');
		$store->opening_hours = $request->input('opening_hours');
		$store->latitude = $request->input('latitude');
		$store->longitude = $request->input('longitude');
		$store->phone_local = $request->input('phone_local');
		$store->phone_mobile = $request->input('phone_mobile');
		
		// upload picture
		if( $request->hasFile('picture') && $request->file('picture')->isValid() ) {
			$file = $request->file('picture');
			$filePath = public_path('images/stores');
			$fileName = 'SO_' . uniqid() . '_' . str_pad($store->id, 6, '0', STR_PAD_LEFT) . '.' . $file->getClientOriginalExtension();
			$file->move($filePath, $fileName);
			$store->picture = '/images/stores/' . $fileName;
		}
		
		$store->save();
		
		return redirect('admin/catalog/stores')
			->with('success', trans('cms.message_store_updated'));
	}

	public function postCities(Request $request)
	{
		$cities = Department::find($request->input('department'))->cities;
		return response()->json($cities);
	}
	
	public function getView($id)
	{
		$store = Store::find($id);

		if($store != null){
			return view('admin.catalog.stores.view', ['store' => $store]);
		}else{
			return redirect('admin/catalog/stores');
		}

		
	}
	
	public function getDelete($id)
	{
		$store = Store::find($id);
		$store->delete();
		
		return redirect('admin/catalog/stores')
			->with('success', trans('cms.message_store_deleted'));
	}
	
}
