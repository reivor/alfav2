<?php 

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * Admin Controller
 */
class AdminController extends Controller {
	
	protected $storeId;
	protected $currentStoreId;
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$user = Auth::user();
		view()->share([
			'section' => 'dashboard', 
			'sessionUser' => $user
		]);
		$this->storeId = config('store.id');
		//setlocale(LC_ALL,"es_ES");
		//\Carbon\Carbon::setLocale('es');
		
	}

	public function setCountry($id)
	{
		return back()->withCookie('cms_country', $id, 45000);
	}

	public function setLang($id)
	{
		return back()->withCookie('lang', $id, 45000);
	}
	
	protected function getCountryCode(Request $request)
	{
		$countryCode = $request->cookie('cms_country');
		if( !$countryCode ) {
			$countryCode = config('store.id');
		}
		return $countryCode;
	}
	
}