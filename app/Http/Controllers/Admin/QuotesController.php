<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\QuoteOrder;
use App\Models\QuoteProduct;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Store;
use App\Http\Requests;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;

class QuotesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $id = $req->get('id') ? $req->get('id') : null;
        $name = $req->get('name') ? $req->get('name') : null;
        $date = $req->get('date') ? $req->get('date') : null;
        $email = $req->get('email') ? $req->get('email') : null;
        $city = $req->get('city') ? $req->get('city') : null;
        $manager = $req->get('manager') ? $req->get('manager') : null;
        $room = $req->get('room') ? $req->get('room') : null;
        $status = $req->get('status') ? $req->get('status') : null;
        $status_filter = $req->get('status_filter') ? $req->get('status_filter') : null;
        $room_filter = $req->get('room_filter') ? $req->get('room_filter') : null;

        if($id){
            $quotes = QuoteOrder::orderBy('id', $id);
        }
        else if($date){
            $quotes = QuoteOrder::orderBy('date_created', $date);
        }
        else if($manager){
            $quotes = QuoteOrder::orderBy('manager', $manager);
        }
        else if($status){
            $quotes = QuoteOrder::orderBy('status', $status);
        }
        else if($room){
            $quotes = QuoteOrder::join('stores', 'stores.id', '=', 'quote_orders.store_id')
                ->orderBy('stores.name', $room)
                ->select('quote_orders.*')
                ;
        }
        else if($name){
            $quotes = QuoteOrder::join('clientes', 'clientes.id', '=', 'quote_orders.customer_id')
                ->orderBy('clientes.name', $name)
                ->select('quote_orders.*')
                ;
        }
        else if($city){
            $quotes = QuoteOrder::join('clientes', 'clientes.id', '=', 'quote_orders.customer_id')
                ->join('cities', 'cities.id', '=', 'clientes.city')
                ->orderBy('cities.name', $city)
                ->select('quote_orders.*')
                ;
        }
        else if($email){
            $quotes = QuoteOrder::join('clientes', 'clientes.id', '=', 'quote_orders.customer_id')
                ->orderBy('clientes.email', $email)
                ->select('quote_orders.*')
                ;
        }
        else{
            $quotes = QuoteOrder::orderBy('date_created', 'desc');
        }
        if($status_filter){
            $quotes->where('status', '=', $status_filter);
        }
        if($room_filter){
            $quotes->where('store_id', '=', $room_filter);
        }

        $quotes = $quotes->paginate(10);

        $stores = Store::orderBy('stores.name', 'asc')->get();

        return view('admin.quotes.index', [
                'quotes' => $quotes, 
                'stores' => $stores,
                'id' => $id,
                'date' => $date,
                'name' => $name,
                'email' => $email,
                'city' => $city,
                'manager' => $manager,
                'room' => $room,
                'status' => $status,
                'status_filter' => $status_filter,
                'room_filter' => $room_filter
                ]); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $quote = QuoteOrder::find($id);

        if($quote != null){
        $quote_prods = QuoteProduct::where('quote_order_id', $id)->get();
        $quote_customer = Customer::where('id', $quote->customer_id)->firstOrFail();
        
        
        $grouped = array();
            foreach ($quote_prods as $gp) {
                $grouped[] = Product::where('id', $gp->product_id)->get();
            }

       // dd($grouped);

        return view('admin.quotes.show', ['quote' => $quote, 'quote_customer' => $quote_customer, 'grouped' => $grouped]);
        }else{
         return redirect('admin/quotes');
        }

    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDelete($id)
    {
        $quote = QuoteOrder::find($id);
        $quote->delete();
        return redirect('admin/quotes')
            ->with('success', trans('cms.message_quote_deleted'));
    }

    public function setManager(Request $request){
        $quote = QuoteOrder::where('id',$request->input('id'))->update(['manager' => $request->input('data')]);
    }

    public function setRoom(Request $request){
        $quote = QuoteOrder::where('id',$request->input('id'))->update(['store_id' => $request->input('data')]);
    }

    public function setStatus(Request $request){
        $quote = QuoteOrder::where('id',$request->input('id'))->update(['status' => $request->input('data')]);
    }

    public function setComment(Request $request){
        $quote = QuoteOrder::where('id',$request->input('id'))->update(['comment' => $request->input('data')]);
    }

    public function downloadQuotes(){
        set_time_limit(600);
        ini_set('memory_limit','512M');
        $date = \date("Y_m_d");

        $offset = 0;

        $writer = WriterFactory::create(TYPE::XLSX);
        $writer->openToBrowser('reporte_cotizaciones'.$date.'.xlsx');
        $writer->addRow(['ID','Cliente', 'Cédula','Residencia','Teléfono','Ocupación','Email','Ciudad','Gerente','Sala','Fecha','Estado','Comentario', 'Productos']);
            $data = QuoteOrder::join('clientes', 'clientes.id', '=', 'quote_orders.customer_id')
                ->select('quote_orders.*','clientes.name','clientes.lastname','clientes.email', 'clientes.residence','clientes.cell_phone','clientes.identification', 'clientes.job')
                ->orderBy('id', 'asc')
                ->get();
            $offset= $offset+1000;
            foreach ($data as $key => $value) {
                $status= '';
                switch ($value->status) {
                    case 'open':
                        $status = 'Abierto';
                        break;
                    case 'closed':
                        $status = 'Cerrado';
                        break;
                    case 'new':
                        $status = 'Nuevo';
                        break;
                    case 'sent':
                        $status = 'Enviado';
                        break;
                    case 'received':
                        $status = 'Recibido';
                        break;
                    case 'noinfo':
                        $status = 'Sin Información';
                        break;
                    case 'done':
                        $status = 'Realizado';
                        break;
                    default:
                        # code...
                        break;
                }

                $products = '';
                foreach ($value->quoteProducts as $key => $val) {
                    $products .= $val->name.', ';
                }

                if($products != '')
                $writer->addRow([
                    $value->id,
                    $value->name.' '.$value->lastname,
                    $value->identification,
                    $value->residence,
                    $value->cell_phone,
                    $value->job,
                    $value->email,
                    $value->customer->getCity['name'],
                    $value->manager,
                    $value->store['name'],
                    $value->date_created->format('d-m-Y'),
                    $status,
                    $value->comment,
                    $products
                ]);
            }
        $writer->close();

    }
	
}
