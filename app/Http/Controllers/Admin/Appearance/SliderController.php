<?php 

namespace App\Http\Controllers\Admin\Appearance;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use App\Models\Category;

class SliderController extends AdminController
{

	/**
	 * Shows the home slider view.
	 * @return View
	 */
	public function getHome()
	{
		$categories = Category::getTopLevelCategories()->get();
		return view('admin.appearance.slider.home', ['categories' => $categories]);
	}

}
