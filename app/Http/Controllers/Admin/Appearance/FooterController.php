<?php 

namespace App\Http\Controllers\Admin\Appearance;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\FooterCategory;
use DB;

class FooterController extends AdminController{

	public function getIndex(){
		$categories=Category::buildCategoriesTree();
		$footerCategories= FooterCategory::all();
		$selectedCategories=array();

		foreach ($footerCategories as $footerCategory) {
			$selectedCategories[]= (int)$footerCategory->category_id;
		}

		$selectedCategoriesAux=$selectedCategories;
		$selectedCategories=json_encode($selectedCategories);

		return view('admin.appearance.footer.index', ['categories' => $categories, 'selectedCategories' => $selectedCategories, 'selectedCategoriesAux' => $selectedCategoriesAux]);
	}

	public function postSave(Request $request){

		$success="success";
		$message=trans('cms.message_footer_categories_created');
		$selectedCategories=json_decode($request->selectedCategories);
		$counterSelectedCategories=array();

		foreach ($selectedCategories as $keyCategory => $supremeParent) {
			if($supremeParent!=-1 && (!isset($counterSelectedCategories[$supremeParent]) || count($counterSelectedCategories[$supremeParent])<FooterCategory::MAX_CATEGORIES_LEVEL)){
				$counterSelectedCategories[$supremeParent][]=$keyCategory;
			}
		}

		try{
			DB::beginTransaction();
			FooterCategory::truncate();

			foreach ($counterSelectedCategories as $keyParent => $supremeParent) {
				foreach ($supremeParent as $category) {
					$footerCategory = new FooterCategory;
					$footerCategory->category_id = $category;
					$footerCategory->supreme_parent = $keyParent;
					$footerCategory->save();
				}
			}

			DB::commit();
		}catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			$success="error";
			$message=trans('cms.message_footer_categories_error');

		}

		return redirect('/admin/appearance/footer')
			->with('success', trans('cms.message_footer_categories_created'));
	}

}
