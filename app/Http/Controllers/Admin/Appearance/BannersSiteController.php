<?php

namespace App\Http\Controllers\Admin\Appearance;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Category;
use App\Models\Product;
use App\Models\BannerSite;
use DB;

class BannersSiteController extends AdminController{
	
	public function getIndex(){
		$categories=Category::buildCategoriesTree();
		
		$status = [BannerSite::STATUS_ACTIVE, BannerSite::STATUS_DISABLED];
		
		$banners=BannerSite::all();
		
		$bannersHomeTop=array();
		$bannersHomeFooter=array();
		$bannersSubhome=array();
		$bannersRegistro=array();
		$bannersBlog=array();
		$name="";
		
		foreach ($banners as $banner) {
			if($banner->type==BannerSite::TYPE_PRODUCT){
				$aux=Product::find($banner->entity_id);
				$name=$aux->name;
			}
			$banner->name=$name;
			if($banner->location== BannerSite::LOCATION_HOME_TOP){
				$bannersHomeTop[$banner->position]=$banner;
			}else if($banner->location== BannerSite::LOCATION_HOME_FOOTER){
				$bannersHomeFooter[$banner->position]=$banner;
			}else if($banner->location== BannerSite::LOCATION_SUBHOME){
				$bannersSubhome[$banner->position]=$banner;
			}else if($banner->location== BannerSite::LOCATION_REGISTER){
				$bannersRegistro[$banner->position]=$banner;
			}else if($banner->location== BannerSite::LOCATION_BLOG){
				$bannersBlog[$banner->position]=$banner;
			}
		}

		return view('admin.appearance.bannersSite.index',[
			'categories' => $categories,
			'bannersHomeTop' => $bannersHomeTop, 
			'bannersHomeFooter' => $bannersHomeFooter, 
			'bannersSubHome' => $bannersSubhome, 
			'bannersRegistro' => $bannersRegistro, 
			'bannersBlog' => $bannersBlog,
			'status' => $status]);
	}
	
	
	public function postSave(Request $request){
		$message=trans('cms.message_banners_site_created');
		$status="success";

		try{
			for ($i = 1; $i <= 5; $i++) {
				$this->processBanners($i,$request);
			}
		}catch(\Exception $e){
			$status='error';
			$message=trans('cms.message_banners_site_created');	
		}
		
		return redirect('admin/appearance/banners_site')
			->with($status, $message);
	}
	
	public function processBanners($location, $request){
		if($request->input('type-'.$location)){
			$typeArray=$request->input('type-'.$location);
			$entityArray=$request->input('entity-'.$location);
			$externalArray=$request->input('external-'.$location);
			$statusArray=$request->input('status-'.$location);
			$imageGet=$request->input('image-get-'.$location);
			$fileArray=array();

			if($request->hasFile('image-'.$location)){
				$fileArray=$request->file('image-'.$location);
			}

			try{
				DB::beginTransaction();
				foreach ($typeArray as $i => $type) {
					$nameLocation=$this->getLocation($location);
						
					BannerSite::where('location', '=' , $nameLocation)
								->where('position', '=', $i)
								->delete();
					
					if($type!="" && $type!=null){
						$bannerSite = new BannerSite();
						$bannerSite->type=$type;
						$bannerSite->location=$nameLocation;
						$bannerSite->status = $statusArray[$i];

						if($bannerSite->type==BannerSite::TYPE_EXTERNAL){
							$bannerSite->external_url=$externalArray[$i];
						}else{
							$bannerSite->entity_id=$entityArray[$i];
						}

						$bannerSite->position=$i;
						$bannerSite->save();

						if(isset($fileArray[$i])){
							if( $fileArray[$i]->isValid() ) {
								$currentFile=$fileArray[$i];
								$filePath = public_path('images/bannersSite');
								$fileName = 'SO_' . uniqid() . '_' . str_pad($bannerSite->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
								$currentFile->move($filePath, $fileName);
								$bannerSite->image_url = '/images/bannersSite/' . $fileName;
								$bannerSite->save();
							}else{
								throw new \Exception ("Banner ".$nameLocation." no válido");
							}
						}else{
							if(isset($imageGet[$i])){
								$bannerSite->image_url=$imageGet[$i];
								$bannerSite->save();
							}
						}
					}
				}
				DB::commit();	
			}catch(\Exception $e){
				Log::error($e->getMessage());
				DB::rollBack();
				throw $e;
			}		
		}	
	}
	
	public function getLocation($location){
		$response="";
		switch ($location) {
			case 1:
				$response=BannerSite::LOCATION_HOME_TOP;
				break;
			case 2:
				$response=BannerSite::LOCATION_HOME_FOOTER;
				break;
			case 3:
				$response=BannerSite::LOCATION_SUBHOME;
				break;
			case 4:
				$response=BannerSite::LOCATION_REGISTER;
				break;
			case 5:
				$response=BannerSite::LOCATION_BLOG;
				break;
		}
		
		return $response;
		
	}
	
}