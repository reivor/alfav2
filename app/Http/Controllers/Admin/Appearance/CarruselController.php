<?php

namespace App\Http\Controllers\Admin\Appearance;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Carrusel;
use App\Models\Product;
use DB;

class CarruselController extends AdminController{

	public function index(){
		$carrusel = Carrusel::all();
		return view('admin.appearance.carrusel.index', ['carrusel' => $carrusel]);
	}

	public function create(){
		return view('admin.appearance.carrusel.create');
	}


	public function store(Request $request){
		
		$message=trans('cms.message_carrusel_created');
		$success="success";

		/*$this->validate($request, [
			'name' => 'required|unique:carrusel_home',
			'image_url' => 'required|mimes:png,jpeg,jpg'
		]);*/
		
		try{
			DB::beginTransaction();
			
			$carrusel = new Carrusel();
			$carrusel->type = $request->input('linked_to');
		
			if($carrusel->type==Carrusel::TYPE_EXTERNAL){
				$carrusel->external_url=$request->input('url');
			}else if($carrusel->type==Carrusel::TYPE_CATEGORY){
				$carrusel->entity_id=$request->input('url_category');
			}else{
				$carrusel->entity_id=$request->input('url_product');
			}
			
			$carrusel->save();
			
			if( $request->hasFile('image_url') && $request->file('image_url')->isValid() ) {
				$file = $request->file('image_url');
				$filePath = public_path('images/slider');
				$fileName = 'SO_' . uniqid() . '_' . str_pad($carrusel->id, 6, '0', STR_PAD_LEFT) . '.' . $file->getClientOriginalExtension();
				$file->move($filePath, $fileName);
				$carrusel->image_url = '/images/slider/' . $fileName;
				$carrusel->save();
			}else{
				throw new \Exception ("Imagen no válida");				
			}
		
			DB::commit();
		}catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			$success="error";
			$message=trans('cms.message_carrusel_error');
		}

		return redirect('admin/appearance/carrusel')
				->with($success, $message);
	}


	public function show($id){
		
	}

	public function edit($id){
		
		$carrusel = Carrusel::find($id);
		
		if($carrusel){
			if($carrusel->type!=Carrusel::TYPE_EXTERNAL){
				$aux=Product::find($carrusel->entity_id);
				
				if($aux){
					$carrusel->product=$aux->name;
				}else{
					$carrusel->product="";
				}
			}
			
			return view('admin.appearance.carrusel.edit', ['carrusel' => $carrusel]);
		}
		
		return redirect('admin/appearance/carrusel');
		
	}


	public function update(Request $request, $id){
		$message=trans('cms.message_carrusel_updated');
		$success="success";

		/*$this->validate($request, [
			'name' => 'required|unique:carrusel_home,name,' . $id,
		]);*/
		
		try{
			DB::beginTransaction();
			
			$carrusel = Carrusel::find($id);
			$carrusel->type = $request->input('linked_to');
		
			if($carrusel->type==Carrusel::TYPE_EXTERNAL){
				$carrusel->external_url=$request->input('url');
				$carrusel->entity_id=NULL;
			}else if($carrusel->type==Carrusel::TYPE_CATEGORY){
				$carrusel->entity_id=$request->input('url_category');
				$carrusel->external_url=NULL;
			}else{
				$carrusel->entity_id=$request->input('url_product');
				$carrusel->external_url=NULL;
			}
			
			$carrusel->save();
			
			if( $request->hasFile('image_url') && $request->file('image_url')->isValid() ) {
				$file = $request->file('image_url');
				$filePath = public_path('images/slider');
				$fileName = 'SO_' . uniqid() . '_' . str_pad($carrusel->id, 6, '0', STR_PAD_LEFT) . '.' . $file->getClientOriginalExtension();
				$file->move($filePath, $fileName);
				$carrusel->image_url = '/images/slider/' . $fileName;
				$carrusel->save();
			}
		
			DB::commit();
		}catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			$success="error";
			$message=trans('cms.message_carrusel_error');
		}

		return redirect('admin/appearance/carrusel')
				->with($success, $message);

//		$carrusel = Carrusel::find($id);
//		$carrusel->name = $request->input('name');
//		$carrusel->linked_to = $request->input('linked_to');
//		$carrusel->url_category = $request->input('url_category');
//		$carrusel->url_product = $request->input('url_product');
//		$carrusel->url = $request->input('url');
//		$carrusel->status = $request->input('status');
//
//		if( $request->hasFile('image_url') && $request->file('image_url')->isValid() ) {
//			$file = $request->file('image_url');
//			$filePath = public_path('images/slider');
//			$fileName = 'SO_' . uniqid() . '_' . str_pad($carrusel->id, 6, '0', STR_PAD_LEFT) . '.' . $file->getClientOriginalExtension();
//			$file->move($filePath, $fileName);
//			$carrusel->image_url = '/images/slider/' . $fileName;
//			$carrusel->save();
//		}
//
//		$carrusel->save();
//
//		return redirect('admin/appearance/carrusel')
//				->with('success', trans('cms.message_carrusel_updated'));
	}


	public function destroy($id){
		$carrusel = Carrusel::find($id);
		$carrusel->delete();

		return redirect('admin/appearance/carrusel')
				->with('success', trans('cms.message_carrusel_deleted'));
	}
	
	public function changeStatus($id,$status){
		$message=trans('cms.message_carrusel_updated');
		$success="success";
		
		$carrusel = Carrusel::find($id);

		if($carrusel){
			if($status == Carrusel::STATUS_ACTIVE){
				$carrusel->status = Carrusel::STATUS_ACTIVE;
			}else{
				$carrusel->status = Carrusel::STATUS_DISABLED;
			}
			$carrusel->save();
		}else{
			$message=trans('cms.message_carrusel_error_edit');
			$success="error";
		}
		
		return redirect('admin/appearance/carrusel')
				->with($success, $message);
	}

}
