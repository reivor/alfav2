<?php

namespace App\Http\Controllers\Admin\Appearance;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\BannerHome;
use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;
use DB;

class BannersHomeController extends AdminController{


    public function getIndex(){
		$banners= BannerHome::all();
		
		foreach( $banners as $banner ) {
			$bannersHome[$banner->position] = $banner;
		}

		$setting = Setting::where('name', '=', Setting::LAYOUT_SETTING)->first();
		$layout=0;
		if($setting!=NULL){
			$layout=$setting->value;
		}
		$category=Category::find(Category::AMBIENTES_CATEGORY);
		$childCategories = Category::getCategoriesTree($category);
		$status = [BannerHome::STATUS_ACTIVE, BannerHome::STATUS_DISABLED];

		$products= Product::getTreeProducts(Category::AMBIENTES_CATEGORY)->get();
		
        return view('admin.appearance.banners_home.index', [
			'categories' => $childCategories->categories, 
			'products' => $products, 
			'banners' => $bannersHome, 
			'layout' => $layout,
			'status' => $status]);
    }

    public function getCreate(){
        return view('admin.appearance.banner.create');
    }
	
	public function getEdit($id){
		$category = Category::find($id);
		return view('admin.catalog.categories.create', ['category' => $category]);
	}
	
	public function postSave(Request $request){
		$success="success";
		$message=trans('cms.banners_home_created');

		$typeLayout=$request->input('type-layout');
		$numberItems=$request->input('number-items');
		$typeArray=$request->input('type');
		$entityArray=$request->input('entity');
		$externalArray=$request->input('external');
		$statusArray=$request->input('status');
		$imageGet=$request->input('image-get');
		$imageMobileGet=$request->input('image-mobile-get');
		$fileArray=array();
		$fileMobileArray=array();

		if($request->hasFile('image')){
			$fileArray=$request->file('image');
		}
		
		if($request->hasFile('imageMobile')){
			$fileMobileArray=$request->file('imageMobile');
		}

		try{
			DB::beginTransaction();
			BannerHome::truncate();
			
			for ($i=1; $i<=$numberItems; $i++){
				$bannerHome = new BannerHome();
				$bannerHome->position=$i;
				$bannerHome->type=$typeArray[$i];
				$bannerHome->status = $statusArray[$i];
				
				if($bannerHome->type==BannerHome::TYPE_EXTERNAL){
					$bannerHome->external_url=$externalArray[$i];
				}else{
					$bannerHome->entity_id=$entityArray[$i];
				}

				$bannerHome->save();
				
				//Image normal
				if(isset($fileArray[$i])){
					if( $fileArray[$i]->isValid() ) {
						$currentFile=$fileArray[$i];
						$filePath = public_path('images/bannersHome');
						$fileName = 'SO_' . uniqid() . '_' . str_pad($bannerHome->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
						$currentFile->move($filePath, $fileName);
						$bannerHome->image_url = '/images/bannersHome/' . $fileName;
						Log::error("BannerHome: ".$bannerHome->image_url);
						$bannerHome->save();
					}
				}else{
					if(isset($imageGet[$i])){
						$bannerHome->image_url=$imageGet[$i];
						Log::error("BannerHome: ".$bannerHome->image_url);
						$bannerHome->save();
					}
				}
				//Image mobile
				if(isset($fileMobileArray[$i])){
					if( $fileMobileArray[$i]->isValid() ) {
						$currentFile=$fileMobileArray[$i];
						$filePath = public_path('images/bannersHomeMobile');
						$fileName = 'SO_' . uniqid() . '_' . str_pad($bannerHome->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
						$currentFile->move($filePath, $fileName);
						$bannerHome->image_mobile_url = '/images/bannersHomeMobile/' . $fileName;
						$bannerHome->save();
					}
				}else{
					Log::error("IndexxMobile: ".$i);
					if(isset($imageMobileGet[$i])){
						$bannerHome->image_mobile_url=$imageMobileGet[$i];
						$bannerHome->save();
					}
				}
					
			}
			
			$setting = Setting::firstOrNew(['name' => Setting::LAYOUT_SETTING]);
			$setting->value=$typeLayout;
			$setting->save();
			
			DB::commit();	
		}catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			$success="error";
			$message=trans('cms.message_banners_home_error');
		}

		return redirect('admin/appearance/banners_home')
			->with($success, $message);

	}
}
