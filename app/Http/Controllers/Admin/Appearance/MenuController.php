<?php 

namespace App\Http\Controllers\Admin\Appearance;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\BannerMenu;
use DB;

class MenuController extends AdminController{

	public function getIndex(){//Muestra todas las categorias de nivel 1
		$categories = Category::getTopLevelCategories()->get();
		return view('admin.appearance.menu.index', ['categories' => $categories]);
	}
	
	public function categories($id){//Obtiene las categorias hijas de la categoria de nivel 1 seleccionada
		$category=Category::find($id);
		if($category){
			$childCategories = Category::getCategoriesTree($category);
			$products= Product::getTreeProducts($id)->get();
			$bannersMenu=BannerMenu::where('category_id', '=' , $id)->get();
			$banners=array();
			
			foreach ($bannersMenu as $banner) {
				$banners[$banner->position]=$banner;
			}
			
			$status = [BannerMenu::STATUS_ACTIVE, BannerMenu::STATUS_DISABLED];
			//$currentFeaturedProducts =  MenuProduct:: getMenuProducts($id)->get();
			
			return view('admin.appearance.menu.categories', [
				'childCategories' => $childCategories, 
				'products' => $products, 
				'banners' => $banners, 
				'status' => $status]);

		}
		
		return redirect('admin/appearance/menu/categories');
	
	}
	
	public function postProducts(Request $request){//Obtiene los productos relacionados a la categoria seleccionada
		$products=  Product::getProductsByCategory($request->selectedCategory)->get();
		return json_encode($products);
	}
	
	public function postSave(Request $request){//Guarda la información asociada al mega menu
		$success='success';
		$message=trans('cms.categories_menu_updated');

		$typeArray=$request->input('type');
		$supremeParent=$request->input('supreme-parent');
		$entityArray=$request->input('entity');
		$externalArray=$request->input('external');
		$imageGet=$request->input('image-get');
		$statusArray=$request->input('status');
		$fileArray=array();

		if($request->hasFile('image')){
			$fileArray=$request->file('image');
		}

		try{
			DB::beginTransaction();
			
			
			
			foreach ($typeArray as $i=> $type) {
				BannerMenu::where('category_id', '=' , $supremeParent)
							->where('position', '=', $i)
							->delete();
				if($type!="" && $type!=null){
					$bannerMenu = new BannerMenu();
					$bannerMenu->type = $type;
					$bannerMenu->status = $statusArray[$i];

					if($bannerMenu->type==BannerMenu::TYPE_EXTERNAL){
						$bannerMenu->external_url=$externalArray[$i];
					}else{
						$bannerMenu->entity_id=$entityArray[$i];
					}
					
					$bannerMenu->position=$i;
					$bannerMenu->category_id=$supremeParent;
					$bannerMenu->save();
					
					if(isset($fileArray[$i])){
						if( $fileArray[$i]->isValid() ) {
							$currentFile=$fileArray[$i];
							$filePath = public_path('images/bannersMenu');
							$fileName = 'SO_' . uniqid() . '_' . str_pad($bannerMenu->id, 6, '0', STR_PAD_LEFT) . '.' . $currentFile->getClientOriginalExtension();
							$currentFile->move($filePath, $fileName);
							$bannerMenu->image_url = '/images/bannersMenu/' . $fileName;
							$bannerMenu->save();
						}else{
							throw new \Exception (trans('cms.categories_menu_error'));
						}
					}else{
						if(isset($imageGet[$i])){
							$bannerMenu->image_url=$imageGet[$i];
							$bannerMenu->save();
						}
					}
				}
			}
			DB::commit();
		}catch(Exception $e){
			Log::error($e->getMessage());
			DB::rollBack();
			$status='error';
			$message=trans('cms.categories_menu_error');	
		}

		return redirect('/admin/appearance/menu/categories/'.$supremeParent)
			->with($success, $message);
	}
}
