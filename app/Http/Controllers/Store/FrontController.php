<?php

namespace App\Http\Controllers\Store;

use Response;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\BannerHome;
use App\Models\FooterCategory;
use App\Models\Category;
use App\Models\BannerSite;
use App\Models\BannerMenu;
use App\Models\Carrusel;
use App\Models\Product;
use App\Models\PageMeta;
use App\Models\BlogArticle;
use Illuminate\Support\Facades\DB;
use App\Models\QuoteOrder;
use App\Models\Customer;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Reference;

class FrontController extends Controller
{

	protected $storeId;

	public function __construct()
	{
		ini_set('xdebug.max_nesting_level', 200);
		// STORE CONFIG
		$this->storeId = config('store.id');

		//Footer
		$footerItems = FooterCategory::getFooterItems($this->storeId);
		$settings = Setting::get();
		$settings_array = [];
		foreach( $settings as $key => $value ) {
			# code...
			$settings_array[$value->name] = $value->value;
		}

		//Mega menu
		$categoriesMenu = Category::buildCategoriesTreeCache($this->storeId);

		//Banners Menu
		$bannersMenu = BannerMenu::getBannersMenu();
		$loginStatus = $this->isLogin();
		$userLogin = $this->userLogin();

		//Slider Productos mas buscados
		$productsSearch = Product::mostSearched($this->storeId);

		//Cantidad Productos Carrito Cotizacion
		$numberProducts = 0;
		if( $this->isLogin() ) {
			$currentOrder = QuoteOrder::getCurrentOrder($this->userLogin()->id);
			$numberProducts = count($currentOrder->quoteProducts);
		}

		// Ideas: Blog
		$ideas = BlogArticle::getRecentArticles()
			->take(2)
			->get();

		// shared view vars
		view()->share([
			'loginStatus' => $loginStatus,
			'userLogin' => $userLogin,
			'footerItems' => $footerItems,
			'categoriesMenu' => $categoriesMenu,
			'bannersMenu' => $bannersMenu,
			'numberProducts' => $numberProducts,
			'settings' => $settings_array,
			'productsSearch' => $productsSearch,
			'blogIdeas' => $ideas,
			'locale' => config('store.id'),
		]);
	}

	public function isLogin()
	{
		if( session('Customer') ) {
			return true;
		}
		else {
			return false;
		}
	}

	public function userLogin()
	{
		if( $this->isLogin() ) {
			return Customer::where('id', session('Customer'))->first();
		}
		else {
			return null;
		}
	}

	public function home()
	{
		//Banner Principales
		$currentLayout = $this->getCurrentLayoutHome();
		$bannersHome = [];

		if( $currentLayout != 0 ) {
			$bannersHome = BannerHome::getBannersHome();
		}

		//Slider Principal
		$carouselBanners = Carrusel::where(['status' => Carrusel::STATUS_ACTIVE])->get();

		//Banners Promocionales
		$bannersPromoHomeTop = BannerSite::getBannersHomeTop();
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();

		//META PAGES
		$metadata = PageMeta::metaPage('home');

		return view('store.pages.home', [
			'currentLayout' => $currentLayout,
			'bannersHome' => $bannersHome,
			'bannersPromoHomeTop' => $bannersPromoHomeTop,
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'carouselBanners' => $carouselBanners,
			'metadata' => $metadata,
		]);
	}

	public function getCurrentLayoutHome()
	{//Layout de los banners del home
		$settingLayout = Setting::where(['name' => Setting::LAYOUT_SETTING])->first();
		$currentLayout = 0;

		if( $settingLayout != null ) {
			$currentLayout = $settingLayout->value;
		}
		return $currentLayout;
	}

	public function aboutUs()
	{
		//META PAGES
		$metadata = PageMeta::metaPage('aboutus');
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();
		return view('store.pages.about_us', [
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'metadata' => $metadata,
		]);
	}

	public function alfaAmbiental()
	{
		//META PAGES
		$metadata = PageMeta::metaPage('ambiente');
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();
		return view('store.pages.alfa_ambiental', [
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'metadata' => $metadata
		]);
	}

	public function trabajos()
	{
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();
		//META PAGES
		$metadata = PageMeta::metaPage('workus');
		return view('store.pages.trabajos', [
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'$metadata' => $metadata
		]);
	}

	public function financiacion()
	{
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();
		//META PAGES
		$metadata = PageMeta::metaPage(' financiamiento');
		return view('store.pages.financiacion', [
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'$metadata' => $metadata
		]);
	}

	public function instalacion()
	{
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();
		//META PAGES
		$metadata = PageMeta::metaPage('instalacion');
		return view('store.pages.instalacion', [
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'$metadata' => $metadata
		]);
	}

	public function diseno()
	{
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();
		//META PAGES
		$metadata = PageMeta::metaPage('diseno');
		return view('store.pages.diseno', [
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'$metadata' => $metadata
		]);
	}

	public function catalogos()
	{

		$files = glob(public_path("files/catalogos/*.pdf"));
		$catalogos = [];

		foreach( $files as $key => $file ) {
			$pathParts = pathinfo($file);
			$catalogos[$key]['basename'] = $pathParts['basename'];
			$catalogos[$key]['filename'] = $pathParts['filename'];
		}
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();
		//META PAGES
		$metadata = PageMeta::metaPage('catalogo');
		return view('store.pages.catalogos', [
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'catalogos' => $catalogos,
			'$metadata' => $metadata]);
	}

	public function certificaciones()
	{
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();
		//META PAGES
		$metadata = PageMeta::metaPage('certificacion');

		return view('store.pages.certificaciones', [
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'$metadata' => $metadata
		]);
	}

	public function clientInfo()
	{
		return view('store.pages.clientInfo');
	}
	
	public function cmp($a, $b)
	{
	    return strcmp($a->slug, $b->slug);
	}
	
	public function clientInfoProduct($type, $category = "", $product = 0)
	{	
		$moreProducts = false;
		$productsSelect = [];
		$categoryParent = Category::where('code', Category::CATEGORY_EXPERTS)->first();
		$categoriesSelect = [];

		Category::getLeafCategoriesArray($categoryParent,$categoriesSelect);

		usort($categoriesSelect, array($this, "cmp"));

		$ids = [];

		if( $category != "" ) {
			// Filtro por categoria: colecciones
			// $rootCategory = Category::where('code', Category::CATEGORY_EXPERTS)->first();
			// $categories = Category::where('slug', $rootCategory)->get();
			// $ids = array_unique(
			// 	array_merge($ids, Category::getCategoriesTree($rootCategory)->lists('id')->toArray())
			// );

			$cat_array = [];
			Category::getCategoriesArray($categoryParent, $cat_array);
			foreach ($cat_array as $key => &$value) {
				$arr_cat = [];
				Category::getCategoriesTreeParents($value);
				Category::getCategoriesArrayParents($value, $arr_cat);
				if(in_array($category, $arr_cat)) $ids[] = $value->id;
			}
		}
		else {
			// no filter
			// $categories = Category::getTopLevelCategories()->get();
			// foreach( $categories as $currentCategory ) {
			// 	$ids = array_merge($ids, Category::getCategoriesTree($currentCategory)->lists('id')->toArray());
			// }
			// $ids = array_unique($ids); // remove duplicates

			$cat_array = [];
			Category::getCategoriesArray($categoryParent, $cat_array);
			foreach ($cat_array as $key => $value) {
				$ids[] = $value->id;
			}
		}

		$productIds = ProductCategory::whereIn('category_id', $ids)
			->lists('product_id');

		$type_query = '1';
		switch ($type) {
			case trans('routes.warranties'):
				$type_query = 'references.guarantee is not null';
				break;
			case trans('routes.seals'):
				$type_query = 'references.quality_seal is not null';
				break;
			case trans('routes.manuals'):
				$type_query = 'references.user_manual is not null';
				break;
			case trans('routes.datasheets'):
				$type_query = 'references.data_sheet is not null';
				break;
		}

		//Paginacion
		$productsTotal = Product::join('references', 'products.reference_id', '=', 'references.id')
				->whereIn('products.id', $productIds)
				->whereRaw($type_query)
				->select('products.*')
				->get();

		if( $product != 0 ) {//Filtro por producto
			$products = Product::join('references', 'products.reference_id', '=', 'references.id')
				->where('products.id', $product)
				->whereRaw($type_query)
				->select('products.*')
				->get();
		}
		else {
			$products = Product::join('references', 'products.reference_id', '=', 'references.id')
				->whereIn('products.id', $productIds)
				->whereRaw($type_query)
				->select('products.*')
				->limit(8)
				->get();

			if( count($productsTotal) > 8 ) {
				$moreProducts = true;
			}
		}

		//Populacion selects
		if( $category != "" ) {
			$productsSelect = $productsTotal;
		}

		return view('store.pages.clientInfoProduct', compact('products', 'category', 'moreProducts', 'categoriesSelect', 'productsSelect', 'product'))
				->with('type', $type);
	}

	public function loadMore($type = "",$page = 1, $category = "")
	{
		
		// $ids = [];
		// if( $category != "" ) {
		// 	// Filtro por categoria: colecciones
		// 	$rootCategory = Category::where('code', Category::CATEGORY_COLECCIONES)->first();
		// 	$categories = Category::where('slug', $rootCategory)->get();
		// 	$ids = array_unique(
		// 		array_merge($ids, Category::getCategoriesTree($rootCategory)->lists('id')->toArray())
		// 	);
		// }
		// else {
		// 	// no filter
		// 	$categories = Category::getTopLevelCategories()->get();
		// 	foreach( $categories as $currentCategory ) {
		// 		$ids = array_merge($ids, Category::getCategoriesTree($currentCategory)->lists('id')->toArray());
		// 	}
		// 	$ids = array_unique($ids); // remove duplicates
		// }

		$categoryParent = Category::where('code', Category::CATEGORY_EXPERTS)->first();

		$ids = [];

		if( $category != "" ) {
			// Filtro por categoria: colecciones
			// $rootCategory = Category::where('code', Category::CATEGORY_EXPERTS)->first();
			// $categories = Category::where('slug', $rootCategory)->get();
			// $ids = array_unique(
			// 	array_merge($ids, Category::getCategoriesTree($rootCategory)->lists('id')->toArray())
			// );

			$cat_array = [];
			Category::getCategoriesArray($categoryParent, $cat_array);
			foreach ($cat_array as $key => &$value) {
				$arr_cat = [];
				Category::getCategoriesTreeParents($value);
				Category::getCategoriesArrayParents($value, $arr_cat);
				if(in_array($category, $arr_cat)) $ids[] = $value->id;
			}
		}
		else {
			// no filter
			// $categories = Category::getTopLevelCategories()->get();
			// foreach( $categories as $currentCategory ) {
			// 	$ids = array_merge($ids, Category::getCategoriesTree($currentCategory)->lists('id')->toArray());
			// }
			// $ids = array_unique($ids); // remove duplicates

			$cat_array = [];
			Category::getCategoriesArray($categoryParent, $cat_array);
			foreach ($cat_array as $key => $value) {
				$ids[] = $value->id;
			}
		}

		$productIds = ProductCategory::whereIn('category_id', $ids)
			->lists('product_id');

		$type_query = '1';
		switch ($type) {
			case trans('routes.warranties'):
				$type_query = 'references.guarantee is not null';
				break;
			case trans('routes.seals'):
				$type_query = 'references.quality_seal is not null';
				break;
			case trans('routes.manuals'):
				$type_query = 'references.user_manual is not null';
				break;
			case trans('routes.datasheets'):
				$type_query = 'references.data_sheet is not null';
				break;
		}

		$productsPrevious = $page * 8;
		$productsTotal = Product::join('references', 'products.reference_id', '=', 'references.id')
				->whereIn('products.id', $productIds)
				->whereRaw($type_query)
				->select('products.*')
				->count();

		$products = Product::join('references', 'products.reference_id', '=', 'references.id')
			->whereIn('products.id', $productIds)
			->whereRaw($type_query)
			->select('products.*')
			->limit(8)
			->skip($page * 8)
			->get();

		$currentNumberProducts = $productsPrevious + count($products);
		$moreProducts = false;

		if( $currentNumberProducts < $productsTotal ) {
			$moreProducts = true;
		}

		return Response::json(['products' => $products, 'moreProducts' => $moreProducts]);
	}

}
