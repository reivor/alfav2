<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\City;
use App\Models\Store;

class StoresController extends FrontController{


	public function getIndex(Request $request)
	{
		$storeId = config('store.id');
		$departments = Department::getDepartmentByCountry($storeId)->get();

		if(!$request->cookie('country')){
    		$value = config('store.id');
    	} else{
    		$value = $request->cookie('country');
    	}

    	$store = new Store();

		switch ($value){
			case "co":
				$store->latitude = 4.6482976;
				$store->longitude = -74.107807;
			break;
			case "us":
				$store->latitude = 	25.761681;
				$store->longitude = -80.191788;
			break;
			case "ec":
				$store->latitude = -0.180653;
				$store->longitude = -78.467834;
			break;
		}

		return view('store.pages.stores', [
			'departments' => $departments,
			'store' => $store
		]);
	}

	public function getCities($departmentId)
	{
		$department = Department::find($departmentId);
		$cities = $department->cities;
		return response()->json($cities);
	}

	public function getSearch(Request $request)
	{
		$departmentId = $request->input('department');
		$cityId = $request->input('city');
		$stores = Store::search([
			'department' => $departmentId,
			'city' => $cityId
		])->get();


		$groups = [];
		foreach($stores as $store) {
			if( !isset($groups[$store->city->id]) ) {
				$item = new \stdClass();
				$item->city = $store->city;
				$item->stores = collect([$store]);
				$groups[$store->city->id] = $item;
			}
			else {
				$groups[$store->city->id]->stores->push($store);
			}
		}
		return response()->json($groups);
	}

}
