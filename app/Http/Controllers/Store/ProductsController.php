<?php

namespace App\Http\Controllers\Store;

use Response;
use App\Models\Category;
use App\Models\Product;
use App\Models\BannerCategory;
use Illuminate\Support\Facades\DB;
use App\Models\BannerSite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\ProductCategory;
use App\Models\QuoteOrder;
use App\Models\QuoteProduct;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Mail;
use App\Models\Setting;
use App\Models\User;
use App\Models\City;

class ProductsController extends FrontController {

	public function selectSite($slug, $id)
	{
		
		$category = Category::find($id);

		$parent = $category->parentOfCategory();

		if ($parent->isExpertsCategory()) {
			return $this->getProductsExperts($id);
		}
		else {
			if ($category->isShort()) {
				return $this->categories($id);
			}
			else {
				return $this->subhome($id);
			}
		}
	}

	public function getProductsExperts($idcategory)
	{
		$ids_categories = [];
		$category = Category::find($idcategory);
		$ids_categories[] = $category->id;

		foreach ($category->categories as $key => $category1) {
			$ids_categories[] = $category1->id;
			if ($category1->categories) {
				foreach ($category1->categories as $key => $category2) {
					$ids_categories[] = $category2->id;
					if ($category2->categories) {
						foreach ($category2->categories as $key => $category3) {
							$ids_categories[] = $category3->id;
						}
					}
				}
			}
		}

		$idsproducts = ProductCategory::whereIn('category_id', $ids_categories)->lists('product_id');
		$products = Product::whereIn('id', $idsproducts)->get();
		return $this->categoriesExperto($ids_categories, $category->id);
	}

	public function categories($id)
	{

		// Feed Select de Productos
		$ref_select = Product::join('references', 'references.id', '=', 'products.reference_id')
				->join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'references.*', 'products_images.url')
				->where('products_categories.category_id', '=', $id)
				->where('references.status', '=', "active")
				->groupBy('products.id')
				->orderBy('products.id')
				->get();

		// Contador de Productos TODOS por Id de Categoria 
		$query_count = Product::join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'products_images.url')
				->where('products_categories.category_id', '=', $id)
				->where('products.status', '=', "active")
				->get();
		
		$ref_count = $query_count->count();

		// Productos TODOS por Id de Categoria
		$products = Product::join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'products.id as pid', 'products_images.url')
				->where('products_categories.category_id', '=', $id)
				->where('products.status', '=', "active")
				->groupBy('products.id')
				->orderBy('products.id')
				->limit(8)
				->get();

		// Banner Publicitario
		$banner = BannerCategory::where('category_id', '=', $id)->first();

		$cat = Category::find($id);
		$parent_cat = $cat->parentCategory ? $cat->parentCategory->name : $cat->name; // current category parent name
		$parent_cat_id = $cat->parentCategory->id; // current category parent id
		$pcat = Category::find($parent_cat_id); // get model by parent id
		$parent_childs = $pcat->categories; // current category bros
		//Migajas de pan
		$category = Category::find($id);
		$categories = Category::getCategoriesTreeParents($category);
		$breadcrumbs = [];
		Category::getBreadcrumbs($categories, $breadcrumbs);

		return view('store.pages.categories', [
			'products' => $products, 
			'cat' => $cat, 
			'parent_cat' => $parent_cat, 
			'parent_childs' => $parent_childs, 
			'ref_count' => $ref_count, 
			'ref_select' => $ref_select, 
			'banner' => $banner, 
			'breadcrumbs' => $breadcrumbs
		]);
	}

	public function loadCategoryInfo($id, $pid = null, $pidc = null)
	{

		$cat = Category::find($id);

		/** Feed Select de Productos * */
		$ref_select = Product::join('references', 'references.id', '=', 'products.reference_id')
				->join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'references.*', 'products.id as pid', 'products_images.url')
				->where('products_categories.category_id', '=', $id)
				->where('references.status', '=', "active")
				->groupBy('products.id')
				->orderBy('products.id')
				->get();

		/** Banner Publicitario * */
		$banner = BannerCategory::where('category_id', '=', $id)->first();

		/** Productos TODOS por Id de Categoria * */
		$products = Product::join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'products.id as pid', 'products_images.url')
				->where('products_categories.category_id', '=', $id)
				->where('products.status', '=', "active")
				->groupBy('products.id')
				->orderBy('products.id')
				// ->skip(4)
				->take(16)
				->get();

		/** Producto * */
		$ref = Product::join('references', 'references.id', '=', 'products.reference_id')
				->join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'references.*', 'products.id as pid', 'products_images.url')
				->where('products_categories.category_id', '=', $id)
				->where('references.status', '=', "active")
				->where('references.id', '=', $pid)
				//->groupBy('references.name')
				->get();

		/** Producto * */
		$ref_color = Product::join('references', 'references.id', '=', 'products.reference_id')
				->join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'products_images.url as purl')
				->where('products_categories.category_id', '=', $id)
				->where('products.status', '=', "active")
				->where('products.id', '=', $pidc)
				//->groupBy('references.id')
				->get();

		return Response::json([
			'cat' => $cat, 
			'ref_select' => $ref_select, 
			'banner' => $banner, 
			'products' => $products, 
			'ref' => $ref, 
			'ref_color' => $ref_color
		]);
	}

	public function color($id)
	{
		$colors = Product::join('references', 'references.id', '=', 'products.reference_id')
			->select('products.color', 'products.id as cid')
			->where('products.reference_id', '=', $id)
			->where('products.color', '<>', '')
			->groupBy('products.color')
			->orderBy('products.color')
			->get();
		return Response::json($colors);
	}

	public function imageByColor($id)
	{
		$imgColor = Product::join('references', 'references.id', '=', 'products.reference_id')
			->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
			->select('products.*', 'references.*', 'products.id as pid', 'products_images.url')
			//->select('products_images.url', 'products.id as pid','products.slug','products.id','products.code')
			->where('products.id', '=', $id)
			->where('references.status', '=', "active")
			//->groupBy('references.name')
			->orderBy('products.id')
			->get();

		return Response::json($imgColor);
	}

	public function prods($id) {
		//print($id);
		$product = Product::join('references', 'references.id', '=', 'products.reference_id')
			->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
			->select('products.*', 'products.id as pid', 'references.*', 'products_images.url')
			->where('products.reference_id', '=', $id)
			->where('references.status', '=', "active")
			//->groupBy('references.name')
			->orderBy('products.id')
			->get();
		return Response::json($product);
	}

	public function moreProds($id)
	{
		$ref_more = Product::join('products_categories', 'products_categories.product_id', '=', 'products.id')
			->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
			->select('products.*', 'products.id as pid', 'products_images.url')
			->where('products_categories.category_id', '=', $id)
			->where('products.status', '=', "active")
			->groupBy('products.id')
			->orderBy('products.id')
			->skip(8)
			->take(1000)
			->get();
		return Response::json($ref_more);
	}

	//Busqueda -----------------------------------------------------------------
	public function search(Request $request)
	{

		$q = $request->get('q');

		$results = Product::search($q)
				->with('reference')
				->limit(8)
				->get();

		// search terms
		if (count($results) > 0) {
			Product::updateTermSeach($q, $results->first()->id);
		}
		
		return view('store.pages.search', [
			'results' => $results,
		])->with('search', $q);
	}

	public function searchMore($q)
	{

		$results = Product::search($q)
				->orderBy('products.id')
				->skip(8)
				->take(1000)
				->get();

		return Response::json($results);
	}

	public function detail(Request $request, $slug, $id)
	{
		$product = Product::where('id', $id)->first();

		//Migajas de pan
		$referer = $request->headers->get('referer');

		$producCategory = ProductCategory::where('product_id', $id)->first();

		$breadcrumbs = [];

		if ($producCategory) {
			$category = Category::find($producCategory->category_id);
			$categories = Category::getCategoriesTreeParents($category);
			Category::getBreadcrumbs($categories, $breadcrumbs);
		}

		if ($referer) {
			preg_match("/\/" . trans('routes.categories') . "\/(\d+)/i", $referer, $matches);
			if (isset($matches[1])) {
				$category = Category::find($matches[1]);
				$categories = Category::getCategoriesTreeParents($category);
				$breadcrumbs = [];
				Category::getBreadcrumbs($categories, $breadcrumbs);
			}
		}

		$formats = $product->getFormats();
		$currentFormat = $product->dimensions;
		$currentColor = $product->color_code;
		$productColors = $product->getCurrentColors($currentFormat)->get();
		$recentProducts = $this->getRecentProducts($request, $product->reference_id);
		$relatedProducts = $product->relatedProducts;

		return view('store.pages.detail', [
			'product' => $product,
			'formats' => $formats,
			'currentFormat' => $currentFormat,
			'recentProducts' => $recentProducts,
			'breadcrumbs' => $breadcrumbs,
			'productColors' => $productColors,
			'currentColor' => $currentColor,
			'relatedProducts' => $relatedProducts
		]);
	}

	public function postColors(Request $request)
	{
		//Colores relacionados a un formato
		$product = Product::findOrFail($request->product);
		$productColors = $product->getCurrentColors($request->format)->get();
		$currentColor = "";
		return view('store.partials.products.formatColors', ['productColors' => $productColors, 'currentColor' => $currentColor]);
	}

	public function postVariation(Request $request)
	{

		$currentFormat = $request->format;
		$currentReference = $request->reference;
		$currentColor = $request->color;

		$product = Product::where('dimensions', '=', $currentFormat)
				->where('reference_id', '=', $currentReference)
				->where('color_code', '=', $currentColor)
				->first();

		$formats = $product->getFormats();
		$productColors = $product->getCurrentColors($currentFormat)->get();
		$relatedProducts = $product->relatedProducts;

		return view('store.partials.products.productVariation', [
			'product' => $product,
			'formats' => $formats,
			'currentFormat' => $currentFormat,
			'productColors' => $productColors,
			'currentColor' => $currentColor,
			'relatedProducts' => $relatedProducts
		]);
	}

	public function getRecentProducts(Request $request, $id)
	{
		
		$viewedProducts = unserialize($request->cookie($this->storeId . 'RecentViewedProducts'));

		if ($viewedProducts == null) {
			$viewedProducts = [];
		}

		$cloneViewedProducts = $viewedProducts;
		if (!in_array($id, $cloneViewedProducts)) {
			if (count($cloneViewedProducts) == 10) {
				array_shift($cloneViewedProducts);
			}
			$cloneViewedProducts[] = $id;
		}

		Cookie::queue($this->storeId . 'RecentViewedProducts', serialize($cloneViewedProducts));
		$recentProducts = Product::whereIn('reference_id', $viewedProducts)
				->groupBy('reference_id')
				->get();

		return $recentProducts;
	}

	//Cotizaciones -------------------------------------------------------------
	public function quotes(Request $request) {
		$userId = $this->userLogin();

		if ($userId != null) {
			//Orden actual
			$currentOrder = QuoteOrder::getCurrentOrder($userId->id);

			//Productos recientes
			$viewedProducts = unserialize($request->cookie($this->storeId . 'RecentViewedProducts'));
			$recentProducts = Product::find($viewedProducts);

			//Productos relacionados
			$relatedProducts = [];
			$orderProducts = $currentOrder->quoteProducts;

			if (count($orderProducts) > 0) {
				foreach ($orderProducts->reverse() as $keyProduct => $product) {
					if (count($relatedProducts) >= 10) {
						break;
					} else {
						foreach ($product->relatedProducts as $related) {
							if (count($relatedProducts) < 10) {
								if (!array_key_exists($related->id, $relatedProducts)) {
									$relatedProducts[$related->id] = $related;
								}
							} else {
								break;
							}
						}
					}
				}
			}

			return view('store.pages.quotes', compact('recentProducts', 'relatedProducts', 'currentOrder'));
		} else {
			return redirect('/');
		}
	}

	public function postSaveQuote(Request $request) {
		$success = 0;
		$customer = $this->userLogin();

		if ($customer != NULL) {
			$userId = $customer->id;
			$product = $request->productId;
			$quoteOrder = QuoteOrder::firstOrNew(['customer_id' => $userId, 'status' => QuoteOrder::STATUS_OPEN]);

			try {
				DB::beginTransaction();
				if (!$quoteOrder->exists) {
					$quoteOrder->date_created = Carbon::now();
					$quoteOrder->status = QuoteOrder::STATUS_OPEN;
					$quoteOrder->save();
				}
				QuoteProduct::firstOrCreate(['quote_order_id' => $quoteOrder->id, 'product_id' => $product]);
				DB::commit();

				$success = 1;
			} catch (\Exception $e) {
				Log::error($e->getMessage());
				DB::rollBack();
			}
		}

		return $success;
	}

	public function postDeleteQuote(Request $request) {
		$success = -1;

		if ($request->productId != NULL) {
			$currentOrder = QuoteOrder::getCurrentOrder($this->userLogin()->id);
			try {
				DB::beginTransaction();
				QuoteProduct::where(['quote_order_id' => $currentOrder->id, 'product_id' => $request->productId])->delete();
				DB::commit();

				$success = count($currentOrder->quoteProducts);
			} catch (\Exception $e) {
				Log::error($e->getMessage());
				DB::rollBack();
			}
		}

		return $success;
	}

	public function getSendEmailQuote(Request $request) {
		if ($this->isLogin()) {
			$user = $this->userLogin();
			$currentOrder = QuoteOrder::getCurrentOrder($user->id);
			$city = City::find($user->city);
			//Alfa
			//servicio al cliente
			$settingAlfa = Setting::where(['name' => Setting::ALFA_EMAIL_QUOTES])->first();
			if(!empty($settingAlfa)){
				$users = User::all()->where('role',$settingAlfa->value);
				if(!empty($users)){
					foreach ($users as $userAlfa) {
						Mail::send('store.emails.quoteAlfa', ['user' => $userAlfa, 'currentOrder' => $currentOrder, 'user' => $user, 'city' => $city], function ($m) use ($userAlfa) {
							$m->to($userAlfa->email, $userAlfa->name)->subject(trans('front.subject_email_quotes_user'));
						});
					}
				}
			}
			//try{
			//Usuario
			Mail::send('store.emails.quoteUser', ['user' => $user, 'currentOrder' => $currentOrder], function ($m) use ($user) {
				$m->to($user->email, $user->name)->subject(trans('front.subject_email_quotes_user'));
			});
			/* }catch(\Exception $e){
			  Log::error($e->getMessage());
			  }

			  exit(); */
			
			$currentOrder->status = QuoteOrder::STATUS_NEW;
			$currentOrder->date_closed = Carbon::now();
			$currentOrder->save();
		}

		$request->session()->flash('success', trans('front.email_quote_sent'));
		return redirect('/');
	}

	//Subhome-------------------------------------------------------------------
	public function subhome($id) {
		$items = Category::where('parent', $id)->get();
		$columnas = 3;

		if (count($items) > 0 && count($items[0]->categories) > 0) {
			$columnas = 4;
		}

		//Migajas de pan
		$category = Category::find($id);
		$categories = Category::getCategoriesTreeParents($category);
		$breadcrumbs = [];
		Category::getBreadcrumbs($categories, $breadcrumbs);

		//Banners Promo
		$bannersPromoSubhomeFooter = [];
		$bannersSubhomeFooter = BannerSite::where(['location' => BannerSite::LOCATION_SUBHOME])->get();

		//SUBHOME FOOTER

		foreach ($bannersSubhomeFooter as $banner) {
			$bannersPromoSubhomeFooter[$banner->position] = $banner;
		}
		return view('store.pages.subhome', ['category' => $category, 'items' => $items, 'breadcrumbs' => $breadcrumbs, 'bannersPromoSubhomeFooter' => $bannersPromoSubhomeFooter, 'columnas' => $columnas]);
	}

	public function categoriesExperto($ids_categories, $id) {

		//dd($ids_categories);
		$ref_select = Product::join('references', 'references.id', '=', 'products.reference_id')
				->join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'references.*', 'products_images.url')
				// ->where('products_categories.category_id', '=', $id)
				->whereIn('products_categories.category_id', $ids_categories)
				->where('references.status', '=', "active")
				->groupBy('references.id')
				->orderBy('references.id')
				->get();

		/** Contador de Productos TODOS por Id de Categoria * */
		$ref_count = Product::join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'products_images.url')
				// ->where('products_categories.category_id', '=', $id)
				->whereIn('products_categories.category_id', $ids_categories)
				->where('products.status', '=', "active")
				->count('products.id');

		/** 4 primeros "destacados" * */
		$products = Product::join('products_categories', 'products_categories.product_id', '=', 'products.id')
				->leftJoin('products_images', 'products_images.product_id', '=', 'products.id')
				->select('products.*', 'products.id as pid', 'products_images.url')
				// ->where('products_categories.category_id', '=', $id)
				->whereIn('products_categories.category_id', $ids_categories)
				->where('products.status', '=', "active")
				->groupBy('products.reference_id')
				->orderBy('products.id')
				->limit(8)
				->get();


		/** Banner Publicitario * */
		$banner = BannerCategory::where('category_id', '=', $id)->first();

		$cat = Category::find($id);
		$parent_cat = $cat->parentCategory ? $cat->parentCategory->name : $cat->name; // current category parent name
		$parent_cat_id = $cat->parentCategory ? $cat->parentCategory->id : $cat->id; // current category parent id
		$pcat = Category::find($parent_cat_id); // get model by parent id
		$parent_childs = $cat->categories()->get()->prepend($cat); // current category bros
		//Migajas de pan

		$categories = $cat->categories;

		$breadcrumbs = [];
		Category::getBreadcrumbs($cat, $breadcrumbs);

		return view('store.pages.categories', ['products' => $products, 'cat' => $cat, 'parent_cat' => $parent_cat, 'parent_childs' => $parent_childs, 'ref_count' => $ref_count, 'ref_select' => $ref_select, 'banner' => $banner, 'breadcrumbs' => $breadcrumbs]);
	}

}
