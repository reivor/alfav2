<?php

namespace App\Http\Controllers\Store;

use App\Models\BannerSite;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\City;
use App\Models\Customer;
use App\Models\NewsLetter;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use DB;
use Session;
use Mail;

class UsersController extends FrontController {

	public function registry(Request $request)
	{
		// keep track of where the user came from
		$referer = $request->headers->get('referer');
		$request->session()->set('ref', $referer);
		//Banner Registro
		$bannerRegistro = BannerSite::where(['location'=>BannerSite::LOCATION_REGISTER])->first();
		$bannerMobile = BannerSite::where(['location'=>BannerSite::LOCATION_HOME_TOP])->first();

		$cities = City::get();

		return view('store.pages.registry',['bannerRegistro' => $bannerRegistro, 'bannerMobile' => $bannerMobile, 'cities' => $cities]);
	}
	
	public function postCreate(Request $request)
	{

		$message = trans('front.message_register_user');
		$success = "success";
		$redirect = url("/");
		
		$existEmail = Customer::where('email', '=' , $request->input('email'))->first();
		
		if( !$existEmail ) {
			
			try {
				
				DB::beginTransaction();
				$user = $this->createUserFromRequest($request);
				$user->save();
				session(['Customer' => $user->id]);
				DB::commit();	

				$this->sendWelcomeNewsletter($user, $request->input('subscribe')==1);
				
				if( $request->session()->has('ref') ) {
					$redirect = $request->session()->get('ref');
					$request->session()->remove('ref');
				}
			}
			catch(\Exception $e) {
				Log::error($e->getMessage());
				DB::rollBack();
				$message = trans('front.error_message_register_user');
				$redirect = url(trans('routes.register'));
				$success = "error";
			}
		}
		else {
			$message = trans('front.error_message_existing_user');
			$redirect = url(trans('routes.register'));
			$success = "error";
		}
		
		return redirect($redirect)
			->with($success, $message);
	}
	
	private function createUserFromRequest(Request $request)
	{
		$user = new Customer();
		$user->name = $request->input('name');
		$user->lastname = $request->input('lastName');
		$user->email = $request->input('email');
		$user->job = $request->input('job');
		$user->password = bcrypt($request->input('password'));
		$user->role = Customer::ROLE_GUEST;
		$user->status = Customer::STATUS_ACTIVE;
		$user->fb_id = $request->input('fb_id');
		$user->last_login = Carbon::now();
		$user->avatar = Customer::DEFAULT_PICTURE;
		$user->subscribe = $request->input('subscribe', false);
		$user->registered_store = config('store.id');
		$user->city = $request->input('city');
		$user->residence = $request->input('residence');
		$user->identification = $request->input('identification');
		$user->cell_phone = $request->input('cell_phone');
		return $user;	
	}
	
	private function sendWelcomeNewsletter($user, $subscribe = false)
	{
		Mail::send('store.emails.welcome', ['user' => $user], function($mail) use ($user) {
			$mail->to($user->email, $user->name)
				->subject('¡Bienvenido creador!');
		});

		if($subscribe) {
			NewsLetter::firstOrCreate([
				'name' => $user->fullName,
				'email' => $user->email
			]);
		}
		
	}

	public function postLogin(Request $request)
	{

		$email = $request->input('email');
		$password = $request->input('password');
		$user = Customer::where('email','=',$email)->first();

		if( $user ) {
			
			$find_user = Customer::login($password, $user->password);
			
			if($find_user) {
				
				session(['Customer' => $user->id]);
				
				$redirect = "/";
				if( $request->session()->has('ref') ) {
					$redirect = $request->session()->get('ref');
					$request->session()->remove('ref');
				}
				
				return response()->json([
					'result' => 'true',
					'message' => "",
					'redirect' => $redirect,
				]);
			}
			else{
				return response()->json([
					'result' => 'false',
					'message' => trans('front.invalid_data_user_login')
				]);
			}
		}
		else {
			return response()->json([
				'result' => 'false',
				'message' => trans('front.no_registered_user')
			]);
		}

	}

	public function postUserfacebook(Request $request)
	{
		
		$useridfacebook = $request->input('id');
		
		if( !$useridfacebook ) {
			return response()->json(['result'=>'false']);
		}
		
		$user = Customer::where('fb_id','=',$useridfacebook)->first();
		
		$redirect = "/";
		if( $request->session()->has('ref') ) {
			$redirect = $request->session()->get('ref');
			$request->session()->remove('ref');
		}
		
		if($user) {
			//Si existe logear
			session(['Customer' => $user->id]);
			return response()->json([
				'result'=>'ok',
				'redirect' => $redirect
			]);
		}
		else {
			$user = Customer::where('email','=',$request->input('email'))->first();
			//return response()->json(['result'=>'usuario logeado']);
			if($user){
				//si existe correo se asocia fb_id
				$user->fb_id = $request->input('id');
				$user->save();
				session(['Customer' => $user->id]);
				return response()->json([
					'result' => 'ok',
					'redirect' => $redirect
				]);
			}
			else {
				return response()->json(['result'=>'false']);
			}
		}
		
	}

	public function logout()
	{
		session::forget('Customer');
		return redirect('/');
	}

	public function recoveryPassword(Request $request)
	{
		$email = $request->input('email');
		$user = Customer::recoveryUser($email);

		if($user){
			$recovery_token = sha1(str_random(50)); ;
			$user->recovery_token = $recovery_token;
			$user->date_recovery = date("Y-m-d H:i:s");
			$user->save();

			Mail::send('store.emails.prueba', ['user' => $user], function ($m) use ($user) {
				$m->to($user->email, $user->name)->subject('Your Reminder!');
			});

			return response()->json(['result'=>'true','message'=>""]);
		}
		else{
			return response()->json(['result'=>'false','message'=>trans('front.no_registered_user')]);
		}
	}

	public function viewPasswordReset($id,$recovery)
	{

		$user = Customer::find($id);
		
		$_token = $recovery;
		$datetime1 = $user->date_recovery;
		$datetime2 = date("Y-m-d H:i:s");
		$_datetime1 = New \DateTime($datetime1);  
		$_datetime2 = New \DateTime($datetime2);
		$diference_time = $_datetime1->diff($_datetime2);
		$interval = $diference_time->format('%h');


		if($_token == $user->recovery_token){
			if($interval < (5*24)){
				$user->recovery_token = " ";
				$user->save();
				return view('store.pages.resetpassword',['user_id'=>$id]);
			}
			else{
				return redirect('/');
			}
		}
		else{
			return redirect('/');
		}

	}

	public function getChangepass()
	{
		return view('store.pages.resetpassword',['user_id'=>session('Customer')]);
	}

	public function passwordReset(Request $request)
	{
		$user = Customer::find($request->input('user_id'));		
		$user->password = bcrypt($request->input('password'));
		$user->save();
		return redirect('/');
	}

	public function getCheckEmail(Request $request)
	{
		$email = $request->input('email');
		$user = Customer::where(['email' => $email])->first();
		$response = ['valid' => $user ? false : true];
		return response()->json($response);
	}

	public function postNewsletters(Request $request)
	{
		$email = $request->input('email');
		$name = $request->input('name');
		
		$newsletterUser = NewsLetter::where('email','=',$email)->first();
		
		if(!$newsletterUser){
			$newsletter = new NewsLetter();
			$newsletter->name = $name;
			$newsletter->email = $email;
			$newsletter->save();
			
			// send newsletter notification
			$user = new Customer();
			$user->name = $name;
			$user->email = $email;
			$this->sendWelcomeNewsletter($user, true);
			return response()->json(['result'=>trans('front.mss_newsletter')]);
		}
		else{
			return response()->json(['result'=>trans('front.mss_newsletter_error')]);
		}
	}
	
}
