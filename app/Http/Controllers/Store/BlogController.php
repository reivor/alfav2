<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Models\BlogArticle;
use App\Models\BlogComment;
use App\Models\Customer;
use App\Models\BannerSite;
use Carbon\Carbon;

class BlogController extends FrontController{
    
	public function getIndex()
	{
		
		$numArticles = 6;
		
		$articles = BlogArticle::getRecentArticles()
				->skip(1)
				->take($numArticles)
				->get();
		
		$featured = BlogArticle::getRecentArticles()->first();
		
		//$lang = config('store.id');

		$totalPages = BlogArticle::getCountArticlePages($numArticles);
		
		return view('store.pages.blog', [
			'featured' => $featured,
			'articles' => $articles,
			'totalPages' => $totalPages,
			'locate' => config('store.id'),
		]);
	}
	
	public function getArticle($category, $slug, $id)
	{
		$article = BlogArticle::find($id);
		
		$relatedArticles = BlogArticle::getRelatedArticles($article)
			->take(5)
			->get();
		
		$comments = $article->getRootComments()
			->get();
		
		$customer = $this->userLogin() ? $this->userLogin() : new Customer();
		
		//BANNERS
		$bannersPromoHomeFooter = BannerSite::getBannersHomeFooter();

		return view('store.pages.article', [
			'article' => $article,
			'relatedArticles' => $relatedArticles,
			'comments' => $comments,
			'customer' => $customer,
			'bannersPromoHomeFooter' => $bannersPromoHomeFooter,
			'locate' => config('store.id'),
		]);
	}
	
	public function getArticles(Request $request)
	{
		$page = intval($request->input('page', 1)) - 1;
		
		$numArticles = 6;
		
		$articles = BlogArticle::getRecentArticles()
				->skip($page * $numArticles)
				->take($numArticles)
				->get();
		
		return response()->json(['articles' => $articles]);
	}
	
	public function postComment(Request $request)
	{
		$comment = new BlogComment();
		$comment->comment = $request->input('text');
		$comment->customer_id = $request->input('customer_id');
		$comment->blog_comment_id = $request->input('comment_id') ? $request->input('comment_id') : null;
		$comment->blog_article_id = $request->input('article_id');
		$comment->status = BlogComment::STATUS_ACTIVE;
		$comment->save();
		
		return response()->json([
			'success' => 'ok',
			'comment' => $comment,
		]);
	}
	
}
