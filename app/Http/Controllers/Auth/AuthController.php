<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;    
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Contracts\Auth\Guard;
use App\Services\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Carbon\Carbon;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;
        $this->middleware('guest', ['except' => 'getLogout']);
    }
	
	public function getLogin()
    {
        return view('auth.pages.login');
    }

    public function postLogin(Request $request)
    {
        $credentials = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
			'status'    => User::STATUS_ACTIVE 
        ];
        
        $remember = $request->input('remember', false);
		
        if( Auth::attempt($credentials, $remember) ) {
            Auth::user()->last_login = Carbon::now();
            Auth::user()->save();
            return $this->redirectAfterLogin();
        }
        else {
            return redirect(route('auth/login'))
                ->withInput()
                ->with('error', trans('auth.failed_login'));
        }
    }
	
	public function getRegister()
    {
        return view('auth.pages.register');
    }

    public function postRegister(Request $request)
    {
        $user = new User();

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->status = User::STATUS_PENDING;
        $user->role = User::ROLE_ADMIN;
        $user->avatar = User::DEFAULT_PICTURE;
        $user->save();

        return redirect(route('auth/login'))
			->with('success', trans('auth.account_created'));
    }

    /**
     * Redirects the user after successful login.
     * 
     * @return Response
     */
    private function redirectAfterLogin() 
    {    
        $user = Auth::user();
        
        switch($user->role) {
            case User::ROLE_ADMIN:
                return redirect()
                    ->route('admin/dashboard');
            case User::ROLE_GUEST:
                return redirect()
                    ->route('auth/login');
            case User::ROLE_SERVICIOCLIENTE:
                return redirect()
                    ->route('admin/quotes');
        }
		
		return redirect("/");
        
    }
	
    public function getLogout()
	{
        Auth::logout();
        return redirect(route('auth/login'));
    }
	
	public function getForgot()
	{
		return view('auth.pages.forgot');
	}
	
	public function postForgot(Request $request)
	{
		
	}

    public function getActivate(Request $request, $token)
    {
		$email = $request->input('email');
		$user = User::where(['email' => $email])->first();
		
		try {
			
			if( !$user ) {
				throw new \Exception( trans('auth.message_user_not_found') );
			}
			
			if( $user->isActive() ) {
				throw new \Exception( trans('auth.message_user_account_active') );
			}
			
			if( $user->activation_token !== $token ) {
				throw new \Exception( trans('auth.message_invalid_activation_token') );
			}
			
			return view('auth.pages.activate', ['user' => $user]);
			
		}
		catch( \Exception $e ) {
			return redirect( route('auth/login') )
				->with('error', $e->getMessage());
		}
		
    }

    public function postActivate(Request $request)
    {
        // update user
        $uid = $request->input('uid');
        $user = User::find($uid);
        $user->password = bcrypt($request->input('password'));
        $user->activation_token = null;
        $user->status = 'active';
        $user->save();
        
        // authenticate
        Auth::loginUsingId($uid);
		return $this->redirectAfterLogin();
    }
	
	/**
	 * Checks whether an email is already taken
	 * 
	 * @return \Illuminate\Http\Response json
	 */
	public function getCheckEmail(Request $request)
	{
		$email = $request->input('email');
		$user = User::where(['email' => $email])->first();
		$response = ['valid' => $user ? false : true];
		return response()->json($response);
	}
	
}
