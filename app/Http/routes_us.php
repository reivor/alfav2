<?php

//Route::get('categories/{id}', 'ProductsController@categories');
Route::get('categories/{slug}/{id}', 'ProductsController@selectSite');
Route::get('products/{slug}/{id}', 'ProductsController@detail');
Route::get('search', 'ProductsController@search');
Route::get('quotes', 'ProductsController@quotes');

Route::controller('products', 'ProductsController');
Route::controller('stores', 'StoresController');

/* Pages */
Route::get('about-us', 'FrontController@aboutUs');
Route::get('environmental-alfa', 'FrontController@alfaAmbiental');
Route::get('work-with-us', 'FrontController@trabajos');
Route::get('financing', 'FrontController@financiacion');
Route::get('installation', 'FrontController@instalacion');
Route::get('design', 'FrontController@diseno');
Route::get('catalogs', 'FrontController@catalogos');
Route::get('certifications', 'FrontController@certificaciones');

Route::get('client-info', 'FrontController@clientInfo');
Route::get('client-info/{type}', 'FrontController@clientInfoProduct');
Route::get('client-info/{type}/{category}', 'FrontController@clientInfoProduct');
Route::get('client-info/{type}/{category}/{product}', 'FrontController@clientInfoProduct');
Route::get('clientInfoByCatId/{id}', 'FrontController@clientInfoByCatId');
Route::get('clientInfoProd/{id}', 'FrontController@clientInfoProd');

// Users
Route::get('register', 'UsersController@registry');
Route::get('logout', 'UsersController@logout');
Route::get('change-password', 'UsersController@getChangepass');