<?php
namespace App\Http\Middleware;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

use Closure;

class AclMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$user = Auth::user();

        if($user != null && $user->role == User::ROLE_SERVICIOCLIENTE)
            if($request->is('logout') || $request->is('admin/profile') || $request->is('admin/profile/*'))
                return $next($request);
			else if( !$request->is('admin/quotes/*') && $request->path() != 'admin/quotes') 
				return redirect()
                    ->route('admin/quotes');
		
        return $next($request);
    }

}
