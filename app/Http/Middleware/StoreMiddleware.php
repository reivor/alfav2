<?php

namespace App\Http\Middleware;

use Closure;

class StoreMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // set store locale
		$value = config('store.id');
        config(['database.default' => $value]);
		
		// set admin locale
		if( $request->is('admin/*') ) {
			$default = config('store.id');
			$value = $request->cookie('cms_country');
			if( !$value ) {
				$value = $default;
			}
			config(['database.default' => $value]);
			view()->share(['storeCountry' => $value]);
		}
		
        return $next($request);
    }

}
