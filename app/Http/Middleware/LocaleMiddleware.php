<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		
		// set store settings
		$this->checkStoreConfig($request);
		
		// set locale
		$this->setLocale($request);
		if( !$request->is('admin/*') ) {
			//set cookie country front
			if( !$request->cookie('country') ) {
				Cookie::queue('country', config('store.id'));
			}
		}
		
        return $next($request);
    }
	
	private function checkStoreConfig($request)
	{
		
		switch($_SERVER['HTTP_HOST']) {
			case env('CO_STORE_URL'):
				config([
					'store.id' => env('CO_STORE_ID', 'co'),
					'store.locale' => env('CO_STORE_LOCALE', 'es'),
					'store.timezone' => env('CO_STORE_TIMEZONE', 'America/Bogota')
				]);
				break;
			case env('EC_STORE_URL'):
				config([
					'store.id' => env('EC_STORE_ID', 'ec'),
					'store.locale' => env('EC_STORE_LOCALE', 'es'),
					'store.timezone' => env('EC_STORE_TIMEZONE', 'America/Bogota')
				]);
				break;
			case env('US_STORE_URL'):
				config([
					'store.id' => env('US_STORE_ID', 'us'),
					'store.locale' => env('US_STORE_LOCALE', 'en'),
					'store.timezone' => env('US_STORE_TIMEZONE', 'America/Bogota')
				]);
				break;
			default:
				config([
					'store.id' => env('CO_STORE_ID', 'co'),
					'store.locale' => env('CO_STORE_LOCALE', 'es'),
					'store.timezone' => env('CO_STORE_TIMEZONE', 'America/Bogota')
				]);
		}
		
	}
	
	private function setLocale($request)
	{
		// check if the lang cookie is present
		$fallback = config('store.locale');
		
		//$lang = $request->cookie('lang');
		$lang = config('store.id');
		
		if( !$lang ) {
			$lang = $fallback;
		}
		
		switch( $lang ) {
			case 'co':
			case 'ec':
				$locale = 'es';
				$localeCarbon = 'es';
				break;
			case 'us':
				$locale = 'en';
				$localeCarbon = 'en';
				break;
			default:
				$locale = 'es';
				$localeCarbon = 'es';
				break;
		}
		
		config(['app.locale' => $locale]);		// app config
		App::setLocale($locale);
		setLocale(LC_TIME, $locale);			// php locale
		Carbon::setLocale($localeCarbon);		// Carbon locale
		view()->share(['storeLang' => $lang]);	// view var
	}
	
}
