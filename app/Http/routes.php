<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::group(['namespace' => 'Store', 'middleware' => ['web', 'locale', 'store']], function() {

	Route::get('/', 'FrontController@home');


	/* Products */

	Route::get('subhome/{id}', 'ProductsController@subhome');


	/* Rutas reales slug */

	Route::get('detail/{id}', 'ProductsController@detail');
	Route::get('color/{id}', 'ProductsController@color');
	Route::get('prods/{id}', 'ProductsController@prods');
	Route::get('moreProds/{id}', 'ProductsController@moreProds');
	Route::get('searchMore/{q}', 'ProductsController@searchMore');

	Route::get('loadCategoryInfo/{id}/{pid?}/{pidc?}', 'ProductsController@loadCategoryInfo');


	Route::get('imageByColor/{id}', 'ProductsController@imageByColor');
	//Route::controller('products', 'ProductsController');
	//Route::controller('stores', 'StoresController');

	/* Users */
	Route::controller('users', 'UsersController');
	Route::post('recoverypassword', 'UsersController@recoveryPassword');
	Route::get('/passwordReset/{id}/{recovery}', 'UsersController@viewPasswordReset');
	Route::post('/passwordReset', 'UsersController@passwordReset');
	Route::post('/newsletter', 'UsersController@postNewsletters');

	/* Blog */
	Route::post('blog/comments/save', 'BlogController@postComment');
	Route::get('blog/{category}/{slug}/{id}', 'BlogController@getArticle');
	Route::controller('blog', 'BlogController');
	
	Route::get('loadMore/{page}', 'FrontController@loadMore');
	Route::get('loadMore/{page}/{category}', 'FrontController@loadMore');
/*pages  store */
//--Route::get('/{url}', 'PageController@getPage'); //---RB ***************************
});

/*
 * AUTH
 */

Route::group(['namespace' => 'Auth', 'middleware' => ['web']], function () {

	Route::get('/login', ['uses' => 'AuthController@getLogin', 'as' => 'auth/login']);

	Route::post('/login', 'AuthController@postLogin');

	Route::get('/register', ['uses' => 'AuthController@getRegister', 'as' => 'auth/register']);

	Route::post('/register', 'AuthController@postRegister');

	Route::get('/logout', ['uses' => 'AuthController@getLogout', 'as' => 'auth/logout']);

	Route::get('/forgot', ['uses' => 'AuthController@getForgot', 'as' => 'auth/forgot']);

	Route::post('/forgot', ['uses' => 'PasswordController@postEmail']);

	Route::get('/password/reset/{token}', ['uses' => 'PasswordController@getReset', 'as' => 'auth/reset']);

	Route::post('/password/reset/{token}', ['uses' => 'PasswordController@postReset']);

	Route::get('/activate/account/{token}', ['uses' => 'AuthController@getActivate', 'as' => 'auth/activate']);

	Route::post('/activate/account/{token}', ['uses' => 'AuthController@postActivate']);

	Route::get('/check/email', ['uses' => 'AuthController@getCheckEmail']);
});

//Route::auth();

/*
 *
 */
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['web', 'auth', 'store', 'acl']], function() {

	Route::get('/', function() {
		return redirect(route('admin/dashboard'));
	});

	Route::get('/action/country/{id}', ['uses' => 'AdminController@setCountry']);
	Route::get('/action/lang/{id}', ['uses' => 'AdminController@setLang']);

	Route::get('/dashboard', ['uses' => 'HomeController@dashboard', 'as' => 'admin/dashboard']);

	Route::group(['namespace' => 'Catalog', 'prefix' => 'catalog'], function() {

		/* Categories */
		Route::controller('categories', 'CategoriesController');

		/* Products */

		/*
		  Route::get('products/edit/{id}', 'ProductsController@edit');
		  Route::get('products/show/{id}', 'ProductsController@show');
		  Route::post('products/update/{id}', 'ProductsController@update');
		  Route::get('products/destroy/{id}', 'ProductsController@destroy');
		  Route::post('products/store', 'ProductsController@store');
		  Route::get('products/upload', 'ProductsController@getUpload');
		  Route::get('products/uploadlog', 'ProductsController@getUploadLog');
		  Route::get('products/tags', 'ProductsController@getTags');
		  Route::get('products/productbytags/{tag}', 'ProductsController@productbytags');
		  Route::get('products/borrartags/{tag}', 'ProductsController@borrartags');
		  Route::resource('products', 'ProductsController');
		 */

		Route::get('/check/code/{id?}', ['uses' => 'ProductsController@checkCode']);
		Route::get('products/uploadlog', 'ProductsController@getUploadLog');
		Route::controller('products', 'ProductsController');

		/* Products Images */
		Route::get('productsImages/destroy/{id}', 'ProductsImagesController@destroy');
		Route::controller('productsImages', 'ProductsImagesController');

		/* References */
		Route::get('references', 'ReferencesController@index');
		Route::get('references/edit/{id}', 'ReferencesController@edit');
		Route::get('references/create', 'ReferencesController@create');
		Route::get('references/show/{id}', 'ReferencesController@show');
		Route::post('references/update/{id}', 'ReferencesController@update');
		Route::get('references/destroy/{id}', 'ReferencesController@destroy');
		Route::post('references/store', 'ReferencesController@store');
		Route::get('references/deleteDataSheet/{id}', 'ReferencesController@deleteDataSheet');
		Route::get('references/deleteuManual/{id}', 'ReferencesController@deleteuManual');
		Route::get('references/deletemManual/{id}', 'ReferencesController@deletemManual');
		Route::get('references/deleteGuarantee/{id}', 'ReferencesController@deleteGuarantee');
		Route::get('references/category', 'ReferencesController@category');

		/* Stores */
		Route::controller('stores', 'StoresController');
	});

	Route::group(['namespace' => 'Appearance', 'prefix' => 'appearance'], function() {
		/* Menu */
		Route::get('menu/categories/{id}', 'MenuController@categories');
		//Route::resource('menu', 'MenuController');
		Route::controller('menu', 'MenuController');

		Route::controller('slider', 'SliderController');
		Route::controller('footer', 'FooterController');

		/* Carrusel */
		Route::resource('carrusel', 'CarruselController');
		Route::get('carrusel/edit/{id}', 'CarruselController@edit');
		Route::post('carrusel/update/{id}', 'CarruselController@update');
		Route::get('carrusel/destroy/{id}', 'CarruselController@destroy');
		Route::get('carrusel/change-status/{id}/{status}', 'CarruselController@changeStatus');
		Route::post('carrusel/store', 'CarruselController@store');

		/* Banners home */
		Route::controller('banners_home', 'BannersHomeController');

		/* Banners site */
		Route::controller('banners_site', 'BannersSiteController');
	});

	/* USERS */
	Route::get('profile', 'UsersController@getProfile');
	Route::get('profile/edit', 'UsersController@getEditProfile');
	Route::post('profile/edit', 'UsersController@postEditProfile');
	Route::get('profile/change_password', 'UsersController@getChangePassword');
	Route::post('profile/change_password', 'UsersController@postChangePassword');
	//Route::post('/users/create', 'UsersController@postCreate');
	Route::controller('users', 'UsersController');

	/* CUSTOMERS */
	Route::get('customers', 'CustomersController@index');
	Route::get('customers/show/{id}', 'CustomersController@show');
	Route::get('customers/subscribers', 'CustomersController@subscribers');
	Route::get('customers/unsubscribe/{id}', 'CustomersController@unsubscribe');

	/* COUNTRIES */
	Route::controller('countries', 'CountriesController');

	/* DEPARTMENTS */
	Route::controller('departments', 'DepartmentsController');

	/* CITIES */
	Route::controller('cities', 'CitiesController');

	/* SERVICES */
	Route::controller('services', 'ServicesController');

	/* QUOTES */
	Route::get('quotes', 'QuotesController@index');
	Route::get('quotes/show/{id}', 'QuotesController@show');
	Route::get('quotes/edit/{id}', 'QuotesController@edit');
	Route::get('quotes/setManager', 'QuotesController@setManager');
	Route::get('quotes/setRoom', 'QuotesController@setRoom');
	Route::get('quotes/setStatus', 'QuotesController@setStatus');
	Route::get('quotes/setComment', 'QuotesController@setComment');
	Route::get('quotes/downloadQuotes', 'QuotesController@downloadQuotes');
	Route::controller('quotes', 'QuotesController');
	Route::get('/quotes', ['uses' => 'QuotesController@index', 'as' => 'admin/quotes']);

	/* SETTINGNS */
	Route::controller('settings', 'SettingsController');

	/* BLOGS */
	Route::controller('blog', 'BlogController');

/*pages---*/	
Route::controller('page', 'PageController'); //*

	/*SEO*/
	Route::controller('seo','SeoController');
});

/*
 * 	FRONT
 */
Route::group(['namespace' => 'Store', 'middleware' => ['web']], function() {
	Route::get('/customer/email', ['uses' => 'UsersController@getCheckEmail']);


});


Route::group(['namespace' => 'Store', 'middleware' => ['web', 'locale', 'store']], function() {

Route::get('/{url}', 'PageController@getPage'); //---RB ***************************

});