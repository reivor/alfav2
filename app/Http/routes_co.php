<?php

	
//Route::get('categories/{id}', 'ProductsController@categories');
Route::get('categorias/{slug}/{id}', 'ProductsController@selectSite');
Route::get('productos/{slug}/{id}', 'ProductsController@detail');
Route::get('buscar', 'ProductsController@search');
Route::get('cotizaciones', 'ProductsController@quotes');

Route::controller('products', 'ProductsController');
Route::controller('tiendas', 'StoresController');

// Pages
Route::get('quienes-somos', 'FrontController@aboutUs');
Route::get('alfa-ambiental', 'FrontController@alfaAmbiental');
Route::get('trabaja-con-nosotros', 'FrontController@trabajos');
Route::get('financiacion', 'FrontController@financiacion');
Route::get('instalacion', 'FrontController@instalacion');
Route::get('diseno', 'FrontController@diseno');
Route::get('catalogos', 'FrontController@catalogos');
Route::get('certificaciones', 'FrontController@certificaciones');
Route::get('informacion', 'FrontController@clientInfo');
Route::get('informacion/{type}', 'FrontController@clientInfoProduct');
Route::get('informacion/{type}/{category}', 'FrontController@clientInfoProduct');
Route::get('informacion/{type}/{category}/{product}', 'FrontController@clientInfoProduct');
Route::get('clientInfoByCatId/{id}', 'FrontController@clientInfoByCatId');
Route::get('clientInfoProd/{id}', 'FrontController@clientInfoProd');

// Users
Route::get('registro', 'UsersController@registry');
Route::get('salir', 'UsersController@logout');
Route::get('cambiar-contrasena', 'UsersController@getChangepass');
