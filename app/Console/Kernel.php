<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		Commands\GetProductsSearch::class,
		Commands\ScanProductImages::class,
		Commands\SearchIndexCommand::class,
		Commands\SiteMapCategory::class,
		Commands\SiteMapProducts::class,
		Commands\ExecuteSiteMaps::class,
		Commands\ParseExcelProducts::class,
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		// $schedule->command('inspire')
		//          ->hourly();
	}

}
