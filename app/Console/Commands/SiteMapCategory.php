<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Category;

class SiteMapCategory extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $name='sitemap:category';
    protected $signature = 'sitemap:category {country}';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Command description';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        config(['database.default' => $this->argument('country')]);
        switch ($this->argument('country')) {
            case 'co':
				$store_url = "http://".env("CO_STORE_URL");
				break;
            case 'ec':
				$store_url = "http://".env("EC_STORE_URL");
				break;
            case 'us':
				$store_url = "http://".env("US_STORE_URL");
				break;
        }

        $offset = 0;
        $xml = new \DomDocument('1.0', 'UTF-8');
        $attributeurlset = $xml->createAttribute('xmlns');
        $attributeurlset->value='http://www.sitemaps.org/schemas/sitemap/0.9';
        $raiz = $xml->createElement('urlset');
        $raiz->appendChild($attributeurlset);
        $raiz = $xml->appendChild($raiz);
		
        while(true) {

            $categories = Category::whereIn('id', function($query) {
					$query->selectRaw('category_id')
						->from('sitemap_categories');
				})
				->limit(100)
				->offset($offset)
				->get();

            if($categories->isEmpty()){
				break;
			}
			
            foreach ($categories as $key => $category) {
                $url = $xml->createElement('url');
                $url = $raiz->appendChild($url);
                $loc = $xml->createElement('loc',$store_url.$category->getDetailUrlAttribute());
                $loc = $url->appendChild($loc);
            }
			
            $offset=$offset+100;
        }
		
        $xml->formatOutput = true;
        $xml->saveXML();
        $xml->save(public_path('sitemap_'.$this->argument('country').'_categories.xml'));

        print('XML creado');

    }
}
