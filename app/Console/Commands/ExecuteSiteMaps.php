<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExecuteSiteMaps extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $name='sitemap:execute_all';
    protected $signature = 'sitemap:execute_all {country}';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Command description';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $this->call('sitemap:category', [
            'country' => $this->argument('country')
        ]);
        $this->call('sitemap:products', [
            'country' => $this->argument('country')
        ]);
    }
}
