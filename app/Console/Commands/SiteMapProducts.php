<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use App\Models\Category;

class SiteMapProducts extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $name = 'sitemap:products';
	protected $signature = 'sitemap:products {country}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		config(['database.default' => $this->argument('country')]);
		switch( $this->argument('country') ) {
			case 'co':
				$store_url = "http://" . env("CO_STORE_URL");
				break;

			case 'ec':
				$store_url = "http://" . env("EC_STORE_URL");
				break;
			case 'us':
				$store_url = "http://" . env("US_STORE_URL");
				break;
		}

		$offset = 0;
		$xml = new \DomDocument('1.0', 'UTF-8');
		$attributeurlset = $xml->createAttribute('xmlns');
		$attributeurlset->value = 'http://www.sitemaps.org/schemas/sitemap/0.9';
		$raiz = $xml->createElement('urlset');
		$raiz->appendChild($attributeurlset);
		$raiz = $xml->appendChild($raiz);

		$categories = Category::whereIn('id', function($query) {
				$query->selectRaw('category_id')
					->from('sitemap_products');
			})->pluck('id')->toArray();

		while( true ) {
			$products = Product::getProductsByCategory($categories)->offset($offset)->limit(100)->get();

			if( $products->isEmpty() ) {
				break;
			}
			foreach( $products as $key => $product ) {
				$url = $xml->createElement('url');
				$url = $raiz->appendChild($url);
				$loc = $xml->createElement('loc', $store_url . $product->getDetailUrlAttribute());
				$loc = $url->appendChild($loc);
			}
			$offset = $offset + 100;
		}

		$xml->formatOutput = true;
		$xml->saveXML();
		$xml->save(public_path('sitemap_' . $this->argument('country') . '_products.xml'));

		print('XML creado');
	}

}
