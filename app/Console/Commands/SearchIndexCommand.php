<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use App\Models\SearchIndex;

class SearchIndexCommand extends Command {
	
	protected $name= 'app:search-index';
    
    protected $signature = 'app:search-index {country}';

    protected $description = 'Command description';

    public function __construct()
	{
        parent::__construct();
    }

    public function handle()
	{

		$connection = $this->argument('country');
		config(['database.default' => $connection]);
		
		// delete current index
		printf("Deleting current index...\n");
		SearchIndex::where('id', '>', 0)->delete();
		
		$productModel = new Product();
		$productModel->setConnection($connection);
		$products = $productModel->where(['status' => Product::STATUS_ACTIVE])->get();
		
		printf("Found %d products\n", $products->count());
		
		foreach($products as $product) {
			$index = SearchIndex::firstOrNew(['product_id' => $product->id]);
			$index->setConnection($connection);
			$index->name = $product->name;
			$index->tags = $product->tags;
			$index->color = $product->color;
			$index->description = $product->description;
			printf("Creating entry for product %s [%d]...", $product->name, $product->id);
			if( $index->save() ) {
				printf("OK\n");
			}
			else {
				printf("FAILED\n");
			}
		}
		
		printf("complete.\n");
		
    }
}
