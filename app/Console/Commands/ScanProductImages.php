<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Models\Product;
use App\Models\ProductImages;

class ScanProductImages extends Command{
	
	protected $name = 'products:scan';
    
    protected $signature = 'products:scan {country}';

    protected $description = 'Scan product images';


    public function __construct()
	{
        parent::__construct();
    }


    public function handle()
	{
		$directories = Storage::disk('images')->directories();
        config(['database.default' => 'co']);
        $products_co = Product::get();
        $this->saveImages($products_co, $directories);
        config(['database.default' => 'ec']);
        $products_ec = Product::get();
        $this->saveImages($products_ec, $directories);
        config(['database.default' => 'us']);
        $products_us = Product::get();
        $this->saveImages($products_us, $directories);
    }
	
	private function saveImages($products,$directories)
    {
        foreach ($products as $product) {
            if(array_search($product->code,$directories)!== false) {
                $files = Storage::disk('images')->files($product->code);
				// delete previous images
				ProductImages::where('product_id', $product->id)->delete();
                foreach($files as $file) {
                    $trash = explode('/',$file);
                    $name = explode('.',$trash[1]);
                    if(!empty($name[0]) && !empty($name[1])){
                        if(!ProductImages::where('filename', $trash[1] )->first()){
                            $image = new ProductImages();
                            $image->filename = $trash[1];
                            $image->path = public_path('images/products').'/'.$product->code.'/'.$trash[1];
                            $image->url = '/images/products/'.$product->code.'/'.$trash[1];
                            $image->product_id = $product->id;
                            $image->save();
                        }
                    }
				}
            }
		}
    }
	
}
