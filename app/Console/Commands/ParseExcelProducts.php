<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductUpload;
use App\Models\ProductTemp;
use App\Models\Reference;
use App\Models\RelatedProduct;
use App\Models\ProductCategory;
use App\Models\ProductImages;
use Illuminate\Support\Facades\Log;
use DB;


class ParseExcelProducts extends Command {

	protected $name = 'app:parse-excel-products';
    
    protected $signature = 'app:parse-excel-products';

    protected $description = 'Parse Excel Products';
	
	public function __construct()
	{
        parent::__construct();
    }
	
	public function handle()
	{
		while( ProductUpload::hasUploads() ) {
			
			$upload = ProductUpload::getLastUpload();
			
			try {
				$this->startProcess($upload);
				$this->processFileUpload($upload);
				$this->validateProducts($upload);
				printf("File validated. Total errors: %d\n", intval($upload->total_error));
				if( intval($upload->total_error) === 0 ) {
					$this->loadProducts($upload);
				}
				$this->endProcess($upload, ProductUpload::STATUS_PROCESADO);
			}
			catch(\Exception $e) {
				printf("[ERROR] %s(%s): %s\n", $e->getFile(), $e->getLine(), $e->getMessage());
				$this->endProcess($upload, ProductUpload::STATUS_ERROR);
			}
			
		}
		
		print "complete.\n";
    }
	
	private function processFileUpload($upload)
	{
		printf("Procesando archivo [%s]...\n", $upload->file);
		// reader
		$reader = ReaderFactory::create(Type::XLSX);
		$reader->open($upload->file);
		$this->readFile($upload, $reader);
	}
	
	private function startProcess($upload)
	{
		$upload->status = ProductUpload::STATUS_PROCESANDO;
		$upload->fecha_process_start = date('Y-m-d H:i:s');
		$upload->save();
		
		// delete products_temp
		ProductTemp::where(['upload_id' => $upload->id])
			->delete();
	}
	
	private function endProcess($upload, $status)
	{
		$upload->status = $status;
		$upload->fecha_process_end = date('Y-m-d H:i:s');
		$upload->save();
	}
	
	private function readFile($upload, $reader)
	{
		foreach($reader->getSheetIterator() as $sheet) {
			
			$rows = $sheet->getRowIterator();
			
			foreach($rows as $index => $row) {
					
				if( $index == 1) {
					// skip first row
					continue;
				}
				
				$product = $this->mapProductFromRow($row);
				$product->upload_id = $upload->id;
				$product->line_number = $index;
				$product->save();
			}
			
			break;	// iterate just the first sheet
		}
	}
		
	private function mapProductFromRow($row)
	{
		
		$product = new ProductTemp();
		
		$product->co = isset($row[3]) ? (strtolower(trim($row[3])) === 'x' ? true : false) : false;
		$product->ec = isset($row[4]) ? (strtolower(trim($row[4])) === 'x' ? true : false) : false;
		$product->us = isset($row[5]) ? (strtolower(trim($row[5])) === 'x' ? true : false) : false;
		
		$product->code = isset($row[0]) ? trim($row[0]) : null;
		$product->product_line = isset($row[1]) ? trim($row[1]) : null;
		$product->name = isset($row[2]) ? $row[2] : null;
		$product->dimensions = isset($row[6]) ? trim($row[6]) : null;
		$product->color = isset($row[8]) ? trim($row[8]) : null;
		$product->description = isset($row[9]) ? trim($row[9]) : null;
		$product->enforcement = isset($row[10]) ? trim($row[10]) : null;
		$product->use = isset($row[11]) ? trim($row[11]) : null;
		$product->ambience = isset($row[12]) ? trim($row[12]) : null;
		$product->data_sheet = isset($row[13]) ? trim($row[13]): null;
		$product->user_manual = isset($row[14]) ? trim($row[14]): null;
		$product->maintenance_manual = isset($row[15]) ? trim($row[15]): null;
		$product->guarantee = isset($row[16]) ? trim($row[16]): null;
		$product->quality_seal = isset($row[17]) ? trim($row[17]): null;
		$product->tags = isset($row[19]) ? str_replace(";", ",", $row[19]) : null;
		$product->is_new = isset($row[20]) ? ( strtolower(trim($row[20])) === 'x' ? true : false ) : false;
		$product->color_code = isset($row[21]) ? trim($row[21]) : null;
		
		$product->related = isset($row[18]) ? trim($row[18]) : null;
		$product->categories = isset($row[22]) ? trim($row[22]) : null;
		
		return $product;
	}
	
	private function validateProducts($upload)
	{
		
		printf("Validando productos...\n");
		
		$writer = WriterFactory::create(Type::XLSX);
		$writer->openToFile($upload->file_log);
		
		// headers
		$header = [
			'FILA', 
			'CODIGO', 
			'CAMPOS PRODUCTO', 
			'PRODUCTOS RELACIONADOS', 
			'CATEGORIAS',
			'ADVERTENCIAS', 
		];
		$writer->addRow($header);
		
		$offset = 0;
		$pageSize = 200;
		
		$countErrors = 0;
		$countWarnings = 0;
		$countSuccess = 0;
		
		while( true ) {
			
			$products = ProductTemp::where(['upload_id' => $upload->id])
				->limit($pageSize)
				->offset($offset)
				->get();
			
			if( !$products || $products->count() == 0 ) {
				break;
			}
			
			foreach($products as $product) {
				
				$errors = $product->hasErrors() ? implode(", ", $product->getErrors()) : "OK";
				$warnings = $product->hasWarnings() ? implode(", ", $product->getWarnings()) : "OK";
				$relatedResults = $this->validateRelatedProducts($product);
				$related = $relatedResults ? implode(", ", $relatedResults) : "OK";
				$categoriesResults = $this->validateCategories($product);
				$categories = $categoriesResults ? implode(", ", $categoriesResults) : "OK";
				
				$row = [
					$product->line_number,	// FILA
					$product->code,			// CODIGO
					$errors,				// ERRORES
					$related,				// PRODUCTOS RELACIONADOS
					$categories,			// CATEGORIAS
					$warnings,				// WARNINGS
				];
				
				$writer->addRow($row);
				
				if( $errors !== "OK" || $related !== "OK" || $categories != "OK" ) {
					$countErrors++;
					if( $errors !== "OK" ) {
						printf("Line %s. Code: %s. Validation errors: \n", $errors);
					}
					if( $related !== "OK" ) {
						printf("Line %s. Code: %s. Error with related products: %s\n", $product->line_number, $product->code, $related);
					}
					if( $categories !== "OK" ) {
						printf("Line %s. Code: %s. Error with categories: %s\n", $product->line_number, $product->code, $categories);
					}
				}
				else {
					$countSuccess++;
				}
				
				if( $warnings !== "OK" ) {
					$countWarnings++;
				}
				
			}
			
			$offset += $pageSize;
		}
		
		$writer->close();
		
		$upload->total_success = $countSuccess;
		$upload->total_error = $countErrors;
		$upload->total_warnings = $countWarnings;
		$upload->save();
		
		printf("SUCCESS: %d\n", $countSuccess);
		printf("ERRORS: %d\n", $countErrors);
		printf("WARNINGS: %d\n", $countWarnings);
		
	}
	
	private function validateRelatedProducts($product)
	{
		
		$invalid = [];
		
		$codes = explode(";", $product->related);
		
		foreach($codes as $code) {
			
			$code = trim($code);
			if( !$code ) {
				continue;
			}
			
			// check in CO
			if( $product->co ) {
				if( !$this->productExists($code, 'co') ) {
					$invalid[] = $code;
				}
			}
			
			// check in EC
			if( $product->ec ) {
				if( !$this->productExists($code, 'ec') ) {
					$invalid[] = $code;
				}
			}
			
			// check in US
			if( $product->us ) {
				if( !$this->productExists($code, 'us') ) {
					$invalid[] = $code;
				}
			}
			
		}
		
		return $invalid;
	}
	
	private function productExists($code, $country)
	{
		// check in products_temp
		if( ProductTemp::where(['code' => $code, $country => true])->count() > 0 ) {
			return true;
		}
		
		// check in products => country
		$model = new Product();
		$model->setConnection($country);
		if( $model->where(['code' => $code])->count() > 0 ) {
			return true;
		}
		
		return false;
	}
	
	private function validateCategories($product)
	{
		$invalid = [];
		
		$categories = explode(";", $product->categories);
		
		foreach($categories as $category) {
			
			$category = trim($category);
			
			if( !$category ) {
				continue;
			}
			
			// check in CO
			if( $product->co ) {
				if( !$this->categoryExists($category, 'co') ) {
					$invalid[] = $category;
				}
			}
			
			// check in EC
			if( $product->ec ) {
				if( !$this->categoryExists($category, 'ec') ) {
					$invalid[] = $category;
				}
			}
			
			// check in US
			if( $product->us ) {
				if( !$this->categoryExists($category, 'us') ) {
					$invalid[] = $category;
				}
			}
			
		}
		
		return $invalid;
	}
	
	private function categoryExists($code, $country)
	{
		// check in products => country
		$model = new Category();
		$model->setConnection($country);
		if( $model->where(['code' => $code])->count() > 0 ) {
			return true;
		}
		return false;
	}
	
	private function loadProducts($upload)
	{
		
		try {
			
			DB::connection('co')->beginTransaction();
			DB::connection('ec')->beginTransaction();
			DB::connection('us')->beginTransaction();
			
			printf("Insertando productos...\n");
		
			$offset = 0;
			$pageSize = 200;
		
			while( true ) {
				
				$products = ProductTemp::where(['upload_id' => $upload->id])
					->limit($pageSize)
					->offset($offset)
					->get();

				if( !$products || $products->count() == 0 ) {
					break;
				}

				foreach($products as $temp) {
					
					if( $temp->co ){ 
						$this->loadProductCountry($temp, 'co');
					}

					if($temp->ec){
						$this->loadProductCountry($temp, 'ec');
					}

					if($temp->us){
						$this->loadProductCountry($temp, 'us');
					}
				}
				
				$offset += $pageSize;
			}
			
			DB::connection('co')->commit();
			DB::connection('ec')->commit();
			DB::connection('us')->commit();
			
			printf("Transaction commited.\n");
		}
		catch(\Exception $e) {
			Log::error($e->getMessage());
			printf("Transaction rolled back: %s\n", $e->getMessage());
			DB::connection('co')->rollBack();
			DB::connection('ec')->rollBack();
			DB::connection('us')->rollBack();	
			throw $e;
		}
	}
	
	private function loadProductCountry($temp, $connection)
	{
		
		$modelProduct = new Product();
		$modelProduct->setConnection($connection);
		
		$modelReference = new Reference();
		$modelReference->setConnection($connection);
		
		$modelCategory = new Category();
		$modelCategory->setConnection($connection);
		
		$modelRelatedProduct = new RelatedProduct();
		$modelRelatedProduct->setConnection($connection);
		
		$modelProductCategory = new ProductCategory();
		$modelProductCategory->setConnection($connection);
		
		$modelProductImages = new ProductImages();
		$modelProductImages->setConnection($connection);
		
		// reference
		$reference = $this->_buildReference($temp, $modelReference, $connection);
		printf("Saving reference: %s... ", $reference->name);
		if( $reference->save() ) {
			printf("OK\n");
		}
		else {
			printf("FAIL\n");
		}
		
		// product
		$product = $this->_buildProduct($temp, $reference, $modelProduct, $connection);
		printf("Saving product %s in %s... ", $product->code, $connection);
		if( $product->save() ) {
			printf("OK\n");
		}
		else {
			printf("FAIL\n");
		}
		
		// associate related products
		$this->_buildRelatedProducts($temp, $product, $modelRelatedProduct, $modelProduct, $connection);
		
		// associate categories
		$this->_buildAssociatedCategories($modelProductCategory, $modelCategory, $product, $temp, $connection);
		
		// associate images
		$this->_buildAssociatedImages($modelProductImages, $product, $temp, $connection);
		
	}
	
	private function _buildReference($temp, $model, $connection)
	{
		$reference = $model->firstOrNew(['name' => $temp->product_line]);
		$reference->setConnection($connection);
		$reference->description = $temp->description;
		$reference->ambience = $temp->ambience;
		$reference->enforcement = $temp->enforcement;
		$reference->use = $temp->use;
		$reference->data_sheet = $temp->data_sheet;
		$reference->user_manual = $temp->user_manual;
		$reference->maintenance_manual = $temp->maintenance_manual;
		$reference->guarantee = $temp->guarantee;
		$reference->quality_seal = $temp->quality_seal;
		$reference->featured = $temp->featured;
		$reference->is_new = $temp->is_new;
		$reference->status = $temp->status;
		return $reference;
	}
	
	private function _buildProduct($temp, $reference, $model, $connection)
	{
		$product = $model->firstOrNew(['code' => $temp->code]);
		$product->setConnection($connection);
		$product->code = $temp->code;
		$product->name = $temp->name;
		$product->color_code = $temp->color_code;
		$product->dimensions = $temp->dimensions;
		$product->color = $temp->color;
		$product->tags = $temp->tags;
		$product->status = $temp->status;
		$product->description = $temp->description;
		$product->ambience = $temp->ambience;
		$product->enforcement = $temp->enforcement;
		$product->use = $temp->use;
		$product->data_sheet = $temp->data_sheet;
		$product->user_manual = $temp->user_manual;
		$product->maintenance_manual = $temp->maintenance_manual;
		$product->guarantee = $temp->guarantee;
		$product->quality_seal = $temp->quality_seal;
		$product->featured = $temp->featured;
		$product->is_new = $temp->is_new;
		$product->status = $temp->status;
		$product->reference_id = $reference->id;
		return $product;
	}
	
	private function _buildRelatedProducts($temp, $product, $relatedModel, $productModel, $connection)
	{
		// delete previous products
		$relatedModel->where(['product_id' => $product->id])->delete();
		
		// build new ones...
		$relatedArray = array_unique(explode(";", $temp->related));
		
		foreach($relatedArray as $code) {
			$code = trim($code);
			if( !$code ) {
				continue;
			}
			$pid = $productModel->where(['code' => $code])->first()->id;
			$related = new RelatedProduct();
			$related->setConnection($connection);
			$related->product_id = $product->id;
			$related->product2_id = $pid;
			$related->save();
		}
	}
	
	private function _buildAssociatedCategories($productCategoryModel, $categoryModel, $product, $temp, $connection)
	{
		$productCategoryModel->where(['product_id' => $product->id])->delete();
		$categoriesArray = array_unique(explode(";", $temp->categories));
		
		foreach($categoriesArray as $code) {
			$code = trim($code);
			if( !$code ) {
				continue;
			}
			$cid = $categoryModel->where(['code' => $code])->first()->id;
			$productCategory = new ProductCategory();
			$productCategory->setConnection($connection);
			$productCategory->product_id = $product->id;
			$productCategory->category_id = $cid;
			$productCategory->save();
		}
	}
	
	private function _buildAssociatedImages($imagesModel, $product, $temp, $connection)
	{
		$imagesModel->where(['product_id' => $product->id])->delete();
		
		$directories = Storage::disk('images')->directories();
		
		if(array_search($product->code,$directories)!== false) {
			$files = Storage::disk('images')->files($product->code);
			foreach($files as $file) {
				$trash = explode('/',$file);
				$name = explode('.',$trash[1]);
				if( !empty($name[0]) && !empty($name[1]) ){
					if( !$imagesModel->where('filename', $trash[1] )->first() ) {
						$image = new ProductImages();
						$image->setConnection($connection);
						$image->filename = $trash[1];
						$image->path = public_path('images/products').'/'.$product->code.'/'.$trash[1];
						$image->url = '/images/products/'.$product->code.'/'.$trash[1];
						$image->product_id = $product->id;
						$image->save();
					}
				}
			}
		}
	}
	
}