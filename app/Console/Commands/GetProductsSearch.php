<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Models\Product;

class GetProductsSearch extends Command{
	
	protected $name= 'productsearch:get';
    
    protected $signature = 'productsearch:get {country}';

    protected $description = 'Command description';


    public function __construct(){
        parent::__construct();
    }


    public function handle(){

		$country = $this->argument('country');
		config(['database.default' => $this->argument('country')]);
		$topTenProducts = Product::topTen();
		//$topTenProducts = Product::take(10)->orderByRaw("RAND()")->get();

        $expiresAt = Carbon::now()->addMinutes(60);

		if (Cache::has('products_search_'.$country)){
			Cache::put('products_search_'.$country, $topTenProducts, $expiresAt);
		}else{
			Cache::add('products_search_'.$country, $topTenProducts, $expiresAt);
		}	
    }
}
