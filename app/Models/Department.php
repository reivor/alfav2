<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	
    protected $table = 'departments';
	
    public $timestamps = false;

    public function cities()
	{
        return $this->hasMany('App\Models\City', 'department_id');
    }

    public static function getDepartmentByCountry($code)
    {
        $country = Country::where('code', $code )->first();
        return self::where('country_id', $country->id);
    }
	
}
