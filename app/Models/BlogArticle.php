<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BlogArticle extends Model{
	
	const DEFAULT_PICTURE = '/images/placeholder.png';
	const HEADER_ARTICLE_PICTURE = '/images/placeholderHeaderArticle.png';
	const RELATED_ARTICLE_PICTURE = '/images/placeholderArticleRelated.png';
	const HOME_ARTICLE_PICTURE = '/images/placeholderArticleHome.png';
	const STATUS_ACTIVE = 'active';
	const STATUS_DISABLED = 'disabled';

	
	protected $table = 'blog_articles';
	
	protected $fillable = ['title'];
	
	protected $hidden = ['created_at', 'updated_at'];
	
	protected $dates = ['date_publishing', 'created_at', 'updated_at'];
	
	protected $with = ['category'];
	
	protected $appends = ['url_detail', 'format_publish_date'];
	
	
	// --------------------- STATIC --------------------- //
	
	protected static function boot()
	{		
		static::saving(function($model) {
			// set slug && title_plain
			if( isset($model->title) ) {
				$cleanText = strip_tags($model->title);
				$model->title_plain = $cleanText;
				$model->slug = strtolower(str_slug($cleanText, "-"));
			}
		});
	}
	
	public static function getRecentArticles()
	{
		return self::where(['status' => self::STATUS_ACTIVE])
				->orderBy('date_publishing', 'desc');
	}
	
	public static function getArticlesCount()
	{
		return self::getRecentArticles()->count();
	}
	
	public static function getCountArticlePages($size = 10)
	{
		if( $size <= 0 ) { $size = 10; }
		$count = self::getArticlesCount();
		$totalPages = ceil($count / $size);
		return $totalPages;
	}
	
	public static function truncate($text, $length = 80)
	{
		$cleanText = strip_tags($text);
		
		if( strlen($cleanText) <= $length ) {
			return $cleanText;
		}
		
		$excerpt = substr($cleanText, 0, strrpos(substr($cleanText, 0, $length), ' '));
		
		if( strlen($cleanText) > strlen($excerpt) ) {
			$excerpt .= '...';
		}
		
		return $excerpt;
	}
	
	public static function getRelatedArticles($article)
	{
		$query = self::where(['blog_category_id' => $article->blog_category_id])
			->orderBy('date_publishing', 'desc');
		return $query;
	}
	
	// --------------------- RELATIONS --------------------- //
	
	public function category()
	{
		return $this->belongsTo('App\Models\BlogCategory', 'blog_category_id');
	}
	
	public function blogCategory()
	{
		return $this->belongsTo('App\Models\BlogCategory', 'blog_category_id');
	}
	
	public function articles()
	{
		return $this->hasMany('App\Models\BlogComment', 'blog_article_id');
	}
	
	public function pinPoints()
	{
		return $this->hasMany('App\Models\BlogPoint', 'blog_article_id');
	}
	
	public function comments()
	{
		return $this->hasMany('App\Models\BlogComment', 'blog_article_id');
	}

	// --------------------- ATTRIBUTES --------------------- //
	
	public function getFormatPublishDateAttribute(){
		return $this->getPublishedDate(config('store.id'), false);
	}


	public function getCategoryAttribute()
	{
		if($this->blog_category_id){
			return $this->blogCategory;
		}else{
			$current=new BlogCategory();
			$current->name="default";
			$current->slug="default";
			return $current;
		}
	}
	public function getExcerptAttribute()
	{
		return self::truncate($this->body, 140);
	}
	
	public function getImageHeaderAttribute($value)
	{
		return $value ? $value : self::HEADER_ARTICLE_PICTURE;
	}
	
	public function getImageDefaultAttribute($value)
	{
		return $value ? $value : self::DEFAULT_PICTURE;
	}
	
	public function getImageRelatedAttribute($value)
	{
		return $value ? $value : self::RELATED_ARTICLE_PICTURE;
	}
	
	public function getImageHomeAttribute($value)
	{
		return $value ? $value : self::HOME_ARTICLE_PICTURE;
	}
	
	public function getUrlDetailAttribute()
	{
		return "/blog/{$this->category->slug}/{$this->slug}/{$this->id}";
		
	}
	
	public function getRootComments()
	{
		return $this->comments()
			->where(['status' => BlogComment::STATUS_ACTIVE])
			->whereNull('blog_comment_id');
	}
	
	public function getApprovedCommentsAttribute()
	{
		return $this->comments()
			->where(['status' => BlogComment::STATUS_ACTIVE]);
	}

	public function getMonthSpanish($month){
	
		switch($month){
			case 'January':
				return 'Enero';
			break;
			case 'February':
				return  'Febrero';
			break;
			case 'March':
				return  'Marzo';
			break;
			case 'April':
				return  'Abril';
			break;
			case 'May':
				return  'Mayo';
			break;
			case 'June':
				return  'Junio';
			break;
			case 'July':
				return  'Julio';
			break;
			case 'August':
				return  'Agosto';
			break;
			case 'September':
				return  'Septiembre';
			break;
			case 'October':
				return  'Octubre';
			break;
			case 'November':
				return  'Noviembre';
			break;
			case 'December':
				return  'Diciembre';
			break;
			
		}

	}


	public function getPublishedDate($locate, $home){
			
		if($locate == 'ec' || $locate == 'co'){
			$month = $this->getMonthSpanish($this->date_publishing->format("F"));
			if($home){
				return $month . ' ' . $this->date_publishing->format("d");
			}else{
				return $month . ' ' . $this->date_publishing->format("d, Y");
			}
			
		}else{
			if($home){
				return $this->date_publishing->format("F d");
			}else{
				return $this->date_publishing->format("F d, Y");
			}
			
		}

	}
	
}

