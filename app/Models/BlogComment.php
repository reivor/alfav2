<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model{
	
	const STATUS_ACTIVE = 'active';
	const STATUS_DISABLED = 'disabled';
	const STATUS_REJECTED = 'rejected';
	
	protected $table = 'blog_comments';
	protected $hidden = ['created_at', 'updated_at'];
	
	
	// --------------------- RELATIONS --------------------- //
	
	public function article()
	{
		return $this->belongsTo('App\Models\BlogArticle', 'blog_article_id');
	}
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'customer_id');
	}
	
	public function comments()
	{
		return $this->hasMany('App\Models\BlogComment', 'blog_comment_id');
	}
	
	
	// --------------------- METHODS --------------------- //
	
	public static function getParentComments($idArticle)
	{
		return self::where([
				'blog_article_id' => $idArticle,
				'blog_comment_id' => null,
		]);
	}
	
	public static function getCommentsTree(&$comment)
	{
		foreach( $comment->comments as &$child ) {
			self::getCommentsTree($child);
		}
		return $comment;
	}
	
	public static function buildCommentsTree($tree)
	{
		foreach( $tree as &$leaf ) {
			self::getCommentsTree($leaf);
		}
		return $tree;
	}
	
	// --------------------- PROPERTIES --------------------- //
	
	public function getApprovedComments()
	{
		return $this->comments()
			->where(['status' => self::STATUS_ACTIVE])
			->get();
	}
	
	public function getTimeElapsedAttribute()
	{
		return $this->created_at->diffForHumans();
	}
	
}

