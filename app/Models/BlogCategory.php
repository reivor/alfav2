<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model{
	
	const DEFAULT_PICTURE = '/images/placeholder.png';
	const STATUS_ACTIVE = 'active';
	const STATUS_DISABLED = 'disabled';
	
	protected $table = 'blog_categories';
	protected $fillable = ['name'];
	protected $hidden = ['created_at', 'updated_at'];
	
	
	// --------------------- STATIC --------------------- //
	
	// --------------------- RELATIONS --------------------- //
	public function articles(){
		return $this->hasMany('App\Models\BlogArticle', 'blog_category_id');
	}

	// --------------------- METHODS --------------------- //
	
	public function getSlugAttribute()
	{
		return strtolower(str_slug($this->name, "-"));
	}
	
}

