<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	
    const STATUS_PENDING = 'pending';
    const STATUS_ACTIVE = 'active';
    const STATUS_BLOCKED = 'blocked';

    const ROLE_ADMIN = 'admin';
    const ROLE_SUPERADMIN = 'superadmin';
    const ROLE_GUEST = 'guest';
    const ROLE_SERVICIOCLIENTE = 'serviciocliente';

    const DEFAULT_PICTURE = '/images/users/avatar.png';

	protected $connection = 'common';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
		'remember_token',
    ];
	
	public function isActive()
	{
		return $this->status === self::STATUS_ACTIVE;
	}
	
	public function isBlocked()
	{
		return $this->status === self::STATUS_BLOCKED;
	}
	
	public function isPending()
	{
		return $this->status === self::STATUS_PENDING;
	}
}
