<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BlogPoint extends Model
{
		
	protected $table = 'blog_article_points';
	
	public $timestamps = false;
	
	
	// --------------------- STATIC --------------------- //
	
	
	// --------------------- RELATIONS --------------------- //
	public function product(){
		return $this->belongsTo('App\Models\Product', 'product_id');
	}
	
	// --------------------- METHODS --------------------- //
	
	public function getStyleAttribute()
	{
		switch($this->nearest_point) {
			case 0:
				return sprintf("top:%d%%; left: %d%%; margin-top: %dpx; margin-left: %dpx;", $this->top, $this->left, $this->offset_y, $this->offset_x);
			case 1:
				return sprintf("top:%d%%; left: %d%%; margin-top: %dpx; margin-left: %dpx;", $this->top, $this->left, $this->offset_y, $this->offset_x);
			case 2:
				return sprintf("top:%d%%; left: %d%%; margin-top: %dpx; margin-left: %dpx;", $this->top, 100, $this->offset_y, $this->offset_x);
			case 3:
				return sprintf("top:%d%%; left: %d%%; margin-top: %dpx; margin-left: %dpx;", 100, $this->left, $this->offset_y, $this->offset_x);
			case 4:
				return sprintf("top:%d%%; left: %d%%; margin-top: %dpx; margin-left: %dpx;", 100, $this->left, $this->offset_y, $this->offset_x);
			case 5:
				return sprintf("top:%d%%; left: %d%%; margin-top: %dpx; margin-left: %dpx;", 100, 100, $this->offset_y, $this->offset_x);
			case 6:
				return sprintf("top:%d%%; left: %d%%; margin-top: %dpx; margin-left: %dpx;", 50, 50, $this->offset_y, $this->offset_x);
		}
	}
	
}

