<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carrusel extends Model
{

	use BannersTrait;

	const STATUS_PENDING = 'pending';
	const STATUS_DISABLED = 'disabled';
	const STATUS_ACTIVE = 'active';
	const STATUS_BLOCKED = 'blocked';
	const TYPE_EXTERNAL = 'external';
	const TYPE_CATEGORY = 'category';
	const TYPE_PRODUCT = 'product';

	protected $table = 'slider_home';

	// --------------------- STATIC --------------------- //
	protected static function boot()
	{
		static::saving(function($model) {
			// set http
			if( $model->type == self::TYPE_EXTERNAL ) {
				$url = $model->external_url;
				if( $model->isUrl($url) ) {
					$prefix = substr($url, 0, 4);
					if( strtolower($prefix) !== 'http' ) {
						$model->external_url = "http://" . $model->external_url;
					}
				}
			}
		});
	}

}
