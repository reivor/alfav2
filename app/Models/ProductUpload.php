<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductUpload extends Model
{
	
	const STATUS_EN_COLA = 'EN COLA';
	const STATUS_PROCESANDO = 'PROCESANDO';
    const STATUS_PROCESADO = 'PROCESADO';
	const STATUS_ERROR = 'ERROR';
	
	protected $connection = 'common';
	
    protected $table = 'products_uploads';
	
    public $timestamps = false;
	
    protected $dates = ['fecha_upload', 'fecha_process_start', 'fecha_process_end'];
	

	//
	// STATIC
	//
	
	public static function getLastUpload()
	{
		$upload = self::where('status', '=', self::STATUS_EN_COLA)
			->orderBy('fecha_upload', 'ASC')
			->first();
		return $upload;
	}
	
	public static function hasUploads()
	{
		$count = self::where('status', '=', self::STATUS_EN_COLA)->count();
		return $count > 0;
	}
	
	
	//
	// RELATIONS
	//
	
    public function user()
	{
        return $this->belongsTo('App\Models\User', 'user_id');
    }
	
	
	//
	// PROPERTIES
	//

    public function formatDate($fecha, $formato)
    {
        return ($fecha->timestamp <= 0 ? '' : $fecha->format($formato));
    }
	
	public function hasErrors()
	{
		return intval($this->total_error) > 0;
	}
	
	public function hasWarnings()
	{
		return intval($this->total_warnings) > 0;
	}
	
	public function isQueued()
	{
		return $this->status === self::STATUS_EN_COLA;
	}
	
	public function isProcessing()
	{
		return $this->status === self::STATUS_PROCESANDO;
	}
	
	public function isProcessed()
	{
		return $this->status === self::STATUS_PROCESADO;
	}
	
	public function isInvalid()
	{
		return $this->status === self::STATUS_ERROR;
	}
	
	public function getIconAttribute()
	{
		return 'queue';
	}
	
	public function getFaIconAttribute()
	{
		if( $this->isQueued() ) {
			return 'fa fa-clock-o';
		}
		
		if( $this->isProcessing() ) {
			return 'fa fa-refresh';
		}
		
		if( $this->isProcessed() ) {
			if( $this->hasErrors() ) {
				return 'fa fa-times';
			}
			if( $this->hasWarnings() ) {
				return 'fa fa-exclamation-triangle';
			}
			return 'fa fa-check';
		}
		
		if( $this->isInvalid() ) {
			return 'fa fa-times';
		}
		
	}
	
	public function getTextColorAttribute()
	{
		
		if( $this->isProcessed() ) {
			if( $this->hasErrors() ) {
				return 'text-danger';
			}
			if( $this->hasWarnings() ) {
				return 'text-warning';
			}
			return 'text-success';
		}
		
		if( $this->isInvalid() ) {
			return 'text-danger';
		}
		
		return 'text-normal';
	}
	
	public function getFileUrlAttribute()
	{
		$path = pathinfo($this->file_log);
		return '/files/products/' . $path['basename'];
	}
	
}