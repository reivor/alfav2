<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermSearch extends Model{

	protected $table = 'terms_search';
    public $timestamps = false;
	
	protected $fillable = ['term', 'last_search', 'number_search'];
}
