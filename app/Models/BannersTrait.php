<?php
namespace App\Models;

trait BannersTrait {
	
	public function getUrlBanner(){
		$url="";
		if($this->type==BannerHome::TYPE_EXTERNAL){
			$url = $this->external_url;
		}else if($this->type==BannerHome::TYPE_CATEGORY){
			$category = Category::find($this->entity_id);
			if($category->isShort()){
				$url = '/categories/'.$this->entity_id;
			}else{
				$url = '/subhome/'.$this->entity_id;
			} 
			
		}else if($this->type==BannerHome::TYPE_PRODUCT){
			$url = '/detail/'.$this->entity_id;
		}
		
		return $url;
	}

	public function isExternal(){
		$match=false;
		$url= $this->getUrlBanner();
		
		if($this->isUrl($url)){
			$domain = parse_url($url, PHP_URL_HOST);
			$match=($domain != $_SERVER['SERVER_NAME']);	
		}
			
		return $match;
	}
	
	public function isUrl($url){
		$regex = "/(?:http|https)?(?:\:\/\/)?(?:www.)?(([A-Za-z0-9-]+\.)*[A-Za-z0-9-]+\.[A-Za-z]+)(?:\/.*)?/im";
		return (preg_match($regex, $url));
	}
	
	public function product(){
		return $this->belongsTo('App\Models\Product', 'entity_id');
	}

	public function category(){
		return $this->belongsTo('App\Models\Category', 'entity_id');
	}
	
	public function entity(){
		$entity=NULL;
		
		if ($this->type == "category"){
			$entity=$this->category;
		}else if($this->type == "product"){
			$entity=$this->product;
		}
		
		return $entity;
	}
	
	public function getDisplayAttribute(){
		if($this->entity()){
			return $this->entity()->name;
		}
		
		return "";
	}
	
	public function getDetailUrlAttribute(){
		if($this->entity()==NULL){
			$url=$this->external_url;
		}else{
			$url=$this->entity()->detailUrl;
		}
		return $url;
	}
	
	public function getTargetAttribute(){
		$target="_self";
				
		if($this->entity()==NULL){
			$target="_blank";
		}
		return $target;
	}
}

