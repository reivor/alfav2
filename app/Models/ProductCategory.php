<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	protected $table = 'products_categories';
	public $timestamps = false;

	protected $guarded = [
        'category_id',
        'product_id',
    ];

	public function products()
	{
		return $this->belongsToMany('App\Models\Products');
	}
}
