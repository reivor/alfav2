<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerCategory extends Model{
	use BannersTrait;
    const TYPE_EXTERNAL = 'external';
	const TYPE_CATEGORY = 'category';
	const TYPE_PRODUCT = 'product';
	
	const STATUS_ACTIVE = 'active';
	const STATUS_DISABLED = 'disabled';
	
	const LOCATION_HOME = 'home';
	const LOCATION_SEARCH = 'search';
	const LOCATION_DETAIL = 'detail';
	
	const DEFAULT_PICTURE = '/images/placeholder.png';
	
	protected $table = 'banners_categories';
	
	protected $appends = ['detail_url'];
	
	// --------------------- STATIC --------------------- //
	protected static function boot() {
		static::saving(function($model) {
			// set http
			if($model->type==self::TYPE_EXTERNAL){
				$url = $model->external_url;
				if($model->isUrl($url)){
					$prefix = substr($url,0,4);
					if(strtolower($prefix) !== 'http'){
						$model->external_url = "http://".$model->external_url;
					}	
				}
			}
		});
	}
	
	public function getImageUrlAttribute($value){
		return $value ? $value : self::DEFAULT_PICTURE;
	}
}
