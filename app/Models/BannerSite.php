<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerSite extends Model{
	use BannersTrait;
	
    const TYPE_EXTERNAL = 'external';
	const TYPE_CATEGORY = 'category';
	const TYPE_PRODUCT = 'product';
	
	const STATUS_ACTIVE = 'active';
	const STATUS_DISABLED = 'disabled';
	
	const LOCATION_HOME_TOP = 'homeTop';
	const LOCATION_HOME_FOOTER = 'homeFooter';
	const LOCATION_SUBHOME = 'subhome';
	const LOCATION_REGISTER = 'register';
	const LOCATION_BLOG = 'blog';
	
	const DEFAULT_PICTURE = '/images/placeholder.png';
	
	protected $table = 'banners_site';
	
	// --------------------- STATIC --------------------- //
	protected static function boot() {
		static::saving(function($model) {
			// set http
			if($model->type==self::TYPE_EXTERNAL){
				$url = $model->external_url;
				if($model->isUrl($url)){
					$prefix = substr($url,0,4);
					if(strtolower($prefix) !== 'http'){
						$model->external_url = "http://".$model->external_url;
					}	
				}
			}
		});
	}
	
	public function getImageUrlAttribute($value){
		return $value ? $value : self::DEFAULT_PICTURE;
	}

	
	public static function getBannersHomeTop(){
		$bannersPromoHomeTop = [];
		$bannersHomeTop = self::where(['location'=>self::LOCATION_HOME_TOP,'status'=>self::STATUS_ACTIVE])->get();

		foreach ($bannersHomeTop as $banner) {
			$bannersPromoHomeTop[$banner->position] = $banner;
		}
		
		return $bannersPromoHomeTop;
	}
	
	public static function getBannersHomeFooter(){
		$bannersPromoHomeFooter = [];
		$bannersHomeFooter = BannerSite::where(['location'=>BannerSite::LOCATION_HOME_FOOTER, 'status'=>BannerSite::STATUS_ACTIVE])->get();
		
		foreach ($bannersHomeFooter as $banner) {
			$bannersPromoHomeFooter[$banner->position] = $banner;
		}
		
		return $bannersPromoHomeFooter;
	}
}
