<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchIndex extends Model
{
	
    protected $table = 'search_index';
	
    public $timestamps = false;
	
	protected $fillable = ['name', 'tags', 'color', 'description', 'product_id'];
	
}
