<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTemp extends Model
{

	protected $connection = 'common';
	
	protected $table = 'products_temp';

	protected $fillable = [
		'name',
		'description',
		'ambience',
		'enforcement',
		'use',
		'data_sheet',
		'user_manual',
		'maintenance_manual',
		'guarantee',
		'quality_seal',
		'code',
		'color_code',
		'name',
		'reference_id',
		'image',
		'dimensions',
		'color',
		'tags',
		'status',
		'created_at',
		'updated_at',
	];
	
	
	// --------------------- STATIC --------------------- //

	protected static function boot()
	{
		static::saving(function($model) {
			// set slug
			if( isset($model->name) ) {
				$model->slug = strtolower(str_slug($model->name, "-"));
			}
		});
	}
	
	
	// --------------------- METHODS --------------------- //
	
	public function hasErrors()
	{
		return count($this->getErrors()) > 0;
	}
	
	public function getErrors()
	{
		$invalid = [];
		
		if( !$this->product_line ) {
			$invalid[] = 'LINEA DE PRODUCTO.';
		}
		
		if( !$this->name ) {
			$invalid[] = 'REFERENCIA';
		}
		
		if( !$this->code ) {
			$invalid[] = 'CODIGO';
		}
		
		if( !$this->description ) {
			$invalid[] = 'DESCRIPCIÓN';
		}
		
		return $invalid;
	}
	
	public function hasWarnings()
	{
		return count($this->getWarnings()) > 0;
	}
	
	public function getWarnings()	
	{
		$invalid = [];
		
		if( !$this->dimensions ) {
			$invalid[] = 'DIMENSION';
		}
		
		if( !$this->color ) {
			$invalid[] = 'COLOR';
		}
		
		if( !$this->ambience ) {
			$invalid[] = 'APLICACIÓN';
		}
		
		if( !$this->enforcement ) {
			$invalid[] = 'ESPACIOS';
		}
		
		if( !$this->use ) {
			$invalid[] = 'USOS';
		}
		
		return $invalid;
	}
	
}
