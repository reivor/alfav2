<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageMeta extends Model
{

	protected $table = 'page_meta';
	
	protected $fillable = [
		'type',
		'key',
		'value',
		'page'
	];
	
	public $timestamps = false;

	public static function metaPage($page)
	{
		$metas = self::where('page', $page)->get();
		$metadata = [];
		foreach( $metas as $key => $meta ) {
			$metadata[$meta->type][$meta->key] = $meta->value;
		}
		return $metadata;
	}

}
