<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model{
	const LAYOUT_SETTING = "layout";
	const ALFA_EMAIL_SETTING = "alfa_email";
	const ALFA_EMAIL_QUOTES = "alfa_email_quotes";
	
	public $timestamps = false;
	
	public $incrementing = false;
    protected $table = 'settings';
	protected $primaryKey = 'name';
	protected $fillable = ['name', 'value'];
	
}
