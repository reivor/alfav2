<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

	const STATUS_PENDING = 'pending';
	const STATUS_ACTIVE = 'active';
	const STATUS_BLOCKED = 'blocked';
	const DEFAULT_PICTURE = '/images/users/avatar.png';
	const ROLE_ADMIN = 'admin';
	const ROLE_SUPERADMIN = 'superadmin';
	const ROLE_GUEST = 'guest';

	protected $connection = 'common';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customers';
	
	protected $fillable = [
		'name',
		'email',
		'lastname',
		'job',
		'password',
		'recovery_token',
		'date_recovery',
		'city'
	];

	
	
	// --------------------- METHODS --------------------- //

	public static function login($password, $userpassword)
	{
		return password_verify($password, $userpassword);
	}

	public static function recoveryUser($email)
	{
		$user = self::where('email', $email)->first();

		if( $user ) {
			return $user;
		}
		else {
			return null;
		}
	}
	
	// --------------------- ATTRIBUTES --------------------- //
	
	public function getFullNameAttribute()
	{
		return $this->name . ' ' . $this->lastname;
	}
	
	public function isActive()
	{
		return $this->status === self::STATUS_ACTIVE;
	}

	public function isBlocked()
	{
		return $this->status === self::STATUS_BLOCKED;
	}

	public function isPending()
	{
		return $this->status === self::STATUS_PENDING;
	}

	public function getAvatarAttribute($value)
	{
		return $value ? $value : self::DEFAULT_PICTURE;
	}

	public function getCity(){
		return $this->belongsTo('App\Models\City', 'city');
	}
}
