<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class FooterCategory extends Model{
	const MAX_CATEGORIES_LEVEL = 5;
	public $timestamps = false;
	
    protected $table = 'footer_categories';
	
	public static function getFooterItems($storeId){
		
		config(['database.default' => $storeId]);

		if( Cache::has('footer_items_'.$storeId)){
			$footerItems= Cache::get('footer_items_'.$storeId, []);
		}else{
			$categories = self::orderBy('supreme_parent')->get();
			$footerItems = [];
			foreach ($categories as $category) {
				if(!isset($footerItems[$category->supreme_parent])){
					$parent = Category::find($category->supreme_parent);
					$footerItems[$category->supreme_parent]['parent']=$parent;
				}

				$child = Category::find($category->category_id);
				$footerItems[$category->supreme_parent]['categories'][]=$child;
			}
			
			$expiresAt = Carbon::now()->addMinutes(60);
			Cache::add('footer_items_'.$storeId, $footerItems, $expiresAt);
		}
		
		return $footerItems;
	}

}
