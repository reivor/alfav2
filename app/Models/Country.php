<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    public $timestamps = false;

    public function departments(){
        return $this->hasMany('App\Models\Department', 'country_id');
    }
}