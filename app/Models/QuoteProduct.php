<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteProduct extends Model{
	
	const STATUS_OPEN = 'open';
	const STATUS_CLOSED = 'closed';
	
	public $timestamps = false;

	protected $table = 'quote_products';
	protected $fillable = ['quote_order_id','product_id'];

}
