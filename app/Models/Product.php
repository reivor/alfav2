<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductCategory;
use App\Models\TermSearch;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Models\BannerMenu;
use App\Models\BannerSite;
use App\Models\BannerHome;
use App\Models\BannerCategory;
use Illuminate\Support\Facades\DB;

class Product extends Model
{

	const STATUS_AVAILABLE = 'available';
	const STATUS_UNAVAILABLE = 'unavailable';
	const STATUS_SOON = 'soon';
	const STATUS_DISCONTINUED = 'discontinued';
	const STATUS_DISABLED = 'disabled';
	const STATUS_ACTIVE = 'active';
	const DEFAULT_PICTURE = '/images/placeholder.png';
	const DEFAULT_PICTURE_PRODUCT = '/images/placeholderProduct.png';

	protected $table = 'products';

	protected $fillable = [
		'name',
		'description',
		'ambience',
		'enforcement',
		'use',
		'data_sheet',
		'user_manual',
		'maintenance_manual',
		'guarantee',
		'quality_seal',
		'code',
		'color_code',
		'name',
		'reference_id',
		'image',
		'dimensions',
		'color',
		'tags',
		'status',
		'created_at',
		'updated_at',
	];

	protected $touches = ['reference'];

	protected $appends = ['mainImage', 'imageUrl', 'detail_url', 'guarantee', 'use','data_sheet','maintenance_manual','user_manual','quality_seal'];

	// --------------------- STATIC --------------------- //

	protected static function boot()
	{
		static::saving(function($model) {
			// set slug
			if( isset($model->name) ) {
				$model->slug = strtolower(str_slug($model->name, "-"));
			}

			// create tags
			if( isset($model->tags) ) {
				$tags = explode(",", $model->tags);
				foreach( $tags as $tag ) {
					if( trim($tag) ) {
						Tag::firstOrCreate(['name' => trim($tag)]);
					}
				}
			}
		});

		static::saved(function($model) {
			// search index
			$index = SearchIndex::firstOrNew(['product_id' => $model->id]);
			$index->name = $model->name;
			$index->tags = $model->tags;
			$index->color = $model->color;
			$index->description = $model->description;
			$index->save();
		});

		static::deleted(function($model) {
			self::onDeleteProduct($model);
		});
	}

	//Obtiene productos asociados a la categoria pasada por parámetro y a todas sus categorias hijas
	public static function getTreeProducts($id){
		$category = Category::find($id);
		$categories = Category::getCategoriesTree($category); //Categorias hijas
		$releatedCategories = array();

		if( count($categories) > 0 ) {
			$releatedCategories['ids'][] = $categories->id;
			$releatedCategories['objects'][] = $categories;
			$releatedCategories = Category::getChildCategories($categories->categories, $releatedCategories); //Recorrido de categorias hijas
		}

		$products = self::getProductsByCategory($releatedCategories['ids']);
		return $products;
	}

	public static function getProductsByCategory($id){
		$productCategory = ProductCategory::whereIn('category_id', (array) $id)->get(['product_id']);
		$products = self::where('status', '=', self::STATUS_ACTIVE)->whereIn('id', $productCategory);
		return $products;
	}




	// --------------------- RELATIONS --------------------- //

	public function reference()
	{
		return $this->belongsTo('App\Models\Reference');
	}

	public function images()
	{
		return $this->hasMany('App\Models\ProductImages', 'product_id');
	}

	public function categories()
	{
		return $this->belongsToMany('App\Models\Category', 'products_categories', 'product_id', 'category_id');
	}

	public function pcategories()
	{
		return $this->hasMany('App\Models\ProductCategory', 'product_id');
	}

	/* public function relatedCategories() {
	  return $this->belongsToMany('App\Models\Category', 'products_categories', 'product_id', 'category_id');
	  } */

	public function relatedProducts()
	{
		return $this->belongsToMany('App\Models\Product', 'related_products', 'product_id', 'product2_id');
	}

	// --------------------- METHODS --------------------- //

	public function getAmbienceAttribute()
	{
		return $this->reference->ambience;
	}

	public function getEnforcementAttribute()
	{
		return $this->reference->enforcement;
	}

	public function getUseAttribute()
	{
		return $this->reference->use;
	}

	public function getDataSheetAttribute()
	{
		return $this->reference->data_sheet;
	}

	public function getMaintenanceManualAttribute()
	{
		return $this->reference->maintenance_manual;
	}

	public function getGuaranteeAttribute()
	{
		return $this->reference->guarantee;
	}

	public function getUserManualAttribute()
	{
		return $this->reference->user_manual;
	}

	public function getQualitySealAttribute()
	{
		return $this->reference->quality_seal;
	}


	//-------

	public function getDataSheetNameAttribute()
	{
		return basename($this->data_sheet);
	}

	public function getUserManualNameAttribute()
	{
		return basename($this->user_manual);
	}

	public function getMaintenanceManualNameAttribute()
	{
		return basename($this->maintenance_manual);
	}

	public function getGuaranteeNameAttribute()
	{
		return basename($this->guarantee);
	}

	public function getFirstCategory()
	{
		return $this->category->first();
	}

	public function getMainImageAttribute()
	{
		return $this->images->first();
	}

	public function getImageUrlAttribute()
	{
		$value = $this->mainImage;
		return $value ? $value->url : self::DEFAULT_PICTURE_PRODUCT;
	}

	public function getDetailUrlAttribute()
	{
		return getUrlEntity($this);
		//return "/products/".$this->slug. "/" . $this->code;
	}

	public function getTags()
	{
		$tags = explode(',', $this->tags);
		return $tags;
	}

	public function getQualitySeal()
	{
		$qualitySeals = [];
		$seals = explode(',', $this->quality_seal);

		foreach( $seals as $seal ) {
			$aux['text'] = $seal;
			$pdf = "";

			switch( $seal ) {
				case 01:
					$pdf = "/files/products/quality-seals/Sello de calidad 01.pdf";
					break;
				case 02:
					$pdf = "/files/products/quality-seals/Sello de calidad 02.pdf";
					break;
				case 03:
					$pdf = "/files/products/quality-seals/Sello de calidad 03.pdf";
					break;
				case 04:
					$pdf = "/files/products/quality-seals/Sello de calidad 04.pdf";
					break;
				case 05:
					$pdf = "/files/products/quality-seals/Sello de calidad 05.pdf";
					break;
				case 06:
					$pdf = "/files/products/quality-seals/Sello de calidad 06.pdf";
					break;
			}

			$aux['link'] = $pdf;
			$qualitySeals[] = $aux;
		}

		return $qualitySeals;
	}

	public function getVariations()
	{
		$variations = self::where('reference_id', '=', $this->reference_id);
		return $variations;
	}

	public function getFormats()
	{
		$variations = $this->getVariations()->get();
		$formats = [];

		foreach( $variations as $variation ) {
			if( !in_array($variation->dimensions, $formats) ) {
				$formats[] = $variation->dimensions;
			}
		}

		return $formats;
	}

	public function getCurrentColors($format)
	{
		$colors = self::where('dimensions', '=', $format)
			->where('reference_id', '=', $this->reference_id)
			->groupBy('color_code');

		return $colors;
	}

	public function isNew()
	{
		return $this->is_new == 1;
	}

	public static function deleteTags($tag, $name, $id)
	{
		$tags = explode(',', $tag);
		$key = array_search($name, $tags);
		unset($tags[$key]);
		$newtags = implode(',', $tags);
		$prod = Product::where('id', $id)->first();
		$prod->tags = $newtags;
		$prod->save();
		return true;
	}


	//
	// STATIC
	//

	public static function onDeleteProduct($model){
		//Elimina banners del menu asociados al producto eliminado
		BannerMenu::where(['entity_id' => $model->id, 'type' => BannerMenu::TYPE_PRODUCT])
				->update(['status' => BannerMenu::STATUS_DISABLED, 'entity_id' => null]);

		//Elimina banners del sitio asociados al producto eliminado
		BannerSite::where(['entity_id' => $model->id, 'type' => BannerSite::TYPE_PRODUCT])
				->update(['status' => BannerSite::STATUS_DISABLED, 'entity_id' => null]);

		//Elimina banners del home asociados al producto eliminado
		BannerHome::where(['entity_id' => $model->id, 'type' => BannerHome::TYPE_PRODUCT])
				->update(['status' => BannerHome::STATUS_DISABLED, 'entity_id' => null]);

		//Elimina banners de categorias asociados al producto eliminado
		BannerCategory::where(['entity_id' => $model->id, 'type' => BannerCategory::TYPE_PRODUCT])
				->update(['status' => BannerCategory::STATUS_DISABLED, 'entity_id' => null]);

		//Elimina carruseles asociados al producto eliminado
		Carrusel::where(['entity_id' => $model->id, 'type' => Carrusel::TYPE_PRODUCT])
				->update(['status' => Carrusel::STATUS_DISABLED, 'entity_id' => null]);
	}


	public static function search($q)
	{
		// $index = SearchIndex::whereRaw("MATCH(name,tags,color,description) AGAINST(?)", [$q])
		$index = SearchIndex::whereRaw("name like ? or tags like ? or color like ? or description like ? ", ['%'.$q.'%','%'.$q.'%','%'.$q.'%','%'.$q.'%'])
					->select('product_id', DB::raw(sprintf("MATCH(name,tags,color,description) AGAINST('%s') AS score", $q)))
					->limit(100)
					->orderBy('score', 'desc')
					->lists('product_id');

		$ids = $index->toArray();
		$joined = implode(',', $ids);
		
		//dump([
		//	'q'=> $q,
		//	'ids'=>$ids
		//]);
		
		
		if( $index->isEmpty() ) {
			$query = self::where(['id' => -1]);
			return $query;
		}
		
		$query = self::whereIn('id', $ids)
			->where('status', '=', self::STATUS_ACTIVE)
			->orderByRaw(DB::raw(sprintf("FIELD(id, %s)", $joined)));
			//->groupBy('reference_id');

		return $query;
	}
	
	public static function searchAndGroupByCategory($q)
	{
		$products = self::search($q)->get();
		
		// group by category
		$groups = [];
		foreach ($products as $product) {
			foreach($product->categories as $category) {	
				if( !isset($groups[$category->id]) ) {
					$banner = BannerCategory::where([
						'category_id' => $category->id,
						'location' => 'search'
					])->get();

					$group = new \stdClass();
					$group->category = $category;
					$group->products = collect([]);
					$group->banner = $banner;
					
					$groups[$category->id] = $group;
				}
				
				$collection = $groups[$category->id]->products;
				if( !$collection->contains($product) ) {
					$collection->push($product);
				}
			}
		}
		
		$results = new \stdClass();
		$results->groups = $groups;
		$results->products = $products;
		$results->count = count($products);
		return $results;
	}
	
	public static function updateTermSeach($q, $productId){
		$exists = TermSearch::where('term', $q)->first();
		
		if($exists){
			$exists->increment('number_search');
			$exists->save();	
		}else {
			$term_search = new TermSearch();
			$term_search->term = $q;
			$term_search->product_id = $productId;
			$term_search->number_search = 1;
			$term_search->save();
		}
	}

	public static function topTen()
	{
		$terms=TermSearch::orderBy('number_search', 'desc')
				->limit(10)
				->get();


		$relatedToTerms=[];
		$topTen=[];

		foreach ($terms as $term) {
			$relatedToTerms[$term->term]=self::search($term->term)->get();
		}

		for($i=0; $i<10; $i++){
			foreach ($relatedToTerms as $related) {
				if(count($topTen)<10 && isset($related[$i]) && !array_key_exists($related[$i]->id, $topTen)){
					$topTen[$related[$i]->id]=$related[$i];
				}
			}
			if(count($topTen)>=10) {
				break;
			}
		}

		return $topTen;
	}

	public static function mostSearched($storeId)
	{
		config(['database.default' => $storeId]);

		if( Cache::has('products_search_'.$storeId)){
			$result= Cache::get('products_search_'.$storeId, []);
		}
		else{
			$result = self::topTen();
			if(count($result)>0){
				$expiresAt = Carbon::now()->addMinutes(10);
				Cache::add('products_search_'.$storeId, $result, $expiresAt);
			}
		}
		return $result;
	}

}
