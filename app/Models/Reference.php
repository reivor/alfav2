<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{

	protected $table = 'references';

	const STATUS_DISABLED = 'disabled';
	const STATUS_ACTIVE = 'active';

	protected $fillable = [
		'code',
		'color_code',
		'name',
		'reference_id',
		'image',
		'dimensions',
		'color',
		'tags',
	];

	
	// --------------------- RELATIONS --------------------- //
	
	public function products()
	{
		return $this->hasMany('App\Models\Product', 'reference_id');
	}
	
	public function variations()
	{
		return $this->hasMany('App\Models\Product', 'reference_id');
	}
	
	
	// --------------------- STATIC --------------------- //
	
	public static function getReferenceList()
	{
		return self::where(['status' => self::STATUS_ACTIVE]);
	}

	
	// --------------------- PROPERTIES --------------------- //
	
	public function getImageUrlAttribute()
	{
		$product = $this->products()->first();
		if( !$product ) {
			$product = new Product();
		}
		return $product->imageUrl;
	}
	
	
	public function getDataSheetAttribute($dataSheet){
		if($dataSheet){
			$dataSheet = $dataSheet;
		}

		return $dataSheet;
	}

	public function getMaintenanceManualAttribute($maintenanceManual){
		if($maintenanceManual){
			$maintenanceManual = $maintenanceManual;
		}
		
		return $maintenanceManual;
	}

	public function getGuaranteeAttribute($guarantee){
		if($guarantee){
			$guarantee = $guarantee;
		}
	
		return $guarantee;
	}

	public function getUserManualAttribute($userManual){
		if($userManual){
			$userManual = $userManual;
		}
		
		return $userManual;
	}
	
	
	public function getQualitySeal()
	{
		$qualitySeals = [];
		$seals = explode(',', $this->quality_seal);

		foreach( $seals as $seal ) {
			$aux['text'] = $seal;
			$pdf = "";

			switch( $seal ) {
				case 01:
					$pdf = "/files/products/quality-seals/Sello de calidad 01.pdf";
					break;
				case 02:
					$pdf = "/files/products/quality-seals/Sello de calidad 02.pdf";
					break;
				case 03:
					$pdf = "/files/products/quality-seals/Sello de calidad 03.pdf";
					break;
				case 04:
					$pdf = "/files/products/quality-seals/Sello de calidad 04.pdf";
					break;
				case 05:
					$pdf = "/files/products/quality-seals/Sello de calidad 05.pdf";
					break;
				case 06:
					$pdf = "/files/products/quality-seals/Sello de calidad 06.pdf";
					break;
			}

			$aux['link'] = $pdf;
			$qualitySeals[] = $aux;
		}

		return $qualitySeals;
	}

}
