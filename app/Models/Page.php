<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Page extends Model
{
	const DEFAULT_PICTURE = '/images/placeholder.png';
	//const HEADER_ARTICLE_PICTURE = '/images/placeholderHeaderArticle.png';
	//const RELATED_ARTICLE_PICTURE = '/images/placeholderArticleRelated.png';
	//const HOME_ARTICLE_PICTURE = '/images/placeholderArticleHome.png';
	const STATUS_ACTIVE = 'active';
	const STATUS_DISABLED = 'disabled';
  
    protected $table="pages";

  //  protected $table = 'blog_articles';
	
	protected $fillable = ['title'];
	
	protected $hidden = ['created_at', 'updated_at'];
	
	protected $dates = ['date_publishing', 'created_at', 'updated_at'];
	

}
