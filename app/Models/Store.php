<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
	
	const DEFAULT_PICTURE = '/images/placeholder.png';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'name',
		'description',
		'location',
	];

	// --------------------- RELATIONS --------------------- //

	public function department(){
		return $this->belongsTo('App\Models\Department');
	}

	public function city(){
		return $this->belongsTo('App\Models\City');
	}
	
	
	// --------------------- STATIC --------------------- //
	
	public static function getActiveStores()
	{
		return self::where([]);
	}

	public static function search($options = [])
	{
		
		$criteria = [];
		
		if( isset($options['department']) && !empty($options['department']) ) {
			$criteria['department_id'] = $options['department'];
		}
		
		if( isset($options['city']) && !empty($options['city']) ) {
			$criteria['city_id'] = $options['city'];
		}
		
		$query = self::where($criteria);
		
		return $query;
	}
	
	// --------------------- METHODS --------------------- //
	
	public function getPictureUrlAttribute()
	{
        return $this->picture ? $this->picture : self::DEFAULT_PICTURE;
    }
	
	public function getImageMapAttribute()
	{
		return sprintf(
			'https://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=16&size=800x240&markers=color:red|label:%s|%f,%f',
			$this->latitude, $this->longitude, $this->name, $this->latitude, $this->longitude
		);
	}
	
}
