<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model{

    protected $table = 'products_images';

    public $timestamps = false;
	
	public function getImageUrlAttribute() {
		return $this->url ? $this->url : Product::DEFAULT_PICTURE;
	}
}
