<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerHome extends Model
{

	use BannersTrait;

	const TYPE_EXTERNAL = 'external';
	const TYPE_CATEGORY = 'category';
	const TYPE_PRODUCT = 'product';
	
	const STATUS_ACTIVE = 'active';
	const STATUS_DISABLED = 'disabled';
	
	const DEFAULT_PICTURE = '/images/placeholder.png';

	protected $table = 'banners_home';

	// --------------------- STATIC --------------------- //
	protected static function boot()
	{
		static::saving(function($model) {
			// set http
			if( $model->type == self::TYPE_EXTERNAL ) {
				$url = $model->external_url;
				if( $model->isUrl($url) ) {
					$prefix = substr($url, 0, 4);
					if( strtolower($prefix) !== 'http' ) {
						$model->external_url = "http://" . $model->external_url;
					}
				}
			}
		});
	}

	public function getImageUrlAttribute($value)
	{
		return $value ? $value : self::DEFAULT_PICTURE;
	}

	public static function getBannersHome()
	{//Data de los banners de home
		$bannersHome = [];
		$banners = self::where(['status' => self::STATUS_ACTIVE])->get();

		foreach( $banners as $banner ) {
			$bannersHome[$banner->position] = $banner;
		}
		
		return $bannersHome;
	}

}
