<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteOrder extends Model{
	
	const STATUS_OPEN = 'open';
	const STATUS_CLOSED = 'closed';
	const STATUS_NEW = 'new';
	
	public $timestamps = false;

	protected $table = 'quote_orders';
	
	protected $fillable = ['customer_id'];
	
	protected $dates = ['date_created'];
	
	// --------------------- RELATIONS --------------------- //
	
	public function quoteProducts()
	{
		return $this->belongsToMany('App\Models\Product', 'quote_products', 'quote_order_id', 'product_id');
	}
	
	public function customer()
	{
		return $this->belongsTo('App\Models\Customer', 'customer_id');
	}

	public function store()
	{
		return $this->belongsTo('App\Models\Store', 'store_id');
	}
	
	// --------------------- METHODS --------------------- //
	
	public static function getCurrentOrder($userId)
	{
		$currentOrder = self::firstOrNew(['customer_id'=> $userId, 'status' => self::STATUS_OPEN]);
		return $currentOrder;
	}
}
