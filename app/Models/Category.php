<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class Category extends Model
{

	const STATUS_PENDING = 'pending';
	const STATUS_ACTIVE = 'active';
	const STATUS_BLOCKED = 'disabled';
	const MAX_LEVEL = 4;
	const DEFAULT_PICTURE = '/images/placeholder.png';
	const DEFAULT_PICTURE_CATEGORY_DESKTOP = '/images/placeholderCategoryDesktop.png';
	const DEFAULT_PICTURE_CATEGORY_MOBILE = '/images/placeholderCategoryMobile.png';
	const DEFAULT_PICTURE_HEADER = '/images/placeholderHeader.png';
	const AMBIENTES_CATEGORY = 1;
	const CATEGORY_EXPERTS = "A00005";
	const CATEGORY_IDEAS = "A00006";
	const CATEGORY_COLECCIONES = "A00004";

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'categories';

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['name', 'description'];

	/**
	* The attributes excluded from the model's JSON form.
	*
	* @var array
	*/
	protected $hidden = ['created_at', 'updated_at'];
	protected $appends = ['detail_url'];

	// --------------------- STATIC --------------------- //

	/**

	* The booting method of the model.
	* @return void
	*/
	protected static function boot()
	{

		static::saving(function($model) {
			if( isset($model->name) ) {
				$model->slug = strtolower(str_slug($model->name, "-"));
			}
		});

		static::created(function($model) {
			// create code

			if( isset($model->id) ) {
				$model->code = "A" . str_pad($model->id, 5, "0", STR_PAD_LEFT);

				$model->save();
			}
		});

		static::deleted(function($model) {
			self::onDeleteCategory($model);
		});
	}


	public static function onDeleteCategory($model){
		//Elimina banners del menu asociados a la categoria eliminada
		BannerMenu::where(['entity_id' => $model->id, 'type' => BannerMenu::TYPE_PRODUCT])
		->update(['status' => BannerMenu::STATUS_DISABLED, 'entity_id' => null]);

		//Elimina banners del sitio asociados a la categoria eliminada
		BannerSite::where(['entity_id' => $model->id, 'type' => BannerSite::TYPE_PRODUCT])
		->update(['status' => BannerSite::STATUS_DISABLED, 'entity_id' => null]);

		//Elimina banners del home asociados a la categoria eliminada
		BannerHome::where(['entity_id' => $model->id, 'type' => BannerHome::TYPE_PRODUCT])
		->update(['status' => BannerHome::STATUS_DISABLED, 'entity_id' => null]);

		//Elimina banners de categorias asociados a la categoria eliminada
		BannerCategory::where(['entity_id' => $model->id, 'type' => BannerCategory::TYPE_PRODUCT])
		->update(['status' => BannerCategory::STATUS_DISABLED, 'entity_id' => null]);

		//Elimina carruseles asociados al producto a la categoria eliminada
		Carrusel::where(['entity_id' => $model->id, 'type' => Carrusel::TYPE_PRODUCT])
		->update(['status' => Carrusel::STATUS_DISABLED, 'entity_id' => null]);

	}

	public static function getTopLevelCategories()
	{
		return self::where([
			'status' => self::STATUS_ACTIVE,
			'parent' => null,
		]);
	}

	public static function buildCategoriesTree()
	{
		$tree = self::getTopLevelCategories()->get();
		foreach( $tree as &$leaf ) {
			self::getCategoriesTree($leaf);
		}
		return $tree;
	}

	public static function buildCategoriesTreeCache($storeId){

		config(['database.default' => $storeId]);

		if( Cache::has('categories_' . $storeId) ) {
			$tree = Cache::get('categories_' . $storeId, []);
		}
		else {
			$tree = self::buildCategoriesTree();
			if( count($tree) > 0 ) {
				$expiresAt = Carbon::now()->addMinutes(60);
				Cache::add('categories_' . $storeId, $tree, $expiresAt);
			}
		}
		return $tree;
	}

	public static function getCategoriesTree(&$category)
	{
		foreach( $category->categories as &$child ) {
			self::getCategoriesTree($child);
		}
		return $category;
	}

	public static function getCategoriesArray($category, &$array)
	{
		foreach( $category->categories as $child ) {
			self::getCategoriesArray($child, $array);
		}
		$array[] = $category;
	}

	public static function getLeafCategoriesArray($category, &$array)
	{
		if(count($category->categories))
			foreach( $category->categories as $child )
				self::getLeafCategoriesArray($child, $array);
		else{
			$flag = true;
			foreach ($array as $value) 
				if($value->slug == $category->slug) $flag = false;
			if($flag) $array[] = $category;
		}
	}

	public static function getCategoriesTreeParents(&$category)
	{
		if( $category->parentCategory != null ) {
			self::getCategoriesTreeParents($category->parentCategory);
		}
		return $category;
	}

	public static function getCategoriesArrayParents(&$category, &$array)
	{
		if( $category->parentCategory != null ) {
			self::getCategoriesArrayParents($category->parentCategory, $array);
		}
		$array[] = $category->slug;
	}

	public static function getBreadcrumbs($category, &$breadcrumbs)
	{
		if( $category->parentCategory != null ) {
			self::getBreadcrumbs($category->parentCategory, $breadcrumbs);
		}
		$breadcrumbs[] = $category;
		return $breadcrumbs;
	}

	public static function getChildCategories($childCategories, &$releatedCategories)
	{
		foreach( $childCategories as $childCategory ) {
			$releatedCategories['ids'][] = $childCategory->id;
			$releatedCategories['objects'][] = $childCategory;

			if( isset($childCategory->categories) ) {
				self::getChildCategories($childCategory->categories, $releatedCategories);
			}
		}
		return $releatedCategories;
	}

	// --------------------- METHODS --------------------- //

	public function categories()
	{
		return $this->hasMany('App\Models\Category', 'parent');
	}

	public function parentCategory()
	{
		return $this->belongsTo('App\Models\Category', 'parent');
	}

	//------------------------------------------------------------------------------------
	/* public function footerCategories(){
	return $this->belongsTo('App\Models\footerCategory', 'parent');
} */


public function getImageUrlAttribute($value){
	return $value ? $value : self::DEFAULT_PICTURE_CATEGORY_DESKTOP;
}

public function getImageMobileUrlAttribute($value){
	return $value ? $value : self::DEFAULT_PICTURE_CATEGORY_MOBILE;
}


public function getHeaderUrlAttribute($value){
	return $value ? $value : self::DEFAULT_PICTURE_HEADER;
}

public function isTwoColumns(){
	return (count($this->categories)==2);
}

public function isBig(){
	return (count($this->categories)>2);
}

public function isShort(){
	return (count($this->categories)==0);
}

public function isExpertsCategory(){
	return ($this->code==self::CATEGORY_EXPERTS);
}

public function isIdeasCategory(){
	return ($this->code==self::CATEGORY_IDEAS);
}

public function getDetailUrlAttribute(){
	return getUrlEntity($this);
}

public function parentOfCategory(){

	$id_parent = $this->parent;
	
	if(!$id_parent){
		return $this;
	}
	
	while(true){
		if($id_parent){
			$category = self::find($id_parent);
			$id_parent = $category->parent;
		}else{
			break;
		}
	}
	
	return $category;
}


}
