<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class BannerMenu extends Model{
	use BannersTrait;
	
	const TYPE_EXTERNAL = 'external';
	const TYPE_CATEGORY = 'category';
	const TYPE_PRODUCT = 'product';
	
	const STATUS_ACTIVE = 'active';
	const STATUS_DISABLED = 'disabled';

	protected $table = 'banners_menu';
	
	// --------------------- STATIC --------------------- //
	protected static function boot() {
		static::saving(function($model) {
			// set http
			if($model->type==self::TYPE_EXTERNAL){
				$url = $model->external_url;
				if($model->isUrl($url)){
					$prefix = substr($url,0,4);
					if(strtolower($prefix) !== 'http'){
						$model->external_url = "http://".$model->external_url;
					}	
				}
			}
		});
	}
	
	public static function getMenuProducts($id){
		$productCategory=self::where('category_id', '=' , $id)->get(['product_id']);
		$products=Product::where('status', '=', Product::STATUS_ACTIVE)->whereIn('id', $productCategory);
		return $products; 
	}
	
	public static function getBannersMenu(){
		$banners = self::where(['status' => self::STATUS_ACTIVE])->get();
		$bannersHome = [];
		
		foreach ($banners as $keyBanner => $banner) {
			$bannersHome[$banner->category_id][$banner->position] = $banner;
		}
		
		return $bannersHome;
	}

	public function product()
	{
		return $this->belongsTo('App\Models\Product', 'entity_id');
	}

	public function category()
	{
		return $this->belongsTo('App\Models\Category', 'entity_id');
	}

	public function getTextAttribute(){

		if ($this->type == "category"){
			return $this->category ? $this->category->name : "";
		}else if($this->type == "product"){
			return $this->product ? $this->product->name : "";
		}else{
			return "";
		}
		
	}
	
}
