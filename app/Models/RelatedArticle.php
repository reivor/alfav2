<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatedArticle extends Model{
    protected $table = 'related_articles';
    public $timestamps = false;

}