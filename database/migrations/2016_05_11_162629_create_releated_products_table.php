<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleatedProductsTable extends Migration
{
    
    public function up()
    {
        Schema::create('related_products', function(Blueprint $table) {
			$table->integer('product_id')
                    ->unsigned();
            $table->integer('product2_id')
                  ->unsigned();
			
            $table->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade');
            $table->foreign('product2_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade');
		});
    }

    public function down()
    {
        Schema::dropIfExists('related_products');
    }
}
