<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsUploads extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('products_uploads', function($table) {
			$table->string('original_file')
				->nullable();
		});
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('products_uploads', function($table) {
			$table->dropColumn('original_file');
		});
    }
}
