<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
			$table->string('code', 64);
            $table->string('name', 64);
			$table->string('display_name_first', 128);
			$table->string('display_name_second', 128);
            $table->text('description')
                    ->nullable();
			$table->integer('parent')
					->unsigned()
					->nullable();
			$table->smallInteger('level')
					->unsigned()
					->default(1);
			$table->string('image_name', 255)
				  ->nullable();
			$table->string('image_url', 255)
			      ->nullable();
			$table->string('header_name', 255)
				  ->nullable();
			$table->string('header_url', 255)
				  ->nullable();
			$table->string('slug', 64)
					->nullable();
            $table->string('tags', 128)
                    ->nullable();
            $table->enum('status', ['active', 'disabled'])
                    ->default('active');
			$table->timestamps();
			
			$table->foreign('parent')
                    ->references('id')
                    ->on('categories')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
