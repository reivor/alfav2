<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageMeta extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('page_meta', function(Blueprint $table){
            $table->increments('id');
            $table->enum('type',['title','meta','og']);
            $table->string('key')->nullable();
            $table->string('value',255)->nullable();
            $table->string('page')->nullable();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('page_meta');
    }
}
