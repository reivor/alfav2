<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlogSeo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_articles', function(Blueprint $table) {
			$table->string('title_plain', 255)
                    ->nullable();
			$table->string('tags', 128)
                    ->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_articles', function($table) {
			$table->dropColumn('title_plain');
			$table->dropColumn('tags');
		});
    }
}
