<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SliderHomeTable extends Migration{

    public function up(){
		Schema::create('slider_home', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('image_url');
			$table->enum('type', ['external', 'category', 'product']);
            $table->string('entity_id')
					 ->nullable();
			$table->string('external_url')
					 ->nullable();
			$table->enum('status', ['active', 'disabled'])
					->default('active');
			$table->timestamps();
		});
        
    }

    public function down(){
        
    }
}
