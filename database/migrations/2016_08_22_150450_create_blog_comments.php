<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogComments extends Migration{

    public function up(){
		Schema::create('blog_comments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
			$table->longText('comment');
			$table->integer('customer_id')
					->unsigned()
					->nullable();
			$table->integer('blog_comment_id')
					->unsigned()
					->nullable();
			$table->integer('blog_article_id')
					->unsigned()
					->nullable();
			$table->enum('status', ['active', 'disabled', 'rejected'])
                    ->default('active');
			$table->timestamps();
			
			$table->foreign('blog_comment_id')
                    ->references('id')
                    ->on('blog_comments')
                    ->onDelete('set null');
			$table->foreign('blog_article_id')
				->references('id')
				->on('blog_articles')
				->onDelete('set null');
        });
    }


    public function down(){
        Schema::dropIfExists('blog_comments');
    }
}
