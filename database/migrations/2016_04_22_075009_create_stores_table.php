<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location', 255)
					->nullable();
            $table->string('opening_hours', 64)
					->nullable();
            $table->string('email', 255)
					->nullable();
            $table->string('phone_local', 32)
					->nullable();
            $table->string('phone_mobile', 32)
					->nullable();
			$table->string('picture', 255)
					->nullable();
            $table->string('slug', 255)
					->nullable();
            $table->string('tags', 255)
                    ->nullable();
            $table->float('latitude', 12, 8)
                  ->nullable();
            $table->float('longitude', 12, 8)
                  ->nullable();
			$table->integer('department_id')
                    ->unsigned()
                    ->nullable();
            $table->integer('city_id')
                    ->unsigned()
                    ->nullable();
			$table->timestamps();
            $table->foreign('city_id')
                    ->references('id')
                    ->on('cities')
                    ->onDelete('set null');
            $table->foreign('department_id')
                    ->references('id')
                    ->on('departments')
                    ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
