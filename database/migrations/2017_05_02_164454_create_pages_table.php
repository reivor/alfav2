<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
       Schema::create('pages', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');    //id
            $table->string('title', 255); //title
            $table->longText('body'); //body
            $table->date('date_publishing') //date
                  ->nullable();
            $table->enum('status', ['active', 'disabled']) //estatus
                    ->default('active');
            $table->timestamps(); //timestamps
            $table->string('url')  //url
              ->nullable();
        });  
    }
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        //page undo create
        Schema::dropIfExists('pages');
    }
}