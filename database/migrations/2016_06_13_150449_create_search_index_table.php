<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchIndexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_index', function(Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->string('name');
            $table->string('tags', 128)
                ->nullable();
            $table->string('color')
                ->nullable();
            $table->text('description')
                ->nullable();
        });

        // Here we create the FULLTEXT index
        DB::statement('ALTER TABLE search_index ADD FULLTEXT search(name,tags,color,description)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop the index before dropping the table
        Schema::table('search_index', function($table) {
            $table->dropIndex('search');
        });
        Schema::drop('search_index');
    }
}
