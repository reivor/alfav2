<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.....
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code', 16);
            $table->char('color_code', 8);
            $table->string('name', 64);
            $table->integer('reference_id')->unsigned();  
            $table->string('image') 
                    ->nullable();
            $table->string('dimensions', 64)
                    ->nullable();
            $table->string('color', 64)
                    ->nullable();
            $table->string('slug', 64)
					->nullable();
            $table->string('tags', 128)
                    ->nullable();
            $table->enum('status', ['active', 'disabled'])
                    ->default('active');
            $table->text('description')
                ->nullable();
            $table->integer('views')
                ->nullable()
                ->default(0);
            $table->string('ambience', 64)
                ->nullable();
            $table->string('enforcement', 64)
                ->nullable();
            $table->string('use', 64)
                ->nullable();
            $table->string('data_sheet', 64)
                ->nullable();
            $table->string('user_manual', 64)
                ->nullable();
            $table->string('maintenance_manual', 64)
                ->nullable();
            $table->string('guarantee', 64)
                ->nullable();
            $table->string('quality_seal', 64)
                ->nullable();
            $table->boolean('featured')
                ->nullable()
                ->default(false);
            $table->boolean('is_new')
                ->nullable()
                ->default(false);
            $table->timestamps();
            $table->foreign('reference_id')
                    ->references('id')
                    ->on('references')
                    ->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
