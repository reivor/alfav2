<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteOrderTable extends Migration{

    public function up(){
        Schema::create('quote_orders', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('customer_id')
				  ->unsigned();
			$table->enum('status', ['open', 'closed']);
			$table->dateTime('date_created')
				  ->nullable();
			$table->dateTime('date_closed')
				  ->nullable();
			
			$table->foreign('customer_id')
				  ->references('id')
				  ->on('customers')
				  ->onDelete('cascade');
		});
    }

  
    public function down(){
        Schema::dropIfExists('quote_orders');
    }
}
