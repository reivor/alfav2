<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_uploads', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')
                ->unsigned();
            $table->integer('total_success');
            $table->integer('total_error');
            $table->integer('total_warnings');
            $table->string('file');
            $table->string('file_log');
            $table->string('status');
            $table->dateTime('fecha_upload');
            $table->dateTime('fecha_process_start');
            $table->dateTime('fecha_process_end');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_uploads');
    }
}
