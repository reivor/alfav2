<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraColumnsCarruselTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carrusel_home', function ($table) {
            $table->string('linked_to')
					 ->nullable();
            $table->string('url_category')
					 ->nullable();
            $table->string('url_product')->nullable()
					 ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carrusel_home');
    }
}
