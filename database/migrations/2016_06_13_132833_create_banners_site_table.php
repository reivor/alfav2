<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersSiteTable extends Migration{

	public function up(){
        Schema::create('banners_site', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('image_url')
				  ->nullable();
			$table->enum('location', ['homeTop', 'homeFooter', 'subhome', 'register', 'blog'])
				  ->nullable();
			$table->enum('type', ['external', 'category', 'product']);
			$table->integer('position')
                  ->unsigned()
			      ->nullable();
            $table->integer('entity_id')
                  ->unsigned()
			      ->nullable();
			$table->string('external_url')
				  ->nullable();
			$table->enum('status', ['active', 'disabled'])
                    ->default('active');
			$table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('banners_site');
    }
}
