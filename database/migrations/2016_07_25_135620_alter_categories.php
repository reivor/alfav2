<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCategories extends Migration{

    public function up(){
        Schema::table('categories', function ($table) {
			 $table->string('image_mobile_url', 255);
		});
    }

    public function down(){
        
    }
}
