<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if( !Schema::hasTable('users') ) {
			Schema::create('users', function(Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('name');
				$table->string('email')->unique();
				$table->char('password', 60)
					->nullable();
				$table->string('avatar', 255)
					->nullable();
				$table->enum('role', ['superadmin', 'admin', 'guest'])
					->nullable()
					->default('guest');
				$table->timestamp('last_login')
					->nullable();
				$table->char('registered_store', 2)
					->nullable();
				$table->enum('status', ['active', 'pending', 'blocked'])
					->nullable()
					->default('pending');
				$table->rememberToken();
				$table->char('activation_token', 60)
					->nullable();
				$table->timestamps();
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('users');
    }
}
