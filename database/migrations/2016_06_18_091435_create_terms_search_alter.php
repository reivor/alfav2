<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsSearchAlter extends Migration{

    public function up(){
        Schema::table('terms_search', function ($table) {
			 $table->integer('product_id')
				   ->unsigned();
			 
			 $table->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade');
		});
    }


    public function down(){
        
    }
}
