<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersMenu extends Migration{
    
    public function up(){
        Schema::create('banners_menu', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('image_url')
				  ->nullable();
			$table->enum('type', ['external', 'category', 'product']);
            $table->integer('entity_id')
                  ->unsigned()
			      ->nullable();
			$table->string('external_url')
				  ->nullable();
			$table->integer('category_id')
				  ->unsigned()
				  ->nullable();
			$table->integer('position')
				  ->nullable()
                  ->unsigned();
			$table->enum('status', ['active', 'disabled'])
                    ->default('active');
			$table->timestamps();
        });
    }

    public function down(){
         Schema::dropIfExists('banners_menu');
    }
}
