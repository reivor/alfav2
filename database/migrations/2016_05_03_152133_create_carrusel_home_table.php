<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarruselHomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if( !Schema::hasTable('carrusel_home') ) {
			Schema::create('carrusel_home', function(Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('name')->unique();
				$table->string('image_url');
				$table->string('url')
						 ->nullable();;
				$table->enum('status', ['active', 'disabled'])
						->default('active');
				$table->timestamps();
			});
		}
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('carrusel_home');
    }
}