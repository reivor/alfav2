<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBlogArticles extends Migration{
    public function up(){
		Schema::table('blog_articles', function ($table) {
			 $table->string('image_home', 255)
				  ->nullable();
		});
    }

    public function down(){
		Schema::table('blog_articles', function ($table) {
			$table->dropColumn('image_home');
		});
    }
}
