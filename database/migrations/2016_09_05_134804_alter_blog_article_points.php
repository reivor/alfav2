<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBlogArticlePoints extends Migration{
    public function up(){
        Schema::table('blog_article_points', function(Blueprint $table) {
			$table->dropColumn('x_coordinate');
			$table->dropColumn('y_coordinate');
			$table->integer('top')
				  ->unsigned()
				  ->nullable();
			$table->integer('right')
				  ->unsigned()
				  ->nullable();
			$table->integer('bottom')
				  ->unsigned()
				  ->nullable();
			$table->integer('left')
				  ->unsigned()
				  ->nullable();
			$table->integer('offset_x')
				  ->nullable();
			$table->integer('offset_y')
				  ->nullable();
		});
    }


    public function down(){
        Schema::table('blog_article_points', function($table) {
			$table->dropColumn('top');
			$table->dropColumn('right');
			$table->dropColumn('bottom');
			$table->dropColumn('left');
		});
    }
}
