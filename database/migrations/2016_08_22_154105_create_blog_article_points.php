<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogArticlePoints extends Migration{

    public function up(){
		Schema::create('blog_article_points', function(Blueprint $table) {
			$table->increments('id');
			$table->float('x_coordinate');
			$table->float('y_coordinate');
			$table->string('nearest_point')
				  ->nullable();
			$table->integer('blog_article_id')
				->unsigned();
			$table->integer('product_id')
				->unsigned();
			
			$table->foreign('blog_article_id')
				->references('id')
				->on('blog_articles')
				->onDelete('cascade');
			$table->foreign('product_id')
				->references('id')
				->on('products')
				->onDelete('cascade');
		});
    }

 
    public function down(){
		Schema::dropIfExists('blog_article_points');
    }
}
