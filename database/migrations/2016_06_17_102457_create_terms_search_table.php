<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsSearchTable extends Migration{

    public function up(){
        Schema::create('terms_search', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('term');
			$table->timestamp('last_search');
            $table->integer('number_search')
                  ->unsigned()
			      ->default(1);
        });
    }

    public function down(){
        Schema::dropIfExists('terms_search');
    }
}
