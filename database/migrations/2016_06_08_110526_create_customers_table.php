<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( !Schema::hasTable('customers') ) {
            Schema::create('customers', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->string('lastname');
                $table->string('email')->unique();
                $table->char('password', 60)
                    ->nullable();
                $table->string('avatar', 255)
                    ->nullable();
                $table->enum('role', ['customer'])
                    ->nullable()
                    ->default('customer');
                $table->string('fb_id')
                    ->nullable(); 
                $table->string('tw_id')
                    ->nullable();
                $table->string('job')
                    ->nullable();
                $table->timestamp('last_login')
                    ->nullable();
                $table->char('registered_store', 2)
                    ->nullable();
                $table->enum('status', ['active', 'pending', 'blocked'])
                    ->nullable()
                    ->default('pending');
                $table->rememberToken();
                $table->char('activation_token', 60)
                    ->nullable();
                $table->timestamps();
                $table->boolean('subscribe')
                ->nullable()
                ->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
