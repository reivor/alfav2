<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteProductTable extends Migration{

    public function up(){
        Schema::create('quote_products', function(Blueprint $table) {
			$table->integer('quote_order_id')
                    ->unsigned();
            $table->integer('product_id')
                  ->unsigned();
			
            $table->foreign('quote_order_id')
                    ->references('id')
                    ->on('quote_orders')
                    ->onDelete('cascade');
            $table->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade');
		});
    }

    public function down(){
         Schema::dropIfExists('quote_products');
    }
}
