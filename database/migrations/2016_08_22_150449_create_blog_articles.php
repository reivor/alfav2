<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogArticles extends Migration{

    public function up(){
        Schema::create('blog_articles', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
			$table->string('title', 255);
			$table->string('image_header', 255)
				  ->nullable();
			$table->string('image_related', 255)
				  ->nullable();
			$table->string('image_default', 255)
				  ->nullable();
			$table->longText('body');
			$table->integer('blog_category_id')
				  ->unsigned()
				  ->nullable();
			$table->date('date_publishing')
				  ->nullable();
			$table->enum('status', ['active', 'disabled'])
                    ->default('active');
			$table->timestamps();
			
			$table->foreign('blog_category_id')
				->references('id')
				->on('blog_categories')
				->onDelete('set null');
        });  
    }


    public function down(){
		Schema::dropIfExists('blog_articles');
    }
}
