<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersCategoriesTable extends Migration{

    public function up(){
        Schema::create('banners_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('category_id')
                  ->unsigned()
			      ->nullable();
			$table->string('image_url')
				  ->nullable();
			$table->enum('location', ['detail', 'search'])
				  ->nullable();
			$table->enum('type', ['external', 'category', 'product']);
			$table->integer('position')
                  ->unsigned()
			      ->nullable();
            $table->integer('entity_id')
                  ->unsigned()
			      ->nullable();
			$table->string('external_url')
				  ->nullable();
			$table->enum('status', ['active', 'disabled'])
                    ->default('active');
			$table->timestamps();
        });
    }


    public function down(){
        Schema::dropIfExists('banners_categories');
    }
}
