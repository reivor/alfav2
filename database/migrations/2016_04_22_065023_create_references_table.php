<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferencesTable extends Migration
{
   
    public function up()
    {
        Schema::create('references', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description')
					->nullable();
			$table->integer('views')
					->default(0);
			$table->string('ambience')
					->nullable();
			$table->string('enforcement')
					->nullable();
			$table->string('use')
					->nullable();
			$table->string('data_sheet')
					->nullable();
			$table->string('user_manual')
					->nullable();
			$table->string('maintenance_manual')
					->nullable();
			$table->string('guarantee')
					->nullable();
			$table->string('quality_seal',50)
					->nullable();
			$table->boolean('featured')
					->default(false);
			$table->boolean('is_new')
					->default(false);
			$table->enum('status', ['active', 'disabled'])
                    ->default('active');
			$table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('references');
    }
}
