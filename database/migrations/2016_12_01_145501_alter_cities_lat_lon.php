<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCitiesLatLon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities',function($table){
            $table->float('lat')->nullable;
            $table->float('lon')->nullable;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities',function(Blueprint $table){
            $table->dropColumn('lat');
            $table->dropColumn('lon');
        });
    }
}
