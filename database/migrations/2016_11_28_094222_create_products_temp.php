<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTemp extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
		
        Schema::create('products_temp', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
			$table->integer('upload_id')
				->unsigned();
			$table->integer('line_number')
				->unsigned();
			$table->boolean('co')
                ->nullable()
                ->default(false);
			$table->boolean('ec')
                ->nullable()
                ->default(false);
			$table->boolean('us')
                ->nullable()
                ->default(false);
			$table->string('product_line', 64)
				->nullable();
            $table->string('code', 16)
				->nullable();
            $table->char('color_code', 8)
				->nullable();
            $table->string('name', 64)
				->nullable();
            $table->string('dimensions', 64)
                ->nullable();
            $table->string('color', 64)
                ->nullable();
            $table->string('slug', 64)
				->nullable();
            $table->string('tags', 128)
                ->nullable();
            $table->enum('status', ['active', 'disabled'])
                ->default('active');
            $table->text('description')
                ->nullable();
            $table->integer('views')
                ->nullable()
                ->default(0);
            $table->string('ambience', 64)
                ->nullable();
            $table->string('enforcement', 64)
                ->nullable();
            $table->string('use', 64)
                ->nullable();
            $table->string('data_sheet', 64)
                ->nullable();
            $table->string('user_manual', 64)
                ->nullable();
            $table->string('maintenance_manual', 64)
                ->nullable();
            $table->string('guarantee', 64)
                ->nullable();
            $table->string('quality_seal', 64)
                ->nullable();
            $table->boolean('featured')
                ->nullable()
                ->default(false);
            $table->boolean('is_new')
                ->nullable()
                ->default(false);
			$table->string('related')
				->nullable();
			$table->string('categories')
				->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_temp');
    }
}
