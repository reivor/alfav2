<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuoteOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quote_orders',function($table){
            $table->integer('store_id')
                ->unsigned();
            $table->string('manager', 255)
                ->nullable();
            $table->text('comment')
                ->nullable();
        });

        DB::statement("ALTER TABLE quote_orders CHANGE COLUMN status status ENUM('open','new','sent','received','noinfo','done') DEFAULT 'new' NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quote_orders',function(Blueprint $table){
            $table->dropColumn('store_id');
            $table->dropColumn('manager');
            $table->dropColumn('comment');
        });

        DB::statement("ALTER TABLE quote_orders CHANGE COLUMN status status ENUM('open','closed') DEFAULT 'open' NOT NULL");
    }
}
