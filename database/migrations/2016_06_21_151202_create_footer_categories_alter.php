<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFooterCategoriesAlter extends Migration{

    public function up(){
        Schema::table('footer_categories', function ($table) {
			 $table->integer('supreme_parent')
				   ->unsigned();
		});
    }

    public function down(){
		
    }
}
