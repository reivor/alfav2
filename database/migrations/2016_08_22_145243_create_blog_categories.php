<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCategories extends Migration{

    public function up(){
        Schema::create('blog_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
			$table->string('name');
			$table->string('color')
                    ->nullable();
			$table->enum('status', ['active', 'disabled'])
                    ->default('active');
			$table->timestamps();
        });
    }


    public function down(){
		Schema::dropIfExists('blog_categories');
    }
}
