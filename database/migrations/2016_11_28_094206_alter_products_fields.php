<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsFields extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table) {
			$table->string('product_line', 64);
			$table->string('ambience', 64)
					->nullable();
			$table->string('enforcement', 64)
					->nullable();
			$table->string('use', 64)
					->nullable();
			$table->string('data_sheet', 64)
					->nullable();
			$table->string('user_manual', 64)
					->nullable();
			$table->string('maintenance_manual', 64)
					->nullable();
			$table->string('guarantee', 64)
					->nullable();
			$table->string('quality_seal', 64)
					->nullable();
			$table->boolean('is_new')
					->default(false);
		});
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table) {
			$table->dropColumn('product_line');
			$table->dropColumn('ambience');
			$table->dropColumn('enforcement');
			$table->dropColumn('use');
			$table->dropColumn('data_sheet');
			$table->dropColumn('user_manual');
			$table->dropColumn('maintenance_manual');
			$table->dropColumn('guarantee');
			$table->dropColumn('quality_seal');
			$table->dropColumn('is_new');
		});
    }
}
