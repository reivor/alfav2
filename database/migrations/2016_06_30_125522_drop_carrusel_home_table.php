<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropCarruselHomeTable extends Migration{

    public function up(){
        Schema::dropIfExists('carrusel_home');
    }

    public function down(){
        
    }
}
