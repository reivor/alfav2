<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSearchIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('search_index', function ($table) {
			 $table->integer('product_id')
				->unsigned();
			 $table->foreign('product_id')
				->references('id')
				->on('products')
				->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('search_index', function ($table) {
			$table->dropForeign('product_id');
			$table->dropColumn('product_id');
		});
    }
}
