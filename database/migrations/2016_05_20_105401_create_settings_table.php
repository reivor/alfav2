<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration{

    public function up(){
        Schema::create('settings', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->string('name');
			$table->string('value');
			$table->primary('name');
        });
    }


    public function down(){
        Schema::dropIfExists('settings');
    }
}
