<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers',function($table){
            $table->integer('city')
                ->unsigned();
            $table->string('residence', 255)
                ->nullable();
            $table->string('identification', 255)
                ->nullable();
            $table->string('cell_phone', 255)
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities',function(Blueprint $table){
            $table->dropColumn('city');
            $table->dropColumn('residence');
            $table->dropColumn('identification');
            $table->dropColumn('cell_phone');
        });
    }
}
