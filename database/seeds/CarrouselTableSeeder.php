<?php

use Illuminate\Database\Seeder;
use App\Models\Carrusel;

class CarrouselTableSeeder extends Seeder
{

	protected $records = [
		[
			'name' => 'Banner Publicitario Externo',
			'url' => 'https://www.decohunter.com/',
			'image_url' => '',
			'status' => Carrusel::STATUS_ACTIVE,
		],
		[
			'name' => 'Banner Publicitario 2',
			'url' => 'https://www.google.com/',
			'image_url' => '',
			'status' => Carrusel::STATUS_ACTIVE,
		],
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			Carrusel::create($record);
		}
    }
}
