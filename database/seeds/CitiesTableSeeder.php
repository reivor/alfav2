<?php

use Illuminate\Database\Seeder;
use App\Models\City;

class CitiesTableSeeder extends Seeder
{

    protected $records = [
        [
            'name' => 'Barranquilla',
			'country_id' => '1',
			'department_id' => '1'
        ],
        [
            'name' => 'Barranquilla',
            'country_id' => '1',
            'department_id' => '2',
        ],
        [
            'name' => 'Bogotá',
            'country_id' => '1',
            'department_id' => '3',
        ],
		[
            'name' => 'Bogotá',
            'country_id' => '1',
            'department_id' => '1',
        ],
		[
            'name' => 'Bogotá',
            'country_id' => '1',
            'department_id' => '4',
        ],
		[
            'name' => 'Bogotá',
            'country_id' => '1',
            'department_id' => '2',
        ],
		[
            'name' => 'Bucaramanga',
            'country_id' => '1',
            'department_id' => '1',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			$city = City::firstOrNew(['name' => $record['name'], 'department_id' => $record['department_id']]);
			$city->name = $record['name'];
			$city->country_id = $record['country_id'];
			$city->department_id = $record['department_id'];
			$city->save();
        }
    }
}
