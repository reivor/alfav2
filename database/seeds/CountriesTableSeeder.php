<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountriesTableSeeder extends Seeder
{

    protected $records = [
        [
            'id' => 1,
			'name' => 'Colombia',
            'code' => 'CO',
        ],
        [
			'id' => 2,
            'name' => 'Estados Unidos',
            'code' => 'US',
        ],
        [
			'id' => 3,
            'name' => 'Ecuador',
            'code' => 'EC',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			$country = Country::firstOrNew(['code' => $record['code']]);
			$country->name = $record['name'];
			$country->code = $record['code'];
            $country->save();
        }
    }
}
