<?php

use Illuminate\Database\Seeder;
use App\Models\Carrusel;

class CarrouselTableSeeder3 extends Seeder
{

	protected $records = [
		[
			'name' => 'Banner Promo 3',
			'url' => 'https://www.hotmail.com/',
			'image_url' => '',
			'status' => Carrusel::STATUS_INACTIVE,
		],
		[
			'name' => 'Banner Producto Destacado',
			'url' => 'https://www.bybeeconcept.com/',
			'image_url' => '',
			'status' => Carrusel::STATUS_INACTIVE,
		],
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			Carrusel::create($record);
		}
    }
}
