<?php

use Illuminate\Database\Seeder;
use App\Models\Carrusel;

class CarrouselTableSeeder2 extends Seeder
{

	protected $records = [
		[
			'name' => 'Banner Promo',
			'url' => 'https://www.decohunter.com/',
			'image_url' => '',
			'status' => Carrusel::STATUS_INACTIVE,
		],
		[
			'name' => 'Banner Conocenos',
			'url' => 'https://www.bybeeconcept.com/',
			'image_url' => '',
			'status' => Carrusel::STATUS_INACTIVE,
		],
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			Carrusel::create($record);
		}
    }
}
