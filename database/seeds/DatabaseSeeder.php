<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// $this->call(UsersSeeder::class);
		// $this->call(CategoriesSeeder::class);
		// $this->call(CountriesTableSeeder::class);
		// $this->call(DepartmentsTableSeeder::class);
    // $this->call(CitiesTableSeeder::class);
		$this->call(GeographicalCoordinatesCities::class);
    }
}
