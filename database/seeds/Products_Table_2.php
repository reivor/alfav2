<?php

use Illuminate\Database\Seeder;
use App\Models\References;

class Products_Table_2 extends Seeder
{

		protected $records = [
		[
			'name' => 'MADERA',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => 'Residencial',
			'enforcement' => 'Piso',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '03',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'FRAKTAL',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => 'Residencial',
			'enforcement' => 'Piso',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '05',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'HD NEW YORK',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => 'Residencial',
			'enforcement' => 'Piso',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'NEW YORK',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'CAÑA FLECHA',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'DAKOTA',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'BEIRUT',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'ONDAS',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'STONE',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'ANTIQUE',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'VENATO',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'VESUBIO',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'CAIRO',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
			[
			'name' => 'NATAL',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
			[
			'name' => 'GARDEN JET',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
	];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			References::create($record);
		}
    }
}
