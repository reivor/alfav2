<?php

use Illuminate\Database\Seeder;
use App\Models\References;

class Products_Table extends Seeder
{

	protected $records = [
		[
			'name' => 'CREMA SELECTA',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => 'Residencial',
			'enforcement' => 'Piso',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'DANUBIO',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => 'Residencial',
			'enforcement' => 'Piso',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'PIEDRA CID',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => 'Residencial',
			'enforcement' => 'Piso',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
		[
			'name' => 'BLANCO',
			'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.',
			'views' => '',
			'ambience' => '',
			'enforcement' => 'Pared',
			'use' => '',
			'data_sheet' => '',
			'user_manual' => '',
			'maintenance_manual' => '',
			'guarantee' => '',
			'quality_seal' => '01',
			'featured' => '0',
			'is_new' => '0',
			'status' => References::STATUS_ACTIVE,
		],
	];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			References::create($record);
		}
    }
}
