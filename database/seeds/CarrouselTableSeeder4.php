<?php

use Illuminate\Database\Seeder;
use App\Models\Carrusel;

class CarrouselTableSeeder4 extends Seeder
{

	protected $records = [
		[
			'name' => 'Banner Promo Especial',
			'url' => 'https://www.decohunter.com/',
			'image_url' => '',
			'status' => Carrusel::STATUS_DISABLED,
		],
		[
			'name' => 'Banner Producto Destacado 2',
			'url' => 'https://www.bybeeconcept.com/',
			'image_url' => '',
			'status' => Carrusel::STATUS_DISABLED,
		],
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			Carrusel::create($record);
		}
    }
}