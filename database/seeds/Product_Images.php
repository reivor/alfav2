<?php

use Illuminate\Database\Seeder;
use App\Models\ProductImages;

class Product_Images extends Seeder
{

	protected $records = [
	[
	'filename' => '45X45_1',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '1'
	],
	[
	'filename' => '45X45_2',
	'path' => '',
	'url' => '', 
	'type' => 'jpg',
	'product_id' => '1'
	],
	[
	'filename' => '45X45_2',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '1'
	],
	[
	'filename' => '45X45_3',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '1'
	],
	[
	'filename' => '45X45_4',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '1'
	],
	[
	'filename' => '45X45_5',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '1'
	],
	[
	'filename' => '45X45_6',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '1'
	],
	[
	'filename' => '45X45_7',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '1'
	],
	[
	'filename' => '51X51_1',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '2'
	],
	[
	'filename' => '51X51_2',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '2'
	],
	[
	'filename' => '51X51_3',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '2'
	],
	[
	'filename' => '51X51_4',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '2'
	],
	[
	'filename' => '51X51_5',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '2'
	],
	[
	'filename' => '51X51_6',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '2'
	],
	[
	'filename' => '51X51_7',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '2'
	],
	[
	'filename' => '51X51_8',
	'path' => '',
	'url' => '',
	'type' => 'jpg',
	'product_id' => '2'
	],
	];



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach($this->records as $record) {
    		ProductImages::create($record);
    	}
    }
}
