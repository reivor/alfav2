<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class Product_Variations extends Seeder
{

	protected $records = [
	[
	'code' => '225014387',
	'color_code' => '',
	'name' => 'HD CREMA SELECTA 45X45-1',
	'reference_id' => '1',
	'image' => 'photo.jpg',
	'dimensions' => '45X45',
	'color' => 'Crema',
	'tags' => '',
	'status' => Product::STATUS_ACTIVE,
	],
	[
	'code' => '225016470',
	'color_code' => '',
	'name' => 'HD CREMA SELECTA 51X51-1',
	'reference_id' => '1',
	'image' => 'photo51x51.jpg',
	'dimensions' => '51X51',
	'color' => 'Crema',
	'tags' => '',
	'status' => Product::STATUS_ACTIVE,
	],
	[
	'code' => '225014334',
	'color_code' => '',
	'name' => 'HD DANUBIO BLANCO 30.5X60-1',
	'reference_id' => '2',
	'image' => 'photo.jpg',
	'dimensions' => '30.5X60',
	'color' => 'Blanco ',
	'tags' => '',
	'status' => Product::STATUS_ACTIVE,
	],
	[
	'code' => '225014337',
	'color_code' => '',
	'name' => 'HD DANUBIO RATAN 30.5X60-1',
	'reference_id' => '2',
	'image' => 'photodanubio.jpg',
	'dimensions' => '30.5X60',
	'color' => 'Blanco ',
	'tags' => '',
	'status' => Product::STATUS_ACTIVE,
	],
	];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach($this->records as $record) {
    		Product::create($record);
    	}
    }
}
