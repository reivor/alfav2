<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
	
	protected $records = [
		[
			'id' => 1,
			'name' => 'Administrador',
			'email' => 'admin@bybeeconcept.com',
			'password' => 'adminadmin',
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_ACTIVE
		],
		[
			'id' => 2,
			'name' => 'Ilbert Esculpi',
			'email' => 'ilbert.esculpi@bybeeconcept.com',
			'password' => '123456',
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_ACTIVE
		],
		[
			'id' => 3,
			'name' => 'Daniel Rivas P.',
			'email' => 'daniel.rivas@localhost.com',
			'password' => 'qwerty',
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_BLOCKED
		],
		[
			'id' => 4,
			'name' => 'Adriana Onsalo',
			'email' => 'adriana.onsalo@localhost.com',
			'password' => 'qwerty',
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_ACTIVE
		],
		[
			'id' => 5,
			'name' => 'Christian Flores',
			'email' => 'christian.flores@localhost.com',
			'password' => 'qwerty',
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_ACTIVE
		],
		[
			'id' => 6,
			'name' => 'Miguel A. Corona',
			'email' => 'miguel.corona@localhost.com',
			'password' => null,
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_PENDING
		],
		[
			'id' => 7,
			'name' => 'Roberto Seijas',
			'email' => 'roberto.seijas@localhost.com',
			'password' => null,
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_PENDING
		],
		[
			'id' => 8,
			'name' => 'Darcy Morales',
			'email' => 'darcy.morales@localhost.com',
			'password' => null,
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_PENDING
		],
		[
			'id' => 9,
			'name' => 'Milagros Otaiza',
			'email' => 'milagros.otaiza@localhost.com',
			'password' => null,
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_PENDING
		],
		[
			'id' => 10,
			'name' => 'Kelly Briceño',
			'email' => 'kelly.briceno@localhost.com',
			'password' => null,
			'avatar' => User::DEFAULT_PICTURE,
			'role' => User::ROLE_ADMIN,
			'status' => User::STATUS_PENDING
		],
	];
	
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			$user = User::firstOrNew(['email' => $record['email']]);
			$user->name = $record['name'];
			$user->email = $record['email'];
			$user->password = bcrypt(($record['password']));
			$user->avatar = $record['avatar'];
			$user->role = $record['role'];
			$user->status = $record['status'];
			$user->save();
		}
    }
}
