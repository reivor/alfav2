<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentsTableSeeder extends Seeder
{

    protected $records = [
        [
			'id' => '1',
            'name' => 'Norte',
            'country_id' => '1',
        ],
        [
			'id' => '2',
            'name' => 'Sur',
            'country_id' => '1',
        ],
        [
			'id' => '3',
            'name' => 'Occidente',
            'country_id' => '1',
        ],
		[
			'id' => '4',
            'name' => 'Centro',
            'country_id' => '1',
        ],
		[
			'id' => '5',
            'name' => 'Suroriente',
            'country_id' => '1',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->records as $record) {
			$department = Department::firstOrNew(['name' => $record['name'], 'country_id' => $record['country_id']]);
			$department->name = $record['name'];
			$department->country_id = $record['country_id'];
            $department->save();
        }
    }
}
