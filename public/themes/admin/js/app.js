Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {

    switch (operator) {
        case '==':
        return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
        return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '<':
        return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
        return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
        return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
        return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
        return options.inverse(this);
    }
});


$(function() {
    var $opt = $('.option');
   
    //$opt.first().show();
    $('input[type=radio]').on('change',function() {
        $opt.hide();
        $opt.eq( $('input[type=radio]').index( this ) ).show();
    });
});

$('input[type=radio]').on('change',function() {
    if($(this).val=="link"){
        $('#url').attr('required');
    } else {
        $('#url').removeAttr('required');
    }
});

$(document).ready(function(){
    if ($("#linked_to_1").is(':checked')){
        $("#form-cat").show();
    }
    if ($("#linked_to_2").is(':checked')){
        $("#form-prod").show();
    }
    if ($("#linked_to_3").is(':checked')){
        $("#form-url").show();
    }
});


