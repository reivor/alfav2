
var map, marker, position;

function setupMapEditable() {

	if( !position ) {
		position = {lat: 4.6482976, lng: -74.107807};
	}

	var mapOptions = {
		center: position,
		zoom: 11
	};

	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	marker = new google.maps.Marker({
		position: position,
		map: map,
		draggable:true,
		title: 'Arrastre para definir la posicion del concesionario.'
	});

}
