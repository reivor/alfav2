
var isEmail = function(emailAddress) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(emailAddress);
};

var requireLogin = function () {
    if( !isLogin() ) {
        $('html, body').animate({scrollTop: 0}, "slow");
        if ($(".menu-desktop").is(':visible')) {
            $('#i-seccion').collapse('show');
        }
		else {
            $('#modal-login').modal('show');
        }
        return false;
    }
    return true;
};

$(document).ready(function() {
	
    $('.btn-form-login').click(function (event) {
        showLoader();
        event.preventDefault();
        var idForm = $(this).data('form');
        var dataForm = $('#' + idForm).serializeArray();

        $.post("/users/login", dataForm)
			.done(function(data) {
				hideLoader();
				if (data['result'] === 'true') {
					if( data['redirect'] ) {
						window.location.href = data['redirect'];
					}
					else {
						location.reload();
					}
				}
				else {
					var message=data['message'];
					showAlert(message);
				}
			});
    });
	
	$('#btn-recovery-pass').click(function () {
        var data_recovery = $('#form_recovery_pass').serializeArray();
        $.post('/recoverypassword', data_recovery)
			.done(function(data) {
				if( data['result'] === 'true' ) {
					$('#modal-aceptar').modal('show');
				}
				else {
					var message = data['message'];
					showAlert(message);
				}
			});
    });
	
	 $('#btn-recovery-pass-mobile').click(function () {
        var data_recovery = $('#form_recovery_pass-mobile').serializeArray();
        $.post('/recoverypassword', data_recovery)
			.done(function (data) {
				if (data['result'] === 'true') {
					$('#modal-aceptar-mobile').modal('show');
				}
				else {
					var message = data['message'];
					showAlert(message);
				}
			});
    });
	
});