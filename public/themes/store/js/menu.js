var modalMenu, currentMenu;

var quitMenuItemsActive = function() {
	$(".menu-item-active").each(function (i) {
		$(this).removeClass('menu-item-active');
	});
};

var isSpecialItem = function(item) {
	// item hover es expertos creadores o ideas para crear
	return (item.hasClass('creators-menu') || item.hasClass('ideas-menu'));
};

var closeModalMenu = function() {
	if (modalMenu) {
		modalMenu.collapse('hide');
		modalMenu = null;
	}
};

var openMenu = function(target) {
	if( target === '#' ) {
		closeMenu(currentMenu);
		currentMenu = null;
		return;
	}
	$(target).collapse('show');
	currentMenu = target;
};

var closeMenu = function(target) {
	setTimeout(function() {
		if( target === '#' ) {
			return;
		}
		$(target).collapse('hide');
		quitMenuItemsActive();
	}, 300);
};

$(document).ready(function() {
	
	$('.menu-section .mega-menu a').click(function () {
        $(".menu-section .mega-menu a").each(function (index) {
            $(this).removeClass("menu-item-active");
        });
        $(this).addClass("menu-item-active");
    });

    $('.item-menu > a').click(function (e) {
        if( !isSpecialItem($(this)) ) {
            e.preventDefault();
        }
    });
	
	
	
	/**
	 * Menu:
	 * menu show event
	 */
	$('.submenu, .submenu-2').on('shown.bs.collapse', function(e) {
		var target = $(e.currentTarget);
		var targetId = '#'+target.attr('id');
		if( currentMenu !== targetId ) {
			closeMenu(targetId);
		}
	});

    /*
	 * Menu:
	 * mouse enter event
	 */
    $('.item-menu > a').mouseenter(function () {
		var target = $(this).data('target');
		quitMenuItemsActive();
        $(this).addClass('menu-item-active');
		openMenu(target);
		return;
    });
	
	
	/**
	 * Menu: 
	 * mouse leave event
	 */
	$('.item-menu > a').mouseleave(function (event) {
				
		var target = $(this).data('target');
		console.log('leave', target);
		
		var relatedTarget = $(event.relatedTarget);
		if ( !(relatedTarget.hasClass("well") ) ) {
			console.log('close menu');
			closeMenu(target);
		}
		else {
			console.log('dont close menu');
		}
		
		return;
    });

	/**
	 * Menu:
	 * mouse leave menu box 
	 */
    $(".submenu .well, .submenu-2 .well").mouseleave(function (event) {
        var relatedTarget = $(event.relatedTarget);
        if (!relatedTarget.hasClass("menu-item-active") && !relatedTarget.hasClass("item-menu")) {
            quitMenuItemsActive();
            var target = $(this).parent();
            target.collapse('hide');
            if (modalMenu) {
                modalMenu.collapse('hide');
                modalMenu = null;
            }
        }
    });

    // Menu mobile
    $('.menu-content > li').click(function (e) {
        $(".sub-menu.in").collapse('hide');
    });
	
});