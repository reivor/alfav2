var showLoader = function() {
	$('#overlay').show();
};

var hideLoader = function() {
	$('#overlay').hide();
};

var showAlert = function (message) {
	var dialog = $('#dialog-message');
	$('h5', dialog).text(message);
	dialog.modal('show');
};

$(document).ready(function() {
	
    $('.close, .btn-newsletter').click(function (e) {
        e.preventDefault();
        $('.modal').modal('hide');
        $('.bubble.in').collapse('hide');
    });

    // form newsletter
    $(".btn-form-newsletter").click(function (e) {
        var form_id = $(this).data('form');

        var idName = $("#" + form_id + " input[name='name']").val();
        var idEmail = $("#" + form_id + " input[name='email']").val();

        if( idName != "" && idEmail != "" ) {
			if( isEmail(idEmail) ){
				showLoader();
				var dataform = $("#" + form_id).serialize();
				dataform._token = $('input[name="_token"]').val();
				$.post('/newsletter', dataform).done(function (data) {
					$("#" + form_id + " input[name='name']").val('');
					$("#" + form_id + " input[name='email']").val('');
					showAlert(data['result']);
					hideLoader();
				});
			}
			else{
				$("#" + form_id + " input[name='email']").parent().addClass("has-error");
			}
        }
    });

    $('#aceptar-newsletter .close').click(function (e) {
        e.preventDefault();
        $('.bubble.in').collapse('hide');
    });

    // custom scrollbar
    $("#content-1").mCustomScrollbar({
        theme: "minimal"
    });

    // search form
    $('#form-search').submit(function () {
        var q = $('#q').val();
        if( q ) {
            window.location.href = '/search/' + q;
        }
        return false;
    });
	
});

$(document).click(function (e) {
    if( $('.bubble').has(e.target).length === 0 ) {
        $('.bubble.in').collapse('hide');
    }
});
