<?php

return [
	// cotizacion alfa
	'greeting'=>'Hola',
	'message1'=>'Has recibido una cotizaci&oacute;n de un usuario en nuestra p&aacute;gina web',
	'message2'=>'para revisarla',
	'product'=>'Producto',
	'nameLastName'=>'Nombre y Apellido',
	'mail'=>'Correo',
	'address'=>'Direcci&oacute;n',
	'phone'=>'Tel&eacute;fono o Celular',
	'city'=>'Ciudad',
	'job'=>'Trabajo',
	'enter'=>'Ingresa',
	'here'=>'aqu&iacute;',
	//cotizazion usuario
	'message3'=>'Hemos recibido tu cotizaci&oacute;n de',
	'message4'=>'En nuestra p&aacute;gina web ',
	'message5'=>'en pocos días estaremos dando respuesta a tu solicitud por medio de este correo',
	'message6'=>'Gracias por considerarnos una alternativa para la remodelación de tus espacios',
	'message7'=>'&Aacute;rea de Servicio al Cliente Alfa',
	'sincerely'=>'Atentamente',
];