<?php

return [
	
	// catalog
	'categories' => 'categorias',
	'products' => 'productos',
	'search' => 'buscar',
	
	// pages
	'quotes' => 'cotizaciones',
	'stores' => 'tiendas',
	'aboutus' => 'quienes-somos',
	'jobs' => 'trabaja-con-nosotros',
	'financing' => 'financiacion',
	'installation' => 'instalacion',
	'catalogs' => 'catalogos',
	'design' => 'diseno',
	'certifications' => 'certificaciones',
	'environmental_alfa' => 'alfa-ambiental',
	
	// info
	'client_info' => 'informacion',
	'product_info' => 'informacion/:type',
	'warranties' => 'garantias',
	'seals' => 'sellos-calidad',
	'manuals' => 'manuales',
	'datasheets' => 'fichas-tecnicas',
	
	// users
	'register' => 'registro',
	'logout' => 'salir',
	'reset_password' => 'cambiar-contrasena',
	
];
