<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Tu contraseña ha sido restablecida.',
    'sent' => 'Te hemos enviado un email con instrucciones para recuperar tu contraseña.',
    'token' => 'El enlace para recuperar la contraseña es inválido.',
    'user' => "No hemos encontrado ningún usuario con el correo suministrado.",

];
