<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'failed_login' => 'Usuario o contraseña incorrectos.',
    'account_created' => 'Tu cuenta ha sido creada.',

    'full_name' => 'Nombre Completo',
    'name' => 'Nombre',
    'email' => 'Correo',
    'password' => 'Contraseña',
    'password_confirm' => 'Confirmar Contraseña',
    'remember_me' => 'Recordarme',
    'sign_in' => 'Iniciar Sesión',
    'create_account' => 'Crear Cuenta',
    'terms_agree' => 'He leído y acepto los ',
    'terms_tos' => 'Términos y Condiciones',
    'already_registered' => '¿Ya tienes cuenta?',
    'call_sign_in' => 'Inicia Sesión',
    'forgot_your_password' => '¿Olvidaste tu contraseña?',
    'create_new_account' => 'Crear una nueva cuenta',

	// forms
    'form_required_field' => 'Este campo es requerido.',
    'form_invalid_email' => 'Ingrese una dirección de correo válida',
    'form_password_mismatch' => 'Las contraseñas no coinciden',
    'form_duplicated_email' => 'Este email ya se encuentra registrado',
    'form_password_too_short' => 'La contraseña es muy corta (min 6 caracteres)',
    'form_terms_agree_nocheck'=>'Debe aceptar los Términos y Condiciones',

    'btn_submit' => 'Enviar',
    'btn_register' => 'Crear Cuenta',
    'btn_signin' => 'Entrar',
    'btn_reset_password' => 'Recuperar Contraseña',
    'activated' => 'Activación de la Cuenta',

    ];
