<?php

return [
	
	// catalog
	'categories' => 'categories',
	'products' => 'products',
	'search' => 'search',
	
	// pages
	'quotes' => 'quotes',
	'stores' => 'stores',
	'aboutus' => 'about-us',
	'jobs' => 'work-with-us',
	'financing' => 'financing',
	'installation' => 'installation',
	'catalogs' => 'catalogs',
	'design' => 'design',
	'certifications' => 'certifications',
	'environmental_alfa' => 'environmental-alfa',
	
	// info
	'client_info' => 'client-info',
	'product_info' => 'client-info/:type',
	'warranties' => 'warranties',
	'seals' => 'quality-seals',
	'manuals' => 'manuals',
	'datasheets' => 'datasheets',
	
	// users
	'register' => 'register',
	'logout' => 'logout',
	'reset_password' => 'change-password',
	
];
