<?php

return [
	// quote alfa
	'greeting'=>'Hello',
	'message1'=>'You have received a quote from a user on our web page',
	'message2'=>'to revise',
	'product'=>'Product',
	'nameLastName'=>'Name y Last Name',
	'mail'=>'Mail',
	'address'=>'Address',
	'phone'=>'Phone or Cell Phone',
	'city'=>'City',
	'job'=>'Job',
	'enter'=>'Enter',
	'here'=>'Here',
	// quote user
	'message3'=>'We received your quote from',
	'message4'=>'On our website',
	'message5'=>'In a few days we will be responding to your request through this email',
	'message6'=>'Thank you for considering us an alternative for the remodeling of your spaces',
	'message7'=>'Alpha Customer Service Area',
	'sincerely'=>'Sincerely',
];