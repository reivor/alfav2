<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
	'failed_login' => 'Wrong username or password.',
	'account_created' => 'Your account has been created.',
	
	'full_name' => 'Full Name',
	'name' => 'Name',
	'email' => 'Email',
	'password' => 'Password',
	'password_confirm' => 'Password Confirm',
	'remember_me' => 'Remember me',
	'sign_in' => 'Sign In',
	'create_account' => 'Register',
	'terms_agree' => 'I agree to',
	'terms_tos' => 'Terms of Service',
	'already_registered' => 'Already have an account?',
	'call_sign_in' => 'Sign In',
	'forgot_your_password' => 'Forgot your password?',
	'create_new_account' => 'Create a new account',
	'logout' => 'Logout',
	
	// forms
	'form_required_field' => 'This field is required.',
	'form_invalid_email' => 'Please, provide a valid email address',
	'form_password_mismatch' => 'The password and its confirm are not the same',
	'form_duplicated_email' => 'This email is already registered',
	'form_password_too_short' => 'Password too short (min 6 characters)',
	
	'btn_submit' => 'Submit',
	'btn_register' => 'Create Account',
	'btn_signin' => 'Sign In',
	'btn_reset_password' => 'Reset Password',
	'activated' => 'Activated Account',
	
];
