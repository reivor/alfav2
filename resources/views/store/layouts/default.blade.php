<?php $version = env('APP_VERSION'); ?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
	<!--<![endif]-->
	<head>
		@include('store.partials.extra.gtm_head')
		@stack('noscripts')
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>@yield('title', 'Alfa')</title>
		<meta name="description" content="@yield('description', isset($settings['meta_description']) ? $settings['meta_description'] : '')">
		<meta name="keywords" content="@yield('keywords',isset($settings['meta_keywords']) ? $settings['meta_keywords'] : '')">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="og:title" content="@yield('title', 'Alfa')" />
		<meta property="og:type" content="@yield('type', '')" />
		<meta property="og:url" content="@yield('url', url()->full())" />
		<meta property="og:image" content="@yield('picture', url('/images/alfa-marker.jpg'))" />
		<meta property="og:description" content="@yield('description', isset($settings['meta_description']) ? $settings['meta_description'] : '')" />
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		<link rel="stylesheet" href="/dist/vendor.min.css?v=<?= $version ?>">
		<link rel="stylesheet" href="/dist/styles.min.css?v=<?= $version ?>">
		<link rel="stylesheet" href="/themes/store/vendor/owl.carousel.2.1.0/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="/themes/store/vendor/owl.carousel.2.1.0/assets/owl.theme.default.min.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,300,700,500" rel="stylesheet" type="text/css">
		@stack('styles')
		@include('store.partials.extra.mixpanel')
	</head>
	<body>
		@include('store.partials.extra.gtm_body')

		<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

		@include('store.partials.extra.facebook_sdk')

		@include('store.partials.front.header')

		<div class="container-fluid sin-padding">
			
			@include('store.partials.front.headerMenu')

			<div class="clearfix"></div>

			@yield('content')

			<div class="clearfix"></div>

			@include('store.partials.front.footer')
			@include('store.partials.front.footerMenu')
			@include('store.partials.front.footerCopyright')

		</div>
		<!-- Fin Body -->

		<!-- modal message -->
		<div class="modal fade alert-dialog" id="dialog-message" tabindex="-1" role="dialog" aria-labelledby="aceptar-newsletterLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="modal-body">
						<h5>Message</h5>
						<div class="text-center">
							<button class="btn btn-newsletter">{{ trans('front.close') }}</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- loader -->
		<div id="overlay">
			<div class="background"></div>
			<div class="loading">
				<div class="loader"></div>
			</div>
		</div>

		<script src="/js/vendor/jquery-1.11.2.min.js?v=<?= $version ?>"></script>
		<script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js?v=<?= $version ?>"></script>
		<script src="/dist/vendor.min.js?v=<?= $version ?>"></script>
		<script src="/dist/app.min.js?v=<?= $version ?>"></script>
		
		@include('store.partials.extra.analytics')
		@include('store.partials.extra.chat')
		@include('store.partials.extra.hotjar')
		
		<script>
			var menuLeft = document.getElementById('cbp-spmenu-s1'),
					menuRight = document.getElementById('cbp-spmenu-s2'),
					menuTop = document.getElementById('cbp-spmenu-s3'),
					menuBottom = document.getElementById('cbp-spmenu-s4'),
					showLeft = document.getElementById('showLeft'),
					showRight = document.getElementById('showRight'),
					showTop = document.getElementById('showTop'),
					showBottom = document.getElementById('showBottom'),
					showLeftPush = document.getElementById('showLeftPush'),
					showRightPush = document.getElementById('showRightPush'),
					body = document.body;

			showRight.onclick = function () {
				classie.toggle(this, 'active');
				classie.toggle(menuRight, 'cbp-spmenu-open');
				disableOther('showRight');
			};

			showRight_2.onclick = function () {
				classie.toggle(this, 'active');
				classie.toggle(menuRight, 'cbp-spmenu-open');
				disableOther('showRight');
			};

			function disableOther(button) {
				if (button !== 'showRight') {
					classie.toggle(showRight, 'disabled');
				}
			}
			
			var country = document.querySelector('#country_selector');
			new SelectFx(country, {
				onChange: function () {
					window.location.href = $('#country_selector').val();
				}
			});

			var countryMobile = document.querySelector('#country-mobile');
			new SelectFx(countryMobile, {
				onChange: function () {
					window.location.href = $('#country-mobile').val();
				}
			});
		</script>

		<script type='text/javascript'>
			$(document).ready(function () {
				anchor.init();

				//Mensaje que se muestra al concluir la cotización
				@if (session('success'))
					showAlert("<?=session('success') ?>");
				@endif
	$('[data-toggle="tooltip"]').tooltip(); 
				
			});

			anchor = {
				init: function () {
					$("a.anchorLink").click(function () {
						elementClick = $(this).attr("href");
						if($(".menu-naranja").css('display') == 'none'){
							destination = ($(elementClick).offset().top)-110;
						}else if($(".header-gris").css('display') == 'none'){
							destination = $(elementClick).offset().top;
						}else{
							destination = ($(elementClick).offset().top)-50;
						}

						$("html:not(:animated),body:not(:animated)").animate({
							scrollTop: destination
						}, 1100);
						return false;
					});
				}
			};
		
			var affixElement = '#navbar-main';

			$(affixElement).affix({
				offset: {
					// Distance of between element and top page
					top: function() {
						var h = $("#navbar-main").offset().top + $("#navbar-main").height();
						this.top = h;
						return h;
					},
					// when start #footer
					bottom: function () {
						return (this.bottom = $('#footer').outerHeight(true));
					}
				}
			});
			
			function isLogin(){
				return <?= $loginStatus? "true" : "false" ?>
			}
			
			var initSliders = function() {
				// slider productos mas buscados
				$('#content-slider-search').owlCarousel({
					items: 4,
					margin: 15,
					nav: true,
					responsive:{
						0:{
							items:2
						},
						768:{
							items:2,
							loop: <?= count($productsSearch)>2 ? 'true': 'false '?>
						},
						1024: {
							items: 4,
							loop: <?= count($productsSearch)>4 ? 'true': 'false '?>
						}
					}
				});
			};

			initSliders();
		</script>

		@stack('templates')
		@stack('plugins')
		@stack('scripts')
	</body>
</html>
