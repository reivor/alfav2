@extends('store.layouts.default')


@section('content')  

<!-- Content -->
<section class="container-fluid info-cliente">
	
	<div class="col-xs-12 box-big">
		<figure>
			<img class="img-responsive" src="/img/blog/articulo/pic-art-blog.jpg" alt="">
		</figure>
	</div>
	
	<div class="clearfix"></div>
	
	<div class="container">
		
		<div class="row sin-padding ruta hidden-xs hidden-sm">
			<ol class="breadcrumb">
				<li><a href="/">{{ trans('front.home') }}</a></li>
				<li class="active">{{ trans('front.customer_information') }}</li>
			</ol>
		</div>
		
		<div class="row title-info-client">
			<div class="col-xs-12">
				<h1>{{ trans('front.customer_information') }}</h1>
			</div>	
		</div>
		
		<div class="row row-no-margin">
			<div class="col-xs-6 col-md-3 item-info mediopaddright">
				<a href="<?= url(trans('routes.product_info', ['type' => trans('routes.warranties')])) ?>">
					<img class="img-responsive" src="/img/info-cliente/img-garantia.png" alt="{{ trans('front.warranties') }}">
					<p><?= trans('front.warranties') ?></p>
				</a>
			</div>
			<div class="col-xs-6 col-md-3 item-info mediopaddright">
				<a href="<?= url(trans('routes.product_info', ['type' => trans('routes.seals')])) ?>">
					<img class="img-responsive" src="/img/info-cliente/img-sello.png" alt="{{ trans('front.seals_of_quality') }}">
					<p><?= trans('front.seals_of_quality') ?></p>
				</a>
			</div>
			<div class="col-xs-6 col-md-3 item-info mediopaddright">
				<a href="<?= url(trans('routes.product_info', ['type' => trans('routes.manuals')])) ?>">
					<img class="img-responsive" src="/img/info-cliente/img-manuales.png" alt="{{ trans('front.manuals') }}">
					<p><?= trans('front.manuals') ?></p>
				</a>
			</div>
			<div class="col-xs-6 col-md-3 item-info mediopaddright">
				<a href="#">
					<img class="img-responsive" src="/img/info-cliente/img-f-ambientales.png" alt="{{ trans('front.environmental_specifications') }}">
					<p><?= trans('front.environmental_specifications') ?></p>
				</a>
			</div>
			<div class="col-xs-6 col-md-3 item-info mediopaddright">
				<a href="<?= url(trans('routes.product_info', ['type' => trans('routes.datasheets')])) ?>">
					<img class="img-responsive" src="/img/info-cliente/img-f-tecnica.png" alt="{{ trans('front.datasheets') }}">
					<p><?= trans('front.datasheets') ?></p>
				</a>
			</div>
		</div>
	</div>
</section>
<!-- Fin Content -->

<section class="container">
	<!-- SECTION PRODUCTOS -->
	@include('store.partials.products.productsSearchSlider')
	<!-- end SECTION PRODUCTOS -->
</section>
@endsection