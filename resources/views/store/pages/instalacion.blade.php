@extends('store.layouts.default')

@section('title', trans('front.label_installation'))
	{{-- @if (isset($metadata['title']['title']))
	@section('title', $metadata['title']['title'])
@endif --}}
@if (isset($metadata['meta']['keyword']))
	@section('keywords', $metadata['meta']['keyword'])
	@endif
	@if (isset($metadata['meta']['description']))
		@section('description',$metadata['meta']['description'])
		@endif
		@if (isset($metadata['og']['type']))
			@section('type',$metadata['og']['type'])
			@endif
			@if (isset($metadata['og']['url']))
				@section('url',$metadata['og']['url'])
				@endif
				@if (isset($metadata['og']['image']))
					@section('picture',$metadata['og']['image'])
					@endif
					@section('content')

						<section class="container-fluid quienes-somos">
							<div class="col-xs-12 box-big">
								<figure>
									<img class="img-responsive" src="/images/assets/banner_instalacion.png" alt="<?= trans('front.label_installation') ?>">
								</figure>
							</div>
							<div class="clearfix"></div>
							<div class="container">
								<section class="col-xs-12 col-md-8 content-quienes-somos">
									<h1>Instalación</h1>
									<article>
										<p>
											La mejor forma de iniciar la transformación que deseas es instalando tus productos con nosotros.
										</p>
										<p>
											En Alfa utilizamos nuestros aditivos y pegantes de excelente calidad para ofrecerte
											el mejor servicio de instalación, en manos de un equipo técnico especializado.
											Además, contamos con un portafolio de productos para el mantenimiento de tus paredes y pisos,
											para que luzcan siempre como nuevos.
										</p>
										<p>
											Conoce nuestros productos.
										</p>
									</article>
									<div class="clearfix"></div>
								</section>
								<aside class="col-xs-12 col-md-4 aside-quienes-somos">
									<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
										<figure class="col-xs-12 col-md-6 col-lg-5">
											<a href="http://alfa.com.co/subproducto2/aditivos_y_pegantes/33?pos=8" target="_blank">
												<img src="/images/footer/Producto-Mantenenimiento.jpg" alt="" class="responsive">
											</a>
										</figure>
										<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
											<p><strong><a href="http://alfa.com.co/subproducto2/aditivos_y_pegantes/33?pos=8" target="_blank">Aditivos y pegantes</a></strong> <br>grupo Alfa</p>
										</div>
									</div>
									<div class="separator-aside-qs hidden-xs hidden-sm col-xs-12"></div>
									<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
										<figure class="col-xs-12 col-md-6 col-lg-5">
											<a href="http://alfa.com.co/subproducto2/productos_de_limpieza/35?pos=8" target="_blank">
												<img src="/images/footer/Producto-Mantenenimiento.jpg" alt="" class="responsive">
											</a>
										</figure>
										<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
											<p><strong><a href="http://alfa.com.co/subproducto2/productos_de_limpieza/35?pos=8" target="_blank">Limpiadores</a></strong> <br>grupo Alfa</p>
										</div>
									</div>
									<div class="separator-aside-qs hidden-xs hidden-sm col-xs-12"></div>
									<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
										<figure class="col-xs-12 col-md-6 col-lg-5">
											<a href="http://alfa.com.co/subproducto2/productos_de_proteccion/44?pos=8" target="_blank">
												<img src="/images/footer/Producto-Mantenenimiento.jpg" alt="" class="responsive">
											</a>
										</figure>
										<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
											<p><strong><a href="http://alfa.com.co/subproducto2/productos_de_proteccion/44?pos=8" target="_blank">Protectores</a></strong> <br>grupo Alfa</p>
										</div>
									</div>
									<div class="separator-aside-qs hidden-xs hidden-sm col-xs-12"></div>
									<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
										<figure class="col-xs-12 col-md-6 col-lg-5">
											<a href="http://alfa.com.co/seccionfichas/3/1" target="_blank">
												<img src="/images/footer/manuales.jpg" alt="" class="responsive">
											</a>
										</figure>
										<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
											<p><strong><a href="http://alfa.com.co/seccionfichas/3/1" target="_blank">Manuales de instalación de nuestros productos</a></strong> <br>grupo Alfa</p>
										</div>
									</div>
								</aside>
							</div>
						</section>

						@include('store.partials.front.bannersFooter')

					@endsection
