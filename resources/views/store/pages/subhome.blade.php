@extends('store.layouts.default')

@section('title', $category->name)
@section('description', $category->description)
@section('keywords',$category->tags)

@section('content')   

<!-- Slider -->
<div class="container-fluid sin-padding">
	<div class="slider-categoria">
		<img src="<?= $category->header_url ?>" alt="categoria" class="imgcategoria" />
	</div>
</div>
<!-- Fin Slider -->

<div class="clearfix"></div>

<!-- begin SECTION SUB-HOME -->
<section class="container content-subhome" id="ambientes">
	<!-- Ruta -->
	<div class="row sin-padding ruta hidden-xs hidden-sm">
		<ol class="breadcrumb">
			<li><a href="/">{{trans('front.home')}}</a></li>
			<?php foreach ($breadcrumbs as $keyCrumb => $crumb) { ?>
				<?php if ($keyCrumb + 1 == count($breadcrumbs)) { ?>
					<li class="active"><?= $crumb->name ?></li>
				<?php } else { ?>
					<li><a href="<?= $crumb->detailUrl ?>"><?= $crumb->name ?></a></li>
				<?php } ?>

			<?php } ?>
		</ol>
	</div>
	<!-- Fin Rutas -->
	<div class="grid-subhome">
		<div class="row">
			<?php foreach ($items as $key => $lvl3) { ?>
				<div class="col-xs-6 col-sm-<?=$columnas?> col-md-<?=$columnas?> col-lg-<?=$columnas?> categorias-ambientes">
					<?php $child = App\Models\Category::where('parent', $lvl3->id)->get(); ?>
					<a href="<?= $lvl3->detailUrl ?>">
						<img class="img-responsive hidden-xs hidden-sm" src="{{ $lvl3->image_url }}" alt="{{ $lvl3->name }}">
						<img class="img-responsive hidden-md hidden-lg" src="{{ $lvl3->image_mobile_url }}" alt="{{ $lvl3->name }}">
					</a>
					<!--<div class="">{{ $lvl3->name }}</div>-->
				</div>
			<?php } ?>
		</div>
	</div>
	
</section>
<!-- end SECTION SUB-HOME -->
<div class="clearfix"></div>

@include('store.partials.front.newsletter')

<!-- SECTION PRODUCTOS -->
<section class="container" id="productos">
	@include('store.partials.products.productsSearchSlider')
</section>

<!-- end SECTION PRODUCTOS -->

<section class="container">
	@include('store.partials.front.ideas')
</section>

<!-- SECTION BANNERS -->

<?php if(count($bannersPromoSubhomeFooter)>0){ ?>
<section class="container-fluid content-promo">
	<div class="container">
		<section class="container">
			<hr class="separador hidden-md hidden-lg">
		</section>
		<div class="row row-no-margin grid-footer-home">
			<?php if(isset($bannersPromoSubhomeFooter[1])){?>
				<div class="hidden-xs hidden-sm col-md-4 col-lg-4 promo-left sin-padding-left sin-padding-right">
					<a href="<?=$bannersPromoSubhomeFooter[1]->detailUrl?>" target="<?=$bannersPromoSubhomeFooter[1]->target?>">
						<img class="img-responsive" src="<?=$bannersPromoSubhomeFooter[1]->image_url?>" alt="">
					</a>
				</div>
			<?php }?>
			
			<?php if(isset($bannersPromoSubhomeFooter[2])){?>
				<div class="col-md-8 col-lg-8 promo-right sin-padding-right">
					<a href="<?=$bannersPromoSubhomeFooter[2]->detailUrl?>" target="<?=$bannersPromoSubhomeFooter[2]->target?>">
						<img class="img-responsive img-promo-2" src="<?=$bannersPromoSubhomeFooter[2]->image_url?>" alt="">
					</a>
				</div>
			<?php }?>
			
		</div>
	</div>
</section>
<?php }?>

<!-- end SECTION BANNERS -->
@endsection