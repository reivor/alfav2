@extends('store.layouts.default')

@section('title', $cat->name)
@section('description', $cat->description)
@section('keywords',$cat->tags)

@section('content')

<style>
.item-producto-cat .prod-new:before {
	content: "{{ trans('front.new') }}";
}
</style>

<!-- Slider -->
<div class="container-fluid sin-padding">
	<div class="slider-categoria">
		<img src="<?=$cat->header_url?>" alt="categoria" class="imgcategoria" />
	</div>
</div>
<!-- Fin Slider -->

<div class="clearfix"></div>

<!-- Content -->
<section class="container" id="content">

	<!-- Filtros -->
	<div class="row sin-padding hidden-xs hidden-sm">
		<div class="col-sm-8 col-sm-offset-2 filtros">
			<div class="col-xs-4">
				<form id="form-filter" method="get">
					<select class="cs-select cs-skin-underline" id="categories">
						<option value="" selected disabled>{{ $cat->name }}</option>
						@foreach ($parent_childs as $pchilds)
						<option value="{{ $pchilds->id }}">{{ $pchilds->name }}</option>
						@endforeach
					</select>
				</form>
			</div>
			<div class="col-xs-4" id="prod-col">
				<select class="cs-select cs-skin-underline" id="products" name="products">
					<option value="" selected disabled>{{trans('front.product')}}</option>
					@foreach ($ref_select as $refs)
					<option value="{{ $refs->id }}">{{ $refs->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-xs-4" id="color-col">
				<select class="cs-select cs-skin-underline" id="color" name="color">
					<option value="" disabled selected>{{trans('front.color')}}</option>
				</select>
			</div>
		</div>
	</div>
	<!-- Fin Filtros -->

	<!-- Ruta -->
	<div class="row sin-padding ruta hidden-xs hidden-sm">
		<ol class="breadcrumb">
			<li><a href="/">{{trans('front.home')}}</a></li>
			<?php foreach ($breadcrumbs as $keyCrumb => $crumb) { ?>
			<?php if($keyCrumb+1 == count($breadcrumbs)) {?>
			<li class="active"><?=$crumb->name?></li>
			<?php }else{?>
			<li><a href="<?=$crumb->detailUrl?>"><?=$crumb->name?></a></li>
			<?php }?>
			<?php } ?>
		</ol>
	</div>
	<!-- Fin Rutas -->

	<!-- Productos Destacados -->
	<div class="row sin-padding prod-destacados results_all">
		@foreach ($products->slice(0,4) as $refs)
		<div class="col-xs-6 col-sm-6 col-md-3 producto-cat">
			<div class="item-producto-cat text-center">
				<a href="<?= getUrlEntity($refs, $refs->pid) ?>" class="link">
					<figure class="hvr-float">
						@if($refs->is_new == 1)
						<span class="prod-new" style=""></span>
						@endif
						<img src="{{ isset($refs->url) ? $refs->url : '/images/placeholderProduct.png' }}" alt="{{ $refs->name }}" class="img-responsive">
					</figure>
					<h4>{{ $refs->name }}</h4>
				</a>
			</div>
		</div>
		@endforeach
	</div>
	<!-- Fin Destacados -->

	<!-- Banners Coleccion -->
	@if(!empty($banner->image_url))
	<div class="row banner-coleccion">
		<figure>
			<a href="<?=$banner->detailUrl ?>" target="<?=$banner->target?>">
				<img src="{{ $banner->image_url }}" class="img-responsive" alt="">
			</a>
		</figure>
	</div>
	@endif
	<!-- Fin Banners Coleccion -->

	<!-- Lista de Productos -->
	<div class="row sin-padding prod-list-12">
		@foreach ($products->slice(4,4) as $refa)
			<div class="col-xs-6 col-sm-6 col-md-3 producto-cat">
				<div class="item-producto-cat text-center">
					<a href="<?=getUrlEntity($refa, $refa->pid)?>">
						<figure class="hvr-float">
							@if($refa->is_new == 1)
								<span class="prod-new"></span>
							@endif
							<img src="{{ isset($refa->url) ? $refa->url : '/images/placeholderProduct.png' }}" alt="{{ $refa->name }}" class="img-responsive">
						</figure>
						<h4>{{ $refa->name }}</h4>
					</a>
				</div>
			</div>
		@endforeach
	</div>
	<!-- Fin Lista de Productos -->

	<!-- check for record exists -->
	@if( $ref_count > 8 )
	<div class="col-xs-12 ver-mas-amarillo">
		<a href="#" class="more" data-id="{{ $cat->id }}">{{ trans('front.see_more') }}</a>
	</div>
	@endif

	<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display: none; margin-top: 10px;">
		<div class="internal-loading">
			<div class="internal-loader"></div>
		</div>
		<!--<img src="/images/loader.gif" style="width: 30px; height: 30px;"/>-->
	</div>

</section>
<!-- Fin Content -->
@endsection


@section('filter_mobile_button')
<div class="filtros-mobile">
	<a class="icon-buscador" role="button" data-toggle="collapse" href="#collapsefiltros-mobile" aria-expanded="false" aria-controls="collapsefiltros-mobile">
		<img src="/img/icon-menu/icon-filtro.png" alt="">
	</a>
</div>
@endsection

@section('filter_mobile')

<div class="collapse filtros-mobile-det" id="collapsefiltros-mobile">
	<div class="well col-xs-12">
		<div class="col-xs-12 sin-padding"><p>{{trans('front.looking_for')}}</p></div>
		<div class="col-xs-12 sin-padding">
			<div class="filtros_sel">
				<p>{{trans('front.category')}}</p>
				<select id="categories-mobile" class="cs-select cs-skin-underline">
					<option value="0" selected>{{trans('front.select')}}</option>
					@foreach ($parent_childs as $pchilds)
					<option value="{{ $pchilds->id }}">{{ $pchilds->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="filtros_sel" id="prod-col-mobile">
				<p>{{trans('front.product')}}</p>
				<select id="products-mobile" class="cs-select cs-skin-underline" name="products-mobile">
					<option value="0" selected>{{trans('front.select')}}</option>

				</select>
			</div>
			<div class="filtros_sel" id="color-col-mobile">
				<p>{{trans('front.color')}}</p>
				<select id="colors-mobile" class="cs-select cs-skin-underline">
					<option value="0" selected>{{trans('front.select')}}</option>

				</select>
			</div>
			<div class="filtros_sel">
				<button class="btn-filter">{{trans('front.filter')}}</button>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')

<script>

var colorFx;
var color_mobileFx;
var prodFx;
var prodmFx;

$(function () {

	var color = document.querySelector('#color');
	colorFx = new SelectFx( color, {} );

	var prodm = document.querySelector('#products-mobile');
	prodmFx = new SelectFx( prodm, {} );

	var color_mobile = document.querySelector('#colors-mobile');
	color_mobileFx = new SelectFx( color_mobile, {} );

	var selectElement = document.querySelector('#categories');
	new SelectFx( selectElement, {
		onChange: function(val) {
			showLoader();
			var select = $('#categories');
			$(".prod-list-12").show();
			$(".prod-destacados").show();
			$.getJSON("/loadCategoryInfo/"+ select.val() +"/", function(jsonData) {

				/*** Load de la imagen del header de la Categoria ***/
				var toAppend = '';
				$(".slider-categoria").fadeOut(300, function() {
					$(".slider-categoria").empty();
				});
				toAppend += '<img src="'+jsonData.cat.header_url+'" alt="categoria" class="imgcategoria" />';
				$(".slider-categoria").fadeIn(1500, function() {
					$('.slider-categoria').html(toAppend);
				});
				/*** Fin Cambio de la imagen del header de la Categoria ***/

				/*** Load del breadcrumb ***/
				$('.breadcrumb li').last().remove();
				var toAppend2 = '';
				toAppend2 += ('<li>'+jsonData.cat.name+'</li>');
				$('.breadcrumb').append(toAppend2);
				/*** Fin Load del breadcrumb ***/

				/*** Load 4 productos destacados ***/
				var toAppend3 = '';
				$('.prod-destacados').empty();
				hideLoader();
				$.each(jsonData.ref_four, function(i,data)
				{
					//console.log(data);
					if(!data.url && data.is_new == 0){
						toAppend3 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					if(!data.url && data.is_new == 1){
						toAppend3 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><span class="prod-new"></span><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					if(data.url && data.is_new == 0){
						toAppend3 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					if(data.url && data.is_new == 1){
						toAppend3 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><span class="prod-new"></span><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
				});
				$('.prod-destacados').append(toAppend3);
				/*** Fin 4 productos destacados ***/


				/*** Load banner Publicitario ***/
				var toAppend4 = '';
				$('.banner-coleccion').empty();
				if(!$.isEmptyObject(jsonData.banner)) {
					/*if( jsonData.banner.type == "product" ){
						toAppend4 += '<figure><a href="/detail/'+jsonData.banner.entity_id+'"><img src="'+jsonData.banner.image_url+'" class="img-responsive" alt="'+jsonData.banner.image_url+'"></a></figure>';
					} else if ( jsonData.banner.type == "category" ) {
						toAppend4 += '<figure><a href="/category/'+jsonData.banner.entity_id+'"><img src="'+jsonData.banner.image_url+'" class="img-responsive" alt="'+jsonData.banner.image_url+'"></a></figure>';
					} else {
						toAppend4 += '<figure><a href="'+jsonData.banner.external_url+'"><img src="'+jsonData.banner.image_url+'" class="img-responsive" alt="'+jsonData.banner.image_url+'"></a></figure>';
					}*/

					toAppend4 += '<figure><a href="'+jsonData.banner.detail_url+'"><img src="'+jsonData.banner.image_url+'" class="img-responsive" alt="'+jsonData.banner.image_url+'"></a></figure>';
					$('.banner-coleccion').append(toAppend4);
				}
				/*** Fin Load banner Publicitario ***/

				/*** Load 12 productos despues del banner publicitario ***/
				var toAppend5 = '';
				$('.prod-list-12').empty();
				$.each(jsonData.ref_all, function(i,data)
				{
					if(!data.url && data.is_new == 0){
						toAppend5 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					if(!data.url && data.is_new == 1){
						toAppend5 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><span class="prod-new"></span><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					if(data.url && data.is_new == 0){
						toAppend5 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					if(data.url && data.is_new == 1){
						toAppend5 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><span class="prod-new"></span><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
				});
				$('.prod-list-12').append(toAppend5);
				/*** Fin Load 12 productos despues del banner publicitario ***/

				/*** Load del select de Productos ***/
				var output = [];
				var prodEl = $('#products').empty().clone();
				$(prodFx.selEl).remove();
				output.push('<option disabled selected>Producto</option>');
				$.each(jsonData.ref_select, function(i,data)
				{
					output.push('<option value="' + data.id + '">'+ data.name +'</option>');
				});
				$('#prod-col').append(prodEl);
				$('#products').html(output.join(''));
				/*** Fin Load del select de Productos **/

				/*** Cambiar attr data-id del Boton Ver Mas ***/
				$('.more').css("display", "block");
				$('.more').data('id', jsonData.cat.id);
				$(".more").attr('data-id', jsonData.cat.id);
				/*** Fin Cambiar attr data-id del Boton Ver Mas ***/

				/*** Muestra el Producto seleccionado y carga el Select de Color ***/
				var prod = document.querySelector('#products');
				prodFx = new SelectFx( prod, {
					onChange: function(val) {
						showLoader();
						var select = $('#products');
						var output = [];

						$.getJSON("/color/"+ select.val() +"/", function(jsonData){

							// remove and recreate color dropdown
							var colorEl = $('#color').empty().clone();
							$(colorFx.selEl).remove();

							output.push('<option disabled selected>Color</option>');
							hideLoader();
							$.each(jsonData, function(i,data)
							{
								output.push('<option value="' + data.cid + '">'+ data.color +'</option>');
							});

							$('#color-col').append(colorEl);
							$('#color').html(output.join(''));
							color = document.querySelector('#color');
							colorFx = new SelectFx( color, {
								onChange: function(val){
									showLoader();
									$.getJSON("/imageByColor/"+ val +"/", function(jsonData){
										$.each(jsonData, function(i,data){

											var toAppend = '';

											$('.hvr-float').children().fadeOut(500);

											if(!data.url){
												toAppend += '<img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive">';
												$("a.link").attr("href", data.detail_url);
											}
											else {
												toAppend += '<img src="'+data.url+'" alt="'+data.name+'" class="img-responsive">';
												$("a.link").attr("href", data.detail_url);
											}

											$('.hvr-float').html(toAppend);

										});
										hideLoader();
									});
								}
							});

							// disable color dropdown
							if( jsonData.length === 0 ) {
								$(colorFx.selEl).addClass('dd');
							}
						});

						$('.more').hide();
						$(".prod-list-12").fadeOut(800, function() {
							$(".prod-list-12").empty();
						});

						var selectq = $('#products');
						$.getJSON("/prods/"+ selectq.val() +"/", function(jsonData){
							var toAppend9 = '';
							$('.prod-destacados').empty();
							$.each(jsonData, function(i,data)
							{
								if(!data.url){
									toAppend9 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'" class="link"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
								}
								else {
									toAppend9 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'" class="link"><figure class="hvr-float"><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
								}
							});
							$('.prod-destacados').html(toAppend9);
							hideLoader();
						});
					}
				});
			/*** Muestra el Producto seleccionado y carga el Select de Color ***/
		});
	}
});

	/*** Filtros Mobile ***/

	var categoriesMobile = document.querySelector('#categories-mobile');
	new SelectFx(categoriesMobile, {
		onChange: function (val) {
			showLoader();
			var select = $('#categories-mobile');
			$.getJSON("/loadCategoryInfo/"+ select.val() +"/", function(jsonData) {
				/*** Load del select de Productos ***/
				var output = [];
				var prodmEl = $('#products-mobile').empty().clone();
				//console.log(prodmEl);
				$(prodmFx.selEl).remove();
				output.push('<option value="0" selected>{{trans('front.select')}}</option>');
				$.each(jsonData.ref_select, function(i,data)
				{
					output.push('<option value="' + data.id + '">'+ data.name +'</option>');
				});
				//console.log(output);
				$('#prod-col-mobile').append(prodmEl);
				$('#products-mobile').html(output.join(''));
				/*** Fin Load del select de Productos **/

				var prodm = document.querySelector('#products-mobile');
				prodmFx = new SelectFx( prodm, {
					onChange: function(val) {
						showLoader();
						var select = $('#products-mobile');
						var output = [];

						$.getJSON("/color/"+ select.val() +"/", function(jsonData){

						// remove and recreate color dropdown
						var colormEl = $('#colors-mobile').empty().clone();
						$(color_mobileFx.selEl).remove();

						output.push('<option value="0" selected>{{trans('front.select')}}</option>');
						$.each(jsonData, function(i,data)
						{
							output.push('<option value="' + data.cid + '">'+ data.color +'</option>');
						});

						$('#color-col-mobile').append(colormEl);
						$('#colors-mobile').html(output.join(''));
						colorm = document.querySelector('#colors-mobile');
						color_mobileFx = new SelectFx( colorm, {
							onChange: function(val){

							}
						});
						hideLoader();
					});

					}
				});
				hideLoader();
			});
		}
	});

	/*** Fin Filtros Mobile ***/


	/*** Listado de Productos Inicial ***/

		var prod = document.querySelector('#products');
		prodFx = new SelectFx( prod, {
		onChange: function(val) {
			showLoader();
			var select = $('#products');
			var output = [];
			$.getJSON("/color/"+ select.val() +"/", function(jsonData){

				// remove and recreate color dropdown
				var colorEl = $('#color').empty().clone();
				$(colorFx.selEl).remove();

				output.push('<option disabled selected>Color</option>');
				$.each(jsonData, function(i,data)
				{
					output.push('<option value="' + data.cid + '">'+ data.color +'</option>');
				});

				$('#color-col').append(colorEl);
				$('#color').html(output.join(''));
				color = document.querySelector('#color');
				colorFx = new SelectFx( color, {
					onChange: function(val){
						showLoader();
						$.getJSON("/imageByColor/"+ val +"/", function(jsonData){
							$.each(jsonData, function(i,data){
								var toAppend = '';
								$('.hvr-float').children().fadeOut(500);
								if(!data.url){
									toAppend += '<img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive">';
									$("a.link").attr("href", data.detail_url);
								}
								else {
									toAppend += '<img src="'+data.url+'" alt="'+data.name+'" class="img-responsive">';
									$("a.link").attr("href", data.detail_url);
								}
								$('.hvr-float').html(toAppend);
							});
							hideLoader();
						});
					}
				});
				hideLoader();
			});

			$('.more').hide();
			$(".prod-list-12").fadeOut(800, function() {
			    $(".prod-list-12").empty();
			});

			var select = $('#products');
			$.getJSON("/prods/"+ select.val() +"/", function(jsonData){
				var toAppend = '';
				$('.prod-destacados').empty();
				$.each(jsonData, function(i,data)
				{
					if(!data.url){
						toAppend += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'" class="link"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					else {
						toAppend += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'" class="link"><figure class="hvr-float"><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
				});
				$('.prod-destacados').html(toAppend);
				hideLoader();
			});
		}
	});

	/*** Fin Listado de Productos Inicial ***/


	/*** Botón filtrar (MOBILE) ***/
	$(document).on( 'click', '.btn-filter', function (e) {

		var select_cat = $('#categories-mobile');
		var select_prod = $('#products-mobile');
		var select_color = $('#colors-mobile');

		var cat_id = select_cat.val();
		var prod_id = select_prod.val();
		var color_id = select_color.val();

		$.getJSON("/loadCategoryInfo/"+select_cat.val()+"/"+select_prod.val()+"/"+select_color.val()+"/", function(jsonData) {

			/*** Load de la imagen del header de la Categoria ***/
			var toAppend = '';
			$(".slider-categoria").fadeOut(300, function() {
				$(".slider-categoria").empty();
			});
			toAppend += '<img src="'+jsonData.cat.header_url+'" alt="categoria" class="imgcategoria" />';
			$(".slider-categoria").fadeIn(1500, function() {
				$('.slider-categoria').html(toAppend);
			});
			/*** Fin Cambio de la imagen del header de la Categoria ***/

			/*** Load 4 productos destacados ***/
			var toAppend3 = '';
			$('.prod-destacados').empty();
			$.each(jsonData.ref_four, function(i,data)
			{
				if(!data.url && data.is_new == 0){
					toAppend3 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
				if(!data.url && data.is_new == 1){
					toAppend3 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><span class="prod-new"></span><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
				if(data.url && data.is_new == 0){
					toAppend3 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
				if(data.url && data.is_new == 1){
					toAppend3 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><span class="prod-new"></span><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
			});
			$('.prod-destacados').append(toAppend3);
			/*** Fin 4 productos destacados ***/

			/*** Load banner Publicitario ***/
			var toAppend4 = '';
			$('.banner-coleccion').empty();
			if(!$.isEmptyObject(jsonData.banner)) {
				/*if( jsonData.banner.type == "product" ){
					toAppend4 += '<figure><a href="/detail/'+jsonData.banner.entity_id+'"><img src="'+jsonData.banner.image_url+'" class="img-responsive" alt="'+jsonData.banner.image_url+'"></a></figure>';
				} else if ( jsonData.banner.type == "category" ) {
					toAppend4 += '<figure><a href="/category/'+jsonData.banner.entity_id+'"><img src="'+jsonData.banner.image_url+'" class="img-responsive" alt="'+jsonData.banner.image_url+'"></a></figure>';
				} else {
					toAppend4 += '<figure><a href="'+jsonData.banner.external_url+'"><img src="'+jsonData.banner.image_url+'" class="img-responsive" alt="'+jsonData.banner.image_url+'"></a></figure>';
				}*/

				toAppend4 += '<figure><a href="'+jsonData.banner.detail_url+'"><img src="'+jsonData.banner.image_url+'" class="img-responsive" alt="'+jsonData.banner.image_url+'"></a></figure>';

				$('.banner-coleccion').append(toAppend4);
			}
			/*** Fin Load banner Publicitario ***/

			/*** Load 12 productos despues del banner publicitario ***/
			var toAppend5 = '';
			$('.prod-list-12').empty();
			$.each(jsonData.ref_all, function(i,data)
			{
				if(!data.url && data.is_new == 0){
					toAppend5 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
				if(!data.url && data.is_new == 1){
					toAppend5 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><span class="prod-new"></span><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
				if(data.url && data.is_new == 0){
					toAppend5 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
				if(data.url && data.is_new == 1){
					toAppend5 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><span class="prod-new"></span><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
			});
			$('.prod-list-12').append(toAppend5);
			/*** Fin Load 12 productos despues del banner publicitario ***/

			/*** Cambiar attr data-id del Boton Ver Mas ***/
			$('.more').css("display", "block");
			$('.more').data('id', jsonData.cat.id);
			$(".more").attr('data-id', jsonData.cat.id);
			/*** Fin Cambiar attr data-id del Boton Ver Mas ***/

			if(prod_id != 0) {

				$('.more').hide();
				$(".prod-list-12").fadeOut(800, function() {
					$(".prod-list-12").empty();
				});

			    var toAppendp = '';
				$('.prod-destacados').empty();
				$.each(jsonData.ref, function(i,data)
				{
					if(!data.url){
						toAppendp += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'" class="link"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					else {
						toAppendp += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'" class="link"><figure class="hvr-float"><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
				});
				$('.prod-destacados').html(toAppendp);

			}

			if(color_id != 0) {

				$('.more').hide();
				$('.prod-destacados').empty();
				$(".prod-list-12").empty();

				var toAppendc = '';
				$.each(jsonData.ref_color, function(i,data)
				{
					if(!data.url){
						toAppendc += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'" class="link"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
					else {
						toAppendc += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'" class="link"><figure class="hvr-float"><img src="'+data.purl+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
					}
				});
				$('.prod-destacados').html(toAppendc);

			}

		});

		$("#collapsefiltros-mobile").removeClass('in');
	});
	/*** Fin del Botón filtrar (MOBILE) ***/


	/*** Botón ver más productos ***/
	$('.more').on('click', function (e) {
		var catid = $(this).data('id');
		$(this).hide();
		$('#mini-loader').css('display', 'block');
		$.getJSON("/moreProds/"+ catid +"/", function(jsonData){
			var toAppend8 = '';
			$.each(jsonData, function(i,data)
			{
				if(!data.url){
					toAppend8 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="/images/placeholder.png" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
				else {
					toAppend8 += '<div class="col-xs-6 col-sm-6 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="'+data.detail_url+'"><figure class="hvr-float"><img src="'+data.url+'" alt="'+data.name+'" class="img-responsive"></figure><h4>'+data.name+'</h4></a></div></div>';
				}
			});
			$('.prod-list-12').append(toAppend8);
			$('#mini-loader').css('display', 'none');
		});
		e.preventDefault();
	});
	/*** Fin Botón ver más productos ***/

});


</script>

@endpush
