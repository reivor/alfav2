@extends('store.layouts.default')

@section('content')  


<section class="container" id="registro">
	<!-- Ruta -->
	<div class="row sin-padding ruta hidden-xs hidden-sm">
		<ol class="breadcrumb">
			<li><a href="#">{{trans('front.home')}}</a></li>
			<li class="active">{{trans('front.reset_password_title')}}</li>
		</ol>
	</div>
	<!-- Fin Rutas -->
	<div class="row row-no-margin">
		<div class="col-xs-12">
			<h1>{{trans('front.reset_password_title')}}</h1>
		</div>
	</div>
	<div class="row row-registro row-no-margin">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registro-form pull-right">
			<form class="form-inline col-xs-12" action="/passwordReset" method="post" role="form" data-toggle="validator" id="form-recoverypass">
				{{ csrf_field() }}
				<input type="hidden" name="user_id" value="{{$user_id}}">
				<div class="row">
					<div class="form-group col-xs-12 col-sm-6 has-feedback sin-padding-left">
						<label for="contrasena1">{{trans('front.new_password_label')}}</label> <br>
						<input type="password" class="form-control" name="password" id="contrasena1" required>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group col-xs-12 col-sm-6 has-feedback sin-padding-right">
						<label for="contrasena2">{{trans('front.repeat_new_password_label')}}</label> <br>
						<input type="password" class="form-control" name="password2"id="contrasena2" required>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group col-xs-12 col-sm-12 sin-padding-left">
						<button type="submit" class="btn btn-contrasena">{{trans('front.create_password')}}</button>
					</div>
				</div>

			</form>
			<div class="hidden-md hidden-lg registro-pinturas-mobile">
				<img class="img-responsive" src="img/banners/img-pinturas-mobile.png" alt="Pinturas">
			</div>
		</div>
	</div>
</section>

@endsection

@push('styles')
<link rel="stylesheet" href="/themes/store/vendor/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
@endpush
@push('scripts')
<script src="/themes/store/vendor/bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>
<script>

$(function(){
	$('#form-recoverypass').bootstrapValidator({
		fields:{
			password:{
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'},
					stringLength: {
						min: 6,
						message: '<?= trans('auth.form_password_too_short') ?>'
					}
				}
			},
			password2:{
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'},
					identical: {
						field: 'password',
						message: '<?= trans('auth.form_password_mismatch') ?>'
					}
				}
			}
		}

	});
});
</script>

@endpush