@extends('store.layouts.default')

@section('title', $article->title_plain)
@section('picture', url($article->image_default))
@section('description', $article->excerpt)

@section('content')

<section class="container-fluid art-blog">
	
	<div class="container">
		<h2 class="hidden-xs header-blog"><?= $article->title ?></h2>
	</div>
	
	
	<div class="col-xs-12 box-big">
		<figure>
			<img class="img-responsive" src="<?= $article->image_header ?>" alt="<?= $article->title_plain ?>">
			<?php if( $article->pinPoints->count() ): ?>
			<?php foreach($article->pinPoints as $pinpoint): ?>
			<div class="icon-point-header hidden-xs" style="<?= $pinpoint->style ?>">
				<span>
					<img src="/img/blog/articulo/icon-point-header.png" alt="pinpoint">
				</span>
				<div class="det-point-header">
					<div class="det-exp-point-header">
						<img class="img-det-exp-point-header" src="<?= $pinpoint->product->imageUrl ?>" alt="<?= $pinpoint->product->name ?>">
					</div>
					<div class="det-text-point-header">
						<?= $pinpoint->product->name ?><br>
						<span>REF: <?= $pinpoint->product->code ?></span>
						<div class="btn-det-point-header">
							<a href="<?= $pinpoint->product->detailUrl ?>">Ver</a>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
			<?php endif; ?>
		</figure>
	</div>
	
	<div class="container container-blog">
		
		<div class="col-xs-12 btn-like hidden-xs text-left">
			<!--<img src="/img/blog/articulo/btn-likefacebook.png" alt="Like Facebook">-->
			
			<a href="https://www.pinterest.com/pin/create/button/"
			   data-pin-do="buttonPin"
			   data-pin-count="beside"
			   data-pin-custom="true"
			   data-pin-media="<?= url($article->image_header) ?>"
			   data-pin-description="<?= $article->title_plain ?>"
			   data-pin-url="<?= url()->current() ?>" style="cursor:pointer;">
				<img src="/img/blog/articulo/btn-like-pint.png" alt="Like Pinterest">
			</a>
			
			<div class="fb-like" 
				 data-href="<?= url()->current() ?>" 
				 data-layout="standard" 
				 data-action="like" 
				 data-size="small" 
				 data-show-faces="false" 
				 data-share="false"></div>
			
		</div>
		
		<section class="col-xs-12 col-sm-8 content-article-blog">
			<!-- Text Articulo -->
			<div class="col-xs-5 col-sm-5 col-md-6 text-left sin-padding legend-art hidden-sm hidden-md hidden-lg">
				<legend><?= $article->category->name ?> / <?= $article->getPublishedDate($locate, false) ?></legend>
			</div>
			<h2 class="container hidden-sm hidden-md hidden-lg text-center">
				<?= $article->title ?>
			</h2>
			<article>
				<?= $article->body ?>
			</article>
			<div class="col-xs-5 col-sm-5 col-md-6 text-left sin-padding legend-art hidden-xs">
				<legend><?= $article->category->name ?> / <?= $article->getPublishedDate($locate, false) ?></legend>
			</div>
			<div class="col-xs-7 col-sm-7 col-md-6 text-right sin-padding div-share-art">
				<p class="share-art">{{trans('front.share')}}: </p>
				<ul class="share-art-list">
					<li class="icon-share-art-face">
						<a href="#"></a>
					</li>
					<li class="icon-share-art-pint">
						<a href="https://www.pinterest.com/pin/create/button/"
							data-pin-do="buttonPin"
							data-pin-count="beside"
							data-pin-custom="true"
							data-pin-media="<?= url($article->image_header) ?>"
							data-pin-description="<?= $article->title_plain ?>"
							data-pin-url="<?= url()->current() ?>" style="cursor:pointer;">

						</a>
					</li>
					<li class="icon-share-art-twit">
						<a href="#"></a>
					</li>
				</ul>
			</div>
			<!-- Fin Text Articulo -->
			
			<div class="clearfix"></div>
			
			<!-- Comentarios -->
			<div class="comments">
				<div class="col-xs-12 col-sm-6 text-left total-comments">
					<?= trans_choice('front.comment_count', $article->approvedComments->count()) ?>
				</div>
				<div class="col-sm-6 hidden-xs text-right orden-comments">
					<div class="col-xs-11 col-xs-offset-1 col-sm-11 col-sm-offset-1 col-md-11 col-md-offset-1 col-lg-9 col-lg-offset-2 text-right sin-padding">
						<section>
							<select id="dropdown-comments" class="cs-select cs-skin-underline">
								<option value="" selected>{{trans('front.order_newest_comments')}}</option>
								<option value="1">{{trans('front.order_oldest_comments')}}</option>
								<option value="2">{{trans('front.order_autor_comments')}}</option>
							</select>
						</section>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="well-input">
					<form role="form" class="form-comment" data-target="#commentbox">
						<div class="">
							<img class="media-object" src="<?= $customer->avatar ?>" alt="">
						</div>
						<div class="form-group">
							<textarea class="form-control" rows="2" data-comment-id=""></textarea>
						</div>
					</form>
				</div>
				
				<!-- Posted Comments -->
				<div class="clearfix"></div>
				<hr>
				
				<!-- Comment List -->
				<div id="commentbox">
					<?php foreach($comments as $comment): ?>
						<?php getCommentsTree($comment); ?>
					<?php endforeach; ?>
				</div>
			</div>
			<!-- Fin Comentarios -->
			
		</section>
		
		<aside class="col-sm-4 aside-article">
			
			<?php if($relatedArticles): ?>
			<!-- Articulos Relacionados -->
			<h4>{{trans('front.related_articles')}}</h4>
			<?php foreach($relatedArticles as $related): ?>
			<table class="col-xs-6 col-sm-12 hidden-xs art-blog-rel art-blog-table sin-padding">
				<tr>
					<td class="col-xs-12 col-sm-6 col-md-5 sin-padding">
						<a href="<?= $related->url_detail ?>">
							<img class="img-responsive" src="<?= $related->image_related ?>" alt="">
						</a>
					</td>
					<td class="col-xs-12 col-sm-6 col-md-7 art-blog-rel-p" valign="middle">
						<a href="<?= $related->url_detail ?>">
							<p><?= $related->title ?></p>
						</a>
					</td>
				</tr>
			</table>
			
			<div class="col-xs-6 col-sm-12 hidden-md hidden-sm hidden-lg art-blog-rel sin-padding">
				<div class="col-xs-12 col-sm-6 col-md-5 sin-padding">
					<a href="<?= $related->url_detail ?>">
						<img class="img-responsive" src="<?= $related->image_related ?>" alt="">
					</a>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-7 art-blog-rel-p">
					<a href="<?= $related->url_detail ?>">
						<p><?= $related->title ?></p>
					</a>
				</div>
			</div>
			
			<div class="hidden-xs clearfix"></div>
			<div class="hidden-xs dotted-separator"></div>
			<?php endforeach; ?>
			<!-- Fin Articulos Relacionados -->
			<?php endif; ?>

			<!-- begin SECTION NEWSFEED MOBILE -->
			<!--<div class="col-xs-12 hidden-xs newsfeed-mobile">
				<h1>{!! trans('front.title_newsletter_suscribe_subhome') !!}</h1>
				<form id="form-newsletter-blog">
					<div class="form-group newsfeed-form">
						<label for="nombre"></label>
						<input type="text" name="name" class="form-control newsfeed-form-control" id="nombre" placeholder="Nombre">
					</div>
					<div class="form-group">
						<label for="email"></label>
						<input type="email" name="email" class="form-control newsfeed-form-control" id="email" placeholder="Email">
					</div>
					<div class="btn-news-align">
						<button type="button" class="btn btn-newsfeed">{{trans('front.label_send')}}</button>
					</div>
				</form>
			</div>-->
			<div class="col-xs-12 hidden-xs newsfeed-mobile">
				<h1>{!!trans('front.title_newsletter_suscribe_blog')!!}</h1>
				<form id="form-newsletter-blog">
					{{ csrf_field() }}
					<div class="form-group newsfeed-form">
						<label for="nombre"></label>
						<input type="text" class="form-control newsfeed-form-control" id="nombre" name="name" placeholder="Nombre">
					</div>
					<div class="form-group">
						<label for="email"></label>
						<input type="email" class="form-control newsfeed-form-control email" name="email" placeholder="Email">
					</div>
					<div class="btn-news-align">
						<button type="button" class="btn btn-newsfeed btn-form-newsletter" data-form="form-newsletter-blog">{{trans('front.label_send')}}</button>
					</div>
				</form>
			</div>
		</aside>
	</div>
	
	<!-- NEWSLETTER MOBILE -->
	<div class="hidden-sm hidden-md hidden-lg">
		@include('store.partials.front.newsletter')
	</div>
	
	
</section>

@include('store.partials.front.bannersFooter')

<?php 
function getCommentsTree($comment){?>
	<div class="media media-wrap">
		<a class="pull-left" href="#">
			<img class="media-object" src="<?= $comment->customer->avatar ?>" alt="<?= $comment->customer->fullName ?>">
		</a>
		<div class="media-body">
			<h4 class="media-heading">
				<?= $comment->customer->fullName ?>
				<small><?= $comment->time_elapsed ?></small>
			</h4>
			{{ $comment->comment }}
			<div class="media-footer col-xs-12 sin-padding-left">
				<a href="#" class="btn-reply-comment" data-target="#thread-<?= $comment->id ?>" data-comment-id="<?= $comment->id ?>">Responder</a>
			</div>
			<div id="thread-<?= $comment->id ?>" class="media reply-box">
				<?php foreach( $comment->getApprovedComments() as $child ): ?>
					<?php getCommentsTree($child); ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>	
<?php } ?>

@endsection



@push('templates')
<script id="tpl-form-comment" type="text/x-handlebars-template">
	<div class="well-input">
		<form role="form" class="form-comment" data-target="@{{ target }}">
			<div class="">
				<img class="media-object" src="@{{ customer.avatar }}" alt="">
			</div>
			<div class="form-group">
				<textarea class="form-control" rows="1" data-comment-id="@{{ commentId }}"></textarea>
			</div>
		</form>
	</div>
</script>
<script id="tpl-comment-box" type="text/x-handlebars-template">
	<div class="media media-wrap">
		<a class="pull-left" href="#">
			<img class="media-object" src="@{{ customer.avatar }}" alt="@{{ customer.fullName }}">
		</a>
		<div class="media-body">
			<h4 class="media-heading">
				@{{ customer.fullName }}
				<small>hace pocos momentos</small>
			</h4>
			@{{ comment.text }}
			<div class="media-footer col-xs-12 sin-padding-left">
				<a href="#" class="btn-reply-comment" style="display:none;"
					data-target="#thread-xvvchf" 
					data-comment-id="xvvchf">Responder</a>
			</div>
			<div class="media reply-box">
			</div>
		</div>
	</div>
</script>
@endpush



@push('scripts')
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
<script>
	
	var templateCommentBox = Handlebars.compile( $('#tpl-comment-box').html() );
	
	var postCommentUI = function($form, customer, comment) {
		var $target = $($form.data('target'));
		var html = templateCommentBox({
			customer: customer,
			comment: {
				text: comment
			}
		});
		$target.append( html );
		var newEl = $('.media-wrap:last', $target);
		return newEl;
	};
	
	var postComment = function(text, commentId, customer, article) {
		return $.post(
			"/blog/comments/save", {
				text: text,
				comment_id: commentId,
				customer_id: customer.id,
				article_id: article.id
			}
		);
	};
	
	var getCustomerObject = function() {
		var customer = {
			id: '<?= $customer->id ?>',
			avatar: '<?= $customer->avatar ?>',
			fullName: '<?= $customer->fullName ?>'
		};
		return customer;
	};
	
	var getArticleObject = function() {
		var article = {
			id: '<?= $article->id ?>'
		};
		return article;
	};
	
	$( document ).ready(function() {
		var templateCommentForm = Handlebars.compile( $('#tpl-form-comment').html() );
	
		$( document ).on('click', '.btn-reply-comment', function(e) {
			e.preventDefault();
			var $target = $( $(this).data('target') );
			var commentId = $(this).data('comment-id');
			var html = templateCommentForm({
				target: $(this).data('target'),
				customer: getCustomerObject(),
				commentId: commentId
			});
			$target.append( html );
			//$(this).remove();
		});
		
		$( document ).on('keyup', '.form-comment textarea', function(e) {
			
			if (e.which === 13 && ! e.shiftKey) {
				
				console.log('post comment');
				
				e.preventDefault();
				
				if( !requireLogin() ) {
					return false;
				}
				
				var customer = getCustomerObject();
				var comment = $(this).val();
				var $form = $(this).parents("form");
				var parentCommentId = $(this).data('comment-id');
				var article = getArticleObject();
				var el = postCommentUI($form, customer, comment);
				
				postComment(comment, parentCommentId, customer, article)
						.done(function(json) {
							console.log('comment saved.');
							console.log(json);
							comment = json.comment;
							
							// add reply properties
							
							$('.reply-box', el).prop('id', 'thread-' + comment.id);
							
							var btn = $('.btn-reply-comment', el);
							btn.data('target', '#thread-' + comment.id)
								.data('comment-id', comment.id)
								.show();
						});
				
				if( $form.data('target') !== "#commentbox" ) {
					// remove the form
					$form.remove();
				}
				else {
					// clear form
					$(this).val('');
				}
				return false;
			}
		});
		
		// sharer
		
		$('.icon-share-art-face a').click(function(e) {
			e.preventDefault();
			facebookShare('<?= url()->current() ?>');
		});
		
		$('.icon-share-art-twit a').click(function(e) {
			e.preventDefault();
			twitterShare('<?= $article->title_plain ?>', '<?= url()->current() ?>');
		});
		
		// dropdown ordenar comentarios
		var dropdown = document.querySelector('#dropdown-comments');
		new SelectFx(dropdown, {
			onChange: function () {
				//window.location.href = $('#country_selector').val();
			}
		});
		
	});
	
	
</script>
@endpush		