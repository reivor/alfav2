@extends('store.layouts.default')

@section('title', $product->name)
@section('description', $product->description)
@section('keywords',$product->tags)
@section('picture', url($product->imageUrl))

@push('styles')
<style>
	.ruta .breadcrumb{
		padding: 8px 15px;
	}
</style>

@include('store.partials.front.shareThisScript')
@endpush



@section('content')  

<section class="container">
	
	<?= csrf_field() ?>

	<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display:none; margin-top: 10px;">
		<div class="demo-loader">
		</div>
	</div>
	
	<div id="mini-loader-colors" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display:none; margin-top: 10px;">
		<div class="internal-loading">
			<div class="internal-loader"></div>
		</div>
	</div>
	
	<div class="row sin-padding ruta hidden-xs hidden-sm">
		<div class="col-sm-9 hidden-xs">
			<ol class="breadcrumb">
				<li><a href="/">{{trans('front.home')}}</a></li>
				<?php foreach ($breadcrumbs as $keyCrumb => $crumb): ?>
					<?php if ($keyCrumb + 1 == count($breadcrumbs)): ?>
					<li class="active"><?= $crumb->name ?></li>
					<?php else: ?>
					<li><a href="<?= $crumb->detailUrl ?>"><?= $crumb->name ?></a></li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ol>
		</div>
		
		<div class="col-xs-12 col-sm-3 text-right volver">
			<div class="btn-volver">
				<a href="{{ url()->previous() }}">{{ trans('front.back') }}</a>
			</div>
		</div>
	</div>

	<div id="variation-container">
		@include('store.partials.products.productVariation')
	</div>

	@include('store.partials.products.productsRecent')
	@include('store.partials.products.productsSearchSlider')
	@include('store.partials.front.ideas')
</section>
@endsection


@push('scripts')
<script>

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});
	
	// slider "tambien has visto"
	$('#content-slider-views').owlCarousel({
		items: 4,	
		margin: 15,
		nav: true,
		loop: false,
		responsive:{
			0:{
				items:2
			},
			768:{
				items:2,
				loop: <?= count($recentProducts)>2 ? 'true': 'false '?>
			},
			1024: {
				items: 4,
				loop: <?= count($recentProducts)>4 ? 'true': 'false '?>
			}
		}
	});
	
	function formatEvent(){
		$(".format-color").click(function() {
			var color=$(this).attr('data-color');
			getVariations(color);
		});
	}
	
	function colorsEvent(){
		$(".radio-format").click(function() { 
			var format = $(this).val();
			$("#current-format").val(format);
			$("input:radio[name='optionsRadiosMobile'][value ='"+format+"']").prop('checked', true);
			getColors();
		});
	
		$(".radio-format-mobile").click(function() { 
			var format = $(this).val();
			$("#current-format").val(format);
			$("input:radio[name='optionsRadios'][value ='"+format+"']").prop('checked', true);
			getColors();
		});
		
	}
	
	function carouselInit(sliderReload) {
		$('#content-slider-related').owlCarousel({
			items: 4,
			margin: 15,
			nav: true,
			responsive:{
				0:{
					items:2
				},
				768:{
					items:2,
					loop: <?= count($relatedProducts)>2 ? 'true': 'false '?>
				},
				1024: {
					items: 4,
					loop: <?= count($relatedProducts)>4 ? 'true': 'false '?>
				}
			}
		});

		
		$(".slider-detail").owlCarousel({
			loop:false,
			margin:10,
			nav:true,
			responsive:{
				0:{
					items:6
				},
				768:{
					items:3
				},
				1024: {
					items: 6
				}
			}
		});
		
		$('.thumb-image-detail a').click(function(e) {
			e.preventDefault();
			// change main slider
			var slide = $(this).data('slide');
			$('#myCarousel').carousel(slide);
		});
		
	   
		$('#myCarousel').carousel({
			interval: 5000
		});

		//Handles the carousel thumbnails
		$('[id^=carousel-selector-]').click(function(e) {
			e.preventDefault();
			var id_selector = $(this).attr("id");
			try {
				var id = /-(\d+)$/.exec(id_selector)[1];
				console.log(id_selector, id);
				jQuery('#myCarousel').carousel(parseInt(id));
			}
			catch (e) {
				console.log('Regex failed!', e);
			}
		});
					
		$('#myCarousel').on('slid.bs.carousel', function(e) {
			var id = $('.item.active').data('slide-number');
			$('#carousel-text').html($('#slide-content-' + id).html());
		});
		
	}
	
	//type = 1 - Desktop | type =2 - Mobile
	function sendQuote(type){
		if( isLogin() ){
			var productId = $('#current-product').val();
			var params= {
				"productId" : productId
			};
			$.post( "/products/save-quote", params, function(request) {
				switch(parseInt(request)) {
					case 0:
						break;
					case 1:
						window.location.href = "<?= url(trans('routes.quotes')) ?>";
						break;
				}
			});
		}
		else{
			$('html, body').animate({ scrollTop: 0 }, "slow");
			if($(".menu-desktop").is(':visible')){
				$('#i-seccion').collapse('show');
			}
			else{
				$('#modal-login').modal('show');
			}
		}
	}
	
	function btnQuoteEvent(){
		$(".btn-solicita-cot").click(function(e) {
			e.preventDefault();
			sendQuote();
		});
	}
	
	function getColors(){
		var loader=$("#mini-loader-colors").html();
		$('#color-format-content').empty();
		$('#color-format-content').html(loader);
		
		$('#color-format-mobile-content').empty();
		$('#color-format-mobile-content').html(loader);
		
		var format=$("#current-format").val();
		var product=$("#current-product").val();
		
		var params= {
			"format" : format,
			"product" : product
		};
		
		$.ajax({
			data: params,
			cache: false,
			url: "/products/colors",
			type:  'post',
			success:  function (response) {
				$('#color-format-content').empty();
				$('#color-format-content').html(response);
				$('#color-format-mobile-content').empty();
				$('#color-format-mobile-content').html(response);
				formatEvent();
			}
		});
	}
	
	function getVariations(color){
		
		var format=$("#current-format").val();
		var reference=$("#reference-current-product").val();
		
		var loader=$("#mini-loader").html();
		$('#variation-container').empty();
		showLoader();
		$('#variation-container').html(loader);

		var params= {
			"format" : format,
			"reference" : reference,
			"color": color
		};
		
		$.ajax({
			data: params,
			cache: false,
			url: "/products/variation",
			type:  'post',
			success:  function (response) {
				hideLoader();
				$('#variation-container').empty();
				$('#variation-container').html(response);
				carouselInit(true);
				btnQuoteEvent();		
				formatEvent();
				colorsEvent();
			}
		});
	};

	carouselInit(false);
	btnQuoteEvent();		
	formatEvent();
	colorsEvent();

</script>
@endpush
