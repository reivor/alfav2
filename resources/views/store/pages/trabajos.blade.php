@extends('store.layouts.default')

@section('title', trans('front.work_with_us'))
@if (isset($metadata['title']['title']))
	@section('title', $metadata['title']['title'])
@endif
@if (isset($metadata['meta']['keyword']))
	@section('keywords', $metadata['meta']['keyword'])
@endif
@if (isset($metadata['meta']['description']))
	@section('description',$metadata['meta']['description'])
@endif
@if (isset($metadata['og']['type']))
	@section('type',$metadata['og']['type'])
@endif
@if (isset($metadata['og']['url']))
	@section('url',$metadata['og']['url'])
@endif
@if (isset($metadata['og']['image']))
	@section('picture',$metadata['og']['image'])
@endif

@section('content')

<section class="container-fluid quienes-somos">
	<div class="col-xs-12 box-big">
		<figure>
			<img class="img-responsive" src="/images/assets/banner_aboutus.jpg" alt="Quienes Somos">
		</figure>
	</div>
	<div class="clearfix"></div>
	<div class="container">
		<section class="col-xs-12 col-md-8 content-quienes-somos">
			<h1>Trabaja con nosotros</h1>
			<article>
				<p>
					Tú puedes ser parte de Alfa, la empresa que ofrece
					las mejores experiencias de creación y remodelación de ambientes.
				</p>
				<p>
					Si estás interesado, completa este <a href="http://www.elempleo.com/sitios-empresariales/colombia/alfa" target="_blank">formulario</a>
					y pronto te estaremos dando una respuesta.
				</p>
			</article>
			<div class="clearfix"></div>
		</section>
		<aside class="col-xs-12 col-md-4 aside-quienes-somos">
			<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
				<figure class="col-xs-12 col-md-6 col-lg-5">
					<img src="/img/quienes-somos/pic-aside-quienes-somos.png" alt="" class="responsive">
				</figure>
				<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
					<p><strong>línea de ética</strong> <br>grupo Alfa</p>
				</div>
			</div>
			<div class="separator-aside-qs hidden-xs hidden-sm col-xs-12"></div>
			<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
				<figure class="col-xs-12 col-md-6 col-lg-5">
					<img src="/img/quienes-somos/pic-aside-quienes-somos.png" alt="" class="responsive">
				</figure>
				<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
					<p><strong>línea de ética</strong> <br>grupo Alfa</p>
				</div>
			</div>
		</aside>
	</div>
</section>


@include('store.partials.front.bannersFooter')


@endsection
