@extends('store.layouts.default')
<!--@section('title', $page->title)-->
@section('picture', url($page->image_header))
@section('content')

<section class="container-fluid art-blog">
	<div class="container">
		<h2 class="hidden-xs header-blog"><?= $page->title ?></h2>
	</div>
	
	

	
	<div class="container container-blog">
		
		<!--
		<div class="col-xs-12 btn-like hidden-xs text-left">
			<!--<img src="/img/blog/articulo/btn-likefacebook.png" alt="Like Facebook">--
			
			<a href="https://www.pinterest.com/pin/create/button/"
			   data-pin-do="buttonPin"
			   data-pin-count="beside"
			   data-pin-custom="true"
			   data-pin-media="<?= url($page->image_header) ?>"
			   data-pin-description="<?= $page->title ?>"
			   data-pin-url="<?= url()->current() ?>" style="cursor:pointer;">
				<img src="/img/blog/articulo/btn-like-pint.png" alt="Like Pinterest">
			</a>
			
			<div class="fb-like" 
				 data-href="<?= url()->current() ?>" 
				 data-layout="standard" 
				 data-action="like" 
				 data-size="small" 
				 data-show-faces="false" 
				 data-share="false">	 
			</div>
		</div>
		
		-->

			<div class="col-xs-12 box-big text-center">
		<figure>
			<img class="img-responsive" src="<?= $page->image_header ?>" alt="<?= $page->title ?>" onerror='this.style.display = "none"'>
		</figure>
	</div>

	
		<section class="col-xs-12 col-sm-8 content-article-blog">
			<!-- Text Articulo -->
			<h2 class="container hidden-sm hidden-md hidden-lg text-center" >
				<?= $page->title ?>
			</h2>
			<article class="text-center">
			 <?=  $page->body ?> 
			</article>
			<div class="col-xs-5 col-sm-5 col-md-6 text-left sin-padding legend-art hidden-xs">
				<legend>  <?=  date_format($page->date_publishing, 'd/m/y'); ?></legend>
			</div>


			<!--
			<div class="col-xs-7 col-sm-7 col-md-6 text-right sin-padding div-share-art">			
				<p class="share-art">{{trans('front.share')}}: </p>
				<ul class="share-art-list">
					<li class="icon-share-art-face">
						<a href="#"></a>
					</li>
					<li class="icon-share-art-pint">
						<a href="https://www.pinterest.com/pin/create/button/"
							data-pin-do="buttonPin"
							data-pin-count="beside"
							data-pin-custom="true"
							data-pin-media="<?= url($page->image_header) ?>"
							data-pin-description="<?= $page->title ?>"
							data-pin-url="<?= url()->current() ?>" style="cursor:pointer;">
						</a>
					</li>
					<li class="icon-share-art-twit">
						<a href="#"></a>
					</li>
				</ul>
			</div>
			-->

			<!-- Fin Text Articulo -->
			<div class="clearfix"></div>	
		</section>
	</div>
	
	<!-- NEWSLETTER MOBILE -->
	<div class="hidden-sm hidden-md hidden-lg">
	<!--	@include('store.partials.front.newsletter')-->
	</div>
	
</section>
<!--
@include('store.partials.front.bannersFooter')
-->
@endsection
@push('scripts')
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
<script>
	
	var templateCommentBox = Handlebars.compile( $('#tpl-comment-box').html() );
	var postCommentUI = function($form, customer, comment) {
		var $target = $($form.data('target'));
		var html = templateCommentBox({
			customer: customer,
			comment: {
				text: comment
			}
		});
		$target.append( html );
		var newEl = $('.media-wrap:last', $target);
		return newEl;
	};
	
	var postComment = function(text, commentId, customer, article) {
		return $.post(
			"/blog/comments/save", {
				text: text,
				comment_id: commentId,
				customer_id: customer.id,
				article_id: article.id
			}
		);
	};
	
	var getCustomerObject = function() {
		var customer = {
			id: '<?= $customer->id ?>',
			avatar: '<?= $customer->avatar ?>',
			fullName: '<?= $customer->fullName ?>'
		};
		return customer;
	};
	var getArticleObject = function() {
		var article = {
			id: '<?= $page->id ?>'
		};
		return article;
	};
	$( document ).ready(function() {
		var templateCommentForm = Handlebars.compile( $('#tpl-form-comment').html() );
	
		$( document ).on('click', '.btn-reply-comment', function(e) {
			e.preventDefault();
			var $target = $( $(this).data('target') );
			var commentId = $(this).data('comment-id');
			var html = templateCommentForm({
				target: $(this).data('target'),
				customer: getCustomerObject(),
				commentId: commentId
			});
			$target.append( html );
			//$(this).remove();
		});
		
		$( document ).on('keyup', '.form-comment textarea', function(e) {
			
			if (e.which === 13 && ! e.shiftKey) {
				
				console.log('post comment');
				
				e.preventDefault();
				
				if( !requireLogin() ) {
					return false;
				}	
				var customer = getCustomerObject();
				var comment = $(this).val();
				var $form = $(this).parents("form");
				var parentCommentId = $(this).data('comment-id');
				var article = getArticleObject();
				var el = postCommentUI($form, customer, comment);
				
				postComment(comment, parentCommentId, customer, article)
						.done(function(json) {
							console.log('comment saved.');
							console.log(json);
							comment = json.comment;
							
							// add reply properties
							
							$('.reply-box', el).prop('id', 'thread-' + comment.id);
							
							var btn = $('.btn-reply-comment', el);
							btn.data('target', '#thread-' + comment.id)
								.data('comment-id', comment.id)
								.show();
						});
				
				if( $form.data('target') !== "#commentbox" ) {
					// remove the form
					$form.remove();
				}
				else {
					// clear form
					$(this).val('');
				}
				return false;
			}
		});
		// sharer
		$('.icon-share-art-face a').click(function(e) {
			e.preventDefault();
			facebookShare('<?= url()->current() ?>');
		});
		
		$('.icon-share-art-twit a').click(function(e) {
			e.preventDefault();
			twitterShare('<?= $page->title?>', '<?= url()->current() ?>');
		});
		// dropdown ordenar comentarios
		var dropdown = document.querySelector('#dropdown-comments');
		new SelectFx(dropdown, {
			onChange: function () {
				//window.location.href = $('#country_selector').val();
			}
		});		
	});
</script>
@endpush