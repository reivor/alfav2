@extends('store.layouts.default')

@section('title', trans('front.creation_points_header'))

@section('content')
<section class="container" id="punto-creacion">
	<!-- Ruta -->
	<div class="row sin-padding ruta hidden-xs hidden-sm">
		<ol class="breadcrumb">
			<li><a href="/">{{ trans('front.home') }}</a></li>
			<li class="active">{{ trans('front.creation_points_header') }}</li>
		</ol>
	</div>
	<!-- Fin Rutas -->
	<div class="row row-no-margin sin-padding">
		<div class="col-xs-12">
			<h1>{{ trans('front.creation_points_header') }}</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-9">
			<div class="col-xs-12 col-md-4 div-address">
				<form action="">
					<div class="col-xs-12 select-zona sin-padding">
						<select id="sl-zones" class="cs-select cs-skin-underline city-stores">
							<option value="" disabled selected>{{ trans('front.zone') }}</option>
							<?php foreach($departments as $department): ?>
							<option value="<?= $department->id ?>"><?= $department->name ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-xs-12 select-ciudad sin-padding">
						<select id="sl-cities" class="cs-select cs-skin-underline city-stores">
							<option value="" disabled selected>{{ trans('front.city') }}</option>
						</select>
					</div>
					<div class="col-xs-12 btn-search">
						<button class="btn-search-maps" type="submit">{{ trans('front.label_search') }}</button>
					</div>
				</form>
				<div class="hidden-sm hidden-xs col-xs-12 direccion-det">

				</div>
			</div>
			<div class="col-xs-12 col-md-8">
				<div id="map" class="maps" style="width:100%;">

				</div>
			</div>
		</div>

		<div id="store-listing" class="col-xs-12 col-sm-3 div-direcciones hidden-xs hidden-sm">

		</div>

		<div id="search-results-mobile" class="col-xs-12 hidden-md hidden-lg">

		</div>

		<div id="store-details"></div>
	</div>


</section>

@endsection


@push('templates')

<script id="tpl-search-results" type="text/x-handlebars-template">
	<div class="content mCustomScrollbar">
		@{{#each groups}}
		<div class="ciudades col-xs-12">
			<h5>@{{ city.name }}</h5>
			@{{#each stores}}
			<div class="col-xs-12 sin-padding">
				<a href="#" data-store-id="@{{ id }}" class="store-item">@{{ name }}</a>
				<select class="cs-select cs-skin-underline city-stores selStore hidden">
					<option value="0" disabled selected>@{{ name }}</option>
				</select>
			</div>
			@{{/each}}
		</div>
		@{{/each}}
	</div>
</script>

<script id="tpl-search-results-mobile" type="text/x-handlebars-template">
	@{{#each stores}}
	<div class="hidden-md hidden-lg row row-no-margin direccion-det-mobile">
		<p class="tit-direccion">@{{ name }}</p>
		<p class="address"><span>Dirección</span><br>@{{ location }}</p>
		<p class="phone"><span>Teléfono</span><br>@{{ phone_local }}</p>
		<p class="phone-mobile"><span>Cel.</span><br>@{{ phone_mobile }}</p>
		<p class="mail"><span>mail</span><br>@{{ email }}</p>
		<p class="schedule"><span>Horario de atención</span><br>@{{ opening_hours }}</p>
	</div>
	@{{/each}}
</script>

<script id="tpl-store-details" type="text/x-handlebars-template">
	<p class="tit-direccion">@{{ store.name }}</p>
	<p class="address"><span>{{ trans('front.address') }}</span><br>@{{ store.location }}</p>
	<p class="phone"><span>{{ trans('front.phone') }}</span><br>@{{ store.phone_local }}</p>
	<p class="phone-mobile"><span>{{ trans('front.mobile') }}</span><br>@{{ store.phone_mobile }}</p>
	<p class="mail"><span>mail</span><br>@{{ store.email }}</p>
	<p class="schedule"><span>{{ trans('front.opening_hours') }}</span><br>@{{ store.opening_hours }}</p>
</script>

@endpush

@push('scripts')
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="/themes/store/vendor/handlebars/handlebars.min.js"></script>

<script>
	var dropdownZones, dropdownCities;
	var storeArray = [];

	$( document ).ready(function() {
		console.log($('#sl-zones').get(0));

		dropdownZones = new SelectFx($('#sl-zones').get(0), {
			onChange: function(val) {
				showLoader();
				var departmentId = val;
				console.log('selected department: ', departmentId);
				$.getJSON("/<?= trans('routes.stores') ?>/cities/" + departmentId, function(data) {
					// reset cities dropdown
					var list = $('#sl-cities').empty().clone();
					$(dropdownCities.selEl).remove();

					// populate dropdown
					list.append('<option value="" disabled selected>Ciudad</option>');
					for(var i in data) {
						var city = data[i];
						list.append('<option value="' + city.id + '">' + city.name + '</option>');
					}

					$('.select-ciudad').append(list);
					dropdownCities = new SelectFx( $('#sl-cities').get(0), {} );
					hideLoader();
				});
			}
		});

		dropdownCities = new SelectFx($('#sl-cities').get(0), {
			onChange: function(val) {
				//callback for value change
			}
		});

		var displaySearchResultsDesktop = function(groups) {
			var $target = $('#store-listing');
			var template = Handlebars.compile( $('#tpl-search-results').html() );
			var html = template({ groups: groups });
			$target.html(html);
			// update dropdowns
			setTimeout(function() {
				$('.selStore').each(function(index, item) {
					new SelectFx(item);
				});
			}, 200);
		};

		var displaySearchResultsMobile = function(stores) {
			var $target = $('#search-results-mobile');
			var template = Handlebars.compile( $('#tpl-search-results-mobile').html() );
			var html = template({ stores: stores });
			$target.html(html);
		};

		var updateStoreArray = function(json) {
			storeArray = [];
			for( var i in json ) {
				var group = json[i];
				for( var j in group.stores ) {
					var store = group.stores[j];
					storeArray[store.id] = store;
				}
			}
		};

		var getStoreById = function(id) {
			return storeArray[id];
		};

		var displayStore = function(store) {
			var template = Handlebars.compile( $('#tpl-store-details').html() );
			var html = template({ store: store });
			$('.direccion-det').html(html);
			$('.direccion-det-mobile').html(html);
		};


		var getMarkers = function initialize(json) {
			
			var lat, lon;
			for (var i in json){
				lat =  json[i].city.lat;
				lon =  json[i].city.lon;
			}
			
			var mapProp = {
				// center:new google.maps.LatLng({{ $store->latitude }},{{ $store->longitude }}),
				center:new google.maps.LatLng(lat,lon),
				zoom: 10,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			};
			

			var map = new google.maps.Map(document.getElementById("map"),mapProp);
			var infoWindow = new google.maps.InfoWindow();

			for( var i in json ) {
				var group = json[i];
				for( var j in group.stores ) {
					var image = '/images/alfa-marker.jpg';
					var latLng = new google.maps.LatLng(group.stores[j].latitude, group.stores[j].longitude);
					var marker = new google.maps.Marker({
						position: latLng,
						map: map,
						icon: image,
						title: group.stores[j].name,
						visible: true,
						animation: google.maps.Animation.DROP
					});

					(function(marker, j) {
					google.maps.event.addListener(marker, "click", function(e) {
						infoWindow.setContent('<div class="col-md-12">'+
							'<h1>'+group.stores[j].name+'</h1>'+
							'</div>'+
							'<div class="col-md-6">'+
							'<p><strong>Dirección</strong></p>'+
							'<p>'+group.stores[j].location+'</p>'+
							'<p><strong>Horario</strong></p>'+
							'<p>'+group.stores[j].opening_hours+'</p>'+
							'<p><strong>Teléfonos</strong></p>'+
							'<p>'+group.stores[j].phone_local+ '&nbsp;&nbsp;&nbsp;' +group.stores[j].phone_mobile +'</p>'+
							'<p><strong>Email</strong></p>'+
							'<p><a href="mailto:'+ group.stores[j].email +'">'+group.stores[j].email+'</a></p>'+
							'</div>'+
							'<div class="col-md-6 hidden-xs hidden-sm">'+
							'<img src="'+group.stores[j].picture+'" style="width:100%">'+
							'</div>');
						infoWindow.open(map, marker);
					});
				})(marker, j);
			}
		}
	};

	$('.btn-search-maps').click(function(e) {
		showLoader();
		e.preventDefault();
		var departmentId = getSelectedZone();
		var cityId = getSelectedCity();

		$.getJSON("/<?= trans('routes.stores') ?>/search", {
			department: departmentId ? departmentId : '',
			city: cityId ? cityId : ''
		}, function(groups) {
				// update stores
				updateStoreArray(groups);
				// display search results
				displaySearchResultsDesktop(groups);
				displaySearchResultsMobile(storeArray);
				// display markers
				getMarkers(groups);
				hideLoader();

			});
	});

	$(document).on('click', '.store-item', function(e) {
		e.preventDefault();
		var target = $(this).data('store-id');
		var store = getStoreById(target);
		console.log('selected store', store);
		displayStore(store);
	});

	var getSelectedZone = function() {
		var index = dropdownZones.current;
		var opt = $(dropdownZones.selOpts[index]).data('value');
		return opt !== undefined ? opt : false;
	};

	var getSelectedCity = function() {
		var index = dropdownCities.current;
		var opt = $(dropdownCities.selOpts[index]).data('value');
		return opt !== undefined ? opt : false;
	};

	function initialize() {
		var mapProp = {
			center: new google.maps.LatLng(<?= $store->latitude ?>, <?= $store->longitude ?>),
			zoom: 10,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map=new google.maps.Map(document.getElementById("map"),mapProp);
	}

	google.maps.event.addDomListener(window, 'load', initialize);

});

</script>
@endpush
