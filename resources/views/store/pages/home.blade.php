
@extends('store.layouts.default')

@if (isset($metadata['title']['title']))
	@section('title', $metadata['title']['title'])
@endif
@if (isset($metadata['meta']['keyword']))
	@section('keywords', $metadata['meta']['keyword'])
@endif
@if (isset($metadata['meta']['description']))
	@section('description',$metadata['meta']['description'])
@endif
@if (isset($metadata['og']['type']))
	@section('type',$metadata['og']['type'])
@endif
@if (isset($metadata['og']['url']))
	@section('url',$metadata['og']['url'])
@endif
@if (isset($metadata['og']['image']))
	@section('picture',$metadata['og']['image'])
@endif


@section('content')

<!-- Slider -->
<div class="container-fluid sin-padding">
	<div class="">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<?php if(count($carouselBanners)): ?>
				<?php foreach ($carouselBanners as $key => $banner): ?>
				<div class="item <?= $key == 0 ? 'active' : '' ?>">
					<a href="<?=$banner->detailUrl?>" target="<?= $banner->target ?>">
						<img src="<?= $banner->image_url ?>" alt="">
					</a>
					<div class="btn-go">
						<span class="arrow-down">
							<a href="#ambientes" class="anchorLink"><img src="img/icon/arrow-down.gif" alt=""></a>
						</span>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>

<!-- SECTION AMBIENTES -->
<section class="container" id="ambientes">
	<div class="container section-ambientes-title">
		<h1> <span>{{ trans('front.make') }}</span> {{ trans('front.ambients') }}</h1>
	</div>
	<div class="row layout" id="type-layout">
	</div>
	<!--LAYOUTS-->

	<?php if((int)$currentLayout > (int)0): ?>
	@include('store.partials.home.homeLayout'.$currentLayout)
	<?php endif; ?>

	<?php if( isset($bannersPromoHomeTop[1]) || isset($bannersPromoHomeTop[2]) ): ?>
	<section class="container">
		<hr class="separador hidden-md hidden-lg">
	</section>
	<?php endif; ?>

	<div class="row row-no-margin grid-banners-home">
		<?php if(isset($bannersPromoHomeTop[1])): ?>
		<div class="col-sm-12 col-md-8 col-lg-8 coleccioncoleccion">
			<a href="<?=$bannersPromoHomeTop[1]->detailUrl?>" target="<?= $bannersPromoHomeTop[1]->target?>">
				<img class="img-responsive img-coleccion" src="<?= $bannersPromoHomeTop[1]->image_url ?>" alt="">
			</a>
		</div>
		<?php endif; ?>

		<?php if(isset($bannersPromoHomeTop[2])): ?>
		<div class="hidden-xs hidden-sm col-xs-12 col-sm-4 pinturas-mobile">
			<a href="<?=$bannersPromoHomeTop[2]->detailUrl?>" target="<?=$bannersPromoHomeTop[2]->target?>">
				<img class="img-pinturas hidden-xs" src="<?= $bannersPromoHomeTop[2]->image_url ?>" alt="">
			</a>
			<a href="<?=$bannersPromoHomeTop[2]->detailUrl?>" target="<?=$bannersPromoHomeTop[2]->target?>">
				<img class="img-responsive img-pinturas hidden-sm hidden-md hidden-lg" src="<?= $bannersPromoHomeTop[2]->image_url ?>" alt="Pinturas">
			</a>
		</div>
		<?php endif; ?>
	</div>
</section>
<!-- end SECTION AMBIENTES -->

<!-- SECTION PRODUCTOS -->
<section class="container">
	@include('store.partials.products.productsSearchSlider')
</section>
<!-- end SECTION PRODUCTOS -->

<!-- SECTION IDEAS -->
<section class="container">
	@include('store.partials.front.ideas')
</section>
<!-- end SECTION IDEAS -->

@include('store.partials.front.bannersFooter')

@endsection


@push('scripts')
<script>
	$( document ).ready(function() {
		// activate slider home
		//$('#carousel-example-generic').bcSwipe({ threshold: 50 });

		$("#carousel-example-generic").swiperight(function() {
			$("#carousel-example-generic").carousel('prev');
		});

		$("#carousel-example-generic").swipeleft(function() {
		   $("#carousel-example-generic").carousel('next');
		});

	});
</script>
@endpush
