@extends('store.layouts.default')

@section('title', trans('front.label_design'))
	{{-- @if (isset($metadata['title']['title']))
	@section('title', $metadata['title']['title'])
@endif --}}
@if (isset($metadata['meta']['keyword']))
	@section('keywords', $metadata['meta']['keyword'])
	@endif
	@if (isset($metadata['meta']['description']))
		@section('description',$metadata['meta']['description'])
		@endif
		@if (isset($metadata['og']['type']))
			@section('type',$metadata['og']['type'])
			@endif
			@if (isset($metadata['og']['url']))
				@section('url',$metadata['og']['url'])
				@endif
				@if (isset($metadata['og']['image']))
					@section('picture',$metadata['og']['image'])
					@endif
					@section('content')

						<section class="container-fluid quienes-somos">
							<div class="col-xs-12 box-big">
								<figure>
									<img class="img-responsive" src="/images/assets/banner_diseno.png" alt="<?= trans('front.label_design') ?>">
								</figure>
							</div>
							<div class="clearfix"></div>
							<div class="container">
								<section class="col-xs-12 col-md-8 content-quienes-somos">
									<h1>Diseño</h1>
									<article>
										<p>
											¿Buscas asesoría en diseño?<br>
											Encuentra en los puntos de creación a nuestros expertos en arquitectura,
											diseño de interiores y otras áreas, quienes están siempre dispuestos a ayudarte
											para hacer realidad tus ideas de remodelación.
										</p>
									</article>
									<div class="clearfix"></div>
								</section>
								<aside class="col-xs-12 col-md-4 aside-quienes-somos">
									<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
										<figure class="col-xs-12 col-md-6 col-lg-5">
											<a href="http://www.pisosalfa.com.ec/multimedia_gres/entorno.html" target="_blank">
												<img src="/images/footer/simulador_gres.jpg" alt="" class="responsive">
											</a>
										</figure>
										<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
											<p><strong><a href="http://www.pisosalfa.com.ec/multimedia_gres/entorno.html" target="_blank">Simulador de gres</a></strong> <br>grupo Alfa</p>
										</div>
									</div>
									<div class="separator-aside-qs hidden-xs hidden-sm col-xs-12"></div>
									<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
										<figure class="col-xs-12 col-md-6 col-lg-5">
											<a href="http://www.corevcolombia.com/cotizador_alfa/" target="_blank">
												<img src="/images/footer/simulador_pinturas.jpg" alt="" class="responsive">
											</a>
										</figure>
										<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
											<p><strong><a href="http://www.corevcolombia.com/cotizador_alfa/" target="_blank">Simulador de pinturas</a></strong> <br>grupo Alfa</p>
										</div>
									</div>
								</aside>
							</div>
						</section>


						@include('store.partials.front.bannersFooter')


					@endsection
