@extends('store.layouts.default')

@section('title', 'Blog Alfa')

@section('content')
<section class="container-fluid blog">
	
	<div class="container">
		
		<?php if( $featured ): ?>
		<!-- Header Blog -->
		<section class="header-blog col-xs-12 sin-padding">
			<div class="col-xs-12 col-sm-7 col-lg-8 sin-padding">
				<figure>
					<a href="<?= $featured->url_detail ?>">
						<img class="img-responsive" src="<?= $featured->image_header ?>" alt="">
					</a>
				</figure>
			</div>
			<div class="col-xs-12 col-sm-5 col-lg-4">
				<div class="col-xs-12 legend-art">
					<legend><?= $featured->category->name ?> / <?= $featured->getPublishedDate($locate, false) ?></legend>
				</div>
				<div class="clearfix"></div>
				<h2><a href="<?= $featured->url_detail ?>"><?= $featured->title ?></a></h2>
				<p><?= $featured->excerpt ?></p>
				<a href="<?= url("/blog/{$featured->category->slug}/{$featured->slug}/{$featured->id}") ?>">
					<img class="btn-ver-mas-blog img-responsive" src="/img/ideas/btn-ver-mas.png" alt="Ver Más">
				</a>
			</div>
		</section>
		<!-- Fin Header Blog -->
		<?php endif; ?>
		
		<div class="clearfix"></div>
		
		<!-- Content Blog -->
		<section class="content-blog col-xs-12">
			
			<div class="container section-ideas-recientes-title">
				<h2>{!! trans('front.recent_ideas')!!}</h2>
			</div>
			
			<div class="row">
				<section id="pinBoot">
					<?php foreach($articles as $article): ?>
					<article class="item-blog item-orange" style="background-color: <?= $article->category->color ?>">
						<a href="<?= $article->url_detail ?>">
							<img src="<?= $article->image_default ?>" alt="<?= $article->title_plain ?>">
						</a>
						<div class="col-xs-12 legend-art-blog">
							<legend><?= $article->category->name ?> / <?= $article->format_publish_date ?></legend>
						</div>
						<h4>
							<a href="<?= $article->url_detail ?>">
								<?= $article->title ?>
							</a>
						</h4>
						<p><?= $article->excerpt ?></p>
					</article>
					<?php endforeach; ?>
				</section>
			</div>
			
			<div class="clearfix"></div>
			<?php if( $totalPages > 1 ): ?>
			<div class="col-xs-12 ver-mas-amarillo">
				<a href="#" class="btn-more">{{ trans('front.see_more') }}</a>
			</div>
			<?php endif; ?>
		</section>
		<!-- Fin Content Blog -->
	</div>
</section>

@endsection

@push('scripts')
<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
<script src="/themes/store/js/blog.js"></script>
<script>
	$( document ).ready(function() {
		
		var template = Handlebars.compile( $('#tpl-article-box').html() );
		var currentPage = 1;
		var totalPages = <?= $totalPages ?>;
		
		$('.btn-more').click(function(e) {
			e.preventDefault();
			
			var $button = $(this);
			var currentText = $button.text();
			$button.text('cargando...');
			
			var $container = $('#pinBoot');
			
			$.get('/blog/articles/', {
				page: currentPage + 1
			}, function(data) {
				console.log(data);
				
				for( var i in data.articles ) {
					var article = data.articles[i];
					var html = template({article: article});
					$container.append(html);
				}
				
				$button.text(currentText);
				
				currentPage++;
				if( currentPage === totalPages ) {
					$button.hide();
				}
			});
		});
		
	});
</script>
@endpush

@push('templates')
<script id="tpl-article-box" type="text/x-handlebars-template">
	<article class="item-blog item-orange" style="background-color: @{{ article.category.color }}">
		<a href="@{{ article.url_detail }}">
			<img src="@{{ article.image_default }}" alt="@{{ article.title_plain }}">
		</a>
		<div class="col-xs-12 legend-art-blog">
			<legend>@{{ article.category.name }} / @{{ article.format_publish_date }}</legend>
		</div>
		<h4>
			<a href="@{{ article.url_detail }}">
				@{{{ article.title }}}
			</a>
		</h4>
		<p>@{{ article.excerpt }}</p>
	</article>
</script>
@endpush