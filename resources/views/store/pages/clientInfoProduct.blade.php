@extends('store.layouts.default')
@section('content')

<!-- Content -->
<section class="container-fluid info-cliente-producto">
	<div class="container">
		<!-- Ruta -->
		<div class="row row-no-margin sin-padding ruta hidden-xs hidden-sm">
			<div class="col-xs-12">
				<ol class="breadcrumb sin-padding">
					<li><a href="/">{{ trans('front.home') }}</a></li>
					<li><a href="<?= url(trans('routes.client_info')) ?>">{{ trans('front.customer_information') }}</a></li>
					<li class="active"><?= $type ?></li>
				</ol>
			</div>
		</div>
		<!-- Fin Rutas -->
		
		<div class="row row-no-margin">
			<div class="col-xs-12">
				<h1><?= $type ?></h1>
			</div>	
		</div>

		<!-- Filtros -->
		<div class="row row-no-margin  sin-padding hidden-xs hidden-sm">
			<div class="col-sm-6 col-sm-offset-3 filtros">
				<div class="col-xs-6">
					<select class="cs-select cs-skin-underline select-info" id="category-select">
						<option value="" disabled>Categoria</option>
						<?php foreach($categoriesSelect as $cat): ?>
						<option value="<?= $cat->slug ?>" <?= $cat->slug == $category ? 'selected' : '' ?>><?=$cat->name?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-xs-6" id="product-col">
					<select class="cs-select cs-skin-underline select-info" id="product-select">
						<option value="" disabled>Productos</option>
						<?php foreach($productsSelect as $prod): ?>
						<option value="<?= $prod->id ?>" <?= $prod->id == $product ? 'selected' : '' ?>><?= $prod->name ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
		<!-- Fin Filtros -->
		<div id="results">
			<div class="row row-no-margin container-results">
			</div>
			<div class="row row-no-margin">
				<?php if($moreProducts): ?>
				<div class="col-xs-12 ver-mas-purple">
					<a href="#" class="more">{{ trans('front.see_more') }}</a>
				</div>
				<?php endif; ?>

				<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display: none; margin-top: 10px;">
				<div class="internal-loading">
					<div class="internal-loader"></div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<!-- Fin Content -->
@endsection

@push('templates')
	<script id="tpl-info" type="text/x-handlebars-template">
		<div class="col-xs-6 col-sm-3">
			<a href="@{{ url }}" download>
				<figure><img src="/images/icon-item-info.png" alt=""></figure>
				<p class="tit-item-info">@{{ name }}</p>
			</a>
		</div>
	</script>
@endpush

@push('scripts')
<script>

var type = "<?= $type ?>";
var category = "<?= $category ?>";
var page = 1;

var garantiasString = "<?= trans('routes.warranties') ?>";
var sellosString = "<?= trans('routes.seals') ?>";
var manualsString = "<?= trans('routes.manuals') ?>";
var datasheetsString = "<?= trans('routes.datasheets') ?>";

function isGarantias(){
	return (type === garantiasString);
}

function isSellos(){
	return (type === sellosString);
}

function isManuales(){
	return (type === manualsString);
}

function isFichas(){
	return (type === datasheetsString);
}

function makeInfoContent(data){
	var url = "";
	
	if(isGarantias()){
		url = data.guarantee;
	}
	else if(isSellos()){
		url = data.quality_seal;
	}
	else if(isManuales()){
		url = data.user_manual;
	}
	else if(isFichas()){
		url = data.data_sheet;
	}

	var templateInfo = Handlebars.compile($('#tpl-info').html());
	var htmlLayout = templateInfo({
		url: url, 
		name: data.name
	});
	
	return htmlLayout;
}

var categorias = document.querySelector('#category-select');
new SelectFx( categorias, {
	onChange: function(val) {
		var select = $('#category-select');
		window.location.replace("/informacion/"+type+"/"+select.val());
	}
});

var productos = document.querySelector('#product-select');
new SelectFx( productos, {
	onChange: function(val) {
		var select = $('#product-select');
		window.location.replace("/informacion/"+type+"/"+category+"/"+select.val());
	}
});

$('.more').click(function (e) {
	e.preventDefault();
	var me = $(this);
	me.hide();
	$('#mini-loader').css('display', 'block');
	
	$.getJSON("/loadMore/"+type+"/"+page+"/"+category, function(jsonData){
		var products = jsonData.products;
		var moreProducts = jsonData.moreProducts;
		
		page = page + 1;

		$.each(products, function(i,data){
			var htmlLayout = makeInfoContent(data);
			$(".container-results").append(htmlLayout);
		});
		
		if(moreProducts){
			me.show();
		}
		
		$('#mini-loader').css('display', 'none');
	});
});

$( document ).ready(function() {
	$.each(<?=$products?>, function(i,data){
		var htmlLayout = makeInfoContent(data);
		$(".container-results").append(htmlLayout);
	});
});

</script>
@endpush