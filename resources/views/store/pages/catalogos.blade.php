@extends('store.layouts.default')

@section('title', trans('front.label_catalogues'))
@if (isset($metadata['title']['title']))
	@section('title', $metadata['title']['title'])
@endif
@if (isset($metadata['meta']['keyword']))
	@section('keywords', $metadata['meta']['keyword'])
@endif
@if (isset($metadata['meta']['description']))
	@section('description',$metadata['meta']['description'])
@endif
@if (isset($metadata['og']['type']))
	@section('type',$metadata['og']['type'])
@endif
@if (isset($metadata['og']['url']))
	@section('url',$metadata['og']['url'])
@endif
@if (isset($metadata['og']['image']))
	@section('picture',$metadata['og']['image'])
@endif


@section('content')

<section class="container-fluid info-cliente-producto info-catalogos">
	<div class="container">
		<!-- Ruta -->
		<div class="row row-no-margin sin-padding ruta hidden-xs hidden-sm">
			<div class="col-xs-12">
				<ol class="breadcrumb sin-padding">
					<li><a href="/">{{ trans('front.home') }}</a></li>
					<li class="active">{{ trans('front.label_catalogues') }}</li>
				</ol>
			</div>
		</div>
		<!-- Fin Rutas -->

		<div class="row row-no-margin">
			<div class="col-xs-12">
				<h1>{{ trans('front.label_catalogues') }}</h1>
			</div>
		</div>

		<div id="results">
			<div class="row row-no-margin container-results">
				<?php foreach ($catalogos as $file){ ?>
					<div class="col-xs-6 col-sm-3">
						<a href="/files/catalogos/<?=$file['basename']?>" target="_blank">
							<figure>
								<img src="/files/catalogos/<?=$file['filename']?>.jpg" alt="">
							</figure>
						</a>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
</section>

@endsection
