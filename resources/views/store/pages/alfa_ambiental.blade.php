@extends('store.layouts.default')

@section('title', trans('front.label_environmental_alfa'))
@if (isset($metadata['title']['title']))
	@section('title', $metadata['title']['title'])
@endif
@if (isset($metadata['meta']['keyword']))
	@section('keywords', $metadata['meta']['keyword'])
@endif
@if (isset($metadata['meta']['description']))
	@section('description',$metadata['meta']['description'])
@endif
@if (isset($metadata['og']['type']))
	@section('type',$metadata['og']['type'])
@endif
@if (isset($metadata['og']['url']))
	@section('url',$metadata['og']['url'])
@endif
@if (isset($metadata['og']['image']))
	@section('picture',$metadata['og']['image'])
@endif

@section('content')

<section class="container-fluid quienes-somos">
	<div class="col-xs-12 box-big">
		<figure>
			<img class="img-responsive" src="/images/assets/banner_ambiental.png" alt="<?= trans('front.label_environmental_alfa') ?>">
		</figure>
	</div>
	<div class="clearfix"></div>
	<div class="container">
		<section class="col-xs-12 col-md-8 content-quienes-somos">
			<h1>Alfa <strong>Ambiental</strong></h1>
			<article>
				<p>
					Estamos comprometidos con el desarrollo sostenible y por esto, promovemos acciones encaminadas a minimizar y prevenir los impactos ambientales generados en el desarrollo de nuestras actividades:
				</p>
				<p>
					1. Optimizamos el consumo de agua en la producción de nuestros productos:
				</p>
				<p>
					Reducimos el consumo de agua en un 6% frente al 2011:
				</p>
				<ul>
					<li>Nuestro gres consume 0.3 litros de agua/kg. producto.</li>
					<li>Nuestra cerámica consume 0.17 litros de agua/kg. producto.</li>
				</ul>
				<p>
					El estándar internacional ambiental indica que el consumo no debe ser mayor a 1lt./kg. de producto.
				</p>
				<p>
					Reutilizamos el 88% de agua residual industrial y el 50% del agua residual doméstica:
				</p>
				<ul>
					<li>
						Implementamos un nuevo uso del agua recuperada en las celdas de secado de gres,
						con consumos promedio al mes de 500 m3 de agua, cantidad suficiente para 50* hogares en un mes.
					</li>
				</ul>
				<p>
					<small>* Informe 4 años para salvar el agua de Bogotá</small>
				</p>
				<p>
					2. Implementamos el compostaje para la recuperación de los suelos:
				</p>
				<ul>
					<li>
						Desde 2012 transformamos y utilizamos los residuos orgánicos generados
						en los restaurantes y cafeterías para la recuperación de los suelos
						de los títulos mineros y el mantenimiento de nuestros jardines.
					</li>
				</ul>
				<p>
					3. Reciclamos para salvar más arboles:
				</p>
				<ul>
					<li>
						Generamos conciencia ambiental reciclando 1.450 kg. de papel equivalentes a salvar 25 árboles.
					</li>
				</ul>
			</article>
			<div class="clearfix"></div>
		</section>
		<aside class="col-xs-12 col-md-4 aside-quienes-somos">
			<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
				<figure class="col-xs-12 col-md-6 col-lg-5">
					<a href="http://alfa.com.co/informe_gestion/index.html" target="_blank">
						<img src="/images/footer/AlfaAmbiental.jpg" alt="" class="responsive">
					</a>
				</figure>
				<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
					<p><strong><a href="http://alfa.com.co/informe_gestion/index.html" target="_blank">Informe de gestión ambiental</a></strong> <br>grupo Alfa</p>
				</div>
			</div>
			<div class="separator-aside-qs hidden-xs hidden-sm col-xs-12"></div>
			<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
				<figure class="col-xs-12 col-md-6 col-lg-5">
					<a href="http://alfa.com.co/fichas_ambiental/21" target="_blank">
						<img src="/images/footer/Fichas_Ambientales.jpg" alt="" class="responsive">
					</a>	
				</figure>
				<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
					<p><strong><a href="http://alfa.com.co/fichas_ambiental/21" target="_blank">Fichas ambientales</strong></a> <br>grupo Alfa</p>
				</div>
			</div>
		</aside>
	</div>
</section>

@include('store.partials.front.bannersFooter')

@endsection
