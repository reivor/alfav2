@extends('store.layouts.default')

@section('title', trans('front.label_about_us'))
@if (isset($metadata['title']['title']))
	@section('title', $metadata['title']['title'])
@endif
@if (isset($metadata['meta']['keyword']))
	@section('keywords', $metadata['meta']['keyword'])
@endif
@if (isset($metadata['meta']['description']))
	@section('description',$metadata['meta']['description'])
@endif
@if (isset($metadata['og']['type']))
	@section('type',$metadata['og']['type'])
@endif
@if (isset($metadata['og']['url']))
	@section('url',$metadata['og']['url'])
@endif
@if (isset($metadata['og']['image']))
	@section('picture',$metadata['og']['image'])
@endif


@section('content')

<!-- Content -->
<section class="container-fluid quienes-somos">
	<div class="col-xs-12 box-big">
		<figure>
			<img class="img-responsive" src="/images/assets/banner_aboutus.jpg" alt="<?= trans('front.label_about_us') ?>">
		</figure>
	</div>
	<div class="clearfix"></div>
	<div class="container">
		<section class="col-xs-12 col-md-8 content-quienes-somos">
			<h1>¿Quiénes somos?</h1>
			<article>
				<p>
					Con más de 40 años de experiencia en el sector de la remodelación,
					nos hemos convertido en expertos de la producción, comercialización,
					distribución, importación, exportación y mercadeo de todo tipo de pisos
					y revestimientos para vivienda, oficinas y construcción en general.
				</p>
				<p>
					Estamos en 18 ciudades de Colombia y, debido a que contamos con productos de inmejorable calidad,
					hemos logrado traspasar fronteras.
					Actualmente exportamos a países americanos, europeos y asiáticos,
					obteniendo gran reconocimiento mundial.
				</p>
				<h3 class="sub-tit">Prevención del riesgo LA/FT</h3>
				<p>
					El Grupo Empresarial Alfa cuenta con un sistema de autocontrol y gestión del riesgo
					de lavado de activos y financiación del terrorismo, con el fin de prevenir y mitigar
					los riesgos asociados a estas infracciones.
					Además, se abstendrá de celebrar negocios de cualquier tipo con personas naturales
					o jurídicas que se encuentren incluidas en las listas restrictivas de estos delitos.
				</p>
				<p>
					Para más información, por favor comunícate con nuestra línea de atención al cliente 3311531
					en Bogotá o al 018000914900 en el resto del país.
				</p>
			</article>
			<div class="clearfix"></div>
		</section>
		<aside class="col-xs-12 col-md-4 aside-quienes-somos">
			<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
				<figure class="col-xs-12 col-md-6 col-lg-5">
					<img src="/img/quienes-somos/pic-aside-quienes-somos.png" alt="" class="responsive">
				</figure>
				<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
					<p><strong>línea de ética</strong> <br>grupo Alfa</p>
				</div>
			</div>
			<div class="separator-aside-qs hidden-xs hidden-sm col-xs-12"></div>
			<div class="col-xs-12 col-sm-6 col-md-12 div-item-aside-qs">
				<figure class="col-xs-12 col-md-6 col-lg-5">
					<img src="/img/quienes-somos/pic-aside-quienes-somos.png" alt="" class="responsive">
				</figure>
				<div class="col-xs-12 col-md-6 col-lg-7 tit-aside-qs">
					<p><strong>línea de ética</strong> <br>grupo Alfa</p>
				</div>
			</div>
		</aside>
	</div>
</section>

@include('store.partials.front.bannersFooter')

@endsection
