@extends('store.layouts.default')

@section('title', trans('front.label_certifications'))
@if (isset($metadata['title']['title']))
	@section('title', $metadata['title']['title'])
@endif
@if (isset($metadata['meta']['keyword']))
	@section('keywords', $metadata['meta']['keyword'])
@endif
@if (isset($metadata['meta']['description']))
	@section('description',$metadata['meta']['description'])
@endif
@if (isset($metadata['og']['type']))
	@section('type',$metadata['og']['type'])
@endif
@if (isset($metadata['og']['url']))
	@section('url',$metadata['og']['url'])
@endif
@if (isset($metadata['og']['image']))
	@section('picture',$metadata['og']['image'])
@endif

@section('content')

<section class="container-fluid quienes-somos">
	<div class="col-xs-12 box-big">
		<figure>
			<img class="img-responsive" src="/images/assets/banner_diseno.png" alt="<?= trans('front.label_certifications') ?>">
		</figure>
	</div>
	<div class="clearfix"></div>
	<div class="container">
		<section class="col-xs-12 col-md-8 content-quienes-somos">
			<h1>Certificaciones</h1>
			<article>
				<p>
					Ingresa <a href="http://certificadostributarios.alfagres.com.co/CertificadoWeb/IngresarDatos.jsp">aquí</a> y solicita tu certificado
				</p>
			</article>
			<div class="clearfix"></div>
		</section>
	</div>
</section>


@include('store.partials.front.bannersFooter')


@endsection
