@extends('store.layouts.default')


@section('content')

<section class="container">
	<!-- Ruta -->
	<div class="row sin-padding ruta hidden-xs hidden-sm">
		<ol class="breadcrumb">
			<li><a href="/">{{trans('front.home')}}</a></li>
			<li class="active">{{trans('front.title_quote')}}</li>
		</ol>
	</div>
	<!-- Fin Rutas -->
	<div class="container cotizacion">
		<h1>{{trans('front.title_quote')}}</h1>
	</div>
	
	<!-- Detalle Cotización-->
	<?= csrf_field() ?>
	<div class="col-xs-12 detalle-cotizacion cotizacion">
		<div class="row row-no-margin head-cotizacion hidden-xs hidden-sm">
			<div class="col-xs-offset-1 col-xs-6">{{trans('front.product')}}</div>
			<div class="col-xs-offset-1 col-xs-2 text-right">{{trans('front.format')}}</div>
			<div class=" col-xs-2 text-right">{{trans('front.color')}}</div>
		</div>
		
		<?php foreach ($currentOrder->quoteProducts as $keyProduct => $product) { ?>
		<div class="row row-no-margin detalle-cotizacion-item <?= $keyProduct%2==0 ? '' : 'cotizacion-item-gris' ?>" id="quote-item-<?=$product->id?>">
			
			<div class="col-xs-12 col-md-1 text-center div-closed cell-quotes">
				<a class="closed-cotizacion" href="" rel="<?=$product->id?>">X</a>
			</div>
			
			<figure class="col-md-2 hidden-xs hidden-sm">
				<a href="<?=$product->detailUrl?>">
					<img class="img-responsive" src="<?=$product->image_url?>" alt="">
				</a>
			</figure>
			
			<div class="col-xs-6 col-md-4 hidden-md hidden-lg tit-prod-cotiza label-gotham-bold">
				{{trans('front.product')}}:
			</div>
			
			<div class="col-xs-6 col-md-4 det-prod-cotiza label-gotham cell-quotes">
				<h5><a href="<?=$product->detailUrl?>"><?=$product->name?></a></h5>
				<p>{{trans('front.code')}}: <?=$product->code?></p>
			</div>
			
			<div class="col-md-1 hidden-xs hidden-sm">
				
			</div>

			<div class="col-xs-6 hidden-md hidden-lg tit-prod-cotiza label-gotham-bold text-right">
				{{trans('front.format')}}:
			</div>
			
			<div class="col-xs-6 col-md-2 det-prod-cotiza label-gotham text-right cell-quotes">
				<?=$product->dimensions?>
			</div>
			
			<div class="col-xs-6 hidden-md hidden-lg tit-prod-cotiza label-gotham-bold text-right">
				{{trans('front.color')}}:
			</div>
			
			<div class="col-xs-6 col-md-2 det-prod-cotiza label-gotham text-right cell-quotes">
				<?=$product->color?>
			</div>

		
		</div>
		<?php } ?>
		
		<div class="row row-no-margin">
			<div class="col-xs-12 col-md-6 col-md-offset-6 sin-padding">
				<div class="col-xs-12 col-md-6 ver-mas-amarillo">
					<a href="/">{{trans('front.add_more_products')}}</a>
				</div>
				<div class="col-xs-12 col-md-6  ver-mas-orange">
					<a id="send-quote" href="products/send-email-quote">{{trans('front.send_quote')}}</a>
				</div>  
			</div>
			
		</div>
		
	</div>
	<!-- Fin Detalle Cotización-->
	<div class="clearfix"></div>
	
	<!-- Productos relacionados -->
	@include('store.partials.products.productsRelated')
	<!-- Fin Productos relacionados -->
	
	<!-- Tambien ha Visto -->
	@include('store.partials.products.productsRecent')
	<!-- Fin TAMBIÉN HAS VISTO -->
	
	<!-- SECTION PRODUCTOS -->
	<!--<div class="row row-no-margin">-->
		@include('store.partials.products.productsSearchSlider')
	<!--</div>-->
	
	<!-- end SECTION PRODUCTOS -->
</section>
<!-- Fin Content -->
@endsection

@push('scripts')
<script>

	//-----------------------------------------------
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});
	
	//-----------------------------------------------
	$('#content-slider-related').owlCarousel({
		items: 4,
		margin: 15,
		nav: true,
		responsive:{
			0:{
				items:2
			},
			768:{
				items:2,
				loop: <?= count($relatedProducts)>2 ? 'true': 'false '?>
			},
			1024: {
				items: 4,
				loop: <?= count($relatedProducts)>4 ? 'true': 'false '?>
			}
		}
	});
	
	//-----------------------------------------------
	$('#content-slider-views').owlCarousel({ // slider "tambien has visto"
		items: 4,	
		margin: 15,
		nav: true,
		loop: false,
		responsive:{
			0:{
				items:2
			},
			768:{
				items:2,
				loop: <?= count($recentProducts)>2 ? 'true': 'false '?>
			},
			1024: {
				items: 4,
				loop: <?= count($recentProducts)>4 ? 'true': 'false '?>
			}
		}
	});
	
	//-----------------------------------------------
	function numberItems(){
		var numberItems=$('.cant-item-car-float').text();
		return numberItems;
	}
	
	//-----------------------------------------------
	function sendQuote(){
		$('#send-quote').click(function(e) {
			var number = numberItems();

			if(parseInt(number)==0){
				e.preventDefault();
			}
		});
	}

	//-----------------------------------------------
	$(".closed-cotizacion").click(function(e) {
		var productId = $(this).attr("rel");
		
		var params= {
			"productId" : productId
		};
		
		e.preventDefault();
		
		$.post( "/products/delete-quote", params, function(request) {
			if(request!=-1 && $.isNumeric(request)){
				$("#quote-item-"+productId).remove();
				$('span.cant-item-car-float').text(request);
			}
		});
	});
	
	//-----------------------------------------------
	sendQuote();
	
	</script>
	@endpush

