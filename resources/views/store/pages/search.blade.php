@extends('store.layouts.default')

@section('title', trans('front.search_results_title', ['term' => $search]))

@section('content')

<section class="hidden-xs container" id="busqueda">
	<div class="row">
		<div class="row sin-padding ruta hidden-xs hidden-sm">
			<ol class="breadcrumb">
				<li><a href="#">{{ trans('front.home') }}</a></li>
				<li class="active">{{ trans('front.search_results') }}</li>
			</ol>
		</div>
		<div class="col-md-6 col-lg-6 busqueda-desktop-h1">
			<h1 class="hidden-xs hidden-sm">{!! trans('front.search_results_term', ['term' => $search]) !!}</h1>
		</div>
		<div class="col-md-6 col-lg-6">
			<form id="form-search-new" action="<?= url(trans('routes.search')) ?>" method="get" role="search">
				<div class="hidden-xs hidden-sm input-group busqueda-input">
					<input type="text" class="form-control busqueda-form-control" name="q" placeholder="{{ trans('front.new_search') }}">
					<span class="input-group-btn">
						<button class="btn btn-busqueda-box" type="submit">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
						</button>
					</span>
				</div>
			</form>
		</div>
	</div>
</section>
<div class="hidden-md hidden-lg container busqueda-result-mobile">
	<p>{{ $search }}</p>
</div>

<!-- begin SECTION -->
<div class="container busquedamod">
	<!-- Busqueda -->
	<div class="row sin-padding results">	
		<br>
		<?php foreach ($results as $k => $product): ?>
			<?php
			if ($k % 2 == 0) {
				$typePadding = "mediopaddright";
			} else {
				$typePadding = "mediopaddleft";
			}
			?>
			<div class="col-xs-6 col-sm-4 col-md-3 producto-cat <?= $typePadding ?>">
				<div class="item-producto-cat text-center">
					<a href="<?= $product->detailUrl ?>">
						<figure class="hvr-float">
							<?php if ($product->isNew()): ?>
								<span class="prod-new"></span>
							<?php endif; ?>
							<img src="{{ $product->imageUrl }}" alt="{{ $product->name }}">
						</figure>
						<h4>{{ $product->name }}</h4>
					</a>
				</div>
			</div>

			<?php if ($k == 7): ?>
				<div class="col-xs-12 ver-mas-magenta" style="margin-bottom:40px">
					<a href="#" class="loadmore" data-search="{{ $search }}">{{ trans('front.see_more') }}</a>
				</div>
				<?php break; ?>
			<?php endif; ?>

		<?php endforeach; ?>
	</div>
	
	<div id="mini-loader" class="col-xs-12 text-center" style="display: none; margin-top: 10px;">
		<div class="internal-loading">
			<div class="internal-loader"></div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
	$('.loadmore').on('click', function (e) {
		e.preventDefault();
		$('#mini-loader').css('display', 'block');
		var q = $(this).attr("data-search");
		var me = $(this);
		me.parent().css('display', 'none');
		$.getJSON("/searchMore/" + q, function (jsonData) {
			var toAppend = '';
			$.each(jsonData, function (i, data) {
				var isNewClass = "";
				
				if(data.is_new == 1){
					isNewClass = "prod-new";
				}
				toAppend += '<div class="col-xs-6 col-sm-4 col-md-3 producto-cat"><div class="item-producto-cat text-center"><a href="' + data.detail_url + '"><figure class="hvr-float"><span class="'+isNewClass+'"></span><img src="' + data.imageUrl + '" alt="' + data.name + '"></figure><h4>' + data.name + '</h4></a></div></div>';
			});
			$(".results").append(toAppend);
			$('#mini-loader').css('display', 'none');
			me.parent().css('display', 'block');
			me.parent().remove();
		});
	});
</script>
@endpush
