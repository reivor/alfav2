@extends('store.layouts.default')
@section('content')

<style>
.select-form{margin: 0 !important;border: 1px solid #ccc;}
.select-form span {padding: 0.5em 1em 0.5em;}
.select-form > span::after{top:15px;}
</style>


<section class="container" id="registro">
	<!-- Ruta -->
	<div class="row sin-padding ruta hidden-xs hidden-sm">
		<ol class="breadcrumb">
			<li><a href="/">{{ trans('front.home') }}</a></li>
			<li class="active">{{ trans('front.registry') }}</li>
		</ol>
	</div>
	<!-- Fin Rutas -->
	<div class="container">
		@if (session('success'))
		<div class="alert alert-success alert-dismissible site-alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p>{{ session('success') }}</p>
		</div>
		@endif

		@if (session('error'))
		<div class="alert alert-danger alert-dismissible site-alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p>{{ session('error') }}</p>
		</div>
		@endif
		<h1>{{ trans('front.sign_up') }}</h1>
	</div>

	<div class="row row-no-margin row-registro">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 registro-info pull-right">
			<div>
				<p>{{ trans('front.form_info') }}</p>
			</div>
			<?php if( $bannerRegistro != NULL ) { ?>
			<div class="hidden-xs hidden-sm registro-pinturas-desktop">
				<a href="<?= $bannerRegistro->detailUrl ?>" target="<?=$bannerRegistro->target?>">
					<img class="img-responsive" src="<?= $bannerRegistro->image_url ?>" alt="">
				</a>
			</div>
			<?php } ?>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 registro-form pull-right">
			<div class="social-buttons">
				<a class="btn-facebook" href="#" onclick="fbLogin(); return false;">
					<img class="btn-facebook-icon img-responsive" src="/img/registro/facebook-btn-icon.png" alt="Facebook">
					{{ trans('front.connect_with_fb') }}
				</a>
				
				<!-- <a class="btn-twitter" href="#"><img class="img-responsive btn-twitter-icon" src="/img/registro/twitter-btn-icon.png" alt="Twitter">conectar con twitter</a> -->
			</div>
			<div class=" section-registro-lines">
				<h2>{{ trans('front.complete_the_form') }}</h2>
			</div>
			<form action="users/create" method="post" id="form-register" class="form-inline col-xs-12" role="form" >
				{{ csrf_field() }}

				<input type="hidden" id="fb_id" name="fb_id" value="">

				<div class="row">
					<div class="form-group col-xs-12 col-sm-6 has-feedback form-name sin-padding-left">
						<label for="nombre">{{ trans('front.name') }} *</label> <br>
						<input type="text" class="form-control" name="name" id="name" required>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group col-xs-12 col-sm-6 has-feedback form-apellido sin-padding-right">
						<label for="apellido">{{ trans('front.last_name') }} *</label> <br>
						<input type="text" class="form-control" name="lastName" id="lastname" required>
						<div class="help-block with-errors"></div>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-xs-12 col-sm-6 has-feedback form-identification sin-padding-left">
						<label for="identification">{{ trans('front.identification') }}</label> <br>
						<input type="text" maxlength="10" class="form-control" name="identification" id="identification">
						<div class="help-block with-errors"></div>
					</div>
                    <div class="form-group col-xs-12 col-sm-6 has-feedback form-mail sin-padding-right">
                    	<label for="email">{{ trans('front.email') }} *</label> <br>
                    	<input type="email" class="form-control" name="email" id="email_registry" required>
                    	<div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="row">
					<div class="form-group col-xs-12 col-sm-6 has-feedback form-residence sin-padding-left">
						<label for="residence">{{ trans('front.residence') }}</label> <br>
						<input type="text" class="form-control" name="residence" id="residence">
						<div class="help-block with-errors"></div>
					</div>     

					<div class="form-group col-xs-12 col-sm-6 has-feedback form-cell_phone sin-padding-right">
						<label for="cell_phone">{{ trans('front.cell_phone') }} *</label> <br>
						<input type="text" maxlength="10" class="form-control" name="cell_phone" id="cell_phone" required>
						<div class="help-block with-errors"></div>
					</div>
                </div>

                <div class="row">
					<div class="form-group col-xs-12 col-sm-6 has-feedback form-actividad sin-padding-left">
						<label for="city">{{ trans('front.city') }} *</label> <br>
						<div>
							<select class="cs-select cs-skin-underline select-form select-job" name="city" id="city" required>
	                        	<option value="" disabled selected>{{ trans('front.select_city') }}</option>
				                @foreach ($cities as $city)
				                	<option value="{{$city->id}}">{{$city->name}}</option>
								@endforeach
							</select>
						</div>
						<div id="error_city" class="help-block with-errors"></div>
					</div> 

					<div class="form-group col-xs-12 col-sm-6 has-feedback form-actividad sin-padding-right">
                        <label for="actividad">{{ trans('front.your_job') }}</label> <br>
                        <select name="job" id="job" class="cs-select cs-skin-underline select-form select-job">
                        	<option value="" disabled selected>{{ trans('front.select') }}</option>
                        	<option value="{{ trans('front.architect') }}">{{ trans('front.architect') }}</option>
                        	<option value="{{ trans('front.interior_designer') }}">{{ trans('front.interior_designer') }}</option>
                        	<option value="{{ trans('front.industrial_designer') }}">{{ trans('front.industrial_designer') }}</option>
                        	<option value="{{ trans('front.foreman') }}">{{ trans('front.foreman') }}</option>
                        	<option value="{{ trans('front.engineer') }}">{{ trans('front.engineer') }}</option>
                        	<option value="{{ trans('front.inspector') }}">{{ trans('front.inspector') }}</option>
                        	<option value="{{ trans('front.housewife') }}">{{ trans('front.housewife') }}</option>
                        	<option value="{{ trans('front.photographer') }}">{{ trans('front.photographer') }}</option>
                        	<option value="{{ trans('front.other') }}">{{ trans('front.other') }}</option>
                        </select>
                    </div>       
                </div>


                <div class="row">
                	<div class="form-group col-xs-12 col-sm-6 has-feedback form-password sin-padding-left">
                		<label for="password">{{ trans('front.password') }}</label> <br>
                		<input type="password" class="form-control" name="password" id="password" required>
                		<div class="help-block with-errors"></div>
                	</div>
                	<div class="form-group col-xs-12 col-sm-6 has-feedback form-password sin-padding-right">
                		<label for="password_confirm">{{ trans('front.confirm_pswd') }}</label> <br>
                		<input type="password" class="form-control" name="password_confirm" id="password_confirm" required>
                		<div class="help-block with-errors"></div>
                	</div>                       
                </div>

                <div class="row term-section">
                	<div class="form-group col-xs-12 col-sm-6 form-terminos sin-padding-left">
                		<div class="checkbox">
							<label><input type="checkbox" data-error="{{trans('front.required_terms_message')}}" required>{{ trans('front.accept') }} <a href="/files/habeas_data.pdf" target="_blank">{{ trans('front.terms_and_conditions') }}</a></label>
                			<div class="help-block with-errors"></div>
                		</div>
                		<div class="checkbox">
                			<label><input type="checkbox" name="subscribe" value="1">{{trans('front.newsletter_accept')}}</label>
                			<div class="help-block with-errors"></div>
                		</div>
                	</div>
                	<div class="clearfix hidden-xs"></div>
                	<button type="submit" class="btn btn-block btn-registro-form">{{ trans('front.create-account') }}</button>
                </div>
            </form>
			
			
            <?php if($bannerMobile!=NULL){ ?>
            <div class="hidden-md hidden-lg registro-pinturas-mobile">
				<hr class="separador hidden-md hidden-lg">
            	<img class="img-responsive banner-registro" src="<?=$bannerMobile->image_url?>" alt="Pinturas">
            </div>
            <?php }?>
        </div>
    </div>
</section>

@endsection

@push('styles')
<link rel="stylesheet" href="/themes/store/vendor/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
@endpush
@push('scripts')
<script src="/themes/store/vendor/bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>
<script>
var temp;
$(function () {
	var selectElement = document.querySelector('#job');
	new SelectFx(selectElement, {
	});
	var selectElement2 = document.querySelector('#city');
	temp = new SelectFx(selectElement2, {
		onChange: function(val) {
		    if(temp.current == -1){
				$('#error_city').siblings('div').children('.cs-select').css('border','1px solid #a94442');
				$('#error_city').siblings('.help-block').show(0);
			}
			else{
				$('#error_city').siblings('div').children('.cs-select').css('border','1px solid #ccc');
				$('#error_city').siblings('.help-block').hide(0);
			}
	  	}
	});

	$('#error_city').siblings('div').children('.cs-select').children('.cs-options').css('overflow-y','scroll');
	$('#error_city').siblings('div').children('.cs-select').children('.cs-options').css('max-height','400px');
});

</script>


<script>
$(function () {

	$('#form-register').bootstrapValidator({
		fields: {
			name: {
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'}
				}
			},
			lastName: {
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'}
				}
			},
			email: {
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'},
					emailAddress: {message: '<?= trans('auth.form_invalid_email') ?>'},
					remote: {
						url: '/customer/email',
						message: '<?= trans('auth.form_duplicated_email') ?>',
						delay: 1000
					}
				}
			},
			password: {
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'},
					stringLength: {
						min: 6,
						message: '<?= trans('auth.form_password_too_short') ?>'
					}
				}
			},
			password_confirm: {
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'},
					identical: {
						field: 'password',
						message: '<?= trans('auth.form_password_mismatch') ?>'
					}
				}
			},
			agree: {
				validators: {
					notEmpty: {
						message: '<?= trans('auth.form_terms_agree_nocheck') ?>'
					}
				}
			},
			city: {
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'}
				}
			},
			cell_phone: {
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'}
				}
			}
		},onSuccess: function(e) {
			if(temp.current == -1){
				$('#error_city').siblings('.help-block').css('color','#a94442');
				$('#error_city').siblings('..form-actividad div.cs-skin-underline').css('border','1px solid #a94442');
				console.log($('#error_city').siblings());
				$('#error_city').siblings('.help-block').show(0);
				e.preventDefault();
			}
			else{
				$('#error_city').siblings('.help-block').hide(0);
			}
	    },onError: function(e) {
			e.preventDefault();
			if(temp.current == -1){
				$('#error_city').siblings('.help-block').css('color','#a94442');
				$('#error_city').siblings('div').children('.cs-select').css('border','1px solid #a94442');
				$('#error_city').siblings('.help-block').show(0);
			}
			else{
				$('#error_city').siblings('div').children('.cs-select').css('border','1px solid #ccc');
				$('#error_city').siblings('.help-block').hide(0);
			}
	    }
	});
});

$('#cell_phone, #identification').on('keydown', function(e){
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }

	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

</script>

<script>

window.fbAsyncInit = function () {
	FB.init({
		appId: '<?= env('FB_APP_ID') ?>',
			cookie: true, // enable cookies to allow the server to access the session
			xfbml: true, // parse social plugins on this page
			version: 'v2.2' // use version 2.2
		});

		// Now that we've initialized the JavaScript SDK, we call 
		// FB.getLoginStatus().  This function gets the state of the
		// person visiting this page and can return one of three states to
		// the callback you provide.  They can be:
		//
		// 1. Logged into your app ('connected')
		// 2. Logged into Facebook, but not your app ('not_authorized')
		// 3. Not logged into Facebook and can't tell if they are logged into
		//    your app or not.
		//
		// These three cases are handled in the callback function.

		// FB.getLoginStatus(function (response) {
		// 	statusChangeCallback(response);
		// });

};

function fbLogin() {
	FB.login(function (response) {
		testAPI();
	}, {scope: 'public_profile,email'});
}

	// This is called with the results from from FB.getLoginStatus().
	function statusChangeCallback(response) {
		console.log('statusChangeCallback');
		console.log(response);
		// The response object is returned with a status field that lets the
		// app know the current login status of the person.
		// Full docs on the response object can be found in the documentation
		// for FB.getLoginStatus().
		if (response.status === 'connected') {
			// Logged into your app and Facebook.
			testAPI();
			// FB.api('/me?fields=email,name', function(responseFromFB){                                                           

			// 	var name = responseFromFB.name; 
			// 	var email = responseFromFB.email;
			// 	console.log(email);
			// });
}
else if (response.status === 'not_authorized') {
			// The person is logged into Facebook, but not your app.
			document.getElementById('status').innerHTML = 'Please log into this app.';
			FB.login(function (response) {
			}, {scope: 'public_profile,email'});
		}
		else {
			// The person is not logged into Facebook, so we're not sure if
			// they are logged into this app or not.
			document.getElementById('status').innerHTML = 'Please log into Facebook.';
			FB.login(function (response) {
			}, {scope: 'public_profile,email'});
		}
	}


	// This function is called when someone finishes with the Login
	// Button.  See the onlogin handler attached to it in the sample
	// code below.
	function checkLoginState() {
		console.log('checkLoginState()');
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	// Load the SDK asynchronously
	(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// Here we run a very simple test of the Graph API after login is
	// successful.  See statusChangeCallback() for when this call is made.
	function testAPI() {
		
		FB.api('/me?fields=email,first_name,last_name', function (response) {

			console.log('Successful login for: ' + response.email);
			console.log(response);
			response._token = $('input[name="_token"]').val();
			if(!response.error){
				$.post('users/userfacebook', response).done(function (data) {
					if (data['result'] === 'false') {
						$('#email_registry').val(response.email);
						$('#name').val(response.first_name);
						$('#lastname').val(response.last_name);
						$('#fb_id').val(response.id);
					}
					else if (data['result'] === 'ok') {
						console.log(data['usuario']);
						if( data['redirect'] ) {
							window.location.href = data['redirect'];
						}
						else {
							window.location.href = "/";
						}
					}
				});
			}
		});

	}

	</script>

	@endpush