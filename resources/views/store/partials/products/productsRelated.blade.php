<section class="container">
	<hr class="separador hidden-md hidden-lg sin-padding">
</section>

<!-- Productos relacionados -->
<?php if(count($relatedProducts)>0): ?>
	<div class="row sin-padding prod-relacionados-det">
		<h2>{{ trans('front.relate_products') }}</h2>
		<div class="slider-products">
			<div class="item">
				<div id="content-slider-related" class="owl-carousel yellow">
					<?php foreach($relatedProducts as $related): ?>
						<div>
							<a href="<?=$related->detailUrl?>" class="nohover">
								<img class="img-responsive hover" src="<?= $related->getImageUrlAttribute() ?>" alt="">
								<p><?= $related->reference->name ?></p>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<!-- Fin Productos relacionados -->
