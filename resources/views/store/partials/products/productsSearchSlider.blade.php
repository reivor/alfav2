<section class="container">
	<hr class="separador hidden-md hidden-lg">
</section>

<?php if(count($productsSearch)>0): ?>
	<div id="productos" class="container section-productos-title">
		<h1>{!!trans('front.title_most_searched_products')!!}</h1>
	</div>
	<div class="slider-products">
		<div class="item">
			<div id="content-slider-search" class="owl-carousel blue">
				<?php foreach ($productsSearch as $keyProduct => $product): ?>
					<div>
						<a href="<?= $product->detailUrl ?>" class="nohover">
							<?php if($product->is_new) {?>
								<span class="prod-new-azul"></span>
							<?php }?>
							<img class="img-responsive hover" src="<?= $product->getImageUrlAttribute() ?>" alt="">
							<p><?= $product->reference->name ?></p>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>