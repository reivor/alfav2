<?php foreach ($productColors as $keyProduct => $productColors) { ?>
	<div class="color-item <?php if($productColors->color_code==$currentColor) {?>color-active <?php }?>">
		<p class="content-color-variation">
			<span class="format-color" data-color="<?=$productColors->color_code?>" style="background:<?=$productColors->color_code?>"></span>
			<?=$productColors->color?>
		</p>
	</div>	
<?php }?>

