<!-- Detalle Producto -->
<input type="hidden" id="current-product" value="<?= $product->id ?>" />
<input type="hidden" id="current-format" value="<?= $currentFormat ?>" />
<input type="hidden" id="reference-current-product" value="<?= $product->reference_id ?>" />

<div class="row detalle-producto">
	
	<div class="col-xs-12 col-sm-6 detalle-slider">
		
		<div class="row">
			
			<div class="col-sm-12 sin-padding box-img-detalle" id="carousel-bounding-box">
				
				<div class="carousel slide" id="myCarousel">
					
					<ol class="carousel-indicators hidden-sm hidden-md hidden-lg">
						<?php for($i=0; $i< count($product->images); $i++): ?>
						<li data-target="#myCarousel" data-slide-to="<?= $i ?>" class="<?= ($i==0) ? 'active' : '' ?>"></li>
						<?php endfor; ?>
					</ol>
					
					<div class="carousel-inner">
						<?php if(count($product->images)>0): ?>
						<?php foreach ($product->images as $keyImage => $image): ?>
						<div class="<?= ($keyImage==0) ? 'active' : '' ?> item" data-slide-number="<?= $keyImage ?>">
							<img src="<?= $image->imageUrl?>">
						</div>
						<?php endforeach; ?>
						<?php else: ?>
						<div class="active item" data-slide-number="0">
							<img src="<?= $product::DEFAULT_PICTURE_PRODUCT ?>">
						</div>
						<?php endif; ?>
					</div>
					
				</div>
				
			</div>
			
		</div>
		<!--/Slider-->
		
		<!-- Slider thumbnails -->
		<div class="row">
			<div class="col-sm-12 slider-detail hidden-xs owl-carousel purple">
				<?php if( $product->images->count() ): ?>
				<?php foreach ($product->images as $keyImage => $image): ?>
				<div class="thumb-image-detail item">
					<a href="#" data-slide="<?= $keyImage ?>">
						<img src="<?= $image->url ?>" alt="">
					</a>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="row share-this">
			<div class="hidden-xs col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-2">
				<div class="pull-right">
					<span class="share-this-text">{{ trans('front.share_in') }}</span>
					@include('store.partials.front.shareThisStyle')
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-xs-12 col-sm-6 detalle-producto-descr hidden-xs">
		<div class="col-xs-12 titulo-produc sin-padding-left">
			<h3><?= $product->name ?></h3>
			<p><?= trans('front.code') ?>: <?= $product->code ?></p>
		</div>
		<div class="col-xs-12 det-formato sin-padding-left">
			 <div class="col-xs-12 col-sm-8 col-md-6 sin-padding-left">
				<h4><?= trans('front.title_format') ?></h4>

				<?php foreach($formats as $keyVariation => $variation): ?>
				<div class="col-xs-3 col-sm-6 text-left sin-padding">
					<input class="radio-format" type="radio" name="optionsRadios" id="optionsRadios<?=$keyVariation+1?>" value="<?=$variation?>" <?php if($currentFormat==$variation) {?> checked="true" <?php }?>> <?=$variation?>
				</div>		
				<?php endforeach; ?>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-6 hidden-xs">
				<h4><?= trans('front.ambients') ?></h4>
				<ul>
					<li><?= $product->ambience ?></li>
				</ul>
			</div>
		</div>
		<div class="col-xs-12 det-descr sin-padding-left">
			<h4><?= trans('front.description') ?></h4>
			<p><?= $product->description ?></p>
		</div>
		<div class="clearfix"></div>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel-detalle">
				<div class="panel-heading" role="tab" id="headingTwo-desktop">
					<h4 class="panel-title"><a class="panel-open" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsecolores-desktop" aria-expanded="false" aria-controls="collapsecolores-desktop">{{ trans('front.colors') }}</a></h4>
				</div>
				<div id="collapsecolores-desktop" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo-desktop">
					<div id="color-format-content" class="panel-body">
						@include('store.partials.products.formatColors')
					</div>
				</div>
			</div>
			<div class="panel-detalle">
				<div class="panel-heading" role="tab" id="headingThree-desktop">
					<h4 class="panel-title"><a class="collapsed panel-open" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefichas-desktop" aria-expanded="false" aria-controls="collapsefichas-desktop">{{ trans('front.datasheets') }}</a></h4>
				</div>
				<div id="collapsefichas-desktop" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-desktop">
					<div class="panel-body">
						<div class="col-xs-12 det-ficha">
							<div class="col-xs-6">
								<p class="f-manual-instalacion"><a href="{{ $product->maintenance_manual }}" target="_blank">{!! trans('front.maintenance_manual') !!}</a></p>
							</div>
							<div class="col-xs-6">
								<p class="f-garantia"><a href="{{ $product->guarantee }}" target="_blank">{{ trans('front.warranty') }}</a></p>
							</div>
							<div class="col-xs-12">
								<p class="sello">
									<a>{{ trans('front.seal_of_quality') }}</a>
								</p>
								<span></span>
								<ul class="list-calidad">
									<?php foreach($product->getQualitySeal() as $keyTag => $tag): ?>
									<li><a href="<?= $tag['link'] ?>" target="_blank"><?= $tag['text'] ?></a></li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="etiquetas">
			<div class="col-xs-12 sin-padding">
				<?php foreach($product->getTags() as $keyTag => $tag): ?>
				<span class="etiq<?= ($keyTag+1) ?>"><a href="/<?= trans('routes.search') . '?q=' . $tag ?>"><?= $tag ?></a></span>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="col-xs-12 text-right sin-padding-right">
			<div class="solicita-cot">
				<a href="" class="btn-solicita-cot">{{ trans('front.request_a_quote') }}</a>
			</div>
		</div>
	</div>
	
	<!--MOBILE -->
	<div class="col-xs-12 detalle-producto-descr-mobile hidden-sm hidden-md hidden-lg">
		<div class="col-xs-12 titulo-produc sin-padding-left">
			<h3><?= $product->name ?></h3>
			<p><?= trans('front.code') ?>: <?= $product->code ?></p>
		</div>
		<div class="clearfix"></div>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel-detalle">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a class="collapsed panel-open" role="button" 
						   data-toggle="collapse" data-parent="#accordion" 
						   href="#collapseformato" 
						   aria-expanded="true" aria-controls="collapseformato">
							<?= trans('front.format') ?>
						</a>
					</h4>
				</div>
				<div id="collapseformato" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
						<div class="det-formato">
							<div class="col-xs-12 sin-padding-left">
								<?php foreach($formats as $keyVariation => $variation): ?>
								<div class="col-xs-6 col-sm-6 text-left no-padding-mob">
									<input class="radio-format-mobile" type="radio" name="optionsRadiosMobile" id="optionsRadios<?=$keyVariation+1?>" value="<?=$variation?>" <?php if($currentFormat==$variation) {?> checked="true" <?php }?>> <?=$variation?>
								</div>		
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-detalle">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a class="panel-open" role="button" 
						   data-toggle="collapse" data-parent="#accordion" 
						   href="#collapsedescripcion" 
						   aria-expanded="false" aria-controls="collapsedescripcion">
							<?= trans('front.description') ?>
						</a>
					</h4>
				</div>
				<div id="collapsedescripcion" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						<div class="no-padding-mob">
							<p><?= $product->description ?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-detalle">
				<div class="panel-heading" role="tab" id="headingTwo">
					<h4 class="panel-title"><a class="panel-open" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsecolores" aria-expanded="false" aria-controls="collapsecolores">{{ trans('front.colors') }}</a></h4>
				</div>
				<div id="collapsecolores" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
					<div class="panel-body">
						<div id="color-format-mobile-content" class="col-xs-12 det-color no-padding-mob">
							@include('store.partials.products.formatColors')
						</div>
					</div>
				</div>
			</div>
			<div class="panel-detalle">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title"><a class="collapsed panel-open" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefichas" aria-expanded="false" aria-controls="collapsefichas">{{ trans('front.datasheets') }}</a></h4>
				</div>
				<div id="collapsefichas" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						<div class="col-xs-12 det-ficha">
							<div class="col-xs-6">
								<p class="f-manual-instalacion"><a href="{{ $product->maintenance_manual }}" target="_blank">{!! trans('front.maintenance_manual') !!}</a></p>
							</div>
							<div class="col-xs-6">
								<p class="f-garantia"><a href="{{ $product->guarantee }}" target="_blank">{{ trans('front.warranty') }}</a></p>
							</div>
							<div class="col-xs-12">
								<p class="sello">
									<a><?= trans('front.seal_of_quality') ?></a>
								</p>
								<span></span>
								<ul class="list-calidad">
									<?php foreach($product->getQualitySeal() as $keyTag => $tag): ?>
									<li><a href="<?= $tag['link'] ?>" target="_blank"><?= $tag['text'] ?></a></li>
									<?php endforeach; ?>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 text-right sin-padding-right">
			<div class="solicita-cot">
				<a href="" class="btn-solicita-cot">{{ trans('front.request_a_quote') }}</a>
			</div>
		</div>
		
		<div class="row share-this-mobile">
			<div class="col-xs-12 text-center share-this-text">
				<span>{{ trans('front.share_in') }}</span>
			</div>
			<div class="col-xs-12 text-center">
				@include('store.partials.front.shareThisStyle')
			</div>
		</div>
	</div>
</div>
<!-- Fin Detalle Producto -->

<div class="clearfix"></div>

<!-- Productos relacionados -->
@include('store.partials.products.productsRelated')