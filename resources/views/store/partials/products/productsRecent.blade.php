<?php if(count($recentProducts)>0): ?>
<div class="prod-tambienvistos">
	<h2>{{ trans('front.recently_viewed_items') }}</h2>
	<div class="slider-products">
		<div class="item">
			<div id="content-slider-views" class="owl-carousel orange">
				<?php foreach($recentProducts->reverse() as $product): ?>
				<div>
					<a href="<?=$product->detailUrl?>" class="nohover">
						<img class="img-responsive hover" src="<?=$product->getImageUrlAttribute()?>" alt="">
						<p><?=$product->reference->name?></p>
					</a>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

</div>
<?php endif; ?>