<!-- layouts 4 -->
<div class="row layout-4 categorias-ambientes row-no-margin">
	<!-- Desktop -->
	<?php if(isset($bannersHome[1])){?>
		<div class="col-xs-6 col-md-4 layout-4-1 hidden-xs hidden-sm sin-padding-right">
			<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
				<img class="img-responsive" src="<?= $bannersHome[1]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[2])){?>
		<div class="col-xs-6 col-md-4 layout-4-1 hidden-xs hidden-sm">
			<a href="<?=$bannersHome[2]->detailUrl?>" target="<?=$bannersHome[2]->target?>">
				<img class="img-responsive" src="<?= $bannersHome[2]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[3])){?>
		<div class="col-xs-6 col-md-4 layout-4-1 hidden-xs hidden-sm sin-padding-left">
			<a href="<?=$bannersHome[3]->detailUrl?>" target="<?=$bannersHome[3]->target?>">
				<img class="img-responsive" src="<?= $bannersHome[3]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>

	<!-- Mobile -->
	<?php if(isset($bannersHome[1])){?>
		<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddright">
			<a href="<?=$bannersHome[1]->detail?>" target="<?=$bannersHome[1]->target?>">
				<img class="img-responsive" src="<?=$bannersHome[1]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[2])){?>
		<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddleft">
			<a href="<?=$bannersHome[2]->detail?>" target="<?=$bannersHome[2]->target?>">
				<img class="img-responsive" src="<?=$bannersHome[2]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[3])){?>
		<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddright">
			<a href="<?=$bannersHome[3]->detail?>" target="<?=$bannersHome[3]->target?>">
				<img class="img-responsive" src="<?=$bannersHome[3]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
</div>
<!-- fin layout 4 -->