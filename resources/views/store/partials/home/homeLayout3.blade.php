 <!-- layouts 3 -->
<div class="row layout-3 categorias-ambientes row-no-margin">
	<!-- Desktop -->
	<?php if(isset($bannersHome[1])){?>
		<div class="col-xs-6 hidden-xs hidden-sm banner-left">
			<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
				<img class="img-responsive" src="<?= $bannersHome[1]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[2])){?>
		<div class="col-xs-6 hidden-xs hidden-sm banner-right">
			<a href="<?=$bannersHome[2]->detailUrl?>" target="<?=$bannersHome[2]->target?>">
				<img class="img-responsive" src="<?= $bannersHome[2]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>

	<!-- Mobile -->
	<?php if(isset($bannersHome[1])){?>
		<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddright">
			<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
				<img class="img-responsive" src="<?=$bannersHome[1]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[2])){?>
		<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddleft">
			<a href="<?=$bannersHome[2]->detailUrl?>" target="<?=$bannersHome[2]->target?>">
				<img class="img-responsive" src="<?=$bannersHome[2]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
</div>
<!-- fin layouts 3 -->