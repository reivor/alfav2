<!-- layouts 5 -->
<div class="row layout-2 categorias-ambientes row-no-margin">
	<!-- Desktop -->
	<?php if(isset($bannersHome[1])){?>
		<div class="hidden-sm hidden-xs col-xs-6 col-md-8 layout-2-3">
			<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
				<img id="img-desktop-1" class="img-responsive" src="<?= $bannersHome[1]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[2]) || isset($bannersHome[3])){?>
		<div class="hidden-xs hidden-sm col-md-4 layout-2-1 sin-padding-left">
			<?php if(isset($bannersHome[2])){?>
				<div class="col-md-12 col-lg-12 sin-padding layout-2-mini">
					<a href="<?=$bannersHome[2]->detailUrl?>" target="<?=$bannersHome[2]->target?>">
						<img id="img-desktop-2" class="img-responsive" src="<?=$bannersHome[2]->image_url?>" alt="">
					</a>
				</div>
			<?php }?>
			
			<?php if(isset($bannersHome[3])){?>
				<div class="col-md-12 col-lg-12 sin-padding layout-2-mini">
					<a href="<?=$bannersHome[3]->detailUrl?>" target="<?=$bannersHome[3]->target?>">
						<img id="img-desktop-3" class="img-responsive" src="<?=$bannersHome[3]->image_url?>" alt="">
					</a>
				</div>
			<?php }?>
		</div>
	<?php }?>

	<!-- Mobile -->
	<?php if(isset($bannersHome[1])){?>
		<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddright">
			<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
				<img class="img-responsive" src="<?=$bannersHome[1]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[2])){?>
		<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddleft">
			<a href="<?=$bannersHome[2]->detailUrl?>" target="<?=$bannersHome[2]->target?>">
				<img class="img-responsive" src="<?=$bannersHome[2]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[3])){?>
		<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddright">
			<a href="<?=$bannersHome[3]->detailUrl?>" target="<?=$bannersHome[3]->target?>">
				<img class="img-responsive" src="<?=$bannersHome[3]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
</div>
<!-- fin layouts 5 -->