<!-- layouts 1 -->
<div class="row layout-1 row-no-margin">
	<?php if(isset($bannersHome[1])){?>
		<!-- Normal -->
		<div class="hidden-xs hidden-sm col-xs-12 categorias-ambientes wrap-layout-1">
			<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
				<img id="img-banner-1" class="img-responsive" src="<?= $bannersHome[1]->image_url ?>" alt="">
			</a>
		</div>
		
		<!-- Mobile -->
		<div class="hidden-md hidden-lg col-xs-6 categorias-ambientes wrap-layout-1">
			<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
				<img id="img-mobile-banner-1" class="img-responsive" src="<?=$bannersHome[1]->image_mobile_url?>" alt="">
			</a>
		</div>
	<?php }?>
</div>
<!-- fin layouts 1 -->