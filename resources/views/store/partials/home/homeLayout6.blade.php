<!-- layout 6 -->
<div class="row row-no-margin layout-6 categorias-ambientes">
	<!-- Desktop -->
	<?php if(isset($bannersHome[1])){?>
		<div class="col-xs-6 col-md-4 col-lg-4 layout-6-1 hidden-sm hidden-xs sin-padding-right">
			<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
				<img class="img-responsive" src="<?= $bannersHome[1]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[2]) || isset($bannersHome[4])){?>
		<div class="hidden-xs hidden-sm col-md-4 col-lg-4 layout-6-2">
			<?php if(isset($bannersHome[2])){?>
				<div class="col-md-12 col-lg-12 sin-padding layout-6-2-mini">
					<a href="<?=$bannersHome[2]->detailUrl?>" target="<?=$bannersHome[2]->target?>">
						<img class="img-responsive" src="<?= $bannersHome[2]->image_url ?>" alt="">
					</a>
				</div>
			<?php }?>
			
			<?php if(isset($bannersHome[4])){?>
				<div class="col-md-12 col-lg-12 sin-padding layout-6-2-mini">
					<a href="<?=$bannersHome[4]->detailUrl?>" target="<?=$bannersHome[4]->target?>">
						<img class="img-responsive" src="<?= $bannersHome[4]->image_url ?>" alt="">
					</a>
				</div>
			<?php }?>
		</div>
	<?php }?>
	
	<?php if(isset($bannersHome[3])){?>
		<div class="col-xs-6 col-md-4 col-lg-4 layout-6-1 hidden-sm hidden-xs sin-padding-left">
			<a href="<?=$bannersHome[3]->detailUrl?>" target="<?=$bannersHome[3]->target?>">
				<img class="imagen-categoria img-responsive" src="<?= $bannersHome[3]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>
	
	<!-- Mobile -->
	<div class="row row-no-margin">
		<?php if(isset($bannersHome[1])){?>
			<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddright">
				<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
					<img id="img-mobile-banner-1" class="img-responsive" src="<?=$bannersHome[1]->image_mobile_url?>" alt="">
				</a>
			</div>
		<?php }?>
	
		<?php if(isset($bannersHome[2])){?>
			<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddleft">
				<a href="<?=$bannersHome[2]->detailUrl?>" target="<?=$bannersHome[2]->target?>">
					<img id="img-mobile-banner-2" class="img-responsive" src="<?=$bannersHome[2]->image_mobile_url?>" alt="">
				</a>
			</div>
		<?php }?>
	
		<?php if(isset($bannersHome[4])){?>
			<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddright">
				<a href="<?=$bannersHome[4]->detailUrl?>" target="<?=$bannersHome[4]->target?>">
					<img id="img-mobile-banner-3" class="img-responsive" src="<?=$bannersHome[4]->image_mobile_url?>" alt="">
				</a>
			</div>
		<?php }?>
	
		<?php if(isset($bannersHome[3])){?>
			<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddleft">
				<a href="<?=$bannersHome[3]->detailUrl?>" target="<?=$bannersHome[3]->target?>">
					<img id="img-mobile-banner-4" class="img-responsive" src="<?=$bannersHome[3]->image_mobile_url?>" alt="">
				</a>
			</div>
		<?php }?>
		
	</div>
</div>
<!--Fin layout 6-->