<!-- layouts 2 -->
<div class="row layout-2 categorias-ambientes row-no-margin">
	<!-- Desktop-->
	<?php if(isset($bannersHome[1])){?>
		<div class="hidden-xs hidden-sm col-md-4 layout-2-3 sin-padding-right">
			<a href="<?=$bannersHome[1]->detailUrl?>"target="<?=$bannersHome[1]->target?>">
				<img class="img-responsive" src="<?= $bannersHome[1]->image_url ?>" alt="">
			</a>
		</div>
	<?php }?>

	<div class="hidden-sm hidden-xs col-xs-6 col-md-8 layout-2-3">
		<a href="<?=$bannersHome[2]->detailUrl?>"target="<?=$bannersHome[2]->target?>">
			<img class="img-responsive" src="<?= $bannersHome[2]->image_url ?>" alt="">
		</a>
	</div>

	<!-- Mobile-->
	<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddright">
		<a href="<?=$bannersHome[1]->detailUrl?>" target="<?=$bannersHome[1]->target?>">
			<img class="img-responsive" src="<?=$bannersHome[1]->image_mobile_url?>" alt="">
		</a>
	</div>
	<div class="col-xs-6 hidden-md hidden-lg layout-mobile mediopaddleft">
		<a href="<?=$bannersHome[2]->detail?>" target="<?=$bannersHome[2]->target?>">
			<img class="img-responsive" src="<?=$bannersHome[2]->image_mobile_url?>" alt="">
		</a>
	</div>
</div>
<!-- fin layout 2 -->