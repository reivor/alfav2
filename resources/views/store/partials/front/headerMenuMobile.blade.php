<ul id="menu-content" class="menu-content">
	<?php foreach ($categoriesMenu as $keyCategoryLv1 => $categoryLv1) {?>
		<?php
			$arrow = "";
			if($categoryLv1->isShort()){
				if($categoryLv1->isExpertsCategory()){
					$menuLink="#";
				}
				else if($categoryLv1->isIdeasCategory()){
					$menuLink="#";
				}
				$toggle="";
			}
			else{
				$arrow="<span></span>";
				$menuLink="#";
				$toggle="collapse";
			}
		?>

		<li data-toggle="<?=$toggle?>" data-target="#category-<?=$categoryLv1->code?>" class="collapsed">
			<a href="<?=$menuLink?>"><?=$categoryLv1->display_name_first?> <?=$categoryLv1->display_name_second?><?=$arrow?></a>
		</li>

		<ul class="sub-menu collapse" id="category-<?=$categoryLv1->code?>">
			<?php foreach ($categoryLv1->categories as $keyCategoryLv2 => $categoryLv2) {?>
				<?php if($categoryLv1->isBig()) {?>
					<li><a href="<?=$categoryLv2->detailUrl?>"><?=$categoryLv2->name?></a></li>
				<?php }else{?>
					<?php foreach ($categoryLv2->categories as $keyCategoryLv3 => $categoryLv3) {?>
						<li><a href="<?=$categoryLv3->detailUrl?>"><?=$categoryLv3->name?></a></li>
					<?php }?>
				<?php }?>
			<?php }?>
		</ul>
	<?php }?>

	<li data-target="#category-expertos" class="collapsed">
		<a href="/blog"><?= trans('front.creative_ideas_plain') ?></a>
	</li>
</ul>
