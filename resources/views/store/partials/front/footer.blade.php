<div class="container content-footer">
	<div class="footer-1">
		<div class="col-icon-aside">
			<a href="<?= url(trans('routes.financing')) ?>">
				<i class="aside-icon aside-fina"></i>
				<?= trans('front.label_financing') ?>
			</a>
		</div>
		<div class="col-icon-aside">
			<a href="<?= url(trans('routes.design')) ?>">
				<i class="aside-icon aside-dise"></i>
				<?= trans('front.label_design') ?>
			</a>
		</div>
		<div class="col-icon-aside">
			<a href="<?= url(trans('routes.installation')) ?>">
				<i class="aside-icon aside-insta"></i>
				<?= trans('front.label_installation') ?>
			</a>
		</div>
		<div class="col-icon-aside">
			<a href="<?= url(trans('routes.certifications')) ?>">
				<i class="aside-icon aside-certi"></i>
				<?= trans('front.label_certifications') ?>
			</a>
		</div>
		<div class="col-icon-aside">
			<a href="<?= url(trans('routes.client_info')) ?>">
				<i class="aside-icon aside-info"></i>
				<?= trans('front.label_info_client_footer') ?>
			</a>
		</div>
		<div class="col-icon-aside">
			<a href="<?= url(trans('routes.catalogs')) ?>">
				<i class="aside-icon aside-cata"></i>
				<?= trans('front.label_catalogues') ?>
			</a>
		</div>
		<div class="col-icon-aside">
			<a href="<?= url(trans('routes.environmental_alfa')) ?>">
				<i class="aside-icon aside-alfa"></i>
				<?= trans('front.label_environmental_alfa') ?>
			</a>
		</div>
	</div>
</div>