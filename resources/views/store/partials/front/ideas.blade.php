<?php if( $blogIdeas ): ?>
<!-- SECTION IDEAS -->
<section class="container" id="ideas">
	<div class="section-ideas-title">
		<h1>{!! trans('front.creative_ideas') !!}</h1>
	</div>
	<div class="row row-no-margin row-ideas">
		<?php foreach($blogIdeas as $key=> $idea): ?>
		<div class="col-md-6 col-lg-6 ideas-left">
			<div class="row row-ideas-inner">
				<div class="col-xs-6 col-sm-6 ideas-image sin-padding">
					<a href="<?= $idea->url_detail ?>">
						<img class="img-responsive img-ideas" src="<?= $idea->image_home ?>" alt="<?= $idea->title_plain ?>">
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 ideas-content">
					<h4><?= $idea->getPublishedDate($locale, true) ?></h4>
					<a href="<?= $idea->url_detail ?>">
						<p><?= $idea->title_plain ?></p>
					</a>
					<a href="<?= $idea->url_detail ?>" class="hidden-xs hidden-sm">
						<img class="btn-ver-mas img-responsive" src="/img/ideas/btn-ver-mas.png" alt="{{ trans('front.send_more') }}">
					</a>
					<p class="hidden-xs hidden-sm ideas-comments"><?= trans_choice('front.comment_count', $idea->approvedComments->count()) ?></p>
				</div>
			</div>
			<!-- Boton ver mas mobile -->
			<a href="<?= $idea->url_detail ?>" class="hidden-md hidden-lg btn-ver-mas-mobile">
				<img class="btn-ver-mas img-responsive" src="/img/ideas/btn-ver-mas.png" alt="{{ trans('front.send_more') }}">
			</a>
		</div>
		<?php endforeach; ?>
	</div>
</section>
<?php endif; ?>