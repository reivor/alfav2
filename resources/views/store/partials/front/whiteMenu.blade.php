<div class="container menu-section menu-blanco">
	<div class="col-xs-2 col-lg-3 sin-padding div-logo text-center">
		<a class="img-logo" href="/"><img src="/img/logo.png" alt="ALFA"></a>
		<a class="img-logo-mobile" href="index.html"><img src="img/logo-affix.png" alt="ALFA"></a>
	</div>

	<div class="col-xs-9 col-lg-8 sin-padding">
		<?php foreach ($categoriesMenu as $keyCategory => $topCategory): ?>
			<div class="col-xs-2 item-menu text-center sin-padding">
				<a href="#submenu-c-<?=$topCategory->id?>" class="collapsed" role="button" data-toggle="collapse"
				   data-target="#submenu-c-<?= $topCategory->id ?>" data-index="<?= $keyCategory ?>"
				   aria-expanded="false" aria-controls="submenu-c-<?=$topCategory->id?>">
					<?= $topCategory->display_name_first ?><br>
					<span><?= $topCategory->display_name_second?></span>
				</a>
			</div>
		<?php endforeach; ?>

		<div class="col-xs-2 item-menu text-center sin-padding-left last-item-menu">
			<a href="/blog" class="ideas-menu collapsed" role="button"
			   data-target="#" data-index="5"
			   aria-expanded="false" aria-controls="">
				<?= trans('front.creative_ideas_header')?>
			</a>
		</div>
	</div>

	<div class="col-xs-1 item-car-float text-center">
		<div class="content-item-car">
			<a href="<?= url(trans('routes.quotes')) ?>" style="display: block; width: 40px;"
			   data-toggle="tooltip" data-placement="bottom" 
			   title="<?= trans('front.button_quotes') ?>">
			<img class="item-car" src="/img/icon-menu/cotizador.png" alt="Item Car">
			<img class="item-car-mobile" src="/img/icon-menu/cotizador.png" alt="Item Car">
			<span class="cant-item-car-float"><?=$numberProducts?></span>
			</a>
		</div>
	</div>
</div>