<header>
	<!-- Header Desktop -->
	<div class="container-fluid sin-padding header-gris menu-desktop hidden-xs hidden-sm">
		<div class="container text-center">
			<div class="col-md-12">
				<div class="row">
					<nav class="navbar navbar-default" role="navigation">
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse sin-padding" id="bs-example-navbar-collapse-1">
						<div class="col-md-8 menu-header-gris">
							<ul class="nav navbar-nav pull-right">
								<li>
									<a href="<?= url(trans('routes.stores')) ?>"><?= trans('front.creation_points_header') ?></a>
								</li>
								<li><a class="collapsed" role="button" data-toggle="collapse" href="#r-newsletter" aria-expanded="false" aria-controls="r-newsletter">{{trans('front.subscribe_newsletter')}}</a>
									<!-- modal registro Newsletter -->
									<div class="collapse r-newsletter bubble" id="r-newsletter">
										<div class="well col-xs-12">
											<span class="arrow-modal"></span>
											<h5>{!!trans('front.title_newsletter_suscribe_home')!!}</h5>
											<form id="form-newsletter">
												{{ csrf_field() }}
												<div class="form-group newsfeed-form">
													<label for="email"></label>
													<input type="text" name="name" class="form-control newsfeed-form-control email" placeholder="{{trans('front.label_name')}}">
												</div>
												<div class="form-group">
													<label for="email"></label>
													<input type="email" name="email" class="form-control newsfeed-form-control email" placeholder="{{trans('front.label_email')}}">
												</div>
												<div class="btn-news-align">
													<a id="btn-form-newsletter" data-form="form-newsletter" class="btn btn-newsfeed btn-form-newsletter" data-toggle="modal" >{{trans('front.label_send')}}</a>
												</div>
												<!-- Modal acpetar y cancelar -->
											<!-- 	<div class="modal fade" id="aceptar-newsletter" tabindex="-1" role="dialog" aria-labelledby="aceptar-newsletterLabel">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<div class="modal-body">
																<h5>Se ha enviado un correo! </h5>
																<button class="btn btn-newsletter">{{trans('front.accept')}}</button>
																<button class="btn btn-newsletter">{{trans('front.cancel')}}</button>
															</div>
														</div>
													</div>
												</div>-->
												<!-- fin Modal acpetar y cancelar -->
											</form>
										</div>
									</div>
									<!-- fin modal registro Newsletter -->

								</li>
								@if(!$loginStatus)
								<li>
									<a class="collapsed no-login" role="button" data-toggle="collapse" href="#i-seccion" aria-expanded="false" aria-controls="i-seccion">
										
										{{trans('front.label_login_verb')}}
										
									</a>

									<!-- modal login Desktop -->
									<div class="collapse i-seccion bubble" id="i-seccion">
										<div class="well col-xs-12">
											<span class="arrow-modal"></span>
											<h5>{{trans('front.login_account')}}</h5>
											<form id="form_login_desktop">
												{!! csrf_field() !!}
												<div class="form-group newsfeed-form">
													<label for="email">{{trans('front.label_email')}}</label>
													<input type="email" class="form-control newsfeed-form-control email" name="email" placeholder="{{trans('front.label_email')}}" required>
												</div>
												<div class="form-group">
													<label for="password">{{trans('front.label_password')}}</label>
													<input type="password" class="form-control newsfeed-form-control" id="password" name="password" placeholder="{{trans('front.label_password')}}" required>
												</div>
												<div class="checkbox">
													<label>
														<input name="remember" type="checkbox" value="Recordar contrasena"> {{trans('front.remember_password')}}
													</label>
												</div>
												<div class="form-group">
													<label><a href="" data-toggle="modal" data-target="#olv-contrasena">{{trans('front.forgotten_password')}}</a></label>
												</div>
												<div class="btn-news-align">
													<button type="button" class="btn btn-login btn-form-login" data-form="form_login_desktop">
														<?= trans('front.login_account') ?>
													</button>
													</div>
													<div class="form-group">
														<label><a href="<?= url(trans('routes.register')) ?>"><?= trans('front.register') ?></a></label>
													</div>
												</form>
												<!-- Modal olvide contraseña -->
												<div class="modal fade" id="olv-contrasena" tabindex="-1" role="dialog" aria-labelledby="olv-contrasenaLabel">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<div class="modal-body">
																<h5>{{trans('front.msg_recover_password')}}</h5>
																<form id='form_recovery_pass'>
																	{!! csrf_field() !!}
																	<div class="form-group newsfeed-form">
																		<input type="email" name="email" class="form-control newsfeed-form-control" placeholder="{{trans('front.label_email')}}" required>
																	</div>
																	<div class="btn-news-align">
																		<button type="button" id="btn-recovery-pass" class="btn btn-login">{{trans('front.label_send')}}</button>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
												<!-- fin Modal olvide contraseña -->
												<!-- Modal Confirmacion de envio de recuperacion de contraseña-->
												<div class="modal fade" id="modal-aceptar" tabindex="-1" role="dialog" aria-labelledby="modal-aceptar-Label">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<div class="modal-body">
																<h5>{{trans('front.send_recovery_pass')}}</h5>
																<button type="button" class="btn btn-newsletter">{{trans('front.close')}}</button>
															</div>
														</div>
													</div>
												</div>
												<!-- fin Modal aceptar -->
											</div>
										</div>
										<!-- Fin modal login Desktop -->
									</li>
									@else

									<li class="overflow-user"><a class="collapsed sesionusuario" role="button" data-toggle="collapse" href="#i-seccionusuario" aria-expanded="false" aria-controls="i-seccion">{{$userLogin->name}} {{$userLogin->lastname}}</a>
										<!-- modal login Desktop -->
										<div class="collapse i-seccionusuario bubble" id="i-seccionusuario">
											<div class="well col-xs-12">
												<span class="arrow-modal"></span>
												<ul class="opcionesusuario">
													<li class="user-name-session"><?= $userLogin->fullName ?></li>
													<li class="user-password-session"><a href="<?= url(trans('routes.reset_password')) ?>"><?= trans('front.change_password') ?></a></li>
													<li><a href="<?= url(trans('routes.logout')) ?>"><?= trans('front.close_session') ?></a></li>
												</ul>
											</div>
										</div>
										<!-- Fin modal login Desktop -->
									</li>

									@endif

									<li>
										<!-- icono bandera pais -->
										<span class="bandera-pais"><img class="img-flag" src="/img/icon-menu/icon-flag-<?=config('store.id')?>.png" alt=""></span>
										<section>
											<select class="cs-select cs-skin-underline" id="country_selector">
												<option value="//<?= env('CO_STORE_URL') ?>" <?php if(config('store.id')=="co"){?> selected <?php }?>>{{trans('front.label_co')}}</option>
												<!--
												<option value="//<?= env('EC_STORE_URL') ?>" <?php if(config('store.id')=="ec"){?> selected <?php }?>>{{trans('front.label_ec')}}</option>
												<option value="//<?= env('US_STORE_URL') ?>" <?php if(config('store.id')=="us"){?> selected <?php }?>>{{trans('front.label_us')}}</option>
												-->
											</select>
										</section>
									</li>
								</ul>
							</div>
							<div class="col-md-2 sin-padding header-search">
								<form class="navbar-form sin-padding" action="<?= url(trans('routes.search')) ?>" role="search">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="<?= trans('front.label_search') ?>" name="q">
										<div class="input-group-btn">
											<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-2 header-redes">
								<div class="redes-header">
									<ul class="nav navbar-nav">
										<li class="icon-facebook"><a target="_blank" href="https://www.facebook.com/Momentos-Alfa-560747927325592/">Facebook</a></li>
										<li class="icon-pinterest"><a target="_blank" href="https://es.pinterest.com/momentosalfa/?etslf=59042&eq=momentos%20alfa">Pinterest</a></li>
										<li class="icon-twitter"><a target="_blank" href="https://twitter.com/MomentosAlfa?lang=es">Twitter</a></li>
										<li class="icon-youtube"><a target="_blank" href="https://www.youtube.com/channel/UCHjyevk3ZCJ3dph7DmDMn9Q">YouTube</a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- /.navbar-collapse -->
					</nav>
					
				</div>
				</div>
			</div>
		</div>
		<!-- Header Desktop -->
		<nav id="navbar-main" class="affix-top">
			<div class="container-fluid hidden-xs hidden-sm">
				<!--Menu blanco-->
				@include('store.partials.front.whiteMenu')
				<!--Submenu-->
				@include('store.partials.front.dropdownSubmenu')
				<!--Menu blanco-->
				@include('store.partials.front.orangeMenu')
			</div>
		</nav>

<!-- Fin Header Desktop -->

<!-- Header Mobile -->
<div class="menu-mobile hidden-md hidden-lg">
	<section class="buttonset">
		<button id="showRight"><img src="/img/icon-menu/icon-menu-mobile.png" alt=""></button>
	</section>
	<header class="container-fluid">
		<div class="col-xs-4 text-left buscador-mobile">
			<div class="buscador-mobile-des">
				<a class="icon-buscador" role="button" data-toggle="collapse" href="#collapsebuscador-mobile" aria-expanded="false" aria-controls="collapsebuscador-mobile">
					<img src="/img/icon-menu/icon-search.png" alt="">
				</a>
			</div>
			@yield('filter_mobile_button')
		</div>
		<div class="col-xs-4 text-center logo-mobile">
			<a href="/">
				<img src="/img/logo.png" alt="ALFA" class="img-responsive">
			</a>
		</div>
		<!-- buscador -->
		<div class="collapse buscador-mobile-det" id="collapsebuscador-mobile">
			<div class="well col-xs-12">
				<form action="<?= url(trans('routes.search')) ?>">
					<input type="search" name="q" placeholder="<?= trans('front.label_search') ?>" class="input-search-mobile">
					<button class="btn-search-mobile" type="submit"><i class="glyphicon glyphicon-search"></i></button>
				</form>
			</div>
		</div>
		<!-- filtros -->
		@yield('filter_mobile')
	</header>
	<!-- Fin Header Mobile -->
</div>
</header>