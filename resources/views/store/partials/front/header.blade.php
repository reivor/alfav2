<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right hidden-md hidden-lg" id="cbp-spmenu-s2">
	<div class="nav-side-menu">
		<div class="brand"><button id="showRight_2"><?= trans('front.close') ?></button></div>
		<div class="menu-list">
			<div class="col-xs-12 menu-mobile-user">
				<div class="col-xs-3 text-center">
					<a data-toggle="modal" data-target="#modal-login">
						<img src="/img/icon-menu/icon-user-mobile.png" alt="información del usuario" class="icono_menu_mobile">
					</a>	

					@if(!$loginStatus)
					<!-- Modal login mobile -->
					<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-loginLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<div class="modal-body">
									<h5><?= trans('front.login_account') ?></h5>
									<form id="form_login_mobile">
										{!! csrf_field() !!}
										<div class="form-group newsfeed-form">
											<label for="email">{{ trans('front.label_email') }}</label>
											<input type="email" class="email form-control newsfeed-form-control" name="email" placeholder="{{trans('front.label_email')}}" required>
										</div>
										<div class="form-group">
											<label for="password">{{ trans('front.label_password') }}</label>
											<input type="password" class="form-control newsfeed-form-control" id="password" name="password" placeholder="{{trans('front.label_password')}}" required>
										</div>
										<div class="checkbox">
											<label>
												<input name="remember" type="checkbox" value="Recordar contrasena"> {{trans('front.remember_password')}}
											</label>
										</div>
										<div class="form-group">
											<label><a href="" data-toggle="modal" data-target="#olv-contrasena-2">{{trans('front.forgotten_password')}}</a></label>
										</div>
										<div class="btn-news-align">
											<button type="button" class="btn btn-login btn-form-login" data-form="form_login_mobile">
												<?= trans('front.label_login') ?>
											</button>
										</div>
										<div class="form-group">
											<label><a href="<?= url(trans('routes.register')) ?>"><?= trans('front.register_me') ?></a></label>
										</div>
									</form>
								</div>


							</div>
						</div>
					</div>
					<!-- fin modal login mobile -->
					@else
					<!-- Modal logeado mobile -->
					<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-loginLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<div class="modal-body">
									<ul class="opcionesusuario">
										<li class="user-name-session">
											{{ $userLogin->name }} {{ $userLogin->lastname }}
										</li>
										<li class="user-password-session">
											<a href="/changepass">{{ trans('front.change_password') }}</a>
										</li>
										<li>
											<a href="<?= url(trans('routes.logout')) ?>">
												{{trans('front.close_session')}}
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- fin modal login mobile -->
					@endif
					<!-- Modal olvide contraseña mobile -->
					<div class="modal fade" id="olv-contrasena-2" tabindex="-1" role="dialog" aria-labelledby="olv-contrasena-2Label">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<div class="modal-body">
									<h5>{{ trans('front.msg_recover_password') }}</h5>
									<form id='form_recovery_pass-mobile'>
										{!! csrf_field() !!}
										<div class="form-group newsfeed-form">
											<input type="email" name="email" class="form-control newsfeed-form-control" placeholder="{{trans('front.label_email')}}" required>
										</div>
										<div class="btn-news-align">
											<button type="button" id="btn-recovery-pass-mobile" class="btn btn-login">{{trans('front.label_send')}}</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- fin Modal olvide contraseña mobile -->
					<!-- Modal aceptar -->
					<div class="modal fade" id="modal-aceptar-mobile" tabindex="-1" role="dialog" aria-labelledby="modal-aceptar-mobileLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<div class="modal-body">
									<h5>{{ trans('front.send_recovery_pass') }}</h5>
									<button type="button" class="btn btn-newsletter" data-dismiss="modal" aria-label="Close">{{trans('front.close')}}</button>
								</div>
							</div>
						</div>
					</div>
					<!-- fin Modal aceptar -->
				</div>
				<div class="col-xs-3 text-center lang-mobile">
					<a role="button" data-toggle="collapse" href="#collapsePais" aria-expanded="false" aria-controls="collapsePais" class="btn-pais" ><img src="/img/icon-menu/icon-flag-<?=config('store.id')?>.png" alt="" class="icono_menu_mobile"><span></span></a>
				</div>
				<div class="col-xs-3 text-center">
					<a href="<?= url(trans('routes.stores')) ?>">
						<img src="/img/icon-menu/icon-map-mobile.png" alt="puntos de creación" class="icono_menu_mobile">
					</a>
				</div>
				<div class="col-xs-3 text-center car-item">
					<div class="item-car-float">
						<a href="<?= url(trans('routes.quotes')) ?>" data-toggle="tooltip" data-placement="bottom" title="<?= trans('front.button_quotes') ?>">
							<img class="item-car" src="/img/icon-menu/cotizador.png" alt="Item Car">
							<span class="cant-item-car-float"><?=$numberProducts?></span>
						</a>
					</div>
				</div>
			</div>
			<!-- caja paises seleccion -->
			<div class="collapse" id="collapsePais">
				<div class="well ">
					<ul>
						<li></li>
						<li><a href="//{{env('CO_STORE_URL')}}"><img src="/img/icon-menu/icon-flag-co.png" alt=""></a></li>
						<!--
						<li><a href="//{{env('EC_STORE_URL')}}"><img src="/img/icon-menu/icon-flag-ec.png" alt=""></a></li>
						<li><a href="//{{env('US_STORE_URL')}}"><img src="/img/icon-menu/icon-flag-us.png" alt=""></a></li>
						-->
					</ul>
				</div>
			</div>
			
			<div class="clearfix"></div>
			@include('store.partials.front.headerMenuMobile')
			<div class="menu-mobile-redes">
				<div class="row">
					<div class="col-xs-6 col-xs-offset-3">
						<div class="row text-center">
							<div class="col-xs-3 facebook-mobile"><a target="_blank" href="https://www.facebook.com/Momentos-Alfa-560747927325592/">Facebok</a></div>
							<div class="col-xs-3 pinteres-mobile"><a target="_blank" href="https://es.pinterest.com/momentosalfa/?etslf=59042&eq=momentos%20alfa">Pinteres</a></div>
							<div class="col-xs-3 twitter-mobile"><a target="_blank" href="https://twitter.com/MomentosAlfa?lang=es">Twitter</a></div>
							<div class="col-xs-3 youtube-mobile"><a target="_blank" href="https://www.youtube.com/channel/UCHjyevk3ZCJ3dph7DmDMn9Q">YouTube</a></div>
						</div>
					</div>
					<!--<div class="pinteres-mobile"><a href="">Pinteres</a></div>-->
				</div>
			</div>
		</div>
	</div>
</nav>
