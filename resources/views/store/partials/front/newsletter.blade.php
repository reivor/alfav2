<!-- begin SECTION NEWSFEED MOBILE -->
<div class="hidden-md hidden-lg newsfeed-mobile">
	<h1>{!!trans('front.title_newsletter_suscribe_subhome')!!}</h1>
	<form id="form-newsletter-subhome">
		{{ csrf_field() }}
		<div class="form-group newsfeed-form">
			<label for="nombre"></label>
			<input type="text" class="form-control newsfeed-form-control" id="nombre" name="name" placeholder="Nombre">
		</div>
		<div class="form-group">
			<label for="email"></label>
			<input type="email" class="form-control newsfeed-form-control email" name="email" placeholder="Email">
		</div>
		<div class="btn-news-align">
			<button type="button" class="btn btn-newsfeed btn-form-newsletter" data-form="form-newsletter-subhome">{{trans('front.label_send')}}</button>
		</div>
	</form>
</div>
<!-- end SECTION NEWSFEED MOBILE -->