<div class="container hidden-xs hidden-sm">

	<div class="footer-2">

		<?php foreach($footerItems as $keyParent => $item): ?>
		<div class="col-footer-menu">
			<h4>
				<?= $item['parent']->display_name_first ?><br>
				<?= $item['parent']->display_name_second ?>
			</h4>
			<ul>
				<?php foreach($item['categories'] as $child): ?>
				<li><a href="<?= $child->detailUrl ?>"><?= $child->name ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php endforeach; ?>

		{{-- <div class="col-footer-menu">
			<h4>{!! trans('front.experts_creators_footer') !!}</h4>
			<ul>
				<li><a href="#">{{ trans('front.alfa_masters') }}</a></li>
			</ul>
		</div> --}}

		<div class="col-footer-menu">
			<h4>{!! trans('front.creative_ideas_footer')!!}</h4>
			<ul>
				<?php foreach($blogIdeas as $idea): ?>
				<li><a href="<?= $idea->url_detail ?>"><?= $idea->title_plain ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>

		<div class="col-footer-menu">
			<h4>{!! trans('front.our_company_footer')!!}</h4>
			<ul>
				<li><a href="<?= url(trans('routes.aboutus')) ?>"><?= trans('front.who_we_are') ?></a></li>
				<li><a href="<?= url(trans('routes.jobs')) ?>"><?= trans('front.work_with_us') ?></a></li>
			</ul>
		</div>

		<div class="col-footer-menu">
			<h4>{!! trans('front.creation_points_footer')!!}</h4>
			<ul>
				<li><a href="<?= url(trans('routes.stores')) ?>"><?= trans('front.find_your_store') ?></a></li>
			</ul>
		</div>

	</div>

</div>
