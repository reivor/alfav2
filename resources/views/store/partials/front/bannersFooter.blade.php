<?php if(count($bannersPromoHomeFooter)>0){ ?>
<section class="container content-promo">
	<div class="container">
		<section class="container">
			<hr class="separador hidden-md hidden-lg">
		</section>
		<div class="row row-no-margin grid-footer-home">
			<?php if(isset($bannersPromoHomeFooter[1])){?>
				<div class="hidden-xs hidden-sm col-md-4 col-lg-4 promo-left sin-padding-left sin-padding-right">
					<a href="<?=$bannersPromoHomeFooter[1]->detailUrl?>" target="<?=$bannersPromoHomeFooter[1]->target?>">
						<img class="img-responsive" src="<?=$bannersPromoHomeFooter[1]->image_url?>" alt="">
					</a>
				</div>
			<?php }?>
			
			<?php if(isset($bannersPromoHomeFooter[2])){?>
				<div class="col-md-8 col-lg-8 promo-right sin-padding-right">
					<a href="<?=$bannersPromoHomeFooter[2]->detailUrl?>" target="<?=$bannersPromoHomeFooter[2]->target?>">
						<img class="img-responsive img-promo-2" src="<?=$bannersPromoHomeFooter[2]->image_url?>" alt="">
					</a>
				</div>
			<?php }?>
			
		</div>
	</div>
</section>
<?php }?>