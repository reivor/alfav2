<div class="container menu-section menu-naranja">

	<div class="col-md-2 col-lg-2 sin-padding div-logo text-center">

		<a class="img-logo" href="/" ><img src="/img/logo.png" alt="ALFA"></a>
		<a class="img-logo-mobile" href="/"><img src="/img/logo-affix.png" alt="ALFA"></a>

	</div>

	<div class="col-md-8 col-lg-7 col-lg-offset-0 sin-padding">
		<div>

			<?php foreach( $categoriesMenu as $keyCategory => $topCategory ): ?>
			<div class="col-xs-2 item-menu text-center sin-padding">
				<a class="collapsed" role="button" data-toggle="collapse" data-target="#submenu-c-<?= $topCategory->id ?>" href="#submenu-c-<?= $topCategory->id ?>" aria-expanded="false" aria-controls="submenu-c-<?= $topCategory->id ?>"><?= $topCategory->display_name_first ?><br><span><?= $topCategory->display_name_second ?></span></a>
			</div>
			<?php endforeach; ?>
			
			<div class="col-xs-2 item-menu text-center sin-padding-left last-item-menu">
				<a class="ideas-menu collapsed" role="button" href="/blog" aria-expanded="false" aria-controls=""><?= trans('front.creative_ideas_header') ?></a>
			</div>
		</div>
	</div>

	<div class="col-md-2 col-lg-3 sin-padding">
		<div class="col-md-10 col-lg-8 sin-padding header-search">
			<form class="navbar-form sin-padding" action="<?= url(trans('routes.search')) ?>" role="search">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="<?= trans('front.label_search') ?>" name="q">
					<div class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
			</form>
		</div>

		<div class="col-md-2 col-lg-4 sin-padding item-car-float">
			<div class="content-item-car">
				<a href="<?= url(trans('routes.quotes')) ?>" style="display:block; width: 40px;"
				   data-toggle="tooltip" data-placement="bottom" 
				   title="<?= trans('front.button_quotes') ?>">
					<img class="item-car" src="/img/icon-menu/cotizador.png" alt="Item Car">
					<img class="item-car-mobile" src="/img/icon-menu/cotizador.png" alt="Item Car">
					<span class="cant-item-car-float"><?= $numberProducts ?></span>
				</a>
			</div>
		</div>
	</div>
</div>
