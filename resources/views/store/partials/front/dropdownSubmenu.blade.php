<div class="container menu-section-submenu">
	<?php foreach ($categoriesMenu as $keyCategoryLv1 => $categoryLv1) {?>
		<div class="collapse <?php echo ($categoryLv1->isTwoColumns() ? "submenu" : "submenu-2");?>" id="submenu-c-<?=$categoryLv1->id?>">
			<div class="well col-xs-12">
				<?php if(!$categoryLv1->isTwoColumns()) {?>
				<div class="col-xs-4 sin-padding">
					<div class="col-xs-12 div-list-submenu">
						<?php if( $categoryLv1->categories->count() > 5 ): ?>
						<div class="content">
						<?php endif; ?>
						<ul class="list-submenu">
							<?php foreach ($categoryLv1->categories as $keyCategoryLv2 => $categoryLv2) {?>
							<li class="item-submenu"><a href="<?=$categoryLv2->detailUrl?>"><?=$categoryLv2->name?></a></li>
							<?php }?>
						</ul>
						<?php if( $categoryLv1->categories->count() > 5 ): ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<?php }else{?>
			<div class="col-xs-6 sin-padding">
				<?php foreach ($categoryLv1->categories as $keyCategoryLv2 => $categoryLv2) {?>
				<div class="col-xs-6 div-list-submenu">
					<h5><a href="<?=$categoryLv2->detailUrl?>"><?=$categoryLv2->name?></a></h5>
					<div class="content">
						<ul class="list-submenu">
							<?php foreach ($categoryLv2->categories as $keyCategoryLv3 => $categoryLv3) {?>
							<li class="item-submenu"><a href="<?=$categoryLv3->detailUrl?>"><?=$categoryLv3->name?></a></li>
							<?php }?>
						</ul>
					</div>
				</div>
				<?php }?>
			</div>
		<?php }?>
				<div class="<?php echo ($categoryLv1->isTwoColumns() ? 'col-xs-6 sin-padding' : 'col-xs-8');?>">
					<?php if(isset($bannersMenu[$categoryLv1->id][1])){ ?>
					<div class="col-xs-6 img-categoria text-center">
						<a href="<?=$bannersMenu[$categoryLv1->id][1]->detailUrl?>" target="<?=$bannersMenu[$categoryLv1->id][1]->target?>">
							<img class="img-responsive" src="<?=$bannersMenu[$categoryLv1->id][1]->image_url?>" alt=""><span>{{$bannersMenu[$categoryLv1->id][1]->text}}</span>
						</a>
					</div>
					<?php }?>

					<?php if(isset($bannersMenu[$categoryLv1->id][2])){ ?>
					<div class="col-xs-6 img-categoria text-center">
						<a href="<?=$bannersMenu[$categoryLv1->id][2]->detailUrl?>" target="<?=$bannersMenu[$categoryLv1->id][2]->target?>">
							<img class="img-responsive" src="<?=$bannersMenu[$categoryLv1->id][2]->image_url?>" alt=""><span>{{$bannersMenu[$categoryLv1->id][2]->text}}</span>
						</a>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	<?php }?>
</div>