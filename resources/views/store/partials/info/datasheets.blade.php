@section('content')  

<!-- Content -->
<section class="container-fluid info-cliente-producto">
	<div class="container">
		<!-- Ruta -->
		<div class="row row-no-margin sin-padding ruta hidden-xs hidden-sm">
			<div class="col-xs-12">
				<ol class="breadcrumb sin-padding">
					<li><a href="/">{{ trans('front.home') }}</a></li>
					<li><a href="<?= url(trans('routes.client_info')) ?>">{{ trans('front.customer_information') }}</a></li>
					<li class="active">{{ trans('front.datasheets') }}</li>
				</ol>
			</div>
		</div>
		<!-- Fin Rutas -->

		<div class="row row-no-margin">
			<div class="col-xs-12">
				<h1>{{ trans('front.datasheets') }}</h1>
			</div>	
		</div>

		<!-- Filtros -->
		<div class="row row-no-margin sin-padding hidden-xs hidden-sm">
			<div class="col-sm-6 col-sm-offset-3 filtros">
				<div class="col-xs-6">
					<select class="cs-select cs-skin-underline select-info" id="category">
						<option value="" disabled selected>Categoria</option>
						@foreach ($grouped as $group)
						<option value="{{ $group[0]->cid }}">{{ $group[0]->cname }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-xs-6" id="product-col">
					<select class="cs-select cs-skin-underline select-info" id="product">
						<option value="" disabled selected>Productos</option>
					</select>
				</div>
			</div>
		</div>
		<!-- Fin Filtros -->
		<div id="results">
			@foreach ($grouped as $group)
				<div class="row row-no-margin container-results">
				<h2 class="tit-purple">{{ $group[0]->cname }}</h2>
					@foreach ($group as $i => $product)
						<div class="col-xs-6 col-sm-3">
							<a href="{{ $product->data_sheet }}" download>
								<figure>
									<img src="/images/icon-item-info.png" alt="">
								</figure>
								<p class="tit-item-info">{{ $product->name }}</p>
							</a>
						</div>
						@if($i == 7)
							<div class="col-xs-12 ver-mas-purple">
								<a href="#" class="more" data-id="{{ $product->cid }}">{{ trans('front.see_more') }}</a>
							</div>
							@break
						@endif
					@endforeach
					<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display: none; margin-top: 10px;">
						<div class="internal-loading">
							<div class="internal-loader"></div>
						</div>
						<!--<img src="/images/loader.gif" style="width: 30px; height: 30px;"/>-->
					</div>
				</div>
			@endforeach
		</div>
		
	</div>
</section>
<!-- Fin Content -->
@endsection

@push('scripts')
<script>

var product = document.querySelector('#product');
productFx = new SelectFx( product, {} );

var selectElement2 = document.querySelector('#category');
new SelectFx( selectElement2, {
	onChange: function(val) {
		showLoader();//Mostrar loader
		var select = $('#category');

		var output = [];
		$('#results').html('');
		$.getJSON("/clientInfoByCatId/"+ select.val() +"/datasheets/", function(jsonData){
			var toAppend = '';
			$.each(jsonData, function(i,data)
			{
				toAppend += '<div class="col-xs-6 col-sm-3"><a href="'+data.data_sheet+'" download><figure><img src="/images/icon-item-info.png" alt=""></figure><p class="tit-item-info">'+data.name+'</p></a></div>';
			});
			$('#results').html(toAppend);
			hideLoader();
			// remove and recreate product dropdown
			var productEl = $('#product').empty().clone();
			$(productFx.selEl).remove();

			output.push('<option disabled selected>Producto</option>');
			$.each(jsonData, function(i,data)
			{
				output.push('<option value="' + data.id + '">'+ data.name +'</option>');
			});

			$('#product-col').append(productEl);
			$('#product').html(output.join(''));

			product = document.querySelector('#product');
			productFx = new SelectFx( product, {
				onChange: function(val){
					showLoader();//Mostrar loader
					$('#results').html('');
					$.getJSON("/clientInfoProd/"+ val +"/", function(jsonData){
						var toAppend = '';
						$.each(jsonData, function(i,data){
							toAppend += '<div class="col-xs-6 col-sm-3"><a href="'+data.data_sheet+'" download><figure><img src="/images/icon-item-info.png" alt=""></figure><p class="tit-item-info">'+data.name+'</p></a></div>';
						});
						$('#results').html(toAppend);
						hideLoader();//Ocultar loader
					});
				}
			});
		});
	}
});

$('.container-results').on('click', '.more', function (e) {
	var output = [];
	var catid = $(this).attr("data-id");
	var me = $(this);
	me.hide();
	me.parents(".container-results").children('#mini-loader').css('display', 'block');
	$.getJSON("/loadMore/"+catid+"/datasheets/", function(jsonData){
		console.log(jsonData);
		var toAppend = '';
		$.each(jsonData, function(i,data)
		{
			toAppend += '<div class="col-xs-6 col-sm-3"><a href="'+data.data_sheet+'" download><figure><img src="/images/icon-item-info.png" alt=""></figure><p class="tit-item-info">'+data.name+'</p></a></div>';
		});
		me.parents(".container-results").append(toAppend);
		me.parents(".container-results").children('#mini-loader').css('display', 'none');
	});
	e.preventDefault();
});

</script>
@endpush