<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>ALFA</title>
</head>

<body marginheight="0" marginwidth="0" topmargin="0" leftmargin="0">
  <table width="650" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="100%" bgcolor="#f1f1f1"><img src="<?=url('/images/newsletter/customer_header.jpg')?>" width="650" height="630" alt="Todos Somos Creadores" style="display:block" /></td>
    </tr>
    <tr bgcolor="#9a258f"> 
      <td align="center" valign="middle">
       <table width="90%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="50" align="center" valign="middle" style="text-transform: uppercase;"><font face="Arial, Helvetica, sans-serif" color="#ff7e00" size="6"><strong>{{trans('mail.greeting')}} <?=$user->name?></strong></font></font></td>
        </tr>
        <tr>
          <td height="40" align="center" valign="middle"><font face="Arial, Helvetica, sans-serif" color="#fff" size="2">{{trans('mail.message3')}}</font></td>
        </tr>
        <?php foreach ($currentOrder->quoteProducts as $product) { ?>
	        <tr>
	          <td height="40" align="center" valign="middle"><font face="Arial, Helvetica, sans-serif" color="#fff" size="3"><a href="<?=url($product->detailUrl)?>" style="color:#fff;text-decoration:none;"><?=$product->name?></a></font></td>
	        </tr>
        <?php } ?>
        <tr>
          <td height="40" align="center" valign="middle"><font face="Arial, Helvetica, sans-serif" color="#fff" size="2">{{trans('mail.message4')}}<a href="http://alfa.com.co/" style="color:#fff;text-decoration:none;">www.alfa.com.co</a> {{trans('mail.message5')}}.</font></td>
        </tr>
        <tr>
          <td height="40" align="center" valign="middle"><font face="Arial, Helvetica, sans-serif" color="#fff" size="2px">{{trans('mail.message6')}}.</font></td>
        </tr>
        <tr>
          <td height="60" align="center" valign="middle"><font face="Arial, Helvetica, sans-serif" color="#fff" size="2px">{{trans('mail.sincerely')}},<br />
            {{trans('mail.message7')}}</font></td>
          </tr>
          <tr>
            <td height="100" align="center" valign="middle"><img src="<?=url('/images/newsletter/logo_alfa_white.png')?>" width="84" height="42" /></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>