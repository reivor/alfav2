<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>ALFA</title>
</head>
<body marginheight="0" marginwidth="0" topmargin="0" leftmargin="0">
	<table width="800" align="center" cellpadding="0" cellspacing="0">
	    <tr>
	      <td colspan="2"><img src="<?= url('/images/newsletter/header.jpg') ?>" alt="Somos Creadores" width="800" height="250" /></td>
	    </tr>
	    <tr>
	      <td height="60" colspan="2" align="center" valign="middle" style="text-transform: uppercase;"><font face="Arial, Helvetica, sans-serif" color="#13abc0" size="6px">{{trans('mail.greeting')}}, </font><font face="Arial, Helvetica, sans-serif" color="#13abc0" size="6"><strong>Alfa</strong></font></td>
	    </tr>
	    <tr>
	    	<td height="60" colspan="2" align="center">
	    		<font face="Arial, Helvetica, sans-serif" color="#6d6e71" size="3">{{trans('mail.message1')}}<br />
	        <a href="<?= url('/') ?>" style="color:#6d6e71;text-decoration:none;">www.alfa.com.co</a></font></td>
	    </tr>  
	    <tr>
	        <td height="80" colspan="2" align="center" valign="middle">
	        	<font face="Arial, Helvetica, sans-serif" color="#6d6e71" size="3">
		        	{{trans('mail.product')}}:<br />
			        <?php foreach($currentOrder->quoteProducts as $product): ?>
								<a href="<?= url($product->detailUrl) ?>" style="color:#6d6e71;text-decoration:none"><?= $product->name ?></a><br />
					<?php endforeach; ?>
				</font>
			</td>
		</tr>  
		<tr>
			<td height="80" colspan="2" align="center" valign="middle">
				<font face="Arial, Helvetica, sans-serif" color="#6d6e71" size="3">
					{{trans('mail.nameLastName')}}:<br />
					<?=$user->name?> <?=$user->lastname?><br />
					<br />
					{{trans('mail.mail')}}:<br />
					<?=$user->email?><br />
					<br />
					{{trans('mail.address')}}:<br />
					<?=$user->residence?><br />
					<br />
					{{trans('mail.phone')}}:<br />
					<?=$user->cell_phone?><br />
					<br />
					{{trans('mail.city')}}:<br />
					<?=$city->name?><br />
					<br />
					{{trans('mail.job')}}:<br />
					<?=$user->job?><br />
				</font>
			</td>
		</tr>
	    <tr>
	    	<td height="50" colspan="2" align="center" valign="middle"><font face="Arial, Helvetica, sans-serif" color="#6d6e71" size="3">{{trans('mail.enter')}} <a href="<?=url('/admin/quotes/show/'.$currentOrder->id)?>"><font face="Arial, Helvetica, sans-serif" color="#6d6e71" size="3px">{{trans('mail.here')}}</font></a> {{trans('mail.message2')}}.</font></td>
	    </tr>  
	    <tr >
	    	<td width="50%" align="left" valign="bottom"><img src="<?= url('/images/newsletter/bg-footer.jpg') ?>" width="217" height="169" /></td>
	    	<td width="50%" align="right" valign="bottom"><img src="<?= url('/images/newsletter/logo_alfa.png') ?>" width="130" height="80" alt="ALFA Creamos Contigo" /></td>
	    </tr>  
	</table>
</body>
</html>
