<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>¡Bienvenido creador!</title>
</head>
<style>
.wrap-message { }
</style>
<body>
  <div class="wrap-message" style="width:623px; display:block; margin:0; background-color:#13abc0; text-align:center; color:#FFFFFF; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<div><img src="<?= url('/images/newsletter/top.jpg') ?>" width="623" height="195" alt=""></div>
    <div style=" padding-top: 40px; font-size:32px; text-transform:uppercase"><strong>¡Bienvenido creador! </strong></div>
    <div style="background-color:#13abc0; padding: 40px; font-size:21px;">
		Has quedado registrado en nuestra base de datos 
		y pronto estarás al tanto de novedades, descuentos, 
		nuevos productos y todo lo que necesitas saber para crear tus espacios con estilo propio.
	</div>
    <div><img src="<?= url('/images/newsletter/bottom.jpg') ?>" width="623" height="478" alt=""></div>
  </div>
</body>
</html>