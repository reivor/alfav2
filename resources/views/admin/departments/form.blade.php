@extends('admin.layouts.main')

@section('title', trans('cms.departments'))

@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-home"></i>
            {{ trans('cms.departments') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/departments') }}">
                    <i class="fa fa-home"></i>
                    {{ trans('menu.stores') }}
                </a>
            </li>
            <li class="active">
                {{ $action }}
            </li>
        </ol>
    </section>

    <section class="content">
        <form id="form-create-department" method="post" enctype="multipart/form-data" role="form">
            <?= csrf_field() ?>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('cms.departments') }}</h3>
                </div>
                <div class="box-body">
                    <div id="department_div">
                        <?php if( $departments->isEmpty() ): ?>
                        <div class="alert alert-warning"><?= trans('cms.no_departments_found') ?></div>
                        <?php else: ?>
                            <?php foreach($departments as $department): ?>
                                <div id="div_department_<?= $department->id ?>" class="form-group form-group-lg has-feedback">
                                    <label for="name_<?= $department->id ?>">{{ trans('cms.label_name') }} <a href="" class="delete" data-id="<?= $department->id ?>" data-target="#div_department_<?= $department->id ?>">{{ trans('cms.delete') }}</a></label>
                                    <input type="text" id="name_<?= $department->id ?>" name="department[<?= $department->id ?>]" class="form-control" value="{{ $department->name or old('name') }}" placeholder="{{ trans('cms.label_name') }}" maxlength="255" required data-bv-message="<?= trans('cms.form_required_field') ?>">
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <a href="" id="btn_add_row" class="btn btn-default"><i class="fa fa-plus-circle"></i></a>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
                        <a href="{{ url('admin/departments/index') }}" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </section>

@endsection

@push('templates')
<script id="tpl-department" type="text/x-handlebars-template">
    <div id="div_department_@{{department.id}}" class="form-group form-group-lg has-feedback">
        <label for="name_@{{department.id}}">{{ trans('cms.label_name') }} <a href="" class="delete" data-target="#div_department_@{{department.id}}">{{ trans('cms.delete') }}</a></label>
        <input type="text" id="name_@{{department.id}}" name="name[]" class="form-control" value="@{{department.name}}" placeholder="{{ trans('cms.label_name') }}" maxlength="255" required>
    </div>
</script>
@endpush

@push('scripts')
<script>
    $(function () {
        console.log('ready');
        $('#btn_add_row').click(function(e){
            e.preventDefault();
            var department = {id:new Date().getTime(), name:''};
            var template = Handlebars.compile( $('#tpl-department').html() );
            var html = template({ department: department });
            var $container = $('#department_div');
            $container.append( html );
            $('#form-create-department').bootstrapValidator('addField', $container.find('[name="name[]"]').last());
        });
        $(document).on('click','.delete',function(e){
            e.preventDefault();
            $.ajax({
                url: "/admin/departments/drop",
                method: 'POST',
                data: {id:$(this).data( "id" )}
            });
            var target = $($(this).data('target'));
            $('#form-create-department').bootstrapValidator('removeField', target.find('[name="name[]"]'));
            target.remove();
        });
        $('#form-create-department').bootstrapValidator({
            fields: {
                'name[]': {
                    validators: {
                        notEmpty: {
                            message: '<?= trans('cms.form_required_field') ?>'
                        }
                    }
                }
            }
        });
    });
</script>
@endpush