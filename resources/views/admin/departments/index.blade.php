@extends('admin.layouts.main')


@section('title', trans('cms.departments'))


@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-home"></i>
            {{ trans('cms.departments') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li class="active">{{ trans('menu.departments') }}</li>
        </ol>
    </section>

    <section class="content">

        <div class="row margin-bottom">
            <div class="col-xs-12">
                <a href="/admin/departments/edit" class="btn btn-warning">
                    <i class="fa fa-plus-circle"></i>
                    {{ trans('cms.edit_department') }}
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('cms.departments') }}</h3>
                    </div>
                    <div class="box-body">
                        <?php if( $departments->isEmpty() ): ?>
                        <div class="alert alert-warning"><?= trans('cms.no_departments_found') ?></div>
                        <?php else: ?>
                        <div class="row">
                            <div class="grid col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>{{ trans('cms.label_name') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($departments as $department): ?>
                                        <tr>
                                            <td><a href="<?= url('admin/cities/index/' . $department->id) ?>"> <?= $department->name ?> </a></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection