@extends('admin.layouts.main')

@section('title', trans('cms.users'))

@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-user"></i>
            {{ trans('cms.change_password') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/users') }}">
                    <i class="fa fa-users"></i>
                    {{ trans('cms.users') }}
                </a>
            </li>
            <li class="active">
                {{ trans('cms.change_password') }}
            </li>
        </ol>
    </section>

    <section class="content">

        <form id="form-create-password" method="post" enctype="multipart/form-data" role="form">
            <?= csrf_field() ?>
            <input type="hidden" name="id" value="{{ $user->id }}" />
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('cms.change_password') }}</h3>
                </div>
                <div class="box-body">
                    <div class="form-group form-group-lg">
                        <label for="name">{{ trans('cms.label_user_email') }}</label>
                        <div class="display-block">{{ $user->email }}</div>
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="password">{{ trans('cms.password') }}</label>
                        <input type="password" id="password" name="password" class="form-control" value="" placeholder="{{ trans('cms.password') }}">
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="repeatpassword">{{ trans('cms.repeat_password') }}</label>
                        <input type="password" id="repeatpassword" name="repeatpassword" class="form-control" value="" placeholder="{{ trans('cms.repeat_password') }}">
                    </div>
                </div>
                <div class="box-footer">
                    <div class="form-group form-group-lg">
                        <input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
                        <a href="{{ url('admin/profile') }}" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection


@push('scripts')
<script>
    $(function () {
        $('#form-create-password').bootstrapValidator({
            fields: {
                password: {
                    validators: {
                        notEmpty: { message: '<?= trans('cms.form_required_field') ?>' },
                        identical: {
                            field: 'repeatpassword',
                            message: '<?= trans('cms.invalid_password_identical') ?>'
                        },
                        stringLength: {
                            min: 6,
                            message: '<?= trans('cms.invalid_password_min') ?>'
                        }
                    }
                },
                repeatpassword: {
                    validators: {
                        notEmpty: { message: '<?= trans('cms.form_required_field') ?>' },
                        identical: {
                            field: 'password',
                            message: '<?= trans('cms.invalid_password_identical') ?>'
                        },
                        stringLength: {
                            min: 6,
                            message: '<?= trans('cms.invalid_password_min') ?>'
                        }
                    }
                }
            }
        });
    });
</script>
@endpush