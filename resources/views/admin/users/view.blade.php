@extends('admin.layouts.main')

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-users"></i>
		{{ trans('cms.users') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/users') }}">
				<i class="fa fa-users"></i>
				{{ trans('cms.users') }}
			</a>
		</li>
        <li class="active">
			{{ $user->name }}
		</li>
	</ol>
</section>

<section class="content">
	
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{ trans('cms.users') }}</h3>
			<div class="box-tools pull-right">
				<?php if($user->status != 'active'){?>
					<a href="{{ url("admin/users/activated/{$user->id}") }}" class="btn btn-box-tool"><i class="fa fa-check" aria-hidden="true"></i></a>
				<?php }else{ ?>
					<a href="{{ url("admin/users/desactivated/{$user->id}") }}" class="btn btn-box-tool"><i class="fa fa-ban" aria-hidden="true"></i></a>
				<?php } ?>
				<a href="{{ url("admin/users/edit/{$user->id}") }}" class="btn btn-box-tool">
					<i class="fa fa-edit"></i>
				</a>
				<a href="{{ url("admin/users/delete/{$user->id}") }}" class="btn btn-box-tool" onclick="confirm('{{ trans('cms.delete_user') }}');">
					<i class="fa fa-trash-o"></i>
				</a>
			</div>
		</div>
		<div class="box-body">
			<div class="col-xs-6 col-sm-4">
				<div class="box box-widget widget-user">
					@if($user->role == 'superadmin' | $user->role == 'admin')
					<div class="widget-user-header bg-aqua-active">
					@else 
					<div class="widget-user-header bg-orange-active">
					@endif
						<h3 class="widget-user-username">
							<?= $user->name ?>
						</h3>
						<h5 class="widget-user-desc"><?= $user->role ?></h5>
					</div>
					<div class="widget-user-image">
						<img class="img-circle" src="<?= $user->avatar ?>" alt="<?= $user->name ?>">
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-sm-6 text-center">
								&nbsp;
							</div>
							<div class="col-sm-6 text-center">
								&nbsp;
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-4">
				<dl class="dl-horizontal">
					<dt>{{ trans('cms.label_user_name') }}</dt>
					<dd>{{ $user->name }}</dd>
					<dt>{{ trans('cms.label_user_email') }}</dt>
					<dd>{{ $user->email }}</dd>
					<dt>{{ trans('cms.label_user_role') }}</dt>
					<dd>{{ $user->role }}</dd>
					<dt>{{ trans('cms.label_user_status') }}</dt>
					<?php if( $user->isActive() ): ?>
					<dd><span class="text-success">{{ $user->status }}</span></dd>
					<?php elseif( $user->isBlocked() ): ?>
					<dd><span class="text-danger">{{ $user->status }}</span></dd>
					<?php elseif( $user->isPending() ): ?>
					<dd><span class="text-muted">{{ $user->status }}</span></dd>
					<?php endif; ?>
					<dt>{{ trans('cms.label_user_member_since') }}</dt>
					<dd>{{ $user->created_at->format('d/m/Y') }}</dd>
              </dl>
			</div>
		</div>
	</div>

</section>

@endsection