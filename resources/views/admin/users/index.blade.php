@extends('admin.layouts.main')

@section('title', trans('cms.users'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-users"></i>
		{{ trans('cms.users') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">
			<i class="fa fa-users"></i>
			{{ trans('cms.users') }}
		</li>
	</ol>
</section>

<section class="content">
	
	<div class="row margin-bottom">
		<div class="col-xs-12">
			<a href="/admin/users/create" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_user') }}
			</a>
		</div>
	</div>

	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{ trans('cms.users') }}</h3>
		</div>
		<div class="box-body">
			<?php foreach($users as $user): ?>
			<div class="col-xs-6 col-sm-3">
				<a href="{{ url('admin/users/view/'.$user->id) }}">
					<div class="box box-widget widget-user">
						@if($user->role == 'superadmin' | $user->role == 'admin')
						<div class="widget-user-header bg-aqua-active">
						@else 
						<div class="widget-user-header bg-orange-active">
						@endif
							<h3 class="widget-user-username">
								<?= $user->name ?>
							</h3>
							<h5 class="widget-user-desc"><?= $user->role ?></h5>
						</div>
						<div class="widget-user-image">
							<img class="img-circle" src="<?= $user->avatar ?>" alt="<?= $user->name ?>">
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-sm-12 text-center">
									<span class="text-black"><?= trans('cms.label_user_role') ?>:</span>
									<span><?= $user->role ?></span>
								</div>
								<div class="col-sm-12 text-center">
									<span class="text-black"><?= trans('cms.label_user_status') ?>:</span>
									<?php if( $user->isActive() ): ?>
									<span class="text-success"><?= $user->status ?></span>
									<?php elseif( $user->isBlocked() ): ?>
									<span class="text-danger"><?= $user->status ?></span>
									<?php elseif( $user->isPending() ): ?>
									<span class="text-muted"><?= $user->status ?></span>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	
	{!! $users->links() !!}

</section>

@endsection