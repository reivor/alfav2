@extends('admin.layouts.main')

@section('title', trans('cms.users'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-users"></i>
		{{ trans('cms.users') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/users') }}">
				<i class="fa fa-users"></i>
				{{ trans('cms.users') }}
			</a>
		</li>
        <li class="active">
			{{ $action }}
		</li>
	</ol>
</section>

<section class="content">
	
	<form id="form-create-user" method="post" role="form">
		<?= csrf_field() ?>
		<input type="hidden" name="id" value="{{ $user->id }}" />
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('cms.users') }}</h3>
			</div>
			<div class="box-body">
				<div class="form-group form-group-lg has-feedback">
					<label for="name">{{ trans('cms.label_user_name') }}</label>
					<input type="text" id="name" name="name" class="form-control" value="{{ $user->name or old('name') }}" placeholder="{{ trans('cms.placeholder_user_name') }}">
				</div>
				<div class="form-group form-group-lg has-feedback">
					<label for="name">{{ trans('cms.label_user_email') }}</label>
					<input type="email" id="email" name="email" class="form-control" value="{{ $user->email or old('email') }}" placeholder="{{ trans('cms.placeholder_user_email') }}">
				</div>
				<div class="form-group form-group-lg has-feedback">
					<label for="role">{{ trans('cms.label_user_role') }}</label>
					<select id="role" name="role" class="form-control">
						<option value="<?= $user::ROLE_ADMIN ?>" <?= $user->role == $user::ROLE_ADMIN ? 'selected' : ""?>><?= $user::ROLE_ADMIN ?></option>
						<option value="<?= $user::ROLE_SUPERADMIN ?>" <?= $user->role == $user::ROLE_SUPERADMIN ? 'selected' : ""?>><?= $user::ROLE_SUPERADMIN ?></option>
						<option value="<?= $user::ROLE_SERVICIOCLIENTE?>" <?= $user->role == $user::ROLE_SERVICIOCLIENTE ? 'selected' : ""?>><?= $user::ROLE_SERVICIOCLIENTE ?></option>
					</select>
				</div>
			</div>
			<div class="box-footer">
				<div class="form-group form-group-lg">
					<input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
					<a href="{{ url('admin/users') }}" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
				</div>
			</div>
		</div>
	</form>
	
</section>

@endsection


@push('scripts')
<script>
$(function () {
	console.log('ready');
	
	$('#form-create-user').bootstrapValidator({
		fields: {
			name: {
				validators: {
					notEmpty: { 
						message: '<?= trans('cms.form_required_field') ?>' 
					},
					regexp: {
					  regexp: /^[a-zA-ZáéíóúÁÉÍÓÚ\s]+$/,
					  message: '<?= trans('cms.form_only_letters_field') ?>'
					}		
				}
			},
			email: {
				validators: {
					notEmpty: { message: '<?= trans('cms.form_required_field') ?>' },
					emailAddress: { message: '<?= trans('cms.form_invalid_email') ?>' }
				}
			}
		}
	});
});
</script>
@endpush