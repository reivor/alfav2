@extends('admin.layouts.main')

@section('title', trans('cms.users'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-user"></i>
		{{ trans('cms.my_profile') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/users') }}">
				<i class="fa fa-users"></i>
				{{ trans('cms.users') }}
			</a>
		</li>
        <li class="active">
			{{ trans('cms.my_profile') }}
		</li>
	</ol>
</section>

<section class="content">
	
	<form id="form-create-user" method="post" enctype="multipart/form-data" role="form">
		<?= csrf_field() ?>
		<input type="hidden" name="id" value="{{ $user->id }}" />
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('cms.my_profile') }}</h3>
			</div>
			<div class="box-body">
				<div class="form-group form-group-lg">
					<label for="name">{{ trans('cms.label_user_email') }}</label>
					<div class="display-block">{{ $user->email }}</div>
				</div>
				<div class="form-group form-group-lg has-feedback">
					<label for="name">{{ trans('cms.label_user_name') }}</label>
					<input type="text" id="name" name="name" class="form-control" value="{{ $user->name or old('name') }}" placeholder="{{ trans('cms.placeholder_user_name') }}">
				</div>
				<div class="form-group form-group-lg has-feedback">
					<label for="picture">{{ trans('cms.label_user_profile_picture') }}</label>
					<input type="file" id="picture" name="picture" />
				</div>
			</div>
			<div class="box-footer">
				<div class="form-group form-group-lg">
					<input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
					<a href="{{ url('admin/profile') }}" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
				</div>
			</div>
		</div>
	</form>
	
</section>

@endsection


@push('scripts')
<script>
$(function () {
	$('#form-create-user').bootstrapValidator({
		fields: {
			name: {
				validators: {
					notEmpty: { 
						message: '<?= trans('cms.form_required_field') ?>' 
					},
					regexp: {
					  regexp: /^[a-zA-ZáéíóúÁÉÍÓÚ\s]+$/,
					  message: '<?= trans('cms.form_only_letters_field') ?>'
					}
				}
			},
			picture: {
				validators: {
					file: {
						extension: 'jpeg,jpg,png',
						type: 'image/jpeg,image/png',
						maxSize: 2 * 1024 * 1024,   // 2048 * 1024
						message: '<?= trans('cms.form_invalid_photo') ?>'
					}
				}
			}
		}
	});
});
</script>
@endpush