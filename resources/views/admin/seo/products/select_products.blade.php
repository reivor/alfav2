@extends('admin.layouts.main')

@section('title', trans('cms.products'))

@section('content')

<section class="content-header">
	<h1>
        <i class="fa fa-file-text-o"></i>
        <span><?= trans('cms.site_maps_products') ?></span>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li>
			<a href="/admin/seo">
				<i class="fa fa-chrome"></i>
				{{ trans('menu.seo') }}
			</a>
        </li>
        <li class="active">{{ trans('cms.site_maps_products') }}</li>
	</ol>
</section>

<form id="form-sitemap-products" action="/admin/seo/products" method="post" role="form" enctype="multipart/form-data">
    <section class="content">
		<input type="hidden" id="selectedCategories" name="selectedCategories" value="">
		{{csrf_field()}}
		<div class="row">
			<div class="col-md-10">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Categorias</h3>
					</div>
					<div class="box-body">
						<div class="tree_categories">
							@foreach ($categories as  $categoryLv1)
							<ul>
								<?php $payload = ['root' => true, 'id' => $categoryLv1->id, 'supreme_parent' => -1]; ?>
								<li data-payload='<?= json_encode($payload) ?>'>
									<?= $categoryLv1->name ?>
									<?php if( isset($categoryLv1->categories) ) { ?>
										<ul>
											<?php foreach( $categoryLv1->categories as $categoryLv2 ): ?>
											<?php $payload = ['id' => $categoryLv2->id, 'supreme_parent' => $categoryLv1->id]; ?>
											<li data-jstree='{"selected":<?= in_array($categoryLv2->id, $selectedCategories) ? "true" : "false" ?>}' 
												data-payload='<?= json_encode($payload) ?>'>
												<?= $categoryLv2->name ?>
												<?php if( isset($categoryLv2->categories) ): ?>
												<ul>
													<?php foreach( $categoryLv2->categories as $categoryLv3 ): ?>
													<?php $payload = ['id' => $categoryLv3->id, 'supreme_parent' => $categoryLv1->id] ?>
													<li data-jstree='{"selected":<?= in_array($categoryLv3->id, $selectedCategories) ? "true" : "false" ?>}' 
														data-payload='<?= json_encode($payload) ?>'>
														<?= $categoryLv3->name ?>
														<?php if( isset($categoryLv3->categories) ): ?>
														<ul>
															<?php foreach( $categoryLv3->categories as $categoryLv4 ): ?>
															<?php $payload = ['id' => $categoryLv4->id, 'supreme_parent' => $categoryLv1->id]; ?>
															<li data-jstree='{"selected":<?= in_array($categoryLv4->id, $selectedCategories) ? "true" : "false" ?>}' 
																data-payload='<?= json_encode($payload) ?>'>
																<?= $categoryLv4->name ?>
															</li>
															<?php endforeach; ?>
														</ul>
														<?php endif; ?>
													<?php endforeach; ?>
												</ul>
												<?php endif; ?>
											</li>
											<?php endforeach; ?>
										</ul>
									<?php } ?>
								</li>
							</ul>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-4">
				<div class="form-group">
					<input type="button" id="sendCategories" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
				</div>
			</div>
		</div>
    </section>
</form>
@endsection

@push('plugins')
<link rel="stylesheet" href="/themes/admin/vendor/jstree/dist/themes/default/style.min.css" />
<script src="/themes/admin/vendor/jstree/dist/jstree.min.js"></script>
@endpush

@push('scripts')

<script>
$(function () {
	var counterSelectedNodes = {};

	$(".tree_categories").jstree({
		'core': {
			expand_selected_onload: true
		},
		"checkbox": {
			keep_selected_style: false,
			three_state: false
		},
		"plugins": ["checkbox"],
	}).bind("ready.jstree", function () {
		var selectedElmsAux = $(this).jstree("get_selected", true);
		var supremeParent;
		$.each(selectedElmsAux, function (i) {
			supremeParent = this.data.payload.supreme_parent;
			if (supremeParent !== -1) {
				if (typeof counterSelectedNodes[supremeParent] === 'undefined') {
					counterSelectedNodes[supremeParent] = parseInt(1);
				}
				else {
					counterSelectedNodes[supremeParent] = parseInt(counterSelectedNodes[supremeParent]) + parseInt(1);
				}
			}
		});
	});

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});

	$('#sendCategories').click(function (e) {
		var selectedElmsIds = [];

		var selectedElms = $('.tree_categories').jstree("get_selected", true);
		$.each(selectedElms, function (i) {
			selectedElmsIds.push(this.data.payload.id);
		});

		var selectedCategories = JSON.stringify(selectedElmsIds);
		$("#selectedCategories").val(selectedCategories);
		$("#form-sitemap-products").submit();
	});

});
</script>
@endpush
