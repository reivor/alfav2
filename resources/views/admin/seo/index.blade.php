@extends('admin.layouts.main')
@section('title', trans('menu.seo'))
@section('content')

  <section class="content-header">
    <h1>
      <i class="fa fa-chrome"></i>
        <span>{{trans('menu.seo')}}</span>
    </h1>
    <ol class="breadcrumb">
          <li>
        <a href="{{ route('admin/dashboard') }}">
          <i class="fa fa-dashboard"></i>
          {{ trans('menu.dashboard') }}
        </a>
      </li>
          <li class="active">{{ trans('menu.seo') }}</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">

      <div class="col-lg-3 col-xs-12">
        <a href="/admin/seo/robots/" class="small-box bg-aqua">
          <div class="inner text-center">
            <h4>
              <i class="fa fa-file-text-o"></i>
              {{ trans('cms.file_robots') }}
            </h4>
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-xs-12">
        <a href="/admin/seo/categories" class="small-box bg-green">
          <div class="inner text-center">
            <h4>
              <i class="fa fa-file-text-o"></i>
              {{ trans('cms.site_maps_categories') }}
            </h4>
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-xs-12">
        <a href="/admin/seo/products"  class="small-box bg-red">
          <div class="inner text-center">
            <h4>
              <i class="fa fa-file-text-o"></i>
              {{ trans('cms.site_maps_products') }}
            </h4>
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-xs-12">
        <a href="/admin/seo/select-page"  class="small-box bg-orange">
          <div class="inner text-center">
            <h4>
              <i class="fa fa-file-text-o"></i>
              {{ trans('cms.meta_pages') }}
            </h4>
          </div>
        </a>
      </div>

    </div>
  </section>

@endsection
