@extends('admin.layouts.main')
@section('title', trans('menu.seo'))

@section('content')

<section class="content-header">
    <h1>
		<i class="fa fa-chrome"></i>
        <span>{{trans('menu.seo')}}</span>
    </h1>
    <ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i>
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li class="active">{{ trans('cms.meta_pages') }}</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">
					Meta Data: <strong>{{$page}}</strong>
				</h3>
			</div>
			<form  method="post">
				<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
				<input type="hidden" name="page" value="{{$page}}">
				<div class="box-body">
					<div class="col-md-6">

						<div class="form-group">
							<label for="title">title</label>
							<input type="text" class="form-control" name="title" value="{{$title->value}}" >
						</div>
						<div class="form-group">
							<label for="keyword">keyword</label>
							<input type="text" class="form-control" name="keyword" value="{{$keyword->value}}" >
						</div>
						<div class="form-group">
							<label for="description">description</label>
							<input type="text" class="form-control" name="description" value="{{$description->value}}" >
						</div>

						<h2 class="page-header">Open Graph</h2>


						<div class="form-group">
							<label for="title_og">og:title</label>
							<input type="text" class="form-control" name="og[title]" value="{{$og_title->value}}" >
						</div>
						<div class="form-group">
							<label for="type">og:type</label>
							<input type="text" class="form-control" name="og[type]" value="{{$og_type->value}}" >
						</div>
						<div class="form-group">
							<label for="url_og">og:url</label>
							<input type="text" class="form-control" name="og[url]" value="{{$og_url->value}}">
						</div>
						<div class="form-group">
							<label for="image_og">url:image</label>
							<input type="text" class="form-control" name="og[image]" value="{{$og_image->value}}">
						</div>


					</div>
				</div>
				<div class="box-footer">
					<button class="btn btn-primary" type="submit">{{trans('menu.save')}}</button>
				</div>
			</form>
		</div>
	</div>
</section>

@endsection
