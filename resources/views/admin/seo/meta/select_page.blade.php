@extends('admin.layouts.main')

@section('title', trans('menu.meta'))

  @section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-chrome"></i>
          <span>{{trans('menu.meta')}}</span>
      </h1>
      <ol class="breadcrumb">
            <li>
          <a href="{{ route('admin/dashboard') }}">
            <i class="fa fa-dashboard"></i>
            {{ trans('menu.dashboard') }}
          </a>
        </li>
            <li class="active">{{ trans('menu.meta') }}</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="box box-solid">
          <div class="box-header with-border">
          <h3 class="box-title">Pages</h3>
          </div>
          <div class="box-body">
            <ul>
              <li><a href="{{url('admin/seo/meta-tag/home')}}">Home</a></li>
              <li><a href="{{url('admin/seo/meta-tag/financiamiento')}}">Financiamiento</a></li>
              <li><a href="{{url('admin/seo/meta-tag/diseno')}}">Diseño</a></li>
              <li><a href="{{url('admin/seo/meta-tag/instalacion')}}">Instalación</a></li>
              <li><a href="{{url('admin/seo/meta-tag/certificacion')}}">Certificación</a></li>
              <li><a href="{{url('admin/seo/meta-tag/catalogo')}}">Catálogos</a></li>
              <li><a href="{{url('admin/seo/meta-tag/ambiente')}}">Alfa Ambiente</a></li>
              <li><a href="{{url('admin/seo/meta-tag/aboutus')}}">Sobre Nosotros</a></li>
              <li><a href="{{url('admin/seo/meta-tag/workus')}}">Trabaja con Nosotros</a></li>
            </ul>
          </div>
        </div>
      </div>

    </section>


  @endsection
