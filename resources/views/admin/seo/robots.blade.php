@extends('admin.layouts.main')
@section('title', trans('menu.file_robots'))

@section('content')

<section class="content-header">
	<h1>
        <i class="fa fa-file-text-o"></i>
        <span><?= trans('menu.file_robots') ?></span>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li>
			<a href="/admin/seo">
				<i class="fa fa-chrome"></i>
				{{ trans('menu.seo') }}
			</a>
        </li>
        <li class="active"><?= trans('menu.file_robots') ?></li>
	</ol>
</section>

<section class="content">
	<div class="row">
        <div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= trans('menu.file_robots') ?></h3>
			</div>
			<form id="form_file_robots" method="post">
				<div class="box-body">

					<?= csrf_field() ?>

					<div class="form-group">
						<label for="file_robots">Contenido</label>
						<textarea name="file_robots" rows="8" class="form-control"><?= $content ?></textarea>
					</div>

				</div>
				<div class="box-footer">
					<button class="btn btn-success" type="submit">Guardar</button>
				</div>
			</form>
        </div>
	</div>
</section>
@endsection
