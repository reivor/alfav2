<header class="main-header">
	
	<a href="/admin/dashboard" class="logo">
		<span class="logo-mini"><b>CMS</b></span>
		<span class="logo-lg"><b>CMS</b> Alfa</span>
	</a>
	
	<nav class="navbar navbar-static-top">
		
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<?php
						switch ($storeLang) {
							case 'es':
								echo "ES";
								break;
							case 'en':
								echo "EN";
								break;
						}
						?>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
						<li><a href="/admin/action/lang/es">ES</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="/admin/action/lang/en">EN</a></li>
					</ul>
				</li>
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ $sessionUser->avatar }}" class="user-image" alt="{{ $sessionUser->name }}">
						<span class="hidden-xs">{{ $sessionUser->name }}</span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="{{ $sessionUser->avatar }}" class="img-circle" alt="{{ $sessionUser->name }}">
							<p>
								{{ $sessionUser->name }} - {{ $sessionUser->role }}
								<small>{{ trans('cms.member_since', ['date' => $sessionUser->created_at->formatLocalized('%B %Y')]) }}</small>
							</p>
						</li>
						<!-- Menu Body -->
						<li class="user-body">
							<div class="row">
								<div class="col-xs-4 text-center">
									<a href="#">Followers</a>
								</div>
								<div class="col-xs-4 text-center">
									<a href="#">Sales</a>
								</div>
								<div class="col-xs-4 text-center">
									<a href="#">Friends</a>
								</div>
							</div>
							<!-- /.row -->
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="{{ url('admin/profile') }}" class="btn btn-default btn-flat">{{ trans('cms.profile') }}</a>
							</div>
							<div class="pull-right">
								<a href="{{ route('auth/logout') }}" class="btn btn-default btn-flat">{{ trans('cms.logout') }}</a>
							</div>
						</li>
					</ul>
				</li>
				<!-- Control Sidebar Toggle Button -->
				<li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
				</li>
			</ul>
		</div>
	</nav>
</header>