<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> {{ env('APP_VERSION', '1') }}
	</div>
	<strong>Copyright &copy; 2016 <a href="http://bybeeconcept.com" target="blank">Bee Concept Solutions</a>.</strong>
	All rights reserved.
</footer>