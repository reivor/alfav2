@if (session('success'))
<div class="alert alert-success alert-dismissible site-alert">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<p>{!! session('success') !!}</p>
</div>
@endif

@if (session('error'))
<div class="alert alert-danger alert-dismissible site-alert">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<p>{!! session('error') !!}</p>
</div>
@endif