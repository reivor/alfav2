<aside class="main-sidebar">

	<section class="sidebar">

		<!-- user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{ $sessionUser->avatar }}" class="img-circle" alt="{{ $sessionUser->name }} foto">
			</div>
			<div class="pull-left info">
				<p>{{ $sessionUser->name }}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>

		<!-- menu -->
		<ul class="sidebar-menu">

			<li class="dropdown">
				<a class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php
					switch ($storeCountry) {
						case 'co':
						echo "<img src='/images/countries/colombia.png' width='20'> Colombia";
						break;
						case 'ec':
						echo "<img src='/images/countries/ecuador.png' width='20'> Ecuador";
						break;
						case 'us':
						echo "<img src='/images/countries/usa.png' width='20'> USA";
						break;
						default:
						echo trans('menu.countries');
					}
					?>
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<li><a href="/admin/action/country/co">Colombia</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="/admin/action/country/ec">Ecuador</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="/admin/action/country/us">USA</a></li>
				</ul>
			</li>

			<!-- CATALOG -->
			<li class="header">{{ trans('menu.title_catalog') }}</li>
			<li>
				<a href="/admin/quotes">
					<i class="fa fa-shopping-cart"></i>
					<span>{{ trans('cms.quotes') }}</span>
				</a>
			</li>

		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
