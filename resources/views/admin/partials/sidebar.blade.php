<aside class="main-sidebar">

	<section class="sidebar">

		<!-- user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{ $sessionUser->avatar }}" class="img-circle" alt="{{ $sessionUser->name }} foto">
			</div>
			<div class="pull-left info">
				<p>{{ $sessionUser->name }}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>

		<!-- menu -->
		<ul class="sidebar-menu">

			<li class="dropdown">
				<a class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php
					switch ($storeCountry) {
						case 'co':
						echo "<img src='/images/countries/colombia.png' width='20'> Colombia";
						break;
						case 'ec':
						echo "<img src='/images/countries/ecuador.png' width='20'> Ecuador";
						break;
						case 'us':
						echo "<img src='/images/countries/usa.png' width='20'> USA";
						break;
						default:
						echo trans('menu.countries');
					}
					?>
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<li><a href="/admin/action/country/co">Colombia</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="/admin/action/country/ec">Ecuador</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="/admin/action/country/us">USA</a></li>
				</ul>
			</li>

			<!-- CATALOG -->
			<li class="header">{{ trans('menu.title_catalog') }}</li>
			<li>
				<a href="{{ route('admin/dashboard') }}">
					<i class="fa fa-dashboard"></i>
					<span>{{ trans('menu.dashboard') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/catalog/products">
					<i class="fa fa-cubes"></i>
					<span>{{ trans('menu.products') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/catalog/categories">
					<i class="fa fa-tags"></i>
					<span>{{ trans('menu.categories') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/catalog/stores">
					<i class="fa fa-home"></i>
					<span>{{ trans('menu.stores') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/customers">
					<i class="fa fa-user"></i>
					<span>{{ trans('menu.customers') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/quotes">
					<i class="fa fa-shopping-cart"></i>
					<span>{{ trans('cms.quotes') }}</span>
				</a>
			</li>
			<!-- APPEARANCE -->
			<li class="header">{{ trans('menu.title_appearance') }}</li>
			<li>
				<a href="/admin/appearance/menu">
					<span>{{ trans('menu.menu_categories') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/appearance/carrusel/">
					<span>{{ trans('menu.slider_home') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/appearance/banners_home/">
					<span>{{ trans('menu.banners_home') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/appearance/banners_site/">
					<span>{{ trans('menu.banners_site') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/appearance/footer">
					<span>{{ trans('menu.footer_home') }}</span>
				</a>
			</li>

			<!-- BLOG -->
			<li class="header">{{ trans('menu.blog') }}</li>
			<li>
				<a href="/admin/blog/articles">
					<span>{{ trans('menu.blog_articles') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/blog/categories">
					<span>{{ trans('menu.blog_categories') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/blog/comments">
					<span>{{ trans('menu.blog_comments') }}</span>
				</a>
			</li>



	<!-- PAGE rb-->
			<li class="header">{{ trans('menu.pages') }}</li>
			<li>
				<a href="/admin/page/pages">
					<span>{{ trans('menu.pages') }}</span>
				</a>
			</li>
			<!---
			<li>
				<a href="/admin/blog/categories">
					<span>{{ trans('menu.blog_categories') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/blog/comments">
					<span>{{ trans('menu.blog_comments') }}</span>
				</a>
			</li>
--->

			<!-- SYSTEM -->
			<li class="header">{{ trans('menu.title_system') }}</li>
			<li>
				<a href="/admin/settings/index">
					<i class="fa fa-cog"></i>
					<span>{{ trans('menu.settings') }}</span>
				</a>
			</li>
			<li>
				<a href="/admin/seo">
					<i class="fa fa-chrome"></i>
						<span>{{trans('menu.seo')}}</span>

				</a>
			</li>
			<li>
				<a href="/admin/users">
					<i class="fa fa-users"></i>
					<span>{{ trans('menu.admins') }}</span>
				</a>
			</li>
			<li>
				<a href="{{ route('auth/logout') }}">
					<i class="fa fa-power-off"></i>
					<span>{{ trans('menu.logout') }}</span>
				</a>
			</li>


		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
