@extends('admin.layouts.main')

@section('title', trans('cms.cities'))

@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-home"></i>
            {{ trans('cms.cities') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/cities') }}">
                    <i class="fa fa-home"></i>
                    {{ trans('menu.stores') }}
                </a>
            </li>
            <li class="active">
                {{ $action }}
            </li>
        </ol>
    </section>

    <section class="content">
        <form id="form-create-city" method="post" enctype="multipart/form-data" role="form">
            <?= csrf_field() ?>
            <input type="hidden" name="department_id" id="department_id" value="{{ $department_id }}" />
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('cms.cities') }}</h3>
                </div>
                <div class="box-body">
                    <div id="city_div">
                        <?php if( $cities->isEmpty() ): ?>
                        <div class="alert alert-warning"><?= trans('cms.no_cities_found') ?></div>
                        <?php else: ?>
                        <?php foreach($cities as $city): ?>
                        <div id="div_city_<?= $city->id ?>" class="form-group form-group-lg has-feedback">
                            <div class="col-sm-4">
                                <label for="name_<?= $city->id ?>">{{ trans('cms.label_name') }} <a href="" class="delete" data-id="<?= $city->id ?>" data-target="#div_city_<?= $city->id ?>">{{ trans('cms.delete') }}</a></label>
                                <input type="text" id="name_<?= $city->id ?>" name="city[<?= $city->id ?>][name]" class="form-control" value="{{ $city->name or old('name') }}" placeholder="{{ trans('cms.label_name') }}" maxlength="255" required data-bv-message="<?= trans('cms.form_required_field') ?>">
                            </div>
                            <div class="col-sm-4">
                                <label for="lat">latitud</label>
                                <input type="number" id="lat" name="city[<?= $city->id ?>][lat]" class="form-control" step="0.0001" value="{{$city->lat}}" placeholder="Latitud">
                            </div>
                            <div class="col-sm-4">
                                <label for="lon">longitud</label>
                                <input type="number" id="lon" name="city[<?= $city->id ?>][lon]" class="form-control" step="0.0001" value="{{$city->lon}}" placeholder="Longitud">
                            </div>

                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <a href="" id="btn_add_row" class="btn btn-default"><i class="fa fa-plus-circle"></i></a>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
                        <a href="{{ url('admin/cities/index/'.$department_id) }}" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </section>

@endsection

@push('templates')
<script id="tpl-city" type="text/x-handlebars-template">
    <div id="div_city_@{{city.id}}" class="form-group form-group-lg has-feedback">
        <div class="col-sm-4">
            <label for="name_@{{city.id}}">{{ trans('cms.label_name') }} <a href="" class="delete" data-id="@{{city.id}}" data-target="#div_city_@{{city.id}}">{{ trans('cms.delete') }}</a></label>
            <input type="text" id="name_@{{city.id}}" name="citynew[@{{city.id}}][name]" class="form-control" value="@{{city.name}}" placeholder="{{ trans('cms.label_name') }}" maxlength="255" required>
        </div>
        <div class="col-sm-4">
            <label for="lat_@{{city.id}}">latitud</label>
            <input type="number" id="lat_@{{city.id}}" name="citynew[@{{city.id}}][lat]" class="form-control" step="0.0001">
        </div>
        <div class="col-sm-4">
            <label for="lon_@{{city.id}}">latitud</label>
            <input type="number" id="lon_@{{city.id}}" name="citynew[@{{city.id}}][lon]" class="form-control" step="0.0001">
        </div>

    </div>
</script>
@endpush

@push('scripts')
<script>
    $(function () {
        console.log('ready');
        $('#btn_add_row').click(function(e){
            e.preventDefault();
            var city = {id:new Date().getTime(), name:''};
            var template = Handlebars.compile( $('#tpl-city').html() );
            var html = template({ city: city });
            var $container = $('#city_div');
            $container.append( html );
            $('#form-create-city').bootstrapValidator('addField', $container.find('[name="name[]"]').last());
        });
        $(document).on('click','.delete',function(e){
            e.preventDefault();
            $.ajax({
                url: "/admin/cities/drop",
                method: 'POST',
                data: {id:$(this).data( "id" )}
            });
            var target = $($(this).data('target'));
            $('#form-create-city').bootstrapValidator('removeField', target.find('[name="name[]"]'));
            target.remove();
        });
        $('#form-create-city').bootstrapValidator({
            fields: {
                'name[]': {
                    validators: {
                        notEmpty: {
                            message: '<?= trans('cms.form_required_field') ?>'
                        }
                    }
                }
            }
        });
    });
</script>
@endpush
