@extends('admin.layouts.main')


@section('title', trans('cms.cities'))


@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-home"></i>
            {{ trans('cms.cities') }}
            <small><?= $department->name ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/departments') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.departments') }}
                </a>
            </li>
            <li class="active">{{ trans('menu.cities') }}</li>
        </ol>
    </section>

    <section class="content">

        <div class="row margin-bottom">
            <div class="col-xs-12">
                <a href="/admin/cities/edit/<?= $department_id ?>" class="btn btn-warning">
                    <i class="fa fa-plus-circle"></i>
                    {{ trans('cms.edit_city') }}
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('cms.cities') }}</h3>
                    </div>
                    <div class="box-body">
                        <?php if( $cities->isEmpty() ): ?>
                        <div class="alert alert-warning"><?= trans('cms.no_cities_found') ?></div>
                        <?php else: ?>
                        <div class="row">
                            <div class="grid col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>{{ trans('cms.label_name') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($cities as $city): ?>
                                        <tr>
                                            <td><?= $city->name ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection