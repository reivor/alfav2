@extends('admin.layouts.main')
@section('title', trans('cms.article'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('menu.blog_articles') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.blog') }}</li>
	</ol>
</section>

<section class="content">

	<div class="row margin-bottom">
		<div class="col-lg-12">
			<a href="/admin/blog/article" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_article') }}
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<?= csrf_field() ?>
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.article_list') }}</h3>
				</div>
				<div class="box-body">
					<div class="row header-titles hidden-sm">
						<div class="col-md-3">
							<span>{{ trans('cms.image_header_article') }}</span>
						</div>
						<div class="col-md-4">
							<span>{{ trans('cms.title_article') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.date_publishing') }}</span>
						</div>
						<div class="col-md-1">
							<span>{{ trans('cms.product_status') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.label_actions') }}</span>
						</div>
					</div>

					<?php foreach ($articles as $article) { ?>
					<div class="row margin-bottom">
						<div class="col-md-3">
							<img id="header" src="<?=$article->image_header?>" width="150"/>
						</div>
						<div class="col-md-4">
							<?= $article->title_plain ?>
						</div>
						<div class="col-md-2">
							<?php echo($article->date_publishing==NULL) ? "" :$article->date_publishing->format('d/m/Y')?>
						</div>
						
						<div class="col-md-1">
							<span class="label <?php echo ($article->status==$article::STATUS_ACTIVE) ? 'label-success' : 'label-default'?>"><?= $article->status ?></span>					
						</div>
						<div class="col-md-2">
							<a href="/admin/blog/article/<?=$article->id?>" class="blog-actions">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</a>
							<a href="/admin/blog/points/<?=$article->id?>" class="blog-actions">
								<i class="fa fa-crosshairs" aria-hidden="true"></i>
							</a>
							<a href class="delete-article" rel="<?= $article->id ?>" data-sure="<?= trans('cms.confirm_delete_carrusel') ?>">
								<i class="fa fa-trash" aria-hidden="true" style="color:red"></i>
							</a>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('scripts')
<script>
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});
	
	$(".delete-article").click(function(e) {
		e.preventDefault();
		var index=$(this).attr('rel');
		
		bootbox.confirm({
			message: $(this).data('sure'),
			buttons: {
				'confirm': {
					label: '<?=trans('cms.btn_accept') ?>',
					className: 'btn-default'
				},
				'cancel': {
					label: '<?=trans('cms.btn_cancel') ?>',
					className: 'btn-danger'
				}
			},
			callback: function(result) {
				if (result) {
					var params= {
						"id" : index
					};
					$.post( "/admin/blog/delete-article", params, function(data) {
						window.location.href = "/admin/blog/articles";
					});
				}
			}
		});
	});
		
</script>
@endpush