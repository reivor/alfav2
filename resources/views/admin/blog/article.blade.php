@extends('admin.layouts.main')
@section('title', trans('cms.article'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('cms.article') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/blog/articles">
				<i class="fa fa-tags"></i> 
				{{ trans('menu.blog') }}
			</a>
		</li>
        <li class="active">{{ trans('cms.article') }}</li>
	</ol>
</section>

<section class="content">
	<form id="form-create-article" action="/admin/blog/save-article" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
		<?= csrf_field() ?>
		<input type="hidden" id="id" name="id" value="<?=$article->id?>" />
		<div class="row">
			<div class="col-md-7">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.content_article') }}</h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="title">{{ trans('cms.title_article') }}</label>
							<input type="hidden" id="title" name="title" value="" />
							<div id="title-text"></div>
							<span id="error-title" style="margin-top: -12px;" class="help-block with-errors hidden">{{trans('cms.form_required_field')}}</span>
						</div>
						<div class="form-group">
							<label for="body">{{ trans('cms.body_article') }}</label>
							<input type="hidden" id="body" name="body" value="" />
							<div id="body-text"></div>
						</div>
						<div class="form-group">
							<label>{{ trans('cms.label_category_tags') }}</label><br>
							<input id="tags" name="tags[]" type="text" class="form-control" value="<?=$article->tags?>" data-role="tagsinput" >
						</div>
						<div class="form-group">
							<input type="submit" id="send-article" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
							<a href="/admin/blog/articles" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.general_article') }}</h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="image_header">{{ trans('cms.image_header_article') }}</label><p> ({{trans('cms.suggested_size')}}:1900x838)</p>
							<img id="uploadHeader" src="<?=$article->image_header?>" width="150"/>
							<input type="file" id="image_header" name="image_header" class=""  placeholder=""/>
						</div>
						<div class="form-group">
							<label for="image_default">{{ trans('cms.image_default_article') }}</label><br>
							<img id="uploadDefault" src="<?=$article->image_default?>" width="150"/>
							<input type="file" id="image_default" name="image_default" class=""  placeholder=""/>
						</div>
						<div class="form-group">
							<label for="image_related">{{ trans('cms.image_related_article') }}</label><p> ({{trans('cms.suggested_size')}}:110x110)</p>
							<img id="uploadRelated" src="<?=$article->image_related?>" width="100"/>
							<input type="file" id="image_related" name="image_related" class=""  placeholder=""/>
						</div>
						<div class="form-group">
							<label for="image_home">{{ trans('cms.image_home_article') }}</label><p> ({{trans('cms.suggested_size')}}:344x185)</p>
							<img id="uploadHome" src="<?=$article->image_home?>" width="150"/>
							<input type="file" id="image_home" name="image_home" class=""  placeholder=""/>
						</div>
						<div class="form-group">
							<label>{{ trans('cms.category') }}</label>
							<select class="form-control" name="category" id="category">
								<option value="0">{{trans('cms.select')}}</option>
								<?php foreach ($blogCategories as $keyCategory => $blogCategory) { ?>
									<option <?php echo ($article->blog_category_id==$blogCategory->id) ? 'selected' : '';?> value="<?=$blogCategory->id?>"><?=$blogCategory->name?></option>
								<?php } ?>
							</select>
							<span id="error-category" class="help-block with-errors hidden">{{trans('cms.form_required_field')}}</span>
						</div>
						<div class="form-group">
							<label>{{ trans('cms.date_publishing') }}</label>
							<input class="form-control" type="text" id="date-publishing" name="date-publishing" value="<?php echo($article->date_publishing==NULL) ? "" :$article->date_publishing->format('d/m/Y')?>"/>
							<span id="error-date" class="help-block with-errors hidden">{{trans('cms.form_required_field')}}</span>
						</div>
						<div class="form-group">
							<label>{{ trans('cms.label_carrusel_status') }}</label>
							<select class="form-control" name="status" id="status">
								<?php foreach ($status as $key=> $currentStatus) { ?>
									<option <?php echo ($article->status==$currentStatus) ? 'selected' : '';?> value="<?=$currentStatus?>"><?=$currentStatus?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>

@endsection

@push('plugins')
<link rel="stylesheet" href="/themes/admin/vendor/summernote/dist/summernote.css" />
<script src="/themes/admin/vendor/summernote/dist/summernote.min.js"></script>
<script src="/themes/admin/libs/summernote-image-attributes.js"></script>
@endpush

@push('scripts')
<script>

$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('[name="_token"]').val()
	}
});

function quitAlerts(){
	$(".has-error" ).each(function() {
		$(this).removeClass('has-error');
	});
	
	$(".with-errors" ).each(function() {
		$(this).addClass('hidden');
	});
}

$('#date-publishing').datepicker({
    format: 'dd/mm/yyyy'
});

$('#title-text').summernote({
	height: 50,
	minHeight: null,
	maxHeight: null,
	toolbar: [
		['style', ['bold', 'italic']],
		['view', ['codeview']]
	]
});

var tags = <?= json_encode($article->tags) ?>;

$('#tags').tagsinput({
	typeahead: {
		source: tags
	}
});

$('#body-text').summernote({
	height: 300,
	minHeight: null,
	maxHeight: null,
	toolbar: [
		['style', ['bold', 'italic', 'underline', 'clear', 'strikethrough']],
		['color', ['color']],
		['para', ['ul', 'ol', 'paragraph']],
		['media', ['picture', 'video', 'link', 'hr']],
		['view', ['codeview']]
	],
	popover: {
		image: [
			['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
			['float', ['floatLeft', 'floatRight', 'floatNone']],
			['custom', ['imageAttributes', 'imageShape']],
			['remove', ['removeMedia']]
		]
	}
	/*,lang: 'es-ES'*/
});

var bodyText='<?= str_replace("\r\n", "\\\n", $article->body) ?>';
$('#body-text').summernote('code', bodyText);

var titleText='<?=$article->title?>';
$('#title-text').summernote('code', titleText);

$("#header").change(function() {
	if($(this).val()!=""){
		if ($(this)[0].files && $(this)[0].files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#uploadHeader').attr('src', e.target.result);
			}
			reader.readAsDataURL($(this)[0].files[0]);
		}
	}
});

$("#image_header").change(function() {
	if($(this).val()!=""){
		if ($(this)[0].files && $(this)[0].files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#uploadHeader').attr('src', e.target.result);
			}
			reader.readAsDataURL($(this)[0].files[0]);
		}
	}
});

$("#image_default").change(function() {
	if($(this).val()!=""){
		if ($(this)[0].files && $(this)[0].files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#uploadDefault').attr('src', e.target.result);
			}
			reader.readAsDataURL($(this)[0].files[0]);
		}
	}
});

$("#image_related").change(function() {
	if($(this).val()!=""){
		if ($(this)[0].files && $(this)[0].files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#uploadRelated').attr('src', e.target.result);
			}
			reader.readAsDataURL($(this)[0].files[0]);
		}
	}
});

$("#image_home").change(function() {
	if($(this).val()!=""){
		if ($(this)[0].files && $(this)[0].files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#uploadHome').attr('src', e.target.result);
			}
			reader.readAsDataURL($(this)[0].files[0]);
		}
	}
});
	
$('#form-create-article').submit(function(){
	var success=true;
	quitAlerts();
	var category = parseInt($('#category').val());
	var datePublishing = $('#date-publishing').val();
	
	if($('#title-text').summernote('isEmpty')){
		$('#title').parent().addClass("has-error");
		$('#error-title').removeClass('hidden');
		success=false;
	}else{
		var title = $('#title-text').summernote('code');
		$('#title').val(title);
	}
	
	if(category==0){
		$('#category').parent().addClass("has-error");
		$('#error-category').removeClass('hidden');
		success=false;
	}
	
	if(datePublishing==""){
		$('#date-publishing').parent().addClass("has-error");
		$('#error-date').removeClass('hidden');
		success=false;
	}
	
	var body = $('#body-text').summernote('code');
	$('#body').val(body);
	
	
	return success;
});


</script>
@endpush