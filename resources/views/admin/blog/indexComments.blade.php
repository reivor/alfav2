@extends('admin.layouts.main')
@section('title', trans('cms.article'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('menu.blog_comments') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.blog_comments') }}</li>
	</ol>
</section>

<section class="content">
	<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display:none; margin-top: 10px;">
		<img src="/images/loader.gif" style="width: 20px; height: 20px;"/>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<?= csrf_field() ?>
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.recent_comments') }}</h3>
				</div>
				<div class="box-body">
					<div class="row header-titles hidden-sm">
						<div class="col-md-5">
							<span>{{ trans('cms.comment') }}</span>
						</div>
						<div class="col-md-3">
							<span>{{ trans('cms.article') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.label_user') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.label_actions') }}</span>
						</div>
					</div>

					<?php foreach ($recentComments as $comment) { ?>
					<div class="row margin-bottom">
						<div class="col-md-5">
							<span><?= $comment->comment?></span>
						</div>
						<div class="col-md-3">
							<?= $comment->article->title_plain ?>
						</div>
						<div class="col-md-2">
							<?= $comment->customer->name." ".$comment->customer->lastname ?>
						</div>
						<div class="col-md-2">
							<a href="#" id="reject-<?=$comment->id?>" rel="<?=$comment->id?>" class="reject-comment btn-danger btn btn-xs <?php echo($comment->status==$comment::STATUS_ACTIVE) ? '': 'hidden'?>">
								{{trans('cms.reject_comment')}}
							</a>
							<a href="#" id="accept-<?=$comment->id?>" rel="<?=$comment->id?>" class="accept-comment btn-success btn btn-xs <?php echo($comment->status==$comment::STATUS_ACTIVE) ? 'hidden': ''?>">
								{{trans('cms.accept_comment')}}
							</a>
							<span id="loader-<?=$comment->id?>"></span>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<?= csrf_field() ?>
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.article_list') }}</h3>
				</div>
				<div class="box-body">
					<div class="row header-titles hidden-sm">
						<div class="col-md-1">
							<span>Id</span>
						</div>
						<div class="col-md-5">
							<span>{{ trans('cms.title_article') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.product_status') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.label_actions') }}</span>
						</div>
					</div>

					<?php foreach ($articles as $article) { ?>
					<div class="row margin-bottom">
						<div class="col-md-1">
							<span><?= $article->id?></span>
						</div>
						<div class="col-md-5">
							<?= $article->title_plain ?>
						</div>
						<div class="col-md-2">
							<span class="label <?php echo ($article->status==$article::STATUS_ACTIVE) ? 'label-success' : 'label-default'?>"><?= $article->status ?></span>					
						</div>
						<div class="col-md-2">
							<a href="/admin/blog/article-comments/<?=$article->id?>" style="padding-right:10px;">
								<i class="fa fa-comments"></i>
							</a>
						</div>
					</div>
					<?php }?>
					{{ $articles->links() }}
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('scripts')
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});
	
	var stateActive = "<?=$stateActive?>";
	var stateRejected = "<?=$stateRejected?>";
	var loader = $("#mini-loader").html();
	
	function changeStatusComment(id, status){
		var params= {
			"id" : id,
			"status": status
		};
		
		$.post( "/admin/blog/change-status-comment", params, function(data) {
			$("#loader-"+id).empty();
			if(data===stateActive){
				$("#accept-"+id).addClass('hidden');
				$("#reject-"+id).removeClass('hidden');	
				$("#status-"+id).text(data);
				$("#status-"+id).removeClass('text-danger');
				$("#status-"+id).addClass('text-success');
			}else{
				$("#reject-"+id).addClass('hidden');
				$("#accept-"+id).removeClass('hidden');
				$("#status-"+id).text(data);
				$("#status-"+id).removeClass('text-success');
				$("#status-"+id).addClass('text-danger');
			}
		});
	}

	$(".reject-comment").click(function(e) {
		e.preventDefault();
		var id = $(this).attr('rel');
		$("#loader-"+id).html(loader);
		changeStatusComment(id, stateRejected);
	});
	
	$(".accept-comment").click(function(e) {
		e.preventDefault();
		var id = $(this).attr('rel');
		$("#loader-"+id).html(loader);
		changeStatusComment(id, stateActive);
	});

</script>
@endpush