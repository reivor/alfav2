@extends('admin.layouts.main')
@section('title', trans('cms.blog_categories'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('cms.blog_categories') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/catalog/categories">
				<i class="fa fa-tags"></i> 
				{{ trans('menu.blog') }}
			</a>
		</li>
        <li class="active">{{ trans('cms.blog_categories') }}</li>
	</ol>
</section>

<section class="content">
	<form id="form-create-category" action="/admin/blog/save-category" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
		<?= csrf_field() ?>
		<input type="hidden" id="id" name="id" value="<?=$category->id?>" />
		<div class="col-md-6">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="title">{{ trans('cms.label_name') }}</label>
						<input class="form-control" type="text" id="name" name="name" value="<?= $category->name?>" />
						<span id="error-name" class="help-block with-errors hidden">{{trans('cms.form_required_field')}}</span>
					</div>
					<div class="form-group">
						<label for="color_code">{{ trans('cms.product_color_code') }}</label>
						<div class="form-group input-group colorpicker-component color-code">
							<input type="text" id="color" name="color" class="form-control" value="<?= $category->color?>" placeholder="" maxlength="100">
							<span class="input-group-addon"><i></i></span>
						</div>
						<div id="error-color" class="help-block with-errors hidden" style="margin-top:-10px">{{trans('cms.form_required_field')}}</div>
					</div>
					<div class="form-group">
						<label>{{ trans('cms.label_carrusel_status') }}</label>
						<select class="form-control" name="status" id="status">
							<?php foreach ($status as $key=> $currentStatus) { ?>
								<option <?php echo ($category->status==$currentStatus) ? 'selected' : '';?> value="<?=$currentStatus?>"><?=$currentStatus?></option>
							<?php } ?>
						</select>
						<span id="error-status" class="help-block with-errors hidden">{{trans('cms.form_required_field')}}</span>
					</div>
					
					<div class="form-group">
						<input type="submit" id="send-category" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
						<a href="/admin/blog/categories" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>

@endsection

@push('plugins')
<link rel="stylesheet" href="/themes/admin/vendor/summernote/dist/summernote.css" />
<script src="/themes/admin/vendor/summernote/dist/summernote.min.js"></script>
@endpush

@push('scripts')
<script>
	
$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('[name="_token"]').val()
	}
});

function quitAlerts(){
	$(".has-error" ).each(function() {
		$(this).removeClass('has-error');
	});
	
	$(".with-errors" ).each(function() {
		$(this).addClass('hidden');
	});
}

$('.color-code').colorpicker({
	format: 'hex'
});

$('#form-create-category').submit(function(){
	var success=true;
	quitAlerts();
	var name = $('#name').val();
	var color = $('#color').val();
	var status = $('#status').val();
	
	if(name==""){
		$('#name').parent().addClass("has-error");
		$('#error-name').removeClass('hidden');
		success=false;
	}
	
	if(color==""){
		$('#color').parent().parent().addClass("has-error");
		$('#error-color').removeClass('hidden');
		success=false;
	}
	
	if(status==""){
		$('#status').parent().addClass("has-error");
		$('#error-status').removeClass('hidden');
		success=false;
	}

	return success;
});

</script>
@endpush