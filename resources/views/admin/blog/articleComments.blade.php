@extends('admin.layouts.main')
@section('title', trans('cms.article'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('menu.blog_comments') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/blog/comments">
				<i class="fa fa-tags"></i> 
				{{ trans('menu.blog_articles') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.blog_comments') }}</li>
	</ol>
</section>

<?php 
function getCommentsTree($comment){?>
	<div class="media"> 
		<div class="media-left"> 
			<a href="#"> 
				<img alt="64x64" class="media-object" data-src="holder.js/64x64" style="width: 64px; height: 64px;" src="<?=$comment->customer->avatar?>" data-holder-rendered="true">
			</a> 
		</div> 
		<div class="media-body"> 
			<h4 class="media-heading"><?=$comment->customer->name?></h4>
			Status: <span id="status-<?=$comment->id?>" class="<?php echo ($comment->status==$comment::STATUS_ACTIVE) ? 'text-success' : 'text-danger'?>"><?= $comment->status ?></span><br>	
			{{trans('cms.date_comment')}}: <?= date("d/m/Y", strtotime($comment->created_at))?>
			<p style="min-height: 40px;"><?=$comment->comment?></p>
			<a href="#" id="reject-<?=$comment->id?>" rel="<?=$comment->id?>" class="reject-comment btn-danger btn btn-xs <?php echo($comment->status==$comment::STATUS_ACTIVE) ? '': 'hidden'?>">
				{{trans('cms.reject_comment')}}
			</a>
			<a href="#" id="accept-<?=$comment->id?>" rel="<?=$comment->id?>" class="accept-comment btn-success btn btn-xs <?php echo($comment->status==$comment::STATUS_ACTIVE) ? 'hidden': ''?>">
				{{trans('cms.accept_comment')}}
			</a>
			<span id="loader-<?=$comment->id?>"></span>

			<?php foreach( $comment->comments as $child ) {
				getCommentsTree($child);
			}?>
		</div> 
	</div> 
	
<?php } ?>

<section class="content">
	<?= csrf_field() ?>
	<input type="hidden" id="id" name="id" value="<?=$article->id?>" />
	<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display:none; margin-top: 10px;">
		<img src="/images/loader.gif" style="width: 20px; height: 20px;"/>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title"><?=$article->title_plain?></h3>
				</div>
				<div class="box-body">
					<?php foreach ($comments as $comment) { 
						getCommentsTree($comment);
					} ?>

					{{ $comments->links() }}
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('scripts')
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});
	
	var stateActive = "<?=$comment::STATUS_ACTIVE?>";
	var stateRejected = "<?=$comment::STATUS_REJECTED?>";
	var loader = $("#mini-loader").html();
	
	function changeStatusComment(id, status){
		var params= {
			"id" : id,
			"status": status
		};
		
		$.post( "/admin/blog/change-status-comment", params, function(data) {
			$("#loader-"+id).empty();
			if(data===stateActive){
				$("#accept-"+id).addClass('hidden');
				$("#reject-"+id).removeClass('hidden');	
				$("#status-"+id).text(data);
				$("#status-"+id).removeClass('text-danger');
				$("#status-"+id).addClass('text-success');
			}else{
				$("#reject-"+id).addClass('hidden');
				$("#accept-"+id).removeClass('hidden');
				$("#status-"+id).text(data);
				$("#status-"+id).removeClass('text-success');
				$("#status-"+id).addClass('text-danger');
			}
		});
	}

	$(".reject-comment").click(function(e) {
		e.preventDefault();
		var id = $(this).attr('rel');
		$("#loader-"+id).html(loader);
		changeStatusComment(id, stateRejected);
	});
	
	$(".accept-comment").click(function(e) {
		e.preventDefault();
		var id = $(this).attr('rel');
		$("#loader-"+id).html(loader);
		changeStatusComment(id, stateActive);
	});

</script>
@endpush