@extends('admin.layouts.main')
@section('title', trans('cms.article'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-crosshairs"></i>
        {{ trans('cms.points') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/blog/articles">
				<i class="fa fa-tags"></i> 
				{{ trans('menu.blog') }}
			</a>
		</li>
        <li class="active">{{ trans('cms.article') }}</li>
	</ol>
</section>

<section class="content">
	
	<!--MODAL-->
	<div class="modal fade" id="modal-form-point" tabindex="-1" role="dialog" aria-labelledby="modal-good-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="dismiss-modal">×</button>
					<i class="fa fa-crosshairs"></i> <span id="title-modal" style="font-size:18px;"></span>
				</div>
				<div class="modal-body">
					<input type="hidden" id="current-index-banner" value="" />
					<input type="hidden" id="current-location" value="" />
					<!--Alertas-->
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="alert alert-dismissable alert-danger" id="alert-error-modal" style="display: none">
								{{trans('cms.form_requierd_field_many')}}:
								<ul id="error-text-modal"class="error-text"></ul>
							</div>
						</div>
					</div>
					<!--Fin alertas-->
					<div class="form-group" id="linked-current-categories">
						<label>{{trans('cms.label_banners_site_categories')}}</label>
						<select class="form-control" id="current-category">
							<option value="0">{{trans('cms.select')}}</option>
							<?php foreach ($categories as $categoryLv1) { ?>
								<option value="<?= $categoryLv1->id ?>"><?= $categoryLv1->name ?></option>
								<?php if (isset($categoryLv1->categories)) { ?>
									<?php foreach ($categoryLv1->categories as $categoryLv2): ?>
										<option value="<?= $categoryLv2->id ?>">&nbsp;&nbsp;&nbsp;<?= $categoryLv2->name ?></option>
										<?php if (isset($categoryLv2->categories)) { ?>
											<?php foreach ($categoryLv2->categories as $categoryLv3): ?>
												<option value="<?= $categoryLv3->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $categoryLv3->name ?></option>
												<?php if (isset($categoryLv3->categories)) { ?>
													<?php foreach ($categoryLv3->categories as $categoryLv4): ?>
														<option value="<?= $categoryLv4->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $categoryLv4->name ?></option>
													<?php endforeach; ?>
												<?php } ?>

											<?php endforeach; ?>
										<?php } ?>
									<?php endforeach; ?>
								<?php } ?>
							<?php } ?>
						</select>
						<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display:none; margin-top: 10px;">
							<img src="/images/loader.gif" style="widht: 30px; height: 30px;"/>
						</div>
					</div>
					<div class="form-group" id="linked-product">
						<label>{{ trans('cms.label_carrusel_choose_product') }}</label>
						<select class="form-control" id="entity-product" disabled="true">
							<option value="0">{{trans('cms.select')}}</option>
						</select>
					</div>
					<div class="form-group">
						<input type="button" id="send-point" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--FIN MODAL-->	
	
	<!-- FORMULARIO -->
	<form id="form-create-article-points" action="/admin/blog/save-point" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
		<?= csrf_field() ?>
		<input type="hidden" id="idArticle" name="idArticle" value="<?=$article->id?>" />
		<input type="hidden" id="points" name="points" value="" />
		
		<!--TEMPLATE POINT-->
		<div id="templatePoints" class="hidden">
			<div data-toggle="popover" data-placement="top" data-container="body" data-content="" data-html="true">
				<i class="coordenada fa fa-crosshairs"></i>
			</div>
		</div>
		<!--FIN TEMPLATE POINT-->
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title"><?=$article->title_plain?></h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<a href="#" id="add-point" class="btn btn-primary">
								<i class="fa fa-plus-circle"></i>
								{{trans('cms.add_point')}}
							</a>
						</div>
						<div id="wrapper" class="form-group" style="width: 950px">
							<img id="image-header" src="<?=$article->image_header?>" style="width: 100%"/>	
						</div>
						<div class="form-group">
							<input type="button" id="send-article-points" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
							<a href="/admin/blog/articles" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>

@endsection

@push('plugins')
<link rel="stylesheet" href="/themes/admin/vendor/summernote/dist/summernote.css" />
<script src="/themes/admin/vendor/summernote/dist/summernote.min.js"></script>
@endpush

@push('scripts')
<script>

$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('[name="_token"]').val()
	}
});

var numberPoints=<?=$numberPoints?>;
var currentPoints=<?=$currentPoints?>;
var pointProducts=<?=json_encode($products)?>;

var referencePoints = [
	[0,0], //P0
	[475,0], //P1
	[950,0], //P2
	[0,419], //P3
	[475,419], //P4
	[950,419], //P5
	[475,210] //P6
];

//-----------------------------------------------
function quitAlerts() {
	$(".has-error").each(function () {
		$(this).removeClass('has-error');
	});

	$(".alert-danger").css("display", "none");
	$(".error-text").empty();
}

//-----------------------------------------------
function addPoint(coordinates,productId,productName,i){ //Agrega el punto al html
	var templatePoints=$("#templatePoints > div")
							.clone()
							.prop('id', 'point-'+i)
							.addClass('dragPoint')
							.attr('data-product', productId);
		
	if(coordinates!=null){
		$.each(coordinates, function( index, value ) {
			templatePoints.css(index, value+"px");
		});
	}
	
	$("#wrapper").append(templatePoints);//Se agrega al HTML
	
	$(templatePoints).draggable({ //Evento drag
		containment: "#wrapper", 
		scroll: false,
		start: function( event, ui ) {
			$(ui.helper).popover('hide');
		}
	});

	//Nombre producto popover--------------------------
	$(templatePoints).popover();
	
	var button='<button class="delete-point" rel="'+i+'">X</button>';
	$(templatePoints).attr("data-content", productName+" "+button);
	
	$(templatePoints).on('shown.bs.popover', function () {
		$(".delete-point").click(function(e) {
			e.preventDefault();
			var index=$(this).attr('rel');
			$(templatePoints).popover('destroy');
			$("#point-"+index).remove();	
		});
	});
	
	
}

//-----------------------------------------------
function showModal(){ //Modal para agregar el producto al punto
	var title = "<?= trans('cms.point') ?> "+" "+(numberPoints+1);
	$('#entity-product')
		.prop("disabled", true)
		.empty()
		.append($("<option></option>")
			.text("<?= trans('cms.select') ?>")
			.val(0))
		.val(0);

        $('#current-category').val(0);
	$("#title-modal").text(title);
	$('#modal-form-point').modal('show');
}

//-----------------------------------------------
function findProductsRelated(params) {
	$('#mini-loader').show();
	$('#entity-product').prop("disabled", true);
	$.ajax({
		data: params,
		cache: false,
		url: "/admin/services/releated",
		type: 'post',
		success: function (response) {
			var relatedProducts = jQuery.parseJSON(response);
			$('#entity-product').empty();
			$("#entity-product").append($("<option></option>")
					.text("<?= trans('cms.select') ?>")
					.val(0));

			$.each(relatedProducts, function (key, element) {
				$("#entity-product").append(
						$("<option></option>")
						.text(element.name)
						.val(element.id)
						);
			});

			$('#entity-product').prop("disabled", false);
			$('#mini-loader').hide();
		}
	});
}

//-----------------------------------------------
function shorterDistance(xPoint, yPoint, pn){
	var term1=(pn[0]-xPoint);
	var term2=(pn[1]-yPoint);
	var term3=Math.pow(term1, 2) + Math.pow(term2, 2);
	var dist=Math.pow(term3, 0.5); 
	
	return dist;
}

//-----------------------------------------------
function getPercentages(point){ //Dependiendo del punto más cercano se calculan los porcentajes de distancia a los bordes
	var percentages = {};
	
	switch (parseInt(point)) {
		case 0:
			percentages["top"] = 0;
			percentages["left"] = 0;
			break;
		case 1:
			percentages["top"] = 0;
			percentages["left"] = 50;
			break;
		case 2:
			percentages["top"] = 0;
			percentages["right"] = 0;
			break;
		case 3:
			percentages["bottom"] = 0;
			percentages["left"] = 0;
			break;
		case 4:
			percentages["bottom"] = 0;
			percentages["left"] = 50;
			break;
		case 5:
			percentages["right"] = 0;
			percentages["bottom"] = 0;
			break;
		case 6:
			percentages["top"] = 50;
			percentages["left"] = 50;
			break;
	}
	
	return percentages;
}


//-----------------------------------------------
//Agrega puntos existentes
for (var i = 0; i < numberPoints; i++) {
	var coordinates={};
	
	var offsetX=Math.floor(currentPoints[i]["offset_x"]/2);
	var offsetY=Math.floor(currentPoints[i]["offset_y"]/2);
	var productId=currentPoints[i]["product_id"];
	var productName=pointProducts[productId];
	
	coordinates["left"]=offsetX+referencePoints[currentPoints[i]["nearest_point"]][0];
	coordinates["top"]=offsetY+referencePoints[currentPoints[i]["nearest_point"]][1];
	
	addPoint(coordinates,productId,productName,i+1);
}

//-----------------------------------------------
$('#current-category').on('change', function () {
	var selectedCategory = $(this).val();
	if (parseInt(selectedCategory) !== parseInt(0)) {
		var params = {
			"selectedCategory": selectedCategory,
		};

		findProductsRelated(params);
	} else {
		$('#entity-product').prop("disabled", true);
	}
});

//-----------------------------------------------
$("#send-point").click(function (){ //Envia producto cargado al punto
	quitAlerts();
	var success = true;
	var errorMessage = "";
	
	var product=$("#entity-product").val();
	if(product==0){
		errorMessage = errorMessage + "<li>" + "<?= trans('cms.product') ?>" + "</li>";
		$("#entity-product").parent().addClass("has-error");
		success = false;
	}
	
	if(success){
		var productName=$("#entity-product :selected").text();
		numberPoints=numberPoints+1;
		addPoint(null,product,productName,numberPoints);
		$('#modal-form-point').modal('hide');
	}else{
		$("#error-text-modal").append(errorMessage);
		$("#alert-error-modal").slideDown("slow");
	}
});

//-----------------------------------------------
$("#add-point").click(function(e){ //Agrega un producto-punto
	e.preventDefault();
	
	if(numberPoints < 3){
		showModal();
	}else{
		bootbox.alert("<?=trans('cms.exceeded_add_points')?>", function() {  
		});
	}
});


//-----------------------------------------------
$("#send-article-points").click(function() {//Guarda todos los puntos
	$(this).prop( "disabled", true );
	var points = {};
	
	$(".dragPoint").each(function(i) {
		var product=$(this).attr('data-product');
		var left=parseInt($(this).css('left'));
		var top = parseInt($(this).css('top'));
		
		var currentPoint=new Object();
		
		var bestDistance=parseFloat(99999999);
		var bestPoint=null;
		
		$.each(referencePoints, function( index, value ) {
			var d=parseFloat(shorterDistance(left,top,value));
			
			if(d < bestDistance){
				bestDistance=d;
				bestPoint=index;
			}
		});
		
		var percentages = JSON.stringify(getPercentages(bestPoint));
		var offsetX=(left-referencePoints[bestPoint][0])*2;
		var offsetY=(top-referencePoints[bestPoint][1])*2;
		
		currentPoint["bestPoint"]=bestPoint;
		currentPoint["percentages"]=percentages;
		currentPoint["offsetX"]=offsetX;
		currentPoint["offsetY"]=offsetY;
		currentPoint["product"]=product;
		
		var idPoint=$(this).attr('id');
		points[idPoint]=currentPoint;
	});

	$("#points").val(JSON.stringify(points));
	$("#form-create-article-points").submit();
});

</script>
@endpush