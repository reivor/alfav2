@extends('admin.layouts.main')
@section('title', trans('cms.blog_categories'))

@section('content')


<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('cms.blog_categories') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('cms.blog') }}</li>
	</ol>
</section>

<section class="content">
	<div class="row margin-bottom">
		<div class="col-lg-12">
			<a href="/admin/blog/category" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_blog_category') }}
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<?= csrf_field() ?>
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.article_list') }}</h3>
				</div>
				<div class="box-body">
					<div class="row header-titles hidden-sm">
						<div class="col-md-5">
							<span>{{ trans('cms.title_article') }}</span>
						</div>
						<div class="col-md-1">
							<span>{{ trans('cms.product_color') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.product_status') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.label_actions') }}</span>
						</div>
					</div>

					<?php foreach ($blog_categories as $blog_category) { ?>
					<div class="row margin-bottom">
						<div class="col-md-5">
							<?= $blog_category->name?>
						</div>
						<div class="col-md-1">
							<div style="width: 20px; height: 20px; margin: 0 auto; background: <?=$blog_category->color?>">
							</div>				
						</div>
						<div class="col-md-2">
							<span class="label <?php echo ($blog_category->status==$blog_category::STATUS_ACTIVE) ? 'label-success' : 'label-default'?>"><?= $blog_category->status ?></span>					
						</div>
						<div class="col-md-2">
							<a href="/admin/blog/category/<?=$blog_category->id?>" style="padding-right:10px;">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</a>
							<a href class="delete-category" rel="<?= $blog_category->id ?>" data-sure="<?= trans('cms.confirm_delete_carrusel') ?>">
								<i class="fa fa-trash" aria-hidden="true" style="color:red"></i>
							</a>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('scripts')
<script>
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});

	$(".delete-category").click(function(e) {
		e.preventDefault();
		var index=$(this).attr('rel');
		bootbox.confirm({
			message: $(this).data('sure'),
			buttons: {
				'confirm': {
					label: '<?=trans('cms.btn_accept') ?>',
					className: 'btn-default'
				},
				'cancel': {
					label: '<?=trans('cms.btn_cancel') ?>',
					className: 'btn-danger'
				}
			},
			callback: function(result) {
				if (result) {
					var params= {
						"id" : index
					};
					$.post( "/admin/blog/delete-category", params, function(data) {
						window.location.href = "/admin/blog/categories";
					});
				}
			}
		});
		
		/*bootbox.confirm($(this).data('sure'), function(result) {
			if(result){
				var params= {
					"id" : index
				};
				$.post( "/admin/blog/delete-category", params, function(data) {
					window.location.href = "/admin/blog/categories";
				});
			}
		});*/
	});
		
</script>
@endpush