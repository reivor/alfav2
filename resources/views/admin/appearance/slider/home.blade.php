@extends('admin.layouts.main')


@section('title', trans('cms.slider_home'))


@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-image"></i>
        {{ trans('cms.slider_home') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.slider_home') }}</li>
	</ol>
</section>

<section class="content">
	aqui va la tabla
</section>

@endsection