@extends('admin.layouts.main')

@section('title', trans('cms.categories'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa"></i>
        {{ trans('menu.menu_categories') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/appearance/menu">
				<i class="fa fa-tags"></i> 
				{{ trans('menu.menu_categories') }}
			</a>
		</li>
        <li class="active">{{ $childCategories->name }}</li>
	</ol>
</section>
<form id="form-mega-menu" action="/admin/appearance/menu/save" method="post" role="form" enctype="multipart/form-data">
	<section class="content">
		<?= csrf_field() ?>
		<input type="hidden" id="supreme-parent" name="supreme-parent" value="<?= $childCategories->id?>">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ $childCategories->name." - ".trans('cms.menu_banners_categories') }}</h3>
					</div>
					<div class="box-body">
						<div class="row" id="banners-content">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-4">
				<div class="form-group">
					<input type="submit" id="sendMegaMenu" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
				</div>
			</div>

		</div>
	</section>
</form>

<div class="modal fade" id="modal-form-banner" tabindex="-1" role="dialog" aria-labelledby="modal-good-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="dismiss-modal">×</button>
				<i class="fa fa-image"></i> <span id="title-modal" style="font-size:18px;"></span>
			</div>
			<div class="modal-body">
				<input type="hidden" id="current-index-banner" value="" />
				<!--Alertas-->
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="alert alert-dismissable alert-danger" id="alert-error-modal" style="display: none">
							{{trans('cms.form_requierd_field_many')}}:
							<ul id="error-text-modal"class="error-text"></ul>
						</div>
					</div>
				</div>
				<!--Fin alertas-->
				<div class="form-group">
					<label>{{ trans('cms.label_carrusel_linked_to') }}</label>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-category" value="category" checked="" data-index="">
							{{ trans('cms.label_carrusel_category') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-product" value="product" data-index="">
							{{ trans('cms.label_carrusel_product') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-external" value="external" data-index="">
							{{ trans('cms.label_carrusel_external_link') }}
						</label>
					</div>
				</div>
				<div class="form-group option" id="linked-category">
					<label>{{ trans('cms.label_carrusel_choose_category') }}</label>
					<select class="form-control" id="entity-category">
						<option value="0">{{trans('cms.select')}}</option>
						<?php foreach ($childCategories->categories as $categoryLv2){?>
							<option value="<?=$categoryLv2->id?>"><?=$categoryLv2->name?></option>
							<?php if(isset($categoryLv2->categories)){?>
								<?php foreach($categoryLv2->categories as $categoryLv3): ?>
									<option value="<?=$categoryLv3->id?>">&nbsp;&nbsp;&nbsp;<?=$categoryLv3->name?></option>
									<?php if(isset($categoryLv3->categories)){?>
										<?php foreach($categoryLv3->categories as $categoryLv4): ?>
											<option value="<?=$categoryLv4->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$categoryLv4->name?></option>
										<?php endforeach; ?>
									<?php }?>
								<?php endforeach; ?>
							<?php }?>
						<?php }?>
					</select>
				</div>
				<div class="form-group option" id="linked-product">
					<label>{{ trans('cms.label_carrusel_choose_product') }}</label>
					<select class="form-control" id="entity-product">
						<option value="0">{{trans('cms.select')}}</option>
						<?php foreach ($products as $product) {?>
							<option value="<?= $product->id ?>"><?= $product->name ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group option" id="linked-external">
					<label for="url">{{ trans('cms.label_carrusel_url') }}</label>
					<input type="text" id="entity-external" class="form-control" value="" placeholder="" maxlength="100" />
				</div>
				<div class="form-group image-content">
					<label>{{ trans('cms.label_carrusel_url_image') }}</label> <span id="suggested-size">({{trans('cms.suggested_size')}}: 250x200)</span>
					<input type="file" class="image-url" name="image" />
				</div>
				<div class="form-group" id="banner-status">
					<label>{{ trans('cms.label_status') }}</label>
					<select class="form-control" id="status-banner">
						<?php foreach ($status as $st){?>
							<option value="<?=$st?>"><?=$st?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<input type="button" id="send-banner" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@push('plugins')
<link rel="stylesheet" href="/themes/admin/vendor/jstree/dist/themes/default/style.min.css" />
<script src="/themes/admin/vendor/jstree/dist/jstree.min.js"></script>
@endpush

@push('templates')
<script id="tpl-banner" type="text/x-handlebars-template">
<div class="col-xs-2 col-lg-2">

	<div id="image-@{{ index }}">
		<a href class="clean-banner" rel="@{{ index }}" data-sure="<?= trans('cms.confirm_delete_carrusel') ?>"><i class="fa fa-times" aria-hidden="true" style="float:right; margin-bottom: 3px;"></i></a>
		<img class="image-container-thumb" rel="@{{ index }}" id="image-src-@{{ index }}" src="@{{banner.image_url}}" width="150" height="120" style="cursor:pointer;" />
	</div>
	<input type="hidden" id="image-get-@{{ index }}" name="image-get[@{{ index }}]" value="@{{banner.image_url}}" />
	<input type="hidden" id="type-@{{ index }}" class="type" name="type[@{{ index }}]" value="@{{banner.type}}" />
	<input type="hidden" id="entity-@{{ index }}" class="entity" name="entity[@{{ index }}]" value="@{{banner.entity_id}}" />
	<input type="hidden" id="external-@{{ index }}" name="external[@{{ index }}]" value="@{{banner.external_url}}" />
	<input type="hidden" id="status-@{{ index }}" name="status[@{{ index }}]" value="@{{banner.status}}" />
</div>
</script>

@endpush


@push('scripts')

<script>
	var banners=<?= json_encode($banners)?>;
	var modal=$("#modal-form-banner");
	
$(function() {
	
	//-----------------------------------------------
    function quitAlerts(){
	$(".has-error" ).each(function() {
		$(this).removeClass('has-error');
	});
	
		$(".alert-danger" ).css("display", "none");
		$(".error-text").empty();
    }
    
    //-----------------------------------------------
    function initializeEntities(){
		if ($("#type-category").is(':checked')){
            $("#linked-category").show();
            $("#linked-product").hide();
            $("#linked-external").hide();
        }

        if ($("#type-product").is(':checked')){
            $("#linked-product").show();
            $("#linked-category").hide();
            $("#linked-external").hide();
        }

        if ($("#type-external").is(':checked')){
            $("#linked-external").show();
            $("#linked-product").hide();
            $("#linked-category").hide();
        }
    }
	
	//-----------------------------------------------
	function constructLayout(){
		var containerLayout = $('#banners-content');
		containerLayout.empty();

		var numberItems=2;

		for ( var i = 1, j = parseInt(numberItems); i <= j; i++ ) {	
			if(banners[i] === undefined || banners[i] === null){
				banners[i]={has_image: 0, image_url: '/images/placeholder.png'};
			}else{
				banners[i].has_image=1;
			}

			var templateLayout = Handlebars.compile($('#tpl-banner').html());
			var htmlLayout = templateLayout({ index: i, banner: banners[i]});
			containerLayout.append(htmlLayout);
		}
		
		$(".image-container-thumb").click(function() {
			showModal($(this));
		});
			
		$(".clean-banner").click(function(e) {
			e.preventDefault();
			var index=$(this).attr('rel');
			
			if(parseInt(banners[index].has_image)==parseInt(1)){
				bootbox.confirm({
					message: $(this).data('sure'),
					buttons: {
						'confirm': {
							label: '<?=trans('cms.btn_accept') ?>',
							className: 'btn-default'
						},
						'cancel': {
							label: '<?=trans('cms.btn_cancel') ?>',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if (result) {
							$("input[name='image["+index+"]'").remove();
							$('#image-src-'+index).attr('src',"/images/placeholder.png");
							$('#type-'+index).val("");
							$('entity-'+index).val("");
							$('external-'+index).val("");
							$('image-get-'+index).val("");
							banners[index].has_image=0;
						}
					}
				});
			}
		});
	}
	
	//-----------------------------------------------
	function cleanSection(){
		$('#entity-category').val(0);
		$('#entity-product').val(0);
		$('#entity-external').val("");
	}
	
	//-----------------------------------------------
	function showModal(content){
		quitAlerts();
		var currentBannerIndex=content.attr('rel');
		$("#current-index-banner").val(currentBannerIndex);
		
		var title="<?= trans('cms.data_banner_menu_category')?>"+" "+currentBannerIndex;
		$("#title-modal").text(title);

		var type=$("#type-"+currentBannerIndex).val();
		var entity=$("#entity-"+currentBannerIndex).val();
		var status=$("#status-"+currentBannerIndex).val();

		$('.image-url', modal).val("");

		if(type!=""){
			$('input[name="type-radio"][value="' + type + '"]').prop('checked', true);
			if(type=="external"){
				$('#entity-'+type).val($("#external-"+currentBannerIndex).val());
			}else{
				if(entity){
					$('#entity-'+type).val(entity);
				}
			}
			
			$('#status-banner').val(status);
		}else{
			$('input[name="type-radio"][value="category"]').prop('checked', true);
			cleanSection();
		}

		initializeEntities();

		$('#modal-form-banner').modal('show');
	}
	
	//-----------------------------------------------
    $("#send-banner").click(function() {
        quitAlerts();
		
        var success=true;
        var errorMessage="";
        
		var currentBanner= $("#current-index-banner").val();
        var type=$("input[name=type-radio]:checked").val();
            
        if(type=="category" && $("#entity-category").val()==0){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.category')?>"+"</li>";
            $("#entity-category").parent().addClass("has-error");
            success=false;
        }else if(type=="product" && $("#entity-product").val()==0){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.product')?>"+"</li>";
            $("#entity-product").parent().addClass("has-error");
            success=false;
        }else if(type=="external" && $("#entity-external").val()==""){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.url')?>"+"</li>";
            $("#entity-external").parent().addClass("has-error");
            success=false;
        }

		if(banners[currentBanner].has_image==0){
			if($('.image-url', modal).val()==''){
				$('.image-url', modal).parent().addClass("has-error");
				errorMessage=errorMessage+"<li>"+"<?= trans('cms.image')?>"+"</li>";
				success=false;
			}	
		}

        if(success){
            var entity="";
            $('#type-'+currentBanner).val(type);
            entity=$("#entity-"+type).val();
			if(type=="external"){
				$('#external-'+currentBanner).val(entity);
			}else{
				$('#entity-'+currentBanner).val(entity);
			}
			
			var status = $("#status-banner").val();
			$("#status-"+currentBanner).val(status);

			/* FILE IMAGE*/
			var file=$('.image-url', modal);
			
			if(file.val()!=""){
				var imageClone=$('.image-url', modal).clone();
				$("input[name='image["+currentBanner+"]'").remove();
				file.appendTo("#form-mega-menu").attr('name', 'image['+currentBanner+']').css("opacity", 0);
				imageClone.appendTo(".image-content");
				banners[currentBanner].has_image=1;
			}
			
			if(file.val()!=""){//Preview web site
				if (file[0].files && file[0].files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#image-src-'+currentBanner).attr('src', e.target.result);
					}
					reader.readAsDataURL(file[0].files[0]);
				}
			}

            $('#modal-form-banner').modal('hide');
        }else{
            $("#error-text-modal").append(errorMessage);
            $("#alert-error-modal" ).slideDown("slow");
        }
    });
	
	//-----------------------------------------------

    $('input[type=radio]').on('change',function() {
        $('#linked-category').hide();
        $('#linked-product').hide();
        $('#linked-external').hide();
        $('#linked-'+$(this).val()).show();
		
		cleanSection();
    });
	
	//-----------------------------------------------
	constructLayout();
	

});
</script>
@endpush
