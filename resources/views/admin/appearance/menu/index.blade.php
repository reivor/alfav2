@extends('admin.layouts.main')

@section('title', trans('cms.categories'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa"></i>
        {{ trans('menu.menu_categories') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.menu_categories') }}</li>
	</ol>
</section>

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{ trans('cms.main_categories') }}</h3>
		</div>
		<div class="box-body grid-categories">
			<?php foreach($categories as $category): ?>
			<div class="col-xs-6 col-sm-3 col-lg-2">
				<div class="main-category-container">
					<a href="{{ url('admin/appearance/menu/categories/'.$category->id) }}">
						<div class="image-container">
							<img id="uploadPreview" src="<?= $category->image_url ?>" />
						</div>
						<div class="title-container">
							<h4><?= $category->name ?></h4>
						</div>
					</a>
				</div>
				
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>



@endsection

