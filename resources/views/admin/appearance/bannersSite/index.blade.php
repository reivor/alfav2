@extends('admin.layouts.main')


@section('title', trans('menu.banners_site'))


@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-image"></i>
        {{ trans('menu.banners_site') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.banners_site') }}</li>
	</ol>
</section>
<section class="content">
	<form id="form-banners-site" action="/admin/appearance/banners_site/save" method="post" role="form" enctype="multipart/form-data" novalidate>
		<?= csrf_field() ?>
		<?php if (isset($categories)) { ?> <!--Banners Home -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Home</h3>
				</div>
				<div class="box-body">
					<div class="row">

						<div class="col-xs-6 col-sm-6"><!-- Banners Top -->
							<h4 class="box-title">{{trans('cms.label_banners_site_home_top')}}</h4>
							<div id="banners-1" class="banners-site">

							</div>

						</div>
					</div>
					<hr>
					<div class="row" style="margin-top: 10px;">
						<div class="col-xs-6 col-sm-6"> <!-- Footer -->
							<h4 class="box-title">{{trans('cms.label_banners_site_home_footer')}}</h4>
							<div id="banners-2" class="banners-site">

							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="box"><!-- Banners SubHome -->
				<div class="box-header with-border">
					<h3 class="box-title">{{trans('cms.label_banners_site_subhome')}}</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-6 col-sm-6 banners-site" id="banners-3">

						</div>
					</div>
				</div>
			</div>

			<div class="box"><!-- Banner Registro -->
				<div class="box-header with-border">
					<h3 class="box-title">{{trans('cms.label_banners_site_register')}}</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-6 col-sm-6 banners-site" id="banners-4">

						</div>
					</div>
				</div>
			</div>

			<!--<div class="box"> Banner Blog 
				<div class="box-header with-border">
					<h3 class="box-title">{{trans('cms.label_banners_site_blog')}}</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-6 col-sm-6 banners-site" id="banners-5">

						</div>
					</div>
				</div>
			</div>-->
		<?php } ?>

		<div class="row">
			<div class="col-xs-6 col-sm-6" id="data-banner-home" style="margin-top: 20px;">
				<input type="submit" id="send-form" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
			</div>
		</div>
	</form>
</section>

<div class="modal fade" id="modal-form-banner" tabindex="-1" role="dialog" aria-labelledby="modal-good-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="dismiss-modal">×</button>
				<i class="fa fa-image"></i> <span id="title-modal" style="font-size:18px;"></span>
			</div>
			<div class="modal-body">
				<input type="hidden" id="current-index-banner" value="" />
				<input type="hidden" id="current-location" value="" />
				<!--Alertas-->
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="alert alert-dismissable alert-danger" id="alert-error-modal" style="display: none">
							{{trans('cms.form_requierd_field_many')}}:
							<ul id="error-text-modal"class="error-text"></ul>
						</div>
					</div>
				</div>
				<!--Fin alertas-->
				<div class="form-group">
					<label>{{ trans('cms.label_carrusel_linked_to') }}</label>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-category" value="category" checked="" data-index="">
							{{ trans('cms.label_carrusel_category') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-product" value="product" data-index="">
							{{ trans('cms.label_carrusel_product') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-external" value="external" data-index="">
							{{ trans('cms.label_carrusel_external_link') }}
						</label>
					</div>
				</div>
				<div class="form-group option" id="linked-category">
					<label>{{ trans('cms.label_carrusel_choose_category') }}</label>
					<select class="form-control" id="entity-category">
						<option value="0">{{trans('cms.select')}}</option>
						<?php foreach ($categories as $categoryLv1) { ?>
							<option value="<?= $categoryLv1->id ?>"><?= $categoryLv1->name ?></option>
							<?php if (isset($categoryLv1->categories)) { ?>
								<?php foreach ($categoryLv1->categories as $categoryLv2): ?>
									<option value="<?= $categoryLv2->id ?>">&nbsp;&nbsp;&nbsp;<?= $categoryLv2->name ?></option>
									<?php if (isset($categoryLv2->categories)) { ?>
										<?php foreach ($categoryLv2->categories as $categoryLv3): ?>
											<option value="<?= $categoryLv3->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $categoryLv3->name ?></option>
											<?php if (isset($categoryLv3->categories)) { ?>
												<?php foreach ($categoryLv3->categories as $categoryLv4): ?>
													<option value="<?= $categoryLv4->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $categoryLv4->name ?></option>
												<?php endforeach; ?>
											<?php } ?>

										<?php endforeach; ?>
									<?php } ?>
								<?php endforeach; ?>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
				<div class="form-group option" id="linked-current-categories">
					<label>{{trans('cms.label_banners_site_categories')}}</label>
					<select class="form-control" id="current-category">
						<option value="0">{{trans('cms.select')}}</option>
						<?php foreach ($categories as $categoryLv1) { ?>
							<option value="<?= $categoryLv1->id ?>"><?= $categoryLv1->name ?></option>
							<?php if (isset($categoryLv1->categories)) { ?>
								<?php foreach ($categoryLv1->categories as $categoryLv2): ?>
									<option value="<?= $categoryLv2->id ?>">&nbsp;&nbsp;&nbsp;<?= $categoryLv2->name ?></option>
									<?php if (isset($categoryLv2->categories)) { ?>
										<?php foreach ($categoryLv2->categories as $categoryLv3): ?>
											<option value="<?= $categoryLv3->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $categoryLv3->name ?></option>
											<?php if (isset($categoryLv3->categories)) { ?>
												<?php foreach ($categoryLv3->categories as $categoryLv4): ?>
													<option value="<?= $categoryLv4->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $categoryLv4->name ?></option>
												<?php endforeach; ?>
											<?php } ?>

										<?php endforeach; ?>
									<?php } ?>
								<?php endforeach; ?>
							<?php } ?>
						<?php } ?>
					</select>
					<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display:none; margin-top: 10px;">
						<img src="/images/loader.gif" style="widht: 30px; height: 30px;"/>
					</div>
				</div>

				<div class="form-group option" id="linked-product">
					<label>{{ trans('cms.label_carrusel_choose_product') }}</label>
					<select class="form-control" id="entity-product" disabled="true">
						<option value="0">{{trans('cms.select')}}</option>
					</select>
				</div>
				<div class="form-group option" id="linked-external">
					<label for="url">{{ trans('cms.label_carrusel_url') }}</label>
					<input type="text" id="entity-external" class="form-control" value="" placeholder="" maxlength="100" />
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group image-content">
					<label>{{ trans('cms.label_carrusel_url_image') }}</label> <span id="suggested-size"></span>
					<input type="file" class="image-url" name="image" />
				</div>
				<div class="form-group" id="banner-status">
					<label>{{ trans('cms.label_status') }}</label>
					<select class="form-control" id="status-banner">
						<?php foreach ($status as $st){?>
							<option value="<?=$st?>"><?=$st?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<input type="button" id="send-banner" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('plugins')
<link rel="stylesheet" href="/themes/admin/css/layout-banners.css" />
<script src="/themes/admin/vendor/packery/dist/packery.pkgd.min.js"></script>
@endpush

@push('templates')

<script id="tpl-product-loader" type="text/x-handlebars-template">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-13 text-center">
	<img src="/images/loader.gif" style="widht: 30px; height: 30px;"/>
	</div>
</script>

<script id="tpl-banner" type="text/x-handlebars-template">
	<a href class="clean-banner clean-banner-site" rel="@{{ index }}" data-location="@{{ location }}" data-sure="<?= trans('cms.confirm_delete_carrusel') ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
	<div class="image-container-thumb" rel="@{{ index }}" data-location="@{{ location }}">
	<img class="image-src-@{{ index }}" src="@{{banner.image_url}}"/>
	</div>
	<input type="hidden" class="image-get-@{{ index }}" name="image-get-@{{ location }}[@{{ index }}]" value="@{{banner.image_url}}" />
	<input type="hidden" class="type type-@{{ index }}" name="type-@{{ location }}[@{{ index }}]" value="@{{banner.type}}" />
	<input type="hidden" class="entity entity-@{{ index }}" name="entity-@{{ location }}[@{{ index }}]" value="@{{banner.entity_id}}" />
	<input type="hidden" class="external-@{{ index }}" name="external-@{{ location }}[@{{ index }}]" value="@{{banner.external_url}}" />
	<input type="hidden" id="status-@{{ index }}" name="status-@{{ location }}[@{{ index }}]" value="@{{banner.status}}" />
</script>


<script id="tpl-layout-1" type="text/x-handlebars-template">
	<div class="grid">
	<div class="grid-sizer"></div>
	<div class="grid-item grid-item--width4 item-1"></div>
	<div class="grid-item grid-item--width2 item-2"></div>

	</div>
</script>

<script id="tpl-layout-2" type="text/x-handlebars-template">
	<div class="grid">
	<div class="grid-sizer"></div>
	<div class="grid-item grid-item--width2 item-1"></div>
	<div class="grid-item grid-item--width4 item-2"></div>
	</div>
</script>

<script id="tpl-layout-3" type="text/x-handlebars-template">
	<div class="grid">
	<div class="grid-sizer"></div>
	<div class="grid-item grid-item--width2 item-1"></div>
	<div class="grid-item grid-item--width4 item-2"></div>
	</div>
</script>

<script id="tpl-layout-4" type="text/x-handlebars-template">
	<div class="grid">
	<div class="grid-sizer"></div>
	<div class="grid-item grid-item--width2 grid-item--height2 item-1"></div>
	</div>
</script>

<script id="tpl-layout-5" type="text/x-handlebars-template">
	<div class="grid">
	<div class="grid-sizer"></div>
	<div class="grid-item grid-item--width6 grid-item--height2 item-1"></div>
	</div>
</script>

@endpush

@push('plugins')
<link rel="stylesheet" href="/themes/admin/css/layout-banners.css" />
@endpush



@push('scripts')
<script>

var bannersHomeTop =<?= json_encode($bannersHomeTop) ?>;
var bannersHomeFooter =<?= json_encode($bannersHomeFooter) ?>;
var bannersSubhome =<?= json_encode($bannersSubHome) ?>;
var bannersRegistro =<?= json_encode($bannersRegistro) ?>;
var bannersBlog =<?= json_encode($bannersBlog) ?>;

var modal = $("#modal-form-banner");


$(function () {

    //-----------------------------------------------
    function quitAlerts() {
        $(".has-error").each(function () {
            $(this).removeClass('has-error');
        });

        $(".alert-danger").css("display", "none");
        $(".error-text").empty();
    }

    //-----------------------------------------------
    function initializeEntities() {
        if ($("#type-category").is(':checked')) {
            $("#linked-category").show();
            $("#linked-product").hide();
            $("#linked-external").hide();
            $('#linked-current-categories').hide();
        }

        if ($("#type-product").is(':checked')) {
            $("#linked-product").show();
            $("#linked-category").hide();
            $("#linked-external").hide();
            $('#linked-current-categories').show();
        }

        if ($("#type-external").is(':checked')) {
            $("#linked-external").show();
            $("#linked-product").hide();
            $("#linked-category").hide();
            $('#linked-current-categories').hide();
        }
    }

    //-----------------------------------------------
    function numberItemsLayout(layout) {
        switch (parseInt(layout)) {
            case 1:
            case 2:
            case 3:
                return 2;
            case 4:
            case 5:
                return 1;
        }
    }

    //-----------------------------------------------
    function suggestedSize(indexImage, layout) {
        layout = parseInt(layout);
        indexImage = parseInt(indexImage);

        if (layout == 1) {
            if (indexImage == 1) {
                return "800x200";
            } else {
                return "400x200";
            }
        } else if (layout == 2 || layout == 3) {
            if (indexImage == 1) {
                return "400x200";
            } else {
                return "800x200";
            }
        } else if (layout == 4) {
            return "370x270";
        } else if (layout == 5) {
            return "1200x400";
        }
    }

    //-----------------------------------------------
    function getCurrentSection(layout) {
        switch (parseInt(layout)) {
            case 1:
                return "<?= trans('cms.label_banners_site_home_top') ?>";
            case 2:
                return "<?= trans('cms.label_banners_site_home_footer') ?>";
            case 3:
                return "<?= trans('cms.label_banners_site_subhome') ?>";
            case 4:
                return "<?= trans('cms.label_banners_site_register') ?>";
            case 5:
                return "<?= trans('cms.label_banners_site_blog') ?>";
        }
    }

    //-----------------------------------------------
    function getCurrentBannerArray(layout) {
        switch (parseInt(layout)) {
            case 1:
                return bannersHomeTop;
            case 2:
                return bannersHomeFooter;
            case 3:
                return bannersSubhome;
            case 4:
                return bannersRegistro;
            case 5:
                return bannersBlog;
        }
    }

    //-----------------------------------------------
    function findProductsRelated(params) {
        $('#mini-loader').show();
        $('#entity-product').prop("disabled", true);
        $.ajax({
            data: params,
            cache: false,
            url: "/admin/services/releated",
            type: 'post',
            success: function (response) {
                var relatedProducts = jQuery.parseJSON(response);
                $('#entity-product').empty();
                $("#entity-product").append($("<option></option>")
                        .text("<?= trans('cms.select') ?>")
                        .val(0));

                $.each(relatedProducts, function (key, element) {
                    $("#entity-product").append(
                            $("<option></option>")
                            .text(element.name)
                            .val(element.id)
                            );
                });

                $('#entity-product').prop("disabled", false);
                $('#mini-loader').hide();
            }
        });
    }

    //-----------------------------------------------
    function constructLayout(firstTime, typeLayout) {
        var containerLayout = $('#banners-' + typeLayout);
        containerLayout.empty();
        var templateLayout = Handlebars.compile($('#tpl-layout-' + typeLayout).html());
        containerLayout.append(templateLayout);

        var numberItems = numberItemsLayout(typeLayout);

        $('.grid').packery({
            itemSelector: '.grid-item',
            gutter: 4,
            rowHeight: '.grid-sizer',
            percentPosition: true
        });

        var banners = getCurrentBannerArray(typeLayout);

        for (var i = 1, j = parseInt(numberItems); i <= j; i++) {
            if (firstTime) {
                if (banners[i] === undefined || banners[i] === null) {
                    banners[i] = {has_image: 0, image_url: '/images/placeholder.png'};
                } else {
                    banners[i].has_image = 1;
                }
            } else {
                banners[i] = {has_image: 0, image_url: '/images/placeholder.png'};
            }

            var templateBanner = Handlebars.compile($('#tpl-banner').html());
            var htmlLayout = templateBanner({index: i, banner: banners[i], location: typeLayout});
            $(".item-" + i, "#banners-" + typeLayout).append(htmlLayout);
        }
    }

    //-----------------------------------------------
    function cleanSection() {
        $('#entity-category').val(0);
        $('#entity-external').val("");
        $('#entity-product').prop("disabled", true)
                .empty()
                .append($("<option></option>")
                        .text("<?= trans('cms.select') ?>")
                        .val(0))
                .val(0);

        $('#current-category').val(0);
    }

    //-----------------------------------------------
    function showModal(content) {
        quitAlerts();

        var currentBannerIndex = content.attr('rel');
        var location = content.attr('data-location');
        var textLocation = "";

        $("#current-index-banner").val(currentBannerIndex);
        $("#current-location").val(location);

        var title = "<?= trans('cms.data_banner_category') ?> " + textLocation + " " + currentBannerIndex;
        $("#title-modal").text(title);

        var type = $(".type-" + currentBannerIndex, '#banners-' + location).val();
        var entity = $(".entity-" + currentBannerIndex, '#banners-' + location).val();
        var external = $(".external-" + currentBannerIndex, '#banners-' + location).val();
		var status=$("#status-"+currentBannerIndex, '#banners-' + location).val();

        $('.image-url', modal).val("");

        if (type) {
            $('input[name="type-radio"][value="' + type + '"]').prop('checked', true);
            if (type == "external") {
                $('#entity-' + type).val(external);
            } else if (type == "category") {
                $('#entity-' + type).val(entity);
            } else if (type == "product") {
                var banners = getCurrentBannerArray(location);
                $('#current-category').val(0);
                $('#entity-' + type).val(entity);
                $('#entity-product').prop("disabled", true)
                        .empty()
                        .append($("<option></option>")
                                .text(banners[currentBannerIndex].name)
                                .val(entity))
                        .val(entity);
            }
			
			$('#status-banner').val(status);
        } else {
            $('input[name="type-radio"][value="category"]').prop('checked', true);
            cleanSection();
        }

        initializeEntities();

        var size = suggestedSize(currentBannerIndex, location);
        $('#suggested-size').text("(<?= trans('cms.suggested_size') ?>: " + size + ")");
        $('#modal-form-banner').modal('show');
    }

    //-----------------------------------------------
    function assembleLayouts() {
        for (var i = 1, j = 5; i <= j; i++) {
            constructLayout(true, i);
        }

        $(".image-container-thumb").click(function () {
            showModal($(this));
        });

        $(".clean-banner").click(function (e) {
            e.preventDefault();
            var index = $(this).attr('rel');
            var location = $(this).attr('data-location');
            var banners = getCurrentBannerArray(location);

            if (parseInt(banners[index].has_image) == parseInt(1)) {
				bootbox.confirm({
					message: $(this).data('sure'),
					buttons: {
						'confirm': {
							label: '<?=trans('cms.btn_accept') ?>',
							className: 'btn-default'
						},
						'cancel': {
							label: '<?=trans('cms.btn_cancel') ?>',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if (result) {
							$("input[name='image-" + location + "[" + index + "]'").remove();
							$('.image-src-' + index, '#banners-' + location).attr('src', "/images/placeholder.png");
							$("input[name='type-" + location + "[" + index + "]'").val("");
							$("input[name='entity-" + location + "[" + index + "]'").val("");
							$("input[name='external-" + location + "[" + index + "]'").val("");
							$("input[name='image-get-" + location + "[" + index + "]'").val("");
							banners[index].has_image = 0;
						}
					}
				});
            }
        });
    }

    //-----------------------------------------------
    $('input[type=radio]').on('change', function () {//Types
        $('#linked-category').hide();
        $('#linked-product').hide();
        $('#linked-external').hide();
        $('#linked-' + $(this).val()).show();
        if ($(this).val() == "product") {
            $('#linked-current-categories').show();
        }

        cleanSection();
    });

    //-----------------------------------------------
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });

    //-----------------------------------------------
    $('#current-category').on('change', function () {
        $('#relatedProductsSection').empty();
        var selectedCategory = $(this).val();
        if (parseInt(selectedCategory) !== parseInt(0)) {
            var params = {
                "selectedCategory": selectedCategory,
            };

            findProductsRelated(params);
        } else {
            $('#entity-product').prop("disabled", true);
        }
    });

    //-----------------------------------------------
    $("#send-banner").click(function () {
        quitAlerts();

        var success = true;
        var errorMessage = "";

        var currentBanner = $("#current-index-banner").val();
        var currentLocation = $("#current-location").val();
        var type = $("input[name=type-radio]:checked").val();

        var banners = getCurrentBannerArray(currentLocation);

        if (type == "category" && $("#entity-category").val() == 0) {
            errorMessage = errorMessage + "<li>" + "<?= trans('cms.category') ?>" + "</li>";
            $("#entity-category").parent().addClass("has-error");
            success = false;
        } else if (type == "product" && $("#entity-product").val() == 0) {
            errorMessage = errorMessage + "<li>" + "<?= trans('cms.product') ?>" + "</li>";
            $("#entity-product").parent().addClass("has-error");
            success = false;
        } else if (type == "external" && $("#entity-external").val() == "") {
            errorMessage = errorMessage + "<li>" + "<?= trans('cms.url') ?>" + "</li>";
            $("#entity-external").parent().addClass("has-error");
            success = false;
        }

        if (banners[currentBanner].has_image == 0) {
            if ($('.image-url', modal).val() == '') {
                $('.image-url', modal).parent().addClass("has-error");
                errorMessage = errorMessage + "<li>" + "<?= trans('cms.image') ?>" + "</li>";
                success = false;
            }
        }

        if (success) {
            var entity = "";
            $(".type-" + currentBanner, '#banners-' + currentLocation).val(type);
            entity = $("#entity-" + type).val();
            if (type == "external") {
                $('.external-' + currentBanner, "#banners-" + currentLocation).val(entity);
            } else {
                $('.entity-' + currentBanner, "#banners-" + currentLocation).val(entity);
            }
			
			var status = $("#status-banner").val();
			$("#status-"+currentBanner, "#banners-" + currentLocation).val(status);

            /* FILE IMAGE*/
            var file = $('.image-url', modal);

            if (file.val() != "") {
                var imageClone = $('.image-url', modal).clone();
                $("input[name='image-" + currentLocation + "[" + currentBanner + "]'").remove();
                file.appendTo("#form-banners-site").attr('name', 'image-' + currentLocation + '[' + currentBanner + ']').css("opacity", 0);
                imageClone.appendTo(".image-content");
                banners[currentBanner].has_image = 1;
            }

            if (file.val() != "") {//Preview web site
                if (file[0].files && file[0].files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.image-src-' + currentBanner, '#banners-' + currentLocation).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(file[0].files[0]);
                }
            }

            $('#modal-form-banner').modal('hide');
        } else {
            $("#error-text-modal").append(errorMessage);
            $("#alert-error-modal").slideDown("slow");
        }
    });

    //-----------------------------------------------
    $('#send-form').click(function (e) {
        var success = true;
        var message = "<?= trans('cms.message_select_all_banners') ?>";

        for (i = 1; i <= 5; i++) {
            var selectedBanners = $(".type", "#banners-" + i);
            var numberItems = numberItemsLayout(i);
            var cont = 0;
            for (j = 0; j < numberItems; j++) {
                if (selectedBanners[j].value != "" && selectedBanners[j].value != null) {
                    cont = cont + 1;
                }
            }

            if (cont > 0 && cont != numberItems) {
                var section = getCurrentSection(i);
                message = message + '<br> -' + section;
                success = false;
            }

        }

        if (!success) {
            bootbox.alert(message, function () {
            });

            e.preventDefault();
        }
    });


    //-----------------------------------------------
    assembleLayouts();

});

</script>
@endpush


