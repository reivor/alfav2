@extends('admin.layouts.main')


@section('title', trans('cms.slider_home'))


@section('content')

<section class="content-header">
	
	<h1>
		<i class="fa fa-image"></i>
        {{ trans('cms.slider_home') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.slider_home') }}</li>
	</ol>
</section>

<section class="content">
	<div class="row margin-bottom">
		<div class="col-xs-12">
			<a href="/admin/appearance/carrusel/create" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_carrusel') }}
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.carrusel') }}</h3>
				</div>
				<div class="box-body">
					<?php if( $carrusel->isEmpty() ): ?>
					<div class="alert alert-warning"><?= trans('cms.no_carrusel_found') ?></div>
					<?php else: ?>
			            <div class="box-body table-responsive no-padding">
			              <table class="table table-hover">
			              	<tr>
			                  <th></th>
			                  <th><?= trans('cms.label_carrusel_status') ?></th>
			                  <th>&nbsp;</th>
			                </tr>
			                @foreach ($carrusel as $carr)
	                            <tr>
	                                <td>
	                                    <a href="{{ URL::to('admin/appearance/carrusel/edit/' . $carr->id ) }}"><img src="{{ $carr->image_url }}" width="150" height="70"></a>
	                                </td>
	                                <td>
	                                	@if($carr->status == "active")
											<span class="label label-success">{{ $carr->status }}</span>
										@else
											<span class="label label-default">{{ $carr->status }}</span>
	                                	@endif
	                                </td>
	                                <td>
	                                    <a href="{{ URL::to('admin/appearance/carrusel/destroy/' . $carr->id ) }}" style="color:red" title="<?= trans('cms.delete') ?>" data-confirm="<?= trans('cms.confirm_delete_carrusel') ?>"><i class="fa fa-fw fa-trash"></i></a>
										<?php if($carr->status == $carr::STATUS_ACTIVE) {?>
											<a href="{{ URL::to('admin/appearance/carrusel/change-status/' . $carr->id  . '/' . $carr::STATUS_DISABLED) }}" style="color:red" title="<?= trans('cms.disable') ?>"><i class="fa fa-fw fa-ban"></i></a>
										<?php }else{?>
											<a href="{{ URL::to('admin/appearance/carrusel/change-status/' . $carr->id . '/' . $carr::STATUS_ACTIVE) }}" style="color:green" title="<?= trans('cms.activate') ?>"><i class="fa fa-fw fa-check"></i></a>
										<?php }?>
									</td>
	                            </tr>
                        	@endforeach
			              </table>
			            </div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
