@extends('admin.layouts.main')


@section('title', trans('cms.slider_home'))


@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
		{{ trans('cms.slider_home') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/appearance/carrusel">
				<i class="fa fa-tags"></i> 
				{{ trans('menu.carrusel') }}
			</a>
		</li>
		<li class="active">{{ trans('cms.create') }}</li>
	</ol>
</section>

<section class="content">
	<!--Alertas-->
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="alert alert-dismissable alert-danger" id="alert-error" style="display: none">
				{{trans('cms.form_requierd_field_many')}}:
				<ul id="error-text"class="error-text"></ul>
			</div>
		</div>
	</div>
	<!--Fin alertas-->
	
	<form id="form-create-carrusel" action="/admin/appearance/carrusel/store" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
		<?= csrf_field() ?>
		<input type="hidden" id="id" name="id" value="" />

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('cms.add_carrusel') }}</h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label>{{ trans('cms.label_carrusel_linked_to') }}</label>
					<div class="radio">
						<label>
							<input type="radio" name="linked_to" id="type-category" value="category" checked="">
							{{ trans('cms.label_carrusel_category') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="linked_to" id="type-product" value="product">
							{{ trans('cms.label_carrusel_product') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="linked_to" id="type-external" value="external">
							{{ trans('cms.label_carrusel_external_link') }}
						</label>
					</div>
				</div>
				<div class="form-group option" id="linked-category">
					<label>{{ trans('cms.label_carrusel_choose_category') }}</label>
					<select class="form-control" name="url_category" id="entity-category">
						<option value="0">{{trans('cms.select')}}</option>
						<?php $categories = App\Models\Category::buildCategoriesTree() ?>
						@foreach ($categories as $categoryLv1)
						<option value="{{ $categoryLv1->id }}">{{ $categoryLv1->name}}</option>
						@if(isset($categoryLv1->categories))
						@foreach ($categoryLv1->categories as $categoryLv2)
						<option value="{{ $categoryLv2->id }}">&nbsp;&nbsp;&nbsp;{{ $categoryLv2->name}}</option>
						@if(isset($categoryLv2->categories))
						@foreach ($categoryLv2->categories as $categoryLv3)
						<option value="{{ $categoryLv3->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $categoryLv3->name}}</option>
						@if(isset($categoryLv3->categories))
						@foreach ($categoryLv3->categories as $categoryLv4)
						<option value="{{ $categoryLv4->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $categoryLv4->name}}</option>
						@endforeach
						@endif
						@endforeach
						@endif
						@endforeach
						@endif
						@endforeach
					</select>
				</div>
				<div class="form-group option" id="linked-current-categories">
					<label>{{trans('cms.label_banners_site_categories')}}</label>
					<select class="form-control" id="current-category">
						<option value="0">{{trans('cms.select')}}</option>
						<?php foreach ($categories as $categoryLv1){?>
							<option value="<?=$categoryLv1->id?>"><?=$categoryLv1->name?></option>
							<?php if(isset($categoryLv1->categories)){?>
								<?php foreach($categoryLv1->categories as $categoryLv2): ?>
									<option value="<?=$categoryLv2->id?>">&nbsp;&nbsp;&nbsp;<?=$categoryLv2->name?></option>
									<?php if(isset($categoryLv2->categories)){?>
										<?php foreach($categoryLv2->categories as $categoryLv3): ?>
											<option value="<?=$categoryLv3->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$categoryLv3->name?></option>
											<?php if(isset($categoryLv3->categories)){?>
												<?php foreach($categoryLv3->categories as $categoryLv4): ?>
													<option value="<?=$categoryLv4->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$categoryLv4->name?></option>
												<?php endforeach; ?>
											<?php }?>
												
										<?php endforeach; ?>
									<?php }?>
								<?php endforeach; ?>
							<?php }?>
						<?php }?>
					</select>
					<div id="mini-loader" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="display:none; margin-top: 10px;">
						<img src="/images/loader.gif" style="widht: 30px; height: 30px;"/>
					</div>
				</div>
				<div class="form-group option" id="linked-product">
					<label>{{ trans('cms.label_carrusel_choose_product') }}</label>
					<select class="form-control" name="url_product" id="entity-product" disabled="true">
						<option value="0">{{trans('cms.select')}}</option>
					</select>
				</div>
				<div class="form-group option" id="linked-external">
					<label for="url">{{ trans('cms.label_carrusel_url') }}</label>
					<input type="text" id="entity-external" name="url" class="form-control" value="" placeholder="" maxlength="100" />
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
					<label for="image_url">{{ trans('cms.label_carrusel_url_image') }}</label><span id="suggested-size"> ({{trans('cms.suggested_size')}}: 1426x679)</span>
					<input type="file" id="image_url" name="image_url" required/>
					
				</div>
				<!--<div class="form-group">
					<label>{{ trans('cms.label_carrusel_status') }}</label>
					<select class="form-control" name="status" id="status">
						<option value="active">{{ trans('cms.active') }}</option>
						<option value="disabled">{{ trans('cms.disabled') }}</option>
					</select>
				</div>-->
			</div>
		</div>
		<div class="box-footer">
			<div class="form-group">
				<input type="submit" id="send-carrusel" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" disabled="true" />
				<a href="/admin/appearance/carrusel" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
			</div>
		</div>
	</div>
</form>

</section>

@endsection

@push('scripts')
<script>
$(function () {
	
	//-----------------------------------------------
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});
	
	//-----------------------------------------------
    function quitAlerts(){
		$(".has-error" ).each(function() {
			$(this).removeClass('has-error');
		});

		$(".alert-danger" ).css("display", "none");
		$(".error-text").empty();
    }
	
	//-----------------------------------------------
    function initializeEntities(){
         if ($("#type-category").is(':checked')){
            $("#linked-category").show();
            $("#linked-product").hide();
            $("#linked-external").hide();
			$('#linked-current-categories').hide();
        }

        if ($("#type-product").is(':checked')){
            $("#linked-product").show();
            $("#linked-category").hide();
            $("#linked-external").hide();
			$('#linked-current-categories').show();
        }

        if ($("#type-external").is(':checked')){
            $("#linked-external").show();
            $("#linked-product").hide();
            $("#linked-category").hide();
			$('#linked-current-categories').hide();
        }
		
		$('#send-carrusel').prop('disabled', false);
    }
	
	//-----------------------------------------------
	function findProductsRelated(params){
		$('#mini-loader').show();
		$('#entity-product').prop( "disabled", true );
		$.ajax({
			data: params,
			cache: false,
			url: "/admin/services/releated",
			type:  'post',
			success:  function (response) {
				var relatedProducts=jQuery.parseJSON(response);
				$('#entity-product').empty();
				$("#entity-product").append($("<option></option>")
									.text("<?= trans('cms.select')?>")
									.val(0));
				
				$.each(relatedProducts, function(key, element) {
					$("#entity-product").append(
						$("<option></option>")
							.text(element.name)
							.val(element.id)
					);
				});
				
				$('#entity-product').prop( "disabled", false );
				$('#mini-loader').hide();
			}
		});
	}
	
	//-----------------------------------------------
	function cleanSection(){
		$('#entity-category').val(0);
		$('#entity-external').val("");
		$('#entity-product').prop( "disabled", true )
							.empty()
							.append($("<option></option>")
								.text("<?= trans('cms.select')?>")
								.val(0))
							.val(0);

		$('#current-category').val(0);
	}
	
	//-----------------------------------------------
    $('input[type=radio]').on('change',function() {//Types
        $('#linked-category').hide();
        $('#linked-product').hide();
        $('#linked-external').hide();
        $('#linked-'+$(this).val()).show();
		if($(this).val() == "product"){
			$('#linked-current-categories').show();
		}

		cleanSection();
    });
	
	//-----------------------------------------------
	$('#current-category').on('change',function() {
		$('#relatedProductsSection').empty();
		var selectedCategory=$(this).val();
		if(parseInt(selectedCategory)!==parseInt(0)){
			var params= {
				"selectedCategory" : selectedCategory,
			};
			
			findProductsRelated(params);
		}else{
			$('#entity-product').prop( "disabled", true );
		}
    });
	
	initializeEntities();
	
	//-----------------------------------------------
	$("#send-carrusel").click(function(e) {
		quitAlerts();
		
        var success=true;
        var errorMessage="";
        var type=$("input[name=linked_to]:checked").val();
            
        if(type=="category" && $("#entity-category").val()==0){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.category')?>"+"</li>";
            $("#entity-category").parent().addClass("has-error");
            success=false;
        }else if(type=="product" && $("#entity-product").val()==0){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.product')?>"+"</li>";
            $("#entity-product").parent().addClass("has-error");
            success=false;
        }else if(type=="external" && $("#entity-external").val()==""){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.url')?>"+"</li>";
            $("#entity-external").parent().addClass("has-error");
            success=false;
        }

		
		/*var file=$("#image_url")[0].files;
		if(file.length == 0){
			errorMessage=errorMessage+"<li>"+"<?= trans('cms.image')?>"+"</li>";
			success=false;
		}*/

       if(!success){
			$("#error-text").append(errorMessage);
            $("#alert-error" ).slideDown("slow");
			
			e.preventDefault();
		}
		   
	});
	
	//-----------------------------------------------
	$('#form-create-carrusel').bootstrapValidator({
		fields: {
//			name: {
//				validators: {
//					notEmpty: { message: '' },
//					stringLength: {
//						max: 25,
//						message: ''
//					}
//				}
//			},
			image_url: {
				validators: {
					notEmpty: { 
						message: '<?= trans('cms.form_required_field') ?>' 
					},
					file: {
						extension: 'jpeg,jpg,png',
						type: 'image/jpeg,image/png',
						maxSize: 2097152,   // 2048 * 1024
						message: '<?= trans('cms.form_invalid_photo') ?>'
					}
				}
			}
		}
	});
});
</script>
@endpush