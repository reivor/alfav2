@extends('admin.layouts.main')

@section('title', trans('cms.categories'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa"></i>
        {{ trans('cms.footer') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.footer_home') }}</li>
	</ol>
</section>
<form id="form-make-footer" action="/admin/appearance/footer/save" method="post" role="form" enctype="multipart/form-data">
	<section class="content">
		<input type="hidden" id="selectedCategories" name="selectedCategories" value="">
		<?= csrf_field() ?>
		<div class="row">	
			<?php foreach($categories as $categoryLv1): ?>
				<div class="col-xs-1 col-sm-6 col-lg-6">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title"><?= $categoryLv1->name ?></h3>
						</div>
						<div class="box-body">
							<div class="tree_categories">
								<ul>
									<?php $payload = ['root' => true ,'id' => $categoryLv1->id ,'supreme_parent' => -1]; ?>
									<li data-payload='<?= json_encode($payload) ?>'>
										<?= $categoryLv1->name ?>
										<?php if(isset($categoryLv1->categories)){?>
											<ul>
												<?php foreach($categoryLv1->categories as $categoryLv2): ?>
													<?php $payload = ['id' => $categoryLv2->id,'supreme_parent' => $categoryLv1->id]; ?>
													<li data-jstree='{"selected":<?= in_array($categoryLv2->id,$selectedCategoriesAux)? "true": "false"?>}' data-payload='<?= json_encode($payload) ?>'><?= $categoryLv2->name ?>
														<?php if(isset($categoryLv2->categories)){?>
															<ul>
																<?php foreach($categoryLv2->categories as $categoryLv3): ?>
																	<?php $payload = ['id' => $categoryLv3->id, 'supreme_parent' => $categoryLv1->id] ?>
																	<li data-jstree='{"selected":<?= in_array($categoryLv3->id,$selectedCategoriesAux)? "true": "false"?>}' data-payload='<?= json_encode($payload) ?>'><?= $categoryLv3->name ?>
																	<?php if(isset($categoryLv3->categories)){?>
																		<ul>
																			<?php foreach($categoryLv3->categories as $categoryLv4): ?>
																				<?php $payload = ['id' => $categoryLv4->id, 'supreme_parent' => $categoryLv1->id]; ?>
																				<li data-jstree='{"selected":<?= in_array($categoryLv4->id,$selectedCategoriesAux)? "true": "false"?>}' data-payload='<?= json_encode($payload) ?>'><?= $categoryLv4->name ?>

																				<?php endforeach; ?>
																		</ul>
																	<?php }?>
																<?php endforeach; ?>
															</ul>
														<?php }?>
												<?php endforeach; ?>
											</ul>
										<?php }?>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-4">
				<div class="form-group">
					<input type="button" id="sendCategories" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
				</div>
			</div>

		</div>
	</section>
</form>

@endsection


@push('plugins')
<link rel="stylesheet" href="/themes/admin/vendor/jstree/dist/themes/default/style.min.css" />
<script src="/themes/admin/vendor/jstree/dist/jstree.min.js"></script>
@endpush

@push('scripts')

<script>
$(function() {
	var counterSelectedNodes = {};

	$(".tree_categories").jstree({
		'core' : {
			expand_selected_onload : true
		},
		"checkbox" : {
			keep_selected_style : false,
			three_state: false
		},
		"plugins" : [ "checkbox" ],
	}).bind("ready.jstree", function () {
		var selectedElmsAux = $(this).jstree("get_selected", true);
		var supremeParent;
		$.each(selectedElmsAux, function(i) {
			supremeParent=this.data.payload.supreme_parent;
			if(supremeParent !== -1){
				if (typeof counterSelectedNodes[supremeParent] === 'undefined') {
						counterSelectedNodes[supremeParent]=parseInt(1);
				}else{
						counterSelectedNodes[supremeParent]=parseInt(counterSelectedNodes[supremeParent])+parseInt(1);
				}
			}
		});        
	});
		
	$(".tree_categories").bind("select_node.jstree", function(event, data){
		var l=0;
		$( data.selected ).each(function() {
			l=l+1;
		});
		var node = data.instance.get_node(data.selected[l-1]);
		var supremeParent = node.data.payload.supreme_parent;
		if(supremeParent !== -1){
			if (typeof counterSelectedNodes[supremeParent] === 'undefined') {
					counterSelectedNodes[supremeParent]=parseInt(1);
			}else{
					counterSelectedNodes[supremeParent]=parseInt(counterSelectedNodes[supremeParent])+parseInt(1);
			}
		}

		if(parseInt(counterSelectedNodes[supremeParent]) > <?=App\Models\FooterCategory::MAX_CATEGORIES_LEVEL?>){
			data.instance.deselect_node(data.node);
			bootbox.alert("<?=trans('cms.error_category_exceeds')?>", function() {  
			});
		}
	});

	$(".tree_categories").bind("deselect_node.jstree", function(event, data) {
		var supremeParent=data.node.data.payload.supreme_parent;
		counterSelectedNodes[supremeParent]=parseInt(counterSelectedNodes[supremeParent])-parseInt(1);
		//console.log(counterSelectedNodes);
	});
	

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});

	$('#sendCategories').click(function(e) {
		var selectedElmsIds = {};
		$('.tree_categories').each(function(j) {
			var selectedElms = $(this).jstree("get_selected", true);
			$.each(selectedElms, function(i) {
				selectedElmsIds[this.data.payload.id]=this.data.payload.supreme_parent;
			});

		});
		var selectedCategories = JSON.stringify(selectedElmsIds);
		$("#selectedCategories").val(selectedCategories);
		$( "#form-make-footer" ).submit();
	});

});
</script>
@endpush
