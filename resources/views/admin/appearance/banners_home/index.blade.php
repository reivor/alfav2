@extends('admin.layouts.main')


@section('title', trans('menu.banners_home'))


@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-image"></i>
        {{ trans('menu.banners_home') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.banners_home') }}</li>
	</ol>
</section>
<section class="content">
	<form id="form-banners-home" action="/admin/appearance/banners_home/save" method="post" role="form" enctype="multipart/form-data" novalidate>
		<?= csrf_field() ?>
		<div class="row">
			<div class="col-xs-6 col-sm-6">
				<input type="hidden" id="number-items" name="number-items" value="" />
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.add_banner_home') }}</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>{{ trans('cms.choose_banner_home') }}</label>
									<select class="form-control" name="type-layout" id="type-layout">
										<?php $numberItems=0; ?>
										<option value="0" >{{ trans('cms.select') }}</option>
										<?php for($i=1; $i<=6; $i++):?>
											<option value="<?=$i?>" <?php if ($i==$layout) echo 'selected="selected"';?>>Layout <?= $i ?></option>
										<?php endfor; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row" id="content-layout">

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6" id="data-banner-home">

			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6" id="data-banner-home" style="margin-top: 20px;">
				<input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
			</div>
		</div>
	</form>
</section>

<div class="modal fade" id="modal-form-banner" tabindex="-1" role="dialog" aria-labelledby="modal-good-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="dismiss-modal">×</button>
				<i class="fa fa-image"></i> <span id="title-modal" style="font-size:18px;"></span>
			</div>
			<div class="modal-body">
				<input type="hidden" id="current-index-banner" value="" />
				<!--Alertas-->
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="alert alert-dismissable alert-danger" id="alert-error-modal" style="display: none">
							{{trans('cms.form_requierd_field_many')}}:
							<ul id="error-text-modal"class="error-text"></ul>
						</div>
					</div>
				</div>
				<!--Fin alertas-->
				<div class="form-group">
					<label>{{ trans('cms.label_carrusel_linked_to') }}</label>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-category" value="category" checked="" data-index="">
							{{ trans('cms.label_carrusel_category') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-product" value="product" data-index="">
							{{ trans('cms.label_carrusel_product') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-external" value="external" data-index="">
							{{ trans('cms.label_carrusel_external_link') }}
						</label>
					</div>
				</div>
				<div class="form-group option" id="linked-category">
					<label>{{ trans('cms.label_carrusel_choose_category') }}</label>
					<select class="form-control" id="entity-category">
						<option value="0">{{trans('cms.select')}}</option>
						<?php foreach ($categories as $categoryLv2){?>
							<option value="<?=$categoryLv2->id?>"><?=$categoryLv2->name?></option>
							<?php if(isset($categoryLv2->categories)){?>
								<?php foreach($categoryLv2->categories as $categoryLv3): ?>
									<option value="<?=$categoryLv3->id?>">&nbsp;&nbsp;&nbsp;<?=$categoryLv3->name?></option>
									<?php if(isset($categoryLv3->categories)){?>
										<?php foreach($categoryLv3->categories as $categoryLv4): ?>
											<option value="<?=$categoryLv4->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$categoryLv4->name?></option>
										<?php endforeach; ?>
									<?php }?>
								<?php endforeach; ?>
							<?php }?>
						<?php }?>
					</select>
				</div>
				<div class="form-group option" id="linked-product">
					<label>{{ trans('cms.label_carrusel_choose_product') }}</label>
					<select class="form-control" id="entity-product">
						<option value="0">{{trans('cms.select')}}</option>
						<?php foreach ($products as $product) {?>
							<option value="<?= $product->id ?>"><?= $product->name ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group option" id="linked-external">
					<label for="url">{{ trans('cms.label_carrusel_url') }}</label>
					<input type="text" id="entity-external" class="form-control" value="" placeholder="" maxlength="100" />
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group image-content">
					<label>{{ trans('cms.label_carrusel_url_image') }}</label> <span id="suggested-size"></span>
					<input type="file" class="image-url" name="image" />
				</div>
				<div class="form-group image-mobile-content">
					<label>{{ trans('cms.add_image_mobile') }}</label> <span>({{trans('cms.suggested_size')}} 497x360)</span>
					<input type="file" class="image-mobile-url" name="image-mobile" />
				</div>
				<div class="form-group" id="banner-status">
					<label>{{ trans('cms.label_status') }}</label>
					<select class="form-control" id="status-banner">
						<?php foreach ($status as $st){?>
							<option value="<?=$st?>"><?=$st?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<input type="button" id="send-banner" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('templates')
<script id="tpl-layout" type="text/x-handlebars-template">
	<div class="image-container-thumb" rel="@{{ index }}" id="image-@{{ index }}">
		<img id="image-src-@{{ index }}" src="@{{banner.image_url}}"/>
	</div>
	<input type="hidden" id="image-get-@{{ index }}" name="image-get[@{{ index }}]" value="@{{banner.image_url}}" />
	<input type="hidden" id="image-mobile-get-@{{ index }}" name="image-mobile-get[@{{ index }}]" value="@{{banner.image_mobile_url}}" />
	<input type="hidden" id="type-@{{ index }}" class="type" name="type[@{{ index }}]" value="@{{banner.type}}" />
	<input type="hidden" id="entity-@{{ index }}" class="entity" name="entity[@{{ index }}]" value="@{{banner.entity_id}}" />
	<input type="hidden" id="external-@{{ index }}" name="external[@{{ index }}]" value="@{{banner.external_url}}" />
	<input type="hidden" id="status-@{{ index }}" name="status[@{{ index }}]" value="@{{banner.status}}" />
</script>

<script id="tpl-layout-1" type="text/x-handlebars-template">
	<div class="grid" id="layout-1">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width6 grid-item--height2 item-1"></div>
	 </div>
</script>

<script id="tpl-layout-2" type="text/x-handlebars-template">
	<div class="grid" id="layout-2">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width2 grid-item--height2 item-1"></div>
		<div class="grid-item grid-item--width4 grid-item--height2 item-2"></div>
	</div>
</script>

<script id="tpl-layout-3" type="text/x-handlebars-template">
	<div class="grid">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width3 grid-item--height2 item-1"></div>
		<div class="grid-item grid-item--width3 grid-item--height2 item-2"></div>
	</div>
</script>

<script id="tpl-layout-4" type="text/x-handlebars-template">
	<div class="grid">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width2 grid-item--height2 item-1"></div>
		<div class="grid-item grid-item--width2 grid-item--height2 item-2"></div>
		<div class="grid-item grid-item--width2 grid-item--height2 item-3"></div>
	</div>
</script>

<script id="tpl-layout-5" type="text/x-handlebars-template">
	<div class="grid">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width4 grid-item--height2 item-1"></div>
		<div class="grid-item grid-item--width2 item-2"></div>
		<div class="grid-item grid-item--width2 item-3"></div>
	</div>
</script>

<script id="tpl-layout-6" type="text/x-handlebars-template">
	<div class="grid">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width2 grid-item--height2 item-1"></div>
		<div class="grid-item grid-item--width2 item-2"></div>
		<div class="grid-item grid-item--width2 grid-item--height2 item-3"></div>
		<div class="grid-item grid-item--width2 item-4"></div>
	</div>
</script>

@endpush

@push('plugins')
<link rel="stylesheet" href="/themes/admin/css/layout-banners.css" />
<script src="/themes/admin/vendor/packery/dist/packery.pkgd.min.js"></script>
@endpush



@push('scripts')
<script>
	
	var banners=<?= json_encode($banners)?>;
	var modal=$("#modal-form-banner");

	
$(function () {

    //-----------------------------------------------
    function quitAlerts(){
	$(".has-error" ).each(function() {
            $(this).removeClass('has-error');
	});
	$(".alert-danger" ).css("display", "none");
	$(".error-text").empty();
    }
    
    //-----------------------------------------------
    function initializeEntities(){
         if ($("#type-category").is(':checked')){
            $("#linked-category").show();
            $("#linked-product").hide();
            $("#linked-external").hide();
        }

        if ($("#type-product").is(':checked')){
            $("#linked-product").show();
            $("#linked-category").hide();
            $("#linked-external").hide();
        }

        if ($("#type-external").is(':checked')){
            $("#linked-external").show();
            $("#linked-product").hide();
            $("#linked-category").hide();
        }
    }
	
	//-----------------------------------------------
	function numberItemsLayout(layout){
		switch(parseInt(layout)){
			case 1:
				return 1;
			case 2:
			case 3:
				return 2;
			case 4:
			case 5:
				return 3;
			case 6:
				return 4;		
		}
	}
	
	//-----------------------------------------------
	function suggestedSize (indexImage, layout){
		layout=parseInt(layout);
		indexImage=parseInt(indexImage);
		
		if(layout==1){
			return "1200x400";
		}else if(layout==2){
			if(indexImage==1){
				return "400x400";
			}else{
				return "800x400";
			}
		}else if(layout==3){
			return "600x400";
		}else if(layout==4){
			return "400x400";
		}else if(layout==5){
			if(indexImage==1){
				return "800x400";
			}else{
				return "400x200";
			}
		}else if(layout==6){
			if(indexImage==1 || indexImage==3){
				return "400x400";
			}else{
				return "400x200";
			}
		}
	}
	
	//-----------------------------------------------
	function constructLayout(firstTime, typeLayout){
		var containerLayout = $('#data-banner-home');
		containerLayout.empty();
		
		if(parseInt(typeLayout)!=0){
			var numberItems=numberItemsLayout(typeLayout);
			$("#number-items").val(numberItems);//Numero imagenes

			var templateLayout = Handlebars.compile($('#tpl-layout').html());
			var templateContentLayout = Handlebars.compile($('#tpl-layout-'+typeLayout).html());
			containerLayout.append(templateContentLayout);

			$('.grid').packery({
				// options
				itemSelector: '.grid-item',
				gutter: 4,
				rowHeight: '.grid-sizer',
				percentPosition: true
			});

			for ( var i = 1, j = parseInt(numberItems); i <= j; i++ ) {	

				if(firstTime){
					if(banners[i] === undefined || banners[i] === null){
						banners[i]={has_image: 0, image_url: '/images/placeholder.png', has_mobile_image: 0};
					}else{
						banners[i].has_image=1;
						banners[i].has_mobile_image=1;
					}

				}else{
					banners[i]={has_image: 0, image_url: '/images/placeholder.png', has_mobile_image: 0};
				}

				var htmlLayout = templateLayout({ index: i, banner: banners[i]});
				$(".item-"+i).append(htmlLayout);
			}
			
			$(".image-container-thumb").click(function() {
				createBanner($(this));
			});
		}
	}
	
	//-----------------------------------------------
	function cleanSection(){
		$('#entity-category').val(0);
		$('#entity-product').val(0);
		$('#entity-external').val("");
	}
	
	//-----------------------------------------------
	function createBanner(content){
		quitAlerts();
		var currentBannerIndex=content.attr('rel');
		$("#current-index-banner").val(currentBannerIndex);
		
		var title="<?= trans('cms.data_banner_home')?>"+" "+currentBannerIndex;
		$("#title-modal").text(title);

		var type=$("#type-"+currentBannerIndex).val();
		var entity=$("#entity-"+currentBannerIndex).val();
		var status=$("#status-"+currentBannerIndex).val();

		$('.image-url', modal).val("");
		$('.image-mobile-url', modal).val("");

		if(type!=""){
			$('input[name="type-radio"][value="' + type + '"]').prop('checked', true);
			if(type=="external"){
				$('#entity-'+type).val($("#external-"+currentBannerIndex).val());
			}else{
				if(entity){
					$('#entity-'+type).val(entity);
				}
			}
			
			$('#status-banner').val(status);
		}else{
			$('input[name="type-radio"][value="category"]').prop('checked', true);
			cleanSection();
		}

		initializeEntities();
		var size=suggestedSize(currentBannerIndex,$("#type-layout").val());
		$('#suggested-size').text("(<?= trans('cms.suggested_size')?>: "+size+")");
		

		$('#modal-form-banner').modal('show');
	}
	
	//-----------------------------------------------
    $('input[type=radio]').on('change',function() {//Types
        $('#linked-category').hide();
        $('#linked-product').hide();
        $('#linked-external').hide();
        $('#linked-'+$(this).val()).show();
		
		cleanSection();
    });
	
	//-----------------------------------------------

    $("#type-layout").change(function(){
		$('.image-banner').remove();
		$('.image-banner-mobile').remove();
		constructLayout(false, $(this).val());
    });
	
	//-----------------------------------------------
    $("#send-banner").click(function() {
        quitAlerts();
		
        var success=true;
        var errorMessage="";
        
		var currentBanner= $("#current-index-banner").val();
        var type=$("input[name=type-radio]:checked").val();
            
        if(type=="category" && $("#entity-category").val()==0){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.category')?>"+"</li>";
            $("#entity-category").parent().addClass("has-error");
            success=false;
        }else if(type=="product" && $("#entity-product").val()==0){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.product')?>"+"</li>";
            $("#entity-product").parent().addClass("has-error");
            success=false;
        }else if(type=="external" && $("#entity-external").val()==""){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.url')?>"+"</li>";
            $("#entity-external").parent().addClass("has-error");
            success=false;
        }

		if(banners[currentBanner].has_image==0){
			if($('.image-url', modal).val()==''){
				$('.image-url', modal).parent().addClass("has-error");
				errorMessage=errorMessage+"<li>"+"<?= trans('cms.image')?>"+"</li>";
				success=false;
			}	
		}
		
		if(banners[currentBanner].has_mobile_image==0){
		
			if($('.image-mobile-url', modal).val()==''){
				$('.image-mobile-url', modal).parent().addClass("has-error");
				errorMessage=errorMessage+"<li>"+"<?= trans('cms.label_image_mobile')?>"+"</li>";
				success=false;
			}	
		}

        if(success){
            var entity="";
            $('#type-'+currentBanner).val(type);
            entity=$("#entity-"+type).val();
			if(type=="external"){
				$('#external-'+currentBanner).val(entity);
			}else{
				$('#entity-'+currentBanner).val(entity);
			}
			
			var status = $("#status-banner").val();
			$("#status-"+currentBanner).val(status);

			/* FILE IMAGE*/
			var file=$('.image-url', modal);
			var fileMobile=$('.image-mobile-url', modal);
			
			if(file.val()!="" ){
				var imageClone=$('.image-url', modal).clone();
				$("input[name='image["+currentBanner+"]'").remove();
				file.appendTo("#form-banners-home")
						.attr('name', 'image['+currentBanner+']')
						.addClass('image-banner')
						.css("opacity", 0);
				imageClone.appendTo(".image-content");
				banners[currentBanner].has_image=1;
				
				if (file[0].files && file[0].files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#image-src-'+currentBanner).attr('src', e.target.result);
					}
					reader.readAsDataURL(file[0].files[0]);
				}
			}
			
			if(fileMobile.val()!=""){
				var imageMobileClone=$('.image-mobile-url', modal).clone();
				$("input[name='imageMobile["+currentBanner+"]'").remove();
				fileMobile.appendTo("#form-banners-home")
						.attr('name', 'imageMobile['+currentBanner+']')
						.addClass('image-banner-mobile')
						.css("opacity", 0);
				imageMobileClone.appendTo(".image-mobile-content");
				banners[currentBanner].has_mobile_image=1;
			}
			
            $('#modal-form-banner').modal('hide');
        }else{
            $("#error-text-modal").append(errorMessage);
            $("#alert-error-modal" ).slideDown("slow");
        }
    });
	
	//-----------------------------------------------
    $('#form-banners-home').submit(function(e) {
		var success=true;
		var message="";
		var typeLayout= parseInt($("#type-layout").val());
		
		var selectedBanners=$(".type");
		
		if(typeLayout==0){
			success=false;
			message="<?= trans('cms.banners_home_layout_empty')?>";
		}else{
			
			var numberItems= parseInt($("#number-items").val());
			for (i=0; i<numberItems; i++){
				if(selectedBanners[i].value=="" || selectedBanners[i].value==null){
					message="<?= trans('cms.lack_data_banner_home')?>";
					success=false;
					break;
				}	
			}
		}

		if(!success){
			bootbox.alert(message, function() {  
			});
		}
		return success;
    }); 
	
	//-----------------------------------------------
	
	constructLayout(true,$('#type-layout').val());
});

</script>
@endpush


