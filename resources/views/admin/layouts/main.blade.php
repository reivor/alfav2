<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>CMS Alfa | @yield('title', 'page')</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="icon" type="image/x-icon" href="/favicon.ico" />
		<link rel="stylesheet" href="/themes/admin/vendor/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/fontawesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/ionicons/css/ionicons.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/AdminLTE/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/AdminLTE/dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/themes/admin/css/styles.css">
		<link rel="stylesheet" href="/themes/admin/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
		<link rel="stylesheet" href="/themes/admin/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput-typeahead.css">
		<link rel="stylesheet" href="/themes/admin/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	</head>
	<body class="hold-transition skin-yellow sidebar-mini">

		<div class="wrapper">

			@include('admin.partials.header')
			
			@if ($sessionUser->role == 'serviciocliente')
				@include('admin.partials.sidebar2')
			@else
				@include('admin.partials.sidebar')
			@endif


			<div class="content-wrapper">
				@include('admin.partials.flash')
				@yield('content')
			</div>

			@include('admin.partials.footer')

			@include('admin.partials.control_sidebar')
			
		</div>

		
		<script src="/themes/admin/vendor/jquery/dist/jquery.min.js"></script>
		<script src="/themes/admin/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/themes/admin/vendor/jquery-ui/jquery-ui.min.js"></script>
		
		<!-- plugins -->
		<script src="/themes/admin/vendor/moment/min/moment-with-locales.min.js"></script>
		<script src="/themes/admin/vendor/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="/themes/admin/vendor/AdminLTE/plugins/fastclick/fastclick.js"></script>
		<script src="/themes/admin/vendor/bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>
		<script src="/themes/admin/vendor/handlebars/handlebars.min.js"></script>
		<script src="/themes/admin/vendor/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
		<script src="/themes/admin/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
		<script src="/themes/admin/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
		<script src="/themes/admin/vendor/bootstrap3-typeahead/bootstrap3-typeahead.min.js"></script>
		<script src="/themes/admin/vendor/bootbox.js/bootbox.js"></script>
		<script src="/themes/admin/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
		
		<!-- app scripts -->
		<script src="/themes/admin/vendor/AdminLTE/dist/js/app.min.js"></script>
		<script src="/themes/admin/js/app.js"></script>
		<script src="/themes/admin/js/forms.js"></script>
		
		<script>
			$(document).on( "click",'[data-confirm]', function(e) {
				var href=$(this).prop('href');
				bootbox.confirm({
					message: $(this).data('confirm'),
					buttons: {
						'confirm': {
							label: '<?=trans('cms.btn_accept') ?>',
							className: 'btn-default'
						},
						'cancel': {
							label: '<?=trans('cms.btn_cancel') ?>',
							className: 'btn-danger'
						}
					},
					callback: function(result) {
						if (result) {
							 window.location.href=href;
						}
					}
				});
				 e.preventDefault();
			});
		</script>
		
		@stack('templates')
		@stack('plugins')
		@stack('scripts')
	</body>
</html>
