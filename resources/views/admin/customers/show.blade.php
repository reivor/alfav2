@extends('admin.layouts.main')

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-customers"></i>
		{{ trans('menu.customers') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/customers') }}">
				<i class="fa fa-user"></i>
				{{ trans('menu.customers') }}
			</a>
		</li>
        <li class="active">
			{{ $customer->name }}
		</li>
	</ol>
</section>

<section class="content">
	
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{ trans('menu.customers') }}</h3>
			<div class="box-tools pull-right">
				<?php if($customer->subscribe = 1) { ?>
					<a href="{{ url("admin/customers/unsubscribe/{$customer->id}") }}" class="btn btn-box-tool"><i class="fa fa-ban" aria-hidden="true" title="Dar de baja en Newsletter"></i></a>
				<?php } ?>
			</div>
		</div>
		<div class="box-body">
			<div class="col-xs-6 col-sm-4">
				<div class="box box-widget widget-user">
					<div class="widget-user-header bg-green">
						<h3 class="widget-user-username">
							<?= $customer->name ?>
						</h3>
						<h5 class="widget-user-desc"><?= $customer->role ?></h5>
					</div>
					<div class="widget-user-image">
						<?php if( !empty($customer->avatar) ): ?>
							<img class="img-circle" src="<?= $customer->avatar ?>" alt="<?= $customer->name ?>">
							<?php else: ?>
							<img class="img-circle" src="/images/users/avatar.png" alt="<?= $customer->name ?>">
						<?php endif; ?>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-sm-6 text-center">
								&nbsp;
							</div>
							<div class="col-sm-6 text-center">
								&nbsp;
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-sm-4">
				<dl class="dl-horizontal">
					<dt>{{ trans('cms.label_user_name') }}</dt>
					<dd>{{ $customer->name }}</dd>
					<dt>{{ trans('cms.label_user_email') }}</dt>
					<dd>{{ $customer->email }}</dd>
					<dt>{{ trans('cms.label_customer_job') }}</dt>
					<dd>{{ $customer->job }}</dd>
					<dt>{{ trans('cms.label_user_role') }}</dt>
					<dd>{{ $customer->role }}</dd>
					<dt>{{ trans('cms.customer_newsletter_susbcribe') }}</dt>
					<dd>{{ $customer->subscribe }}</dd>
					<dt>{{ trans('cms.label_user_status') }}</dt>
					<?php if( $customer->isActive() ): ?>
					<dd><span class="text-success">{{ $customer->status }}</span></dd>
					<?php elseif( $customer->isBlocked() ): ?>
					<dd><span class="text-danger">{{ $customer->status }}</span></dd>
					<?php elseif( $customer->isPending() ): ?>
					<dd><span class="text-muted">{{ $customer->status }}</span></dd>
					<?php endif; ?>
					<dt>{{ trans('cms.label_user_member_since') }}</dt>
					<dd>{{ $customer->created_at->format('d/m/Y') }}</dd>
					<?php if( !empty($customer->fb_id) ): ?>
					<hr>
					<dt><i class="fa fa-facebook"></i></dd>
					<?php endif; ?>
					<dd>{{ $customer->fb_id }}</dd>
					<?php if( !empty($customer->tw_id) ): ?>
					<dt><i class="fa fa-twitter"></i></dd>
					<dd>{{ $customer->tw_id }}</dd>
					<?php endif; ?>
              </dl>
			</div>
		</div>
	</div>

</section>

@endsection