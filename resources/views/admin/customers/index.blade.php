@extends('admin.layouts.main')

@section('title', trans('menu.customers'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-user"></i>
		{{ trans('menu.customers') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li class="active">
			<i class="fa fa-user"></i>
			{{ trans('menu.customers') }}
		</li>
	</ol>
</section>

<section class="content">
	
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{ trans('menu.customers') }}</h3>
		</div>
		<div class="box-body">
			<?php foreach($customers as $customer): ?>
			<div class="col-xs-6 col-sm-3">
				<a href="{{ url('admin/customers/show/'.$customer->id) }}" class="text-green">
					<div class="box box-widget widget-user">
						<div class="widget-user-header bg-green">
							<h3 class="widget-user-username">
								<?= $customer->name ?>
							</h3>
							<h5 class="widget-user-desc"><?= $customer->email ?></h5>
						</div>
						<div class="widget-user-image">
							<?php if( !empty($customer->avatar) ): ?>
							<img class="img-circle" src="<?= $customer->avatar ?>" alt="<?= $customer->name ?>">
							<?php else: ?>
							<img class="img-circle" src="/images/users/avatar.png" alt="<?= $customer->name ?>">
							<?php endif; ?>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-sm-12 text-center">
									<span class="text-black"><?= trans('cms.label_customer_job') ?>:</span>
									<?= $customer->job ?>
								</div>
								<div class="col-sm-12 text-center">
									<span class="text-black"><?= trans('cms.label_user_status') ?>:</span>
									<?php if( $customer->isActive() ): ?>
									<span class="text-success"><?= $customer->status ?></span>
								<?php elseif( $customer->isBlocked() ): ?>
								<span class="text-danger"><?= $customer->status ?></span>
							<?php elseif( $customer->isPending() ): ?>
							<span class="text-muted"><?= $customer->status ?></span>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</a>
</div>
<?php endforeach; ?>
</div>
</div>

{!! $customers->links() !!}

</section>

@endsection