@extends('admin.layouts.main')
@section('title', trans('menu.settings'))
@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-image"></i>
		{{ trans('menu.settings') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li class="active">{{ trans('menu.settings') }}</li>
	</ol>
</section>

<section class="content">
	<form method="post" role="form" id="form-settings">
		{{csrf_field()}}

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('menu.settings') }}</h3>
			</div>
			<div class="box-body">

				
				<div class="form-group has-feedback">
					<label for="email">{{trans('cms.email_settings_label')}}</label>
					<input type="email" class="form-control" name="alfa_email" id="alfa_email" value="{{isset($settings['alfa_email'])?$settings['alfa_email']:''}}">
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group has-feedback">
					<label for="meta_description">{{trans('cms.meta_description_label')}}</label>
					<input type="text" class="form-control" name="meta_description" id="meta_description" value="{{isset($settings['meta_description'])?$settings['meta_description']:''}}">
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group has-feedback">
					<label for="meta_keywords">{{trans('cms.meta_keywords_label') }}</label>
					<input type="text" class="form-control" name="meta_keywords" id="meta_keywords" value="{{isset($settings['meta_keywords'])?$settings['meta_keywords']:''}}">
					<p class="help-block">{{ trans('cms.help_keywords')}}</p>
					<div class="help-block with-errors"></div>
				</div>
				
			</div>

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">{{trans('menu.save') }}</button>
			</div>
		</div>
	</form>
</section>

@endsection

@push('styles')
<link rel="stylesheet" href="/themes/store/vendor/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
@endpush

@push('scripts')
<script src="/themes/store/vendor/bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>
<script>
$(function () {

	$('#form-settings').bootstrapValidator({
		fields: {
			alfa_email: {
				validators: {
					notEmpty: {message: '<?= trans('auth.form_required_field') ?>'},
					emailAddress: {message: '<?= trans('auth.form_invalid_email') ?>'},
				}
			}

		}
	});

});

</script>

@endpush