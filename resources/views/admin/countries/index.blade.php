@extends('admin.layouts.main')


@section('title', trans('cms.countries'))


@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-home"></i>
            {{ trans('cms.countries') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li class="active">{{ trans('menu.countries') }}</li>
        </ol>
    </section>

    <section class="content">

        <div class="row margin-bottom">
            <div class="col-xs-12">
                <a href="/admin/countries/create" class="btn btn-primary">
                    <i class="fa fa-plus-circle"></i>
                    {{ trans('cms.add_country') }}
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('cms.countries') }}</h3>
                    </div>
                    <div class="box-body">
                        <?php if( $countries->isEmpty() ): ?>
                        <div class="alert alert-warning"><?= trans('cms.no_countries_found') ?></div>
                        <?php else: ?>
                        <div class="row">
                            <div class="grid col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('cms.label_name') }}</th>
                                            <th>{{ trans('cms.code') }}</th>
                                            <th>{{ trans('cms.details') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                <?php foreach($countries as $country): ?>
                                            <tr>
                                                <th width="10%"><?= $country->id ?></th>
                                                <td width="50%"><a href="<?= url('admin/departments/index/' . $country->id) ?>"> <?= $country->name ?> </a></td>
                                                <td width="15%"><?= $country->code ?></td>
                                                <td width="25%">
                                                    <a href="<?= url('admin/countries/edit/' . $country->id) ?>" class="btn btn-warning"> {{ trans('cms.edit') }} </a>
                                                    <a href="<?= url('admin/countries/delete/' . $country->id) ?>" class="btn btn-danger" onclick="confirm('{{ trans('cms.confirm_delete_countries') }}');"> {{ trans('cms.delete') }} </a>
                                                </td>
                                            </tr>
                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection