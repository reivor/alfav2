@extends('admin.layouts.main')

@section('title', trans('cms.countries'))

@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-home"></i>
            {{ trans('cms.countries') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/countries') }}">
                    <i class="fa fa-home"></i>
                    {{ trans('menu.stores') }}
                </a>
            </li>
            <li class="active">
                {{ $action }}
            </li>
        </ol>
    </section>

    <section class="content">
        <form id="form-create-country" method="post" enctype="multipart/form-data" role="form">
            <?= csrf_field() ?>
            <input type="hidden" name="id" value="{{ $country->id }}" />
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('cms.countries') }}</h3>
                </div>
                <div class="box-body">
                    <div class="form-group form-group-lg has-feedback">
                        <label for="name">{{ trans('cms.label_name') }}</label>
                        <input type="text" id="name" name="name" class="form-control" value="{{ $country->name or old('name') }}" placeholder="{{ trans('cms.label_name') }}" maxlength="255">
                    </div>
                    <div class="form-group form-group-lg has-feedback">
                        <label for="code">{{ trans('cms.code') }}</label>
                        <input type="text" id="code" name="code" class="form-control" value="{{ $country->code or old('code') }}" placeholder="{{ trans('cms.code') }}">
                    </div>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
                        <a href="{{ url('admin/countries') }}" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
                    </div>
                </div>
            </div>
        </form>
    </section>

@endsection


@push('scripts')
<script>
    $(function () {
        console.log('ready');
        $('#form-create-country').bootstrapValidator({
            fields: {
                name: {
                    validators: {
                        notEmpty: { message: '<?= trans('cms.form_required_field') ?>' }
                    }
                },
                code: {
                    validators: {
                        notEmpty: { message: '<?= trans('cms.form_required_field') ?>' },
                        stringLength: {
                            min: 2,
                            message: '<?= trans('cms.form_country_code') ?>'
                        },
                        stringCase: {
                            message: '<?= trans('cms.form_country_code_upper') ?>',
                            'case': 'upper'
                        }
                    }
                }
            }
        });
    });
</script>
@endpush