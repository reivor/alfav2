@extends('admin.layouts.main')
@section('title', trans('cms.page'))
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('menu.pages') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.page') }}</li>
	</ol>
</section>

<section class="content">
	<div class="row margin-bottom">
		<div class="col-lg-12">
			<a href="/admin/page/form" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_page') }}
			</a>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<?= csrf_field() ?>
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.page_list') }}</h3>
				</div>
				<div class="box-body">
					<div class="row header-titles hidden-sm">
					<!--
						<div class="col-md-3">
							<span>{{ trans('cms.image_header_article') }}</span>
						</div>
						-->
							<div class="col-md-3">
							<span>{{ trans('cms.title_page') }}</span>
						</div>

						<div class="col-md-2">
							<span>{{ trans('cms.content_page') }}</span>
						</div>

						<div class="col-md-2">
							<span>{{ trans('cms.url_page') }}</span>
						</div>

						<div class="col-md-2">
							<span>{{ trans('cms.date_publishing') }}</span>
						</div>
						<div class="col-md-1">
							<span>{{ trans('cms.product_status') }}</span>
						</div>
						<div class="col-md-2">
							<span>{{ trans('cms.label_actions') }}</span>
						</div>
					</div>

					<?php foreach ($pages as $page) { ?>
					<div class="row margin-bottom">
					<!--	<div class="col-md-3">
							<img id="header" src="<?=$page->image_header?>" width="150"/>
						</div>-->
						<div class="col-md-3" >
							 <a href="/admin/page/form/<?=$page->id?>"> <?= htmlspecialchars($page->title) ?>  </a>	
						</div>

						<div class="col-md-2">
							 <pre><?= htmlspecialchars($page->body) ?> </pre>
						</div>

						<div class="col-md-2">
						<a href="/<?= $page->url ?>">  <?= $page->url ?> </a>	
						</div>

						<div class="col-md-2">
							<?php echo($page->date_publishing==NULL) ? "" :$page->date_publishing->format('d/m/Y') 
							?>
						</div>
						
						<div class="col-md-1">
							<span class="label <?php echo ($page->status==$page::STATUS_ACTIVE) ? 'label-success' : 'label-default'?>"><?= $page->status ?></span>					
						</div>
						<div class="col-md-2">
							<a href="/admin/page/form/<?=$page->id?>" class="blog-actions">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</a>
							<!--
							<a href="/admin/blog/points/<?=$page->id?>" class="blog-actions">
								<i class="fa fa-crosshairs" aria-hidden="true"></i>
							</a>
							-->
							<a href class="delete-page" rel="<?= $page->id ?>" data-sure="<?= trans('cms.confirm_delete_carrusel') ?>">
								<i class="fa fa-trash" aria-hidden="true" style="color:red"></i>
							</a>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});
	$(".delete-page").click(function(e) { 
		e.preventDefault();
		var index=$(this).attr('rel');
		//console.log(index);


		bootbox.confirm({
			message: $(this).data('sure'),
			buttons: {
				'confirm': {
					label: '<?=trans('cms.btn_accept') ?>',
					className: 'btn-default'
				},
				'cancel': {
					label: '<?=trans('cms.btn_cancel') ?>',
					className: 'btn-danger'
				}
			},
			callback: function(result) {
				if (result) {
					var params= {
						"id" : index
					};
					$.post( "/admin/page/delete-page", params, function(data) {
						window.location.href = "/admin/page/pages";
					});
				}
			}
		});


	});
</script>
@endpush