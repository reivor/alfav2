@extends('admin.layouts.main')

@section('content')

<section class="content-header">
	<h1>
		Dashboard
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-dashboard active"></i>   {{ trans('cms.dashboard') }}</li>
		
	</ol>
</section>

<section class="content">
	<div class="row">

		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>{{ $subscribers_count }}</h3>
					<p>{{trans('cms.customer_newsletter')}}</p>
				</div>
				<div class="icon">
					<i class="ion ion-person-add"></i>
				</div>
				<a href="/admin/customers/subscribers" class="small-box-footer">{{trans('cms.detail')}} <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>{{ $quotes_count }}</h3>
					<p>{{trans('cms.new_quotes')}}</p>
				</div>
				<div class="icon">
					<i class="fa fa-calculator"></i>
				</div>
				<a href="/admin/quotes" class="small-box-footer">{{trans('cms.detail')}} <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

	</div>

	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{trans('cms.regiones')}}</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
				<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-4">
					<a href="/admin/departments">
						<div class="panel panel-default">
							<div class="panel-body">
								{{trans('cms.regiones_ciudades')}}
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{trans('cms.products')}}</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
				<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-4">
					<a href="/admin/catalog/products/uploadlog">
						<div class="panel panel-default">
							<div class="panel-body">
								{{trans('cms.products_uploads')}}
							</div>
						</div>
					</a>
				</div>
				<div class="col-xs-4">
					<a href="/admin/catalog/products/tags">
						<div class="panel panel-default">
							<div class="panel-body">
								Tags
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

</section>

@endsection