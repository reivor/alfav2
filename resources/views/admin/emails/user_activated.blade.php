<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Alfa</title>
</head>
<body>

<table width="600" align="center" border="0">
    <tr>
        <td align="cetner">
            <img src="{{ url('/images/alfa.png') }}" alt="alfa.com" />
        </td>
    </tr>
    <tr>
        <td>
            <h1>¡Bienvenido a alfa!</h1>
            <p>
                Su cuenta ha sido activada por el equipo de Alfa.
            </p>
            <p>
                Para ingresar, haz click <a href="{{ route('auth/login') }}" target="_blank">aquí</a>.
            </p>
        </td>
    </tr>
</table>

</body>
</html>