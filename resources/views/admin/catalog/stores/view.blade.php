@extends('admin.layouts.main')

@section('title', trans('cms.stores'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-home"></i>
		{{ trans('cms.stores') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/catalog/stores') }}">
				<i class="fa fa-home"></i>
				{{ trans('menu.stores') }}
			</a>
		</li>
        <li class="active">
			{{ $store->name }}
		</li>
	</ol>
</section>

<section class="content">
	
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{ $store->name }}</h3>
			<div class="box-tools pull-right">
				<a href="{{ url("admin/catalog/stores/edit/{$store->id}") }}" class="btn btn-box-tool">
					<i class="fa fa-edit"></i>
				</a>
				<a href="{{ url("admin/catalog/stores/delete/{$store->id}") }}" class="btn btn-box-tool"
				   data-confirm="{{ trans('cms.confirm_delete_store') }}">
					<i class="fa fa-trash-o"></i>
				</a>
			</div>
		</div>
		<div class="box-body">
			<div class="xol-sm-6 col-md-4">
				<img src="{{ $store->pictureUrl }}" width="100%" />
			</div>
			<div class="col-sm-6 col-md-8">
				<dl class="dl-horizontal">
					
					<dt>{{ trans('cms.label_store_name') }}</dt>
					<dd>{{ $store->name }}</dd>
					
					<?php if( $store->location ): ?>
					<dt>{{ trans('cms.label_store_location') }}</dt>
					<dd>{{ $store->location }}</dd>
					<?php endif; ?>

					<?php if( !empty($store->department->name) ): ?>
					<dt>{{ trans('cms.label_store_department') }}</dt>
					<dd>{{ $store->department->name }}</dd>
					<?php endif; ?>

					<?php if( !empty($store->city->name) ): ?>
					<dt>{{ trans('cms.label_store_cities') }}</dt>
					<dd>{{ $store->city->name }}</dd>
					<?php endif; ?>
					
					<?php if( $store->opening_hours ): ?>
					<dt>{{ trans('cms.label_store_opening_hours') }}</dt>
					<dd>{{ $store->opening_hours }}</dd>
					<?php endif; ?>
					
					<?php if( $store->latitude && $store->longitude ): ?>
					<dt>{{ trans('cms.label_store_map') }}</dt>
					<dd><img src="{!! $store->imageMap !!}" width="100%" /></dd>
					<?php endif; ?>
					
					<?php if( $store->phone_local ): ?>
					<dt>{{ trans('cms.label_store_phone_local') }}</dt>
					<dd>{{ $store->phone_local }}</dd>
					<?php endif; ?>
					
					<?php if( $store->phone_mobile ): ?>
					<dt>{{ trans('cms.label_store_phone_mobile') }}</dt>
					<dd>{{ $store->phone_mobile }}</dd>
					<?php endif; ?>
					
					<?php if( $store->opening_hours ): ?>
					<dt>{{ trans('cms.label_store_opening_hours') }}</dt>
					<dd>{{ $store->opening_hours }}</dd>
					<?php endif; ?>
					
              </dl>
			</div>
		</div>
	</div>
	
</section>

@endsection