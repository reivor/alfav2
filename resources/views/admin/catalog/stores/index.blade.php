@extends('admin.layouts.main')


@section('title', trans('cms.stores'))


@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-home"></i>
        {{ trans('cms.stores') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.stores') }}</li>
	</ol>
</section>

<section class="content">

	<div class="row margin-bottom">
		<div class="col-xs-12">
			<a href="/admin/catalog/stores/create" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_store') }}
			</a>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.stores') }}</h3>
				</div>
				<div class="box-body">
					<?php if( $stores->isEmpty() ): ?>
					<div class="alert alert-warning"><?= trans('cms.no_stores_found') ?></div>
					<?php else: ?>
					<div class="row">
						<div class="grid col-xs-12">
							<?php foreach($stores as $store): ?>
							<div class="col-xs-6 col-sm-4 col-md-3 text-center">
								<a href="<?= url('admin/catalog/stores/view/' . $store->id) ?>">
								<img src="<?= $store->pictureUrl ?>" />
								</a>
								<h4><?= $store->name ?></h4>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3 text-center">
							{!! $stores->links() !!}
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
</section>

@endsection