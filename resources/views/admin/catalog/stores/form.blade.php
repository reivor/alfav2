@extends('admin.layouts.main')

@section('title', trans('cms.stores'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-home"></i>
		{{ trans('cms.stores') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/catalog/stores') }}">
				<i class="fa fa-home"></i>
				{{ trans('menu.stores') }}
			</a>
		</li>
        <li class="active">
			{{ $action }}
		</li>
	</ol>
</section>

<section class="content">
	
	<form id="form-create-store" method="post" enctype="multipart/form-data" role="form">
		<?= csrf_field() ?>
		<input type="hidden" name="id" value="{{ $store->id }}" />
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('cms.stores') }}</h3>
			</div>
			<div class="box-body">
				<div class="form-group form-group-lg has-feedback">
					<label for="name">{{ trans('cms.label_store_name') }}</label>
					<input type="text" id="name" name="name" class="form-control" value="{{ $store->name or old('name') }}" placeholder="{{ trans('cms.placeholder_store_name') }}" maxlength="255">
				</div>
				<div class="form-group form-group-lg has-feedback">
					<label for="picture">{{ trans('cms.label_store_picture') }}</label>
					<div class="row">
						<div class="col-sm-2">
							<img src="<?= $store->pictureUrl ?>" width="100%" />
						</div>
						<div class="col-sm-12">
							<input type="file" id="picture" name="picture" />
						</div>
					</div>
				</div>
				<div class="form-group has-feedback">
					<label for="location">{{ trans('cms.label_store_location') }}</label>
					<input type="text" id="location" name="location" class="form-control" value="{{ $store->location or old('location') }}" placeholder="{{ trans('cms.placeholder_store_location') }}">
				</div>
				<div class="form-group has-feedback">
					<label>{{ trans('cms.label_store_department') }}</label>
					<select class="form-control" id="department" name="department">
						<option value="">{{trans('cms.select')}}</option>
						<?php if(isset($departments) && !empty($departments)){
							foreach ($departments as $department){?>
								<option value="<?=$department->id?>" <?=($department->id == $store->department_id ? "Selected" : '')?>><?=$department->name?></option>
							<?php }
						}?>
					</select>
				</div>
				<div class="form-group has-feedback">
					<label>{{ trans('cms.label_store_cities') }}</label>
					<select class="form-control" id="cities" name="cities">
						<option value="">{{trans('cms.select')}}</option>
						<?php if(isset($cities) && !empty($cities)){
							foreach ($cities as $city){?>
								<option value="<?=$city->id?>" <?=($city->id == $store->city_id ? "Selected" : '')?>><?=$city->name?></option>
							<?php }
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="map">{{ trans('cms.label_store_map') }}</label>
					<div id="map-canvas" class="map"></div>
					<input type="hidden" id="latitude" name="latitude" value="{{ $store->latitude }}" />
					<input type="hidden" id="longitude" name="longitude" value="{{ $store->longitude }}" />
				</div>
				<div class="form-group has-feedback">
					<label for="phone_local">{{ trans('cms.label_store_phone_local') }}</label>
					<input type="text" id="phone_local" name="phone_local" class="form-control" value="{{ $store->phone_local or old('phone_local') }}" placeholder="{{ trans('cms.placeholder_store_phone_local') }}" maxlength="32">
				</div>
				<div class="form-group has-feedback">
					<label for="phone_mobile">{{ trans('cms.label_store_phone_mobile') }}</label>
					<input type="text" id="phone_mobile" name="phone_mobile" class="form-control" value="{{ $store->phone_mobile or old('phone_mobile') }}" placeholder="{{ trans('cms.placeholder_store_phone_mobile') }}" maxlength="32">
				</div>
				<div class="form-group has-feedback">
					<label for="opening_hours">{{ trans('cms.label_store_opening_hours') }}</label>
					<input type="text" id="opening_hours" name="opening_hours" class="form-control" value="{{ $store->opening_hours or old('opening_hours') }}" placeholder="{{ trans('cms.placeholder_store_opening_hours') }}" maxlength="255">
				</div>
				<div class="form-group has-feedback">
					<label for="email">{{ trans('cms.label_store_email') }}</label>
					<input type="text" id="email" name="email" class="form-control" value="{{ $store->email or old('email') }}" placeholder="{{ trans('cms.placeholder_store_email') }}">
				</div>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
					<a href="{{ url('admin/catalog/stores') }}" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
				</div>
			</div>
		</div>
	</form>
	
</section>

@endsection


@push('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYpOcY_Ev9QEONCGr2bpj3ObJw3zxBcvs"></script>
<script type="text/javascript" src="/themes/admin/js/maps.js"></script>
<script>
$(function () {
	console.log('ready');
	
	$('#form-create-store').bootstrapValidator({
		fields: {
			name: {
				validators: {
					notEmpty: { message: '<?= trans('cms.form_required_field') ?>' }
				}
			},
			department: {
				validators: {
					notEmpty: { message: '<?= trans('cms.form_required_field') ?>' }
				}
			},
			cities: {
				validators: {
					notEmpty: { message: '<?= trans('cms.form_required_field') ?>' }
				}
			},
			email: {
				validators: {
					emailAddress: { message: '<?= trans('cms.form_invalid_email') ?>' }
				}
			},
			picture: {
				validators: {
					file: {
						extension: 'jpeg,jpg,png,JPG,PNG',
						type: 'image/jpeg,image/png',
						maxSize: 2 * 1024 * 1024,   // 2MB
						message: '<?= trans('cms.form_invalid_photo') ?>'
					}
				}
			},
			phone_local: {
				validators: {
					stringLength: {
                        min: 8,
                        message: '<?= trans('cms.form_invalid_phone_number') ?>'
                    }
				}
			},
			phone_mobile: {
				validators: {
					stringLength: {
                        min: 12,
                        message: '<?= trans('cms.form_invalid_phone_number') ?>'
                    }
				}
			}
		}
	});
	
	$( '#form-create-store' ).submit(function() {
		$( '#latitude' ).val( marker.position.lat() );
		$( '#longitude' ).val( marker.position.lng() );
		return true;
	});
	
	$('#phone_local').mask("000-0000", {placeholder: "000-0000"});
	
	$('#phone_mobile').mask("(000)000-0000", {placeholder: "(000)000-0000"});

	$('#department').on('change',function(){
		$('#cities').find('option').remove().end().append('<option value="">{{trans('cms.select')}}</option>');
		$.ajax({
			url: "/admin/catalog/stores/cities",
			method: 'POST',
			data: {department:$('#department').val()},
			success: function(c){
				var ciudades = c;
				addCiudades(ciudades);
			}
		});
	});

	var addCiudades = function(c) {
		for(i = 0; i < c.length; i++){
			$('#cities').append($('<option>', {
				value: c[i].id,
				text: c[i].name
			}));
		}
	};
	
});

	@if( $store->latitude && $store->longitude )
	position = { lat: {{ $store->latitude }}, lng: {{ $store->longitude }} };
	@endif
	google.maps.event.addDomListener(window, 'load', setupMapEditable);
</script>
@endpush