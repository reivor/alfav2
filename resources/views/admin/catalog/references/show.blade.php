@extends('admin.layouts.main')

@section('title', trans('cms.products'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-cubes"></i>
		{{ trans('cms.products') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/catalog/references">
				<i class="fa fa-cubes"></i> 
				{{ trans('cms.products') }}
			</a>
		</li>
		<li class="active">{{ $references->name }}</li>
	</ol>
</section>

<section class="invoice" style="margin: 10px;">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-header user-block">
				{{ $references->name }} @if($references->is_new == 1)<span class="circle">{{ trans('cms.product_is_new') }}</span>@endif
				<span class="description" style="margin-left:0">{{ trans('cms.product_created_at') }} {{ $references->created_at }}</span>
				<a href="{{ URL::to('admin/catalog/references/edit/' . $references->id ) }}" class="btn btn-primary pull-right"><i class="fa fa-fw fa-edit"></i> {{ trans('cms.edit') }}</a>
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<strong>{{ trans('cms.label_carrusel_description') }}</strong>
			<p>{{ $references->description }}</p>
			<strong>{{ trans('cms.label_product_seal_of_quality') }}</strong>
			<p>{{ $references->quality_seal }}</p>
		</div>
		<div class="col-md-3">
			<strong>{{ trans('cms.label_product_ambiance') }}</strong>
			<p>{{ $references->ambience }}</p>
			<hr>
			<strong>{{ trans('cms.label_product_enforcement') }}</strong>
			<p>{{ $references->enforcement }}</p>
			<hr>
			<strong>{{ trans('cms.label_product_use') }}</strong>
			<p>{{ $references->use }}</p>
		</div>
		<div class="col-md-3">
			<strong>{{ trans('cms.label_product_data_sheet') }}</strong>
			@if(!empty($references->data_sheet))
				<p><a href="{{ $references->data_sheet }}" class="btn btn-danger btn-block btn-sm" download><i class="fa fa-fw fa-download"></i> DESCARGAR</a></p>
			@else <p>--</p>
			@endif
			@if(!empty($references->user_manual))
			<strong>{{ trans('cms.label_product_user_manual') }}</strong>
			<p><a href="{{ $references->user_manual }}" class="btn btn-danger btn-block btn-sm" download><i class="fa fa-fw fa-download"></i> DESCARGAR</a></p>
			@else <p>--</p>
			@endif
			@if(!empty($references->maintenance_manual))
			<strong>{{ trans('cms.label_product_maintenance_manual') }}</strong>
			<p><a href="{{ $references->maintenance_manual }}" class="btn btn-danger btn-block btn-sm" download><i class="fa fa-fw fa-download"></i> DESCARGAR</a></p>
			@else <p>--</p>
			@endif
			@if(!empty($references->guarantee))
			<strong>{{ trans('cms.label_product_warranty') }}</strong>
			<p><a href="{{ $references->guarantee }}" class="btn btn-danger btn-block btn-sm" download><i class="fa fa-fw fa-download"></i> DESCARGAR</a></p>
			@else <p>--</p>
			@endif
		</div>
	</div>

	<div class="row" style="margin-top:50px">
		<div class="col-md-12">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					@foreach($products as $key => $product)		
					<li <?php if( $key == 0 ){ echo "class='active'"; } ?>><a href="#{{ $product->id }}" data-toggle="tab">{{ $product->name }}</a></li>
					@endforeach
				</ul>
				<div class="tab-content">
					@foreach($products as $key => $product)	
					<div class="tab-pane fade in <?php if( $key == 0 ){ echo "active"; } ?>" id="{{ $product->id }}">
						<div class="row">
							<div class="col-md-4">
								<strong>{{ trans('cms.product_code') }}</strong>
								<p>{{ $product->code }}</p>
								<hr>
								<strong>{{ trans('cms.product_dimensions') }}</strong>
								<p>{{ $product->dimensions }}</p>
								<hr>
								<strong>{{ trans('cms.product_color') }}</strong>
								<p>{{ $product->color }}</p>
								<hr>
								<strong>{{ trans('cms.product_color_code') }}</strong>
								<p>{{ $product->color_code }}</p>
								<hr>
								<strong>{{ trans('cms.product_tags') }}</strong>
								<p>{{ $product->tags }}</p>
							</div>
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-12">
										<label>{{ trans('cms.categories') }}</label>
										<?php 
											$categories = DB::table('categories')
								            ->join('products_categories', 'categories.id', '=', 'products_categories.category_id')
								            ->select('categories.name')
								            ->where('products_categories.product_id', '=', $product->id)
								            ->get(); 
							            ?>
							            <ul class="breadcrumb cat-rel">
								            @foreach ($categories as $category)
												<li>{{ $category->name }}</li>
											@endforeach
										</ul>
									</div>
									<div class="col-md-12">
										<label style="margin-top:20px">{{ trans('cms.label_product_related') }}</label>
										<?php 
											$products = DB::table('products')
								            ->join('related_products', 'products.id', '=', 'related_products.product2_id')
								            ->select('products.name','products.id')
								            ->where('related_products.product_id', '=', $product->id)
								            ->get(); 
							            ?>
							            <ul class="breadcrumb cat-rel">
								            @foreach ($products as $prod)
												<li><a href="#" class="load-prod-modal" data-url="{{ URL::to('admin/catalog/products/show/' . $prod->id ) }}" data-toggle ="modal" data-target='#product-modal'>{{ $prod->name }}</a></li>
											@endforeach
										</ul>
									</div>
								</div>	
							</div>
							<div class="col-md-4">
								<div class="row">
								<?php $images = App\Models\Product::find($product->id)->images ?>
									@foreach ($images as $image)
									<div class="col-xs-4 col-md-6">
										<div class="img-wrapper">
											<img src="{{ $image->url }}" alt="{{ $product->name }}" class="img-responsive img-shadow" style="margin-bottom:20px">
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>					
		</div>
	</div>
</section>


<div id="product-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    
  </div>
</div>

@endsection


@push('scripts')
<script>

var Modal = {
    init: function () {
        this.initProdModal();
    },
    initProdModal: function() {
        $(document).on('click', '.load-prod-modal', function(event){
            console.log('Modal: '+ $(this).attr('data-url'));
            $('#product-modal .modal-dialog').load($(this).attr('data-url'));
            event.preventDefault();
        });
    },
};

Modal.init();

</script>
@endpush
