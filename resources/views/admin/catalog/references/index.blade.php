@extends('admin.layouts.main')


@section('title', trans('cms.products'))


@section('content')

<meta name="csrf_token" content="{{ csrf_token() }}" />

<section class="content-header">
	<h1>
		<i class="fa fa-cubes"></i>
		{{ trans('cms.products') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li class="active">{{ trans('menu.products') }}</li>
		@if(!empty($cat_name->name))
		<li class="active">
			{{ !empty($cat_name->name) ? $cat_name->name : "" }}
		</li>
		@endif
	</ol>
</section>

<section class="content">
	<div class="row margin-bottom">
		<div class="col-xs-12">
			<a href="/admin/catalog/references/create" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_product') }}
			</a>
			<a href="/admin/catalog/products/upload" class="btn btn-primary">
				<i class="fa fa-upload"></i>
				{{ trans('cms.upload_products') }}
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title box-title-products">
						{{ trans('cms.products_list') }} 
						@if(!empty($cat_name->name))
							&nbsp;&nbsp;<i class="fa fa-filter"></i>
							{{ !empty($cat_name->name) ? $cat_name->name : "" }}
						@endif
					</h3>
					<div class="box-tools">
						<div class="btn-group" style="margin-right:5px">
							<form id="form-filter-reference" method="get">
								<select class="form-control" name="cat" id="cat" style="height: 30px" data-token="{{ csrf_token() }}">
									<?php $categories = App\Models\Category::buildCategoriesTree() ?>
									<option selected>{{ trans('cms.categories') }}</option>
									@foreach ($categories as $categoryLv1)
									<option value="{{ $categoryLv1->id }}">{{ $categoryLv1->name}}</option>
									@if(isset($categoryLv1->categories))
									@foreach ($categoryLv1->categories as $categoryLv2)
									<option value="{{ $categoryLv2->id }}">&nbsp;&nbsp;&nbsp;{{ $categoryLv2->name}}</option>
									@if(isset($categoryLv2->categories))
									@foreach ($categoryLv2->categories as $categoryLv3)
									<option value="{{ $categoryLv3->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $categoryLv3->name}}</option>
									@if(isset($categoryLv3->categories))
									@foreach ($categoryLv3->categories as $categoryLv4)
									<option value="{{ $categoryLv4->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $categoryLv4->name}}</option>
									@endforeach
									@endif
									@endforeach
									@endif
									@endforeach
									@endif
									@endforeach
								</select>
							</form>
						</div>
						<form id="form-search-reference" action="/admin/catalog/references" method="get" role="search" data-toggle="validator" class="pull-right">
							<div class="input-group input-group-sm" style="width: 200px;">
								<input type="search" id="search" name="search" class="form-control pull-right" placeholder="{{ trans('cms.search') }}" value="{{ !empty($search) ? $search : "" }}" required>
								<div class="input-group-btn">
									<button type="submit" class="btn btn-default" style="margin: 0;border-top-left-radius: 0;border-bottom-left-radius: 0;border-left: 0;"><i class="fa fa-search"></i></button>
								</div>
								<a href="/admin/catalog/references" class="btn-reset" id="btn-reset"><i class="fa fa-times"></i></a>
							</div>
						</form>
					</div>
				</div>
				<div class="box-body">
					<?php if( $references->isEmpty() ): ?>
					<div class="alert alert-warning">
						<?= trans('cms.no_products_found') ?>
					</div>
				<?php else: ?>
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tr>
							<th>{{ trans('cms.label_product_name') }}</th>
							<th>{{ trans('cms.label_carrusel_description') }}</th>
							<th>{{ trans('cms.product_is_new') }}</th>
							<th>{{ trans('cms.product_status') }}</th>
							<th>&nbsp;</th>
						</tr>
						@foreach ($references as $refs)
						<tr>
							<td>
								<a href="{{ URL::to('admin/catalog/references/show/' . $refs->id ) }}">{{ $refs->name }}</a>
							</td>
							<td>
								{{ $short_desc = str_limit($refs->description, 100) }}
								{{ $short_desc }}
							</td>
							<td>
								@if($refs->is_new == 1)
								<small class="label bg-yellow">{{ trans('cms.product_is_new') }}</small>
								@endif
							</td>
							<td>
								@if($refs->status == "active")
								<span class="label label-success">{{ $refs->status }}</span>
								@else
								<span class="label label-default">{{ $refs->status }}</span>
								@endif
							</td>
							<td>
								<a href="{{ URL::to('admin/catalog/references/destroy/' . $refs->id ) }}" style="color:red" title="<?= trans('cms.delete') ?>" data-confirm="<?= trans('cms.confirm_delete_product') ?>"><i class="fa fa-fw fa-trash"></i></a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
				<div class="box-footer clearfix text-right">
					{!! $references->links() !!}
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
</div>
</section>

@endsection


@push('scripts')

<script>

$( document ).ready(function() {
	if( $('#search').val().length > 0 ) {
		$('#btn-reset').css('display', 'inline-block');
	}
});

/*$(function(){
	$('#cat').change(function(e) {
		var token = $(this).data('token');
		var optionSelected = $(this).find("option:selected");
    	var cat_id = optionSelected.val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			type: 'get',
			url: '/admin/catalog/references/category/',
			data: { _token :token },
			success: function(data){
				console.log(data);
				window.location.reload();
			}
		});
	});
});*/

$('#cat').on('change', function(e){
    var select = $(this), form = select.closest('form');
    form.attr('action', '/admin/catalog/references/category'); //+ select.val());
    form.submit();
});

</script>

@endpush
