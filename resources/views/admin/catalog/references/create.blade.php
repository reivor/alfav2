@extends('admin.layouts.main')


@section('title', trans('cms.products'))


@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<section class="content-header">
	<h1>
		<i class="fa fa-cubes"></i>
		{{ trans('cms.products') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/catalog/references">
				<i class="fa fa-cubes"></i> 
				{{ trans('cms.products') }}
			</a>
		</li>
		<li class="active">{{ trans('cms.create') }}</li>
	</ol>
</section>

<section class="content">
	<form id="form-create-reference" action="/admin/catalog/references/store" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
		<?= csrf_field() ?>
		<input type="hidden" id="id" name="id" value="" />
		<div class="row">
			<div class="col-md-8">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_data') }}</h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="name">{{ trans('cms.label_product_name') }}</label>
							<input type="text" id="name" name="name" class="form-control" value="" placeholder="" maxlength="100" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="description">{{ trans('cms.label_carrusel_description') }}</label>
							<textarea id="description" name="description" class="form-control" rows="16" cols="50">{{ old('description') }}</textarea>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="quality_seal">{{ trans('cms.label_product_seal_of_quality') }}</label>
							<div class="checkbox">
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="01">01</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="02">02</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="03">03</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="04">04</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="05">05</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="06">06</label>
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="is_new">{{ trans('cms.product_is_new') }}</label>
							<label><input type="checkbox" name="is_new" value="1"></label>					
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label>{{ trans('cms.label_carrusel_status') }}</label>
							<select class="form-control" name="status" id="status">
								<option value="active">active</option>
								<option value="disabled">disabled</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_uses') }}</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="ambience">{{ trans('cms.label_product_ambiance') }}</label>
							<input type="text" id="ambience" name="ambience" class="form-control" value="{{ old('ambience') }}" placeholder="" maxlength="100">							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="enforcement">{{ trans('cms.label_product_enforcement') }}</label>
							<input type="text" id="enforcement" name="enforcement" class="form-control" value="{{ old('enforcement') }}" placeholder="" maxlength="100">							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="use">{{ trans('cms.label_product_use') }}</label>
							<input type="text" id="use" name="use" class="form-control" value="{{ old('use') }}" placeholder="" maxlength="100">							
							<div class="help-block with-errors" style="margin-bottom: 22px;"></div>
						</div>
					</div>
				</div>	
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_files') }}</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="data_sheet">{{ trans('cms.label_product_data_sheet') }}</label>
							<input type="file" id="data_sheet" name="data_sheet" value="{{ old('data_sheet') }}">
							<hr>
						</div>
						<div class="form-group">
							<label for="user_manual">{{ trans('cms.label_product_user_manual') }}</label>
							<input type="file" id="user_manual" name="user_manual">
							<hr>
						</div>
						<div class="form-group">
							<label for="maintenance_manual">{{ trans('cms.label_product_maintenance_manual') }}</label>
							<input type="file" id="maintenance_manual" name="maintenance_manual">
							<hr>
						</div>
						<div class="form-group">
							<label for="guarantee">{{ trans('cms.label_product_warranty') }}</label>
							<input type="file" id="guarantee" name="guarantee">
						</div>
					</div>
				</div>	
			</div>
		</div>

		<!-- Products Line Start -->

		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_variations') }}</h3>
					</div>
					<ul class="nav nav-tabs prods">
						<li class="active"><a href="#hhh" data-toggle="tab"># 1</a></li>
						<li><a href="#" class="add-product">+ {{ trans('cms.product_add') }}</a>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active fade in" id="hhh">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label for="pname">{{ trans('cms.label_product_name') }}</label>
											<input type="text" name="pname[]" class="form-control" value="{{ old('pname') }}" placeholder="" maxlength="100" required>
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="code">{{ trans('cms.product_code') }}</label>
											<input type="text" id="code" name="code[]" class="form-control" value="{{ old('code') }}" placeholder="" maxlength="100" required>							
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="dimensions">{{ trans('cms.product_dimensions') }}</label>
											<input type="text" id="dimensions" name="dimensions[]" class="form-control" value="{{ old('dimensions') }}" placeholder="" maxlength="100">							
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="color">{{ trans('cms.product_color') }}</label>
											<input type="text" id="color" name="color[]" class="form-control" value="{{ old('color') }}" placeholder="" maxlength="100">							
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="color_code">{{ trans('cms.product_color_code') }}</label>
											<div id="cp1" class="form-group input-group colorpicker-component">
												<input type="text" id="color_code" name="color_code[]" class="form-control" value="{{ old('color_code') }}" placeholder="" maxlength="100">
												<span class="input-group-addon"><i></i></span>
											</div>
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="color_code">{{ trans('cms.product_tags') }}</label> <span>({{trans('cms.message_enter_tags')}})</span><br>
											<input id="tags1" name="tags[]" type="text" class="form-control" value="" data-provide="typeahead">
										</div>
										<div class="form-group">
											<label>{{ trans('cms.label_carrusel_status') }}</label>
											<select class="form-control" name="pstatus[]" id="pstatus">
												<option value="active">active</option>
												<option value="disabled">disabled</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="row">
											<div class="col-md-12">
												<label>{{ trans('cms.categories') }}</label>
												<div class="box-body" style="overflow-y: scroll;height: 180px;border: solid 1px #d2d6de;">
													<?php $categories = App\Models\Category::buildCategoriesTree() ?>
													<div class="form-group">
														@foreach ($categories as $categoryLv1)
														<div class="checkbox" style="margin-top:0">
															<label style="font-weight:bold">
																<input type="checkbox" name="cat[1][]" class="cat" value="{{ $categoryLv1->id }}">
																{{ $categoryLv1->name }}
															</label>
														</div>													
														@if(isset($categoryLv1->categories))
														@foreach ($categoryLv1->categories as $categoryLv2)
														<div class="checkbox">
															<label style="padding-left: 40px;">
																<input type="checkbox" name="cat[1][]" class="cat" value="{{ $categoryLv2->id }}">
																{{ $categoryLv2->name }}
															</label>
														</div>
														@if(isset($categoryLv2->categories))
														@foreach ($categoryLv2->categories as $categoryLv3)
														<div class="checkbox">
															<label style="padding-left: 60px;">
																<input type="checkbox" name="cat[1][]" class="cat" value="{{ $categoryLv3->id }}">
																{{ $categoryLv3->name }}
															</label>
														</div>
														@if(isset($categoryLv3->categories))
														@foreach ($categoryLv3->categories as $categoryLv4)
														<div class="checkbox">
															<label style="padding-left: 80px;">
																<input type="checkbox" name="cat[1][]" class="cat" value="{{ $categoryLv4->id }}">
																{{ $categoryLv4->name }}
															</label>
														</div>
														@endforeach
														@endif
														@endforeach
														@endif
														@endforeach
														@endif
														@endforeach
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<label style="margin-top:20px">{{ trans('cms.label_product_related') }}</label>
												<div class="box-body" style="overflow-y: scroll;height: 180px;border: solid 1px #d2d6de;">
													<div class="form-group">
														<?php $references = DB::table('products')->select('id', 'name')->orderBy('name')->get(); ?>
														@foreach ($references as $reference)
														<div class="checkbox" style="margin-top:0">
															<label>
																<input type="checkbox" name="rel[1][]" value="{{ $reference->id }}">
																{{ $reference->name }}
															</label>
														</div>
														@endforeach
													</div>
												</div>
											</div>
										</div>		
									</div>
									<div class="col-md-4">
										<label>{{ trans('cms.label_product_images') }}</label>
										<p>({{trans('cms.suggested_size')}}: 600x467)</p>
										<div class="form-group" id="attach">
											<input type="file" id="img_prod" name="img_prod[1][]" multiple>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>					
				</div>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
					<a href="/admin/catalog/references" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
				</div>
			</div>
		</form>
	</section>

	@endsection

	@push('scripts')

	<script>
		var tag;
		$(function () {
			console.log('ready');
			$('#cp1').colorpicker({
				format: 'hex'
			});

			tag = <?= json_encode($tags) ?>;
			$('#tags1').tagsinput({
				typeahead: {
					source: tag
				}
			});
		});

	/*$(function () {
		console.log('ready');
		$('#form-create-reference').bootstrapValidator({
			fields: {
				name: {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				'pname[]': {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				/*'cat[1][]': {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				'cat[2][]': {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				'rel[1][]': {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				'rel[2][]': {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				'code[]': {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				data_sheet: {
					validators: {
						file: {
							extension: 'pdf',
							type: 'application/pdf',
							message: '{{ trans('cms.form_invalid_file') }}'
						}
					}
				},
				user_manual: {
					validators: {
						file: {
							extension: 'pdf',
							type: 'application/pdf',
							message: '{{ trans('cms.form_invalid_file') }}'
						}
					}
				},
				maintenance_manual: {
					validators: {
						file: {
							extension: 'pdf',
							type: 'application/pdf',
							message: '{{ trans('cms.form_invalid_file') }}'
						}
					}
				},
				guarantee: {
					validators: {
						file: {
							extension: 'pdf',
							type: 'application/pdf',
							message: '{{ trans('cms.form_invalid_file') }}'
						}
					}
				},
				img_prod: {
					validators: {
						file: {
							extension: 'jpeg,jpg,png',
							type: 'image/jpeg,image/png',
							maxSize: 2097152,   // 2048 * 1024
							message: '{{ trans('cms.form_invalid_photo') }}'
						}
					}
				}
			}
		});
});*/

$(".prods").on("click", "a", function (e) {
	e.preventDefault();
	if (!$(this).hasClass('add-product')) {
		$(this).tab('show');
	}
})
.on("click", "span", function () {
	var anchor = $(this).siblings('a');
	$(anchor.attr('href')).remove();
	$(this).parent().remove();
	$(".prods li").children('a').first().click();
	$pname = $clone.find('[name="pname[]"]');
    $('#form-create-reference').bootstrapValidator('removeField', $pname);
});
$('.add-product').click(function (e) {
	e.preventDefault();
	var id = $(".prods").children().length; 
	var tabId = 'prod_' + id;
	$(this).closest('li').before('<li><a href="#prod_' + id + '"># ' + id + '</a> <span> x </span></li>');
	$clone = $('.tab-content').append('<div class="tab-pane active fade" id="' + tabId + '"><div class="row"><div class="col-md-4"><div class="form-group"><label for="pname">{{trans('cms.label_product_name')}}</label><input type="text" name="pname[]" class="form-control" value="{{old('pname')}}" placeholder="" maxlength="100" required><div class="help-block with-errors"></div></div><div class="form-group"><label for="code">{{trans('cms.product_code')}}</label><input type="text" id="code" name="code[]" class="form-control" value="{{old('code')}}" placeholder="" maxlength="100" required><div class="help-block with-errors"></div></div><div class="form-group"><label for="dimensions">{{trans('cms.product_dimensions')}}</label><input type="text" id="dimensions" name="dimensions[]" class="form-control" value="{{old('dimensions')}}" placeholder="" maxlength="100"><div class="help-block with-errors"></div></div><div class="form-group"><label for="color">{{trans('cms.product_color')}}</label><input type="text" id="color" name="color[]" class="form-control" value="{{old('color')}}" placeholder="" maxlength="100"><div class="help-block with-errors"></div></div><div class="form-group"><label for="color_code">{{trans('cms.product_color_code')}}</label><div id="cp'+id+'" class="form-group input-group colorpicker-component"><input type="text" id="color_code" name="color_code[]" class="form-control" value="{{old('color_code')}}" placeholder="" maxlength="100"><span class="input-group-addon"><i></i></span></div><div class="help-block with-errors"></div></div><div class="form-group"><label for="color_code">{{trans('cms.product_tags')}}</label><input id="tags'+id+'" name="tags[]" type="text" class="form-control" value="" data-provide="typeahead"></div><div class="form-group"><label>{{trans('cms.label_carrusel_status')}}</label><select class="form-control" name="pstatus[]" id="pstatus"><option value="active">active</option><option value="disabled">disabled</option></select></div></div><div class="col-md-4"><div class="row"><div class="col-md-12"><label>{{trans('cms.categories')}}</label><div class="box-body" style="overflow-y: scroll;height: 180px;border: solid 1px #d2d6de;"><?php $categories=App\Models\Category::buildCategoriesTree() ?><div class="form-group"> @foreach ($categories as $categoryLv1)<div class="checkbox" style="margin-top:0"><label style="font-weight:bold"><input type="checkbox" name="cat['+id+'][]" class="cat" value="{{$categoryLv1->id}}">{{$categoryLv1->name}}</label></div>@if(isset($categoryLv1->categories)) @foreach ($categoryLv1->categories as $categoryLv2)<div class="checkbox"><label style="padding-left: 40px;"><input type="checkbox" name="cat['+id+'][]" class="cat" value="{{$categoryLv2->id}}">{{$categoryLv2->name}}</label></div>@if(isset($categoryLv2->categories)) @foreach ($categoryLv2->categories as $categoryLv3)<div class="checkbox"><label style="padding-left: 60px;"><input type="checkbox" name="cat['+id+'][]" class="cat" value="{{$categoryLv3->id}}">{{$categoryLv3->name}}</label></div>@if(isset($categoryLv3->categories)) @foreach ($categoryLv3->categories as $categoryLv4)<div class="checkbox"><label style="padding-left: 80px;"><input type="checkbox" name="cat['+id+'][]" class="cat" value="{{$categoryLv4->id}}">{{$categoryLv4->name}}</label></div> @endforeach @endif @endforeach @endif @endforeach @endif @endforeach</div></div></div></div><div class="row"><div class="col-md-12"><label style="margin-top:20px">{{trans('cms.label_product_related')}}</label><div class="box-body" style="overflow-y: scroll;height: 180px;border: solid 1px #d2d6de;"><div class="form-group"><?php $references = DB::table('products')->select('id', 'name')->orderBy('name')->get(); ?> @foreach ($references as $reference)<div class="checkbox" style="margin-top:0"><label><input type="checkbox" name="rel['+id+'][]" value="{{$reference->id}}">{{$reference->name}}</label></div>@endforeach</div></div></div></div></div><div class="col-md-4"><label>{{trans('cms.label_product_images')}}</label><p>({{trans('cms.suggested_size')}}: 600x467)</p><div class="form-group" id="attach"><input type="file" id="img_prod" name="img_prod['+id+'][]" multiple></div></div></div>');
	$('.prods li:nth-child(' + id + ') a').click();
		$('#cp'+id+'').colorpicker({
    	format: 'hex'
    });
	tag = <?= json_encode($tags) ?>;
		$('#tags'+id+'').tagsinput({
			typeahead: {
			source: tag
		}
	});
	/*$pname = $clone.find('[name="pname[]"]');
    $('#form-create-reference').bootstrapValidator('addField', $pname);

    $code = $clone.find('[name="code[]"]');
    $('#form-create-reference').bootstrapValidator('addField', $code);*/

});

</script>
@endpush
