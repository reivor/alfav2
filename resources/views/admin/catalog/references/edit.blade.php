@extends('admin.layouts.main')


@section('title', trans('cms.products'))


@section('content')

<meta name="csrf_token" content="{{ csrf_token() }}" />


@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<section class="content-header">
	<h1>
		<i class="fa fa-cubes"></i>
		{{ trans('cms.products') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/catalog/references">
				<i class="fa fa-cubes"></i> 
				{{ trans('cms.products') }}
			</a>
		</li>
		<li class="active">{{ trans('cms.create') }}</li>
	</ol>
</section>

<section class="content">
	<form id="form-edit-reference" action="/admin/catalog/references/update/{{ $references->id }}" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
		<?= csrf_field() ?>
		<input type="hidden" id="id" name="id" value="" />
		<div class="row">
			<div class="col-md-8">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_data') }}</h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="name">{{ trans('cms.label_product_name') }}</label>
							<input type="text" id="name" name="name" class="form-control" value="{{ $references->name }}" placeholder="" maxlength="100" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="description">{{ trans('cms.label_carrusel_description') }}</label>
							<textarea id="description" name="description" class="form-control" rows="16" cols="50">{{ $references->description }}</textarea>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="quality_seal">{{ trans('cms.label_product_seal_of_quality') }}</label>
							<div class="checkbox">
								<?php $chk = (explode(',',$references->quality_seal)); ?>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="01" <?php if(in_array("01",$chk)) { ?> checked="checked" <?php } ?>>01</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="02" <?php if(in_array("02",$chk)) { ?> checked="checked" <?php } ?>>02</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="03" <?php if(in_array("03",$chk)) { ?> checked="checked" <?php } ?>>03</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="04" <?php if(in_array("04",$chk)) { ?> checked="checked" <?php } ?>>04</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="05" <?php if(in_array("05",$chk)) { ?> checked="checked" <?php } ?>>05</label>
								<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" value="06" <?php if(in_array("06",$chk)) { ?> checked="checked" <?php } ?>>06</label>
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="is_new">{{ trans('cms.product_is_new') }}</label>
							<label><input type="checkbox" @if($references->is_new) checked @endif name="is_new" value="1"></label>					
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label>{{ trans('cms.label_carrusel_status') }}</label>
							<select class="form-control" name="status" id="status">
								<option value="active">active</option>
								<option value="disabled">disabled</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_uses') }}</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="ambience">{{ trans('cms.label_product_ambiance') }}</label>
							<input type="text" id="ambience" name="ambience" class="form-control" value="{{ $references->ambience }}" placeholder="" maxlength="100">							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="enforcement">{{ trans('cms.label_product_enforcement') }}</label>
							<input type="text" id="enforcement" name="enforcement" class="form-control" value="{{ $references->enforcement }}" placeholder="" maxlength="100">							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="use">{{ trans('cms.label_product_use') }}</label>
							<input type="text" id="use" name="use" class="form-control" value="{{ $references->use }}" placeholder="" maxlength="100">							
							<div class="help-block with-errors" style="margin-bottom: 22px;"></div>
						</div>
					</div>
				</div>	
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_files') }}</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="data_sheet">{{ trans('cms.label_product_data_sheet') }}</label>
							<input type="file" id="data_sheet" name="data_sheet">
							@if(!empty($references->data_sheet))<a href="#" class="text-sm"><i class="fa fa-file-pdf-o"></i> <?php echo $rest = substr("$references->data_sheet", -27); ?></a><a href="{{ URL::to('admin/catalog/references/deleteDataSheet/' . $references->id ) }}" class="delete" style="color:red" data-toggle="tooltip" title="{{ trans('cms.delete') }}"> <i class="fa fa-fw fa-trash-o"></i></a>@else<hr>@endif
						</div>
						<div class="form-group">
							<label for="user_manual">{{ trans('cms.label_product_user_manual') }}</label>
							<input type="file" id="user_manual" name="user_manual">
							@if(!empty($references->user_manual))<a href="#" class="text-sm"><i class="fa fa-file-pdf-o"></i> <?php echo $rest = substr("$references->user_manual", -27); ?></a><a href="{{ URL::to('admin/catalog/references/deleteuManual/' . $references->id ) }}" class="delete" style="color:red" data-toggle="tooltip" title="{{ trans('cms.delete') }}"> <i class="fa fa-fw fa-trash-o"></i></a>@else<hr>@endif
						</div>
						<div class="form-group">
							<label for="maintenance_manual">{{ trans('cms.label_product_maintenance_manual') }}</label>
							<input type="file" id="maintenance_manual" name="maintenance_manual">
							@if(!empty($references->maintenance_manual))<a href="#" class="text-sm"><i class="fa fa-file-pdf-o"></i> <?php echo $rest = substr("$references->maintenance_manual", -27); ?></a><a href="{{ URL::to('admin/catalog/references/deletemManual/' . $references->id ) }}" class="delete" style="color:red" data-toggle="tooltip" title="{{ trans('cms.delete') }}"> <i class="fa fa-fw fa-trash-o"></i></a>@else<hr>@endif
						</div>
						<div class="form-group">
							<label for="guarantee">{{ trans('cms.label_product_warranty') }}</label>
							<input type="file" id="guarantee" name="guarantee">
							@if(!empty($references->guarantee))<a href="#" class="text-sm"><i class="fa fa-file-pdf-o"></i> <?php echo $rest = substr("$references->guarantee", -27); ?></a><a href="{{ URL::to('admin/catalog/references/deleteGuarantee/' . $references->id ) }}" class="delete" style="color:red" data-toggle="tooltip" title="{{ trans('cms.delete') }}"> <i class="fa fa-fw fa-trash-o"></i></a>@endif
						</div>
					</div>
				</div>	
			</div>
		</div>

		<!-- Products Line Start -->

		<?php $products = App\Models\Reference::find($references->id)->products ?>

		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_variations') }}</h3>
					</div>
					<ul class="nav nav-tabs prods">
						@foreach($products as $key => $product)	
						<li <?php if( $key == 0 ){ echo "class='active'"; } ?>>
							<a href="#{{ $product->id }}" data-toggle="tab">{{ $product->name }}</a>
							<a href="{{ URL::to('admin/catalog/products/destroy/' . $product->id ) }}" class="btn bg-teal btn-sm delete-product" data-toggle="tooltip" title="{{ trans('cms.delete') }}" data-confirm="{{ trans('cms.confirm_delete_product') }} - {{ $product->name }}"><i class="fa fa-times"></i></a></li>
						@endforeach
						<li><a href="#" class="add-product">+ {{ trans('cms.product_add') }}</a>
						</ul>
						<div class="tab-content">
							<?php $i = 1 ?>
							@foreach($products as $key => $product)	
							<div class="tab-pane fade in <?php if( $key == 0 ){ echo "active"; } ?>" id="{{ $product->id }}">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label for="pname">{{ trans('cms.label_product_name') }}</label>
											<input type="text" name="pname[]" class="form-control" value="{{ $product->name }}" placeholder="" maxlength="100" required>
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="code">{{ trans('cms.product_code') }}</label>
											<input type="text" id="code" name="code[]" class="form-control" value="{{ $product->code }}" placeholder="" maxlength="100" readonly="readonly">							
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="dimensions">{{ trans('cms.product_dimensions') }}</label>
											<input type="text" id="dimensions" name="dimensions[]" class="form-control" value="{{ $product->dimensions }}" placeholder="" maxlength="100">							
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="color">{{ trans('cms.product_color') }}</label>
											<input type="text" id="color" name="color[]" class="form-control" value="{{ $product->color }}" placeholder="" maxlength="100">							
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="color_code">{{ trans('cms.product_color_code') }}</label>
											<div class="form-group input-group colorpicker-component cp">
												<input type="text" id="color_code" name="color_code[]" class="form-control" value="{{ $product->color_code }}" placeholder="" maxlength="100">
												<span class="input-group-addon"><i></i></span>
											</div>
											<div class="help-block with-errors"></div>
										</div>
										<div class="form-group">
											<label for="color_code">{{ trans('cms.product_tags') }}</label> <span>({{trans('cms.message_enter_tags')}})</span><br>
											<input id="tags" name="tags[]" type="text" class="form-control tags" value="{{ $product->tags }}" data-provide="typeahead">
										</div>
										<div class="form-group">
											<label>{{ trans('cms.label_carrusel_status') }}</label>
											<select class="form-control" name="pstatus[]" id="pstatus">
												<option value="active">active</option>
												<option value="disabled">disabled</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="row">
											<div class="col-md-12">
												<label>{{ trans('cms.categories') }}</label>
												<div class="box-body" style="overflow-y: scroll;height: 180px;border: solid 1px #d2d6de;">
													<?php 
														$categories = DB::table('categories')
											            ->join('products_categories', 'categories.id', '=', 'products_categories.category_id')
											            ->select('categories.id')
											            ->where('products_categories.product_id', '=', $product->id)
											            ->get(); 
										            ?>
										           	<?php $arrayc = array_dot(json_decode(json_encode($categories), true)); ?>
													<?php $categories = App\Models\Category::buildCategoriesTree() ?>
													<div class="form-group">
														@foreach ($categories as $categoryLv1)
														<div class="checkbox" style="margin-top:0">
															<label style="font-weight:bold">
																<input type="checkbox" name="cat[{{ $i }}][]" value="{{ $categoryLv1->id }}" <?php if(in_array($categoryLv1->id,$arrayc)) { ?> checked="checked" <?php } ?>>
																{{ $categoryLv1->name }}
															</label>
														</div>													
														@if(isset($categoryLv1->categories))
														@foreach ($categoryLv1->categories as $categoryLv2)
														<div class="checkbox">
															<label style="padding-left: 40px;">
																<input type="checkbox" name="cat[{{ $i }}][]" value="{{ $categoryLv2->id }}" <?php if(in_array($categoryLv2->id,$arrayc)) { ?> checked="checked" <?php } ?>>
																{{ $categoryLv2->name }}
															</label>
														</div>
														@if(isset($categoryLv2->categories))
														@foreach ($categoryLv2->categories as $categoryLv3)
														<div class="checkbox">
															<label style="padding-left: 60px;">
																<input type="checkbox" name="cat[{{ $i }}][]" value="{{ $categoryLv3->id }}" <?php if(in_array($categoryLv3->id,$arrayc)) { ?> checked="checked" <?php } ?>>
																{{ $categoryLv3->name }}
															</label>
														</div>
														@if(isset($categoryLv3->categories))
														@foreach ($categoryLv3->categories as $categoryLv4)
														<div class="checkbox">
															<label style="padding-left: 80px;">
																<input type="checkbox" name="cat[{{ $i }}][]" value="{{ $categoryLv4->id }}" <?php if(in_array($categoryLv4->id,$arrayc)) { ?> checked="checked" <?php } ?>>
																{{ $categoryLv4->name }}
															</label>
														</div>
														@endforeach
														@endif
														@endforeach
														@endif
														@endforeach
														@endif
														@endforeach
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<label style="margin-top:20px">{{ trans('cms.label_product_related') }}</label>
												<div class="box-body" style="overflow-y: scroll;height: 180px;border: solid 1px #d2d6de;">
													<div class="form-group">
														<?php 
															$products = DB::table('products')
												            ->join('related_products', 'products.id', '=', 'related_products.product2_id')
												            ->select('products.id')
												            ->where('related_products.product_id', '=', $product->id)
												            ->get(); 
											            ?>
											            <?php $array = array_dot(json_decode(json_encode($products), true)); ?>
														<?php $references = DB::table('products')->select('id', 'name')->whereNotIn('id', [$product->id])->orderBy('name')->get(); ?>
														@foreach ($references as $reference)
														<div class="checkbox" style="margin-top:0">
															<label>
																<input type="checkbox" name="rel[{{ $i }}][]" value="{{ $reference->id }}" <?php if(in_array($reference->id,$array)) { ?> checked="checked" <?php } ?>>
																{{ $reference->name }}
															</label>
														</div>
														@endforeach
													</div>
												</div>
											</div>
										</div>		
									</div>
									<div class="col-md-4">
										<label>{{ trans('cms.label_product_images') }}</label>
										<p>({{trans('cms.suggested_size')}}: 600x467)</p>
										<div class="row">
										<?php $images = App\Models\Product::find($product->id)->images ?>
											@foreach ($images as $image)
											<div class="col-xs-4 col-md-6">
												<div class="img-wrapper">
													<img src="{{ $image->url }}" alt="{{ $product->name }}" class="img-responsive" style="margin-bottom:20px">
													<div class="mask">
														<a href="#" data-id="{{ $image->id }}" data-token="{{ csrf_token() }}" class="delete-img"><i class="fa fa-trash"></i></a>
													</div>
												</div>
											</div>
											@endforeach
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group" id="attach">
													<input type="file" id="img_prod" name="img_prod[{{ $i }}][]" multiple>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php $i++ ?>
							@endforeach
						</div>
					</div>					
				</div>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
					<a href="/admin/catalog/references" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
				</div>
			</div>
		</form>
	</section>
	
	@endsection

	@push('scripts')

	<script>


	var tag;
	$(function () {

 
			//console.log('ready');
			$('.cp').colorpicker({
				format: 'hex'
			});

			tag = <?= json_encode($tags) ?>;
			$('.tags').tagsinput({
				typeahead: {
					source: tag
				}
			});


		$('#form-edit-reference').bootstrapValidator({
			fields: {
				name: {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				'pname[]': {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				},
				data_sheet: {
					validators: {
						file: {
							extension: 'pdf',
							type: 'application/pdf',
							message: '{{ trans('cms.form_invalid_file') }}'
						}
					}
				},
				user_manual: {
					validators: {
						file: {
							extension: 'pdf',
							type: 'application/pdf',
							message: '{{ trans('cms.form_invalid_file') }}'
						}
					}
				},
				maintenance_manual: {
					validators: {
						file: {
							extension: 'pdf',
							type: 'application/pdf',
							message: '{{ trans('cms.form_invalid_file') }}'
						}
					}
				},
				guarantee: {
					validators: {
						file: {
							extension: 'pdf',
							type: 'application/pdf',
							message: '{{ trans('cms.form_invalid_file') }}'
						}
					}
				},
				img_prod: {
					validators: {
						file: {
							extension: 'jpeg,jpg,png',
							type: 'image/jpeg,image/png',
							maxSize: 2097152,   // 2048 * 1024
							message: '{{ trans('cms.form_invalid_photo') }}'
						}
					}
				},
				'code[]': {
					validators: {
						notEmpty: { message: '{{ trans('cms.form_required_field') }}' }
					}
				}
			}
		});
});

$(".prods").on("click", "a", function (e) {
	e.preventDefault();
	if (!$(this).hasClass('add-product')) {
		$(this).tab('show');
	}
})
.on("click", "span", function () {
	var anchor = $(this).siblings('a');
	$(anchor.attr('href')).remove();
	$(this).parent().remove();
	$(".prods li").children('a').first().click();
});
$('.add-product').click(function (e) {
	e.preventDefault();
	var id = $(".prods").children().length; 
	var tabId = 'prod_' + id;
	$(this).closest('li').before('<li><a href="#prod_' + id + '"># ' + id + '</a> <span> x </span></li>');
	$clone = $('.tab-content').append('<div class="tab-pane active fade" id="' + tabId + '"><div class="row"><div class="col-md-4"><div class="form-group"><label for="pname">{{trans('cms.label_product_name')}}</label><input type="text" name="pname[]" class="form-control" value="{{old('pname')}}" placeholder="" maxlength="100" required><div class="help-block with-errors"></div></div><div class="form-group"><label for="code">{{trans('cms.product_code')}}</label><input type="text" id="code" name="code[]" class="form-control" value="{{old('code')}}" placeholder="" maxlength="100" required><div class="help-block with-errors"></div></div><div class="form-group"><label for="dimensions">{{trans('cms.product_dimensions')}}</label><input type="text" id="dimensions" name="dimensions[]" class="form-control" value="{{old('dimensions')}}" placeholder="" maxlength="100"><div class="help-block with-errors"></div></div><div class="form-group"><label for="color">{{trans('cms.product_color')}}</label><input type="text" id="color" name="color[]" class="form-control" value="{{old('color')}}" placeholder="" maxlength="100"><div class="help-block with-errors"></div></div><div class="form-group"><label for="color_code">{{trans('cms.product_color_code')}}</label><div id="cp'+id+'" class="form-group input-group colorpicker-component"><input type="text" id="color_code" name="color_code[]" class="form-control" value="{{old('color_code')}}" placeholder="" maxlength="100"><span class="input-group-addon"><i></i></span></div><div class="help-block with-errors"></div></div><div class="form-group"><label for="color_code">{{trans('cms.product_tags')}}</label><span>({{trans('cms.message_enter_tags')}})</span><br><input id="tags'+id+'" name="tags[]" type="text" class="form-control" value="" data-provide="typeahead"></div><div class="form-group"><label>{{trans('cms.label_carrusel_status')}}</label><select class="form-control" name="pstatus[]" id="pstatus"><option value="active">active</option><option value="disabled">disabled</option></select></div></div><div class="col-md-4"><div class="row"><div class="col-md-12"><label>{{trans('cms.categories')}}</label><div class="box-body" style="overflow-y: scroll;height: 180px;border: solid 1px #d2d6de;"><?php $categories=App\Models\Category::buildCategoriesTree() ?><div class="form-group"> @foreach ($categories as $categoryLv1)<div class="checkbox" style="margin-top:0"><label style="font-weight:bold"><input type="checkbox" name="cat['+id+'][]" class="cat" value="{{$categoryLv1->id}}">{{$categoryLv1->name}}</label></div>@if(isset($categoryLv1->categories)) @foreach ($categoryLv1->categories as $categoryLv2)<div class="checkbox"><label style="padding-left: 40px;"><input type="checkbox" name="cat['+id+'][]" class="cat" value="{{$categoryLv2->id}}">{{$categoryLv2->name}}</label></div>@if(isset($categoryLv2->categories)) @foreach ($categoryLv2->categories as $categoryLv3)<div class="checkbox"><label style="padding-left: 60px;"><input type="checkbox" name="cat['+id+'][]" class="cat" value="{{$categoryLv3->id}}">{{$categoryLv3->name}}</label></div>@if(isset($categoryLv3->categories)) @foreach ($categoryLv3->categories as $categoryLv4)<div class="checkbox"><label style="padding-left: 80px;"><input type="checkbox" name="cat['+id+'][]" class="cat" value="{{$categoryLv4->id}}">{{$categoryLv4->name}}</label></div> @endforeach @endif @endforeach @endif @endforeach @endif @endforeach</div></div></div></div><div class="row"><div class="col-md-12"><label style="margin-top:20px">{{trans('cms.label_product_related')}}</label><div class="box-body" style="overflow-y: scroll;height: 180px;border: solid 1px #d2d6de;"><div class="form-group"><?php $references = DB::table('products')->select('id', 'name')->orderBy('name')->get(); ?> @foreach ($references as $reference)<div class="checkbox" style="margin-top:0"><label><input type="checkbox" name="rel['+id+'][]" value="{{$reference->id}}">{{$reference->name}}</label></div>@endforeach</div></div></div></div></div><div class="col-md-4"><label>{{trans('cms.label_product_images')}}</label><p>({{trans('cms.suggested_size')}}: 600x467)</p><div class="form-group" id="attach"><input type="file" id="img_prod" name="img_prod['+id+'][]" multiple></div></div></div>');
	$('.prods li:nth-child(' + id + ') a').click();
		$('#cp'+id+'').colorpicker({
    	format: 'hex'
    });
	tag = <?= json_encode($tags) ?>;
		$('#tags'+id+'').tagsinput({
			typeahead: {
			source: tag
		}
	});
});

$('.delete-img').on('click', function(e) {
	var token = $(this).data('token');
	var id = $(this).attr('data-id');
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		type: 'get',
		url: '/admin/catalog/productsImages/destroy/' + id,
		data: { _token :token },
		success: function(data){
			console.log(data);
			window.location.reload();
		}
	});
});


</script>
@endpush