@extends('admin.layouts.main')


@section('title', trans('cms.products'))


@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-cubes"></i>
		{{ trans('cms.products') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/catalog/products') }}">
				<i class="fa fa-cubes"></i>
				{{ trans('menu.products') }}
			</a>
		</li>
		<li class="active">{{ trans('cms.upload_products') }}</li>
	</ol>
</section>

<section class="content">
	
	<div class="box">
		
		<div class="box-header with-border">
			<h3 class="box-title">{{ trans('cms.upload_products') }}</h3>
		</div>
		
		<div class="box-body">
			<form id="form-upload-file-products" action="/admin/catalog/products/upload" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="fileProducts">{{ trans('cms.upload_file_products') }}</label></br>
					<input type="file" id="fileProducts" name="file_products" />
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
					<a href="{{ url('admin/catalog/products') }}" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
				</div>
			</form>
		</div>
		
	</div>
	
</section>

@endsection
