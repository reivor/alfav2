@extends('admin.layouts.main')


@section('title', trans('cms.products'))


@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-cubes"></i>
            {{ trans('cms.products') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/catalog/products') }}">
                    <i class="fa fa-cubes"></i>
                    {{ trans('menu.products') }}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/catalog/products/upload') }}">
                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                    {{ trans('cms.upload_products') }}
                </a>
            </li>
            <li class="active">{{ trans('cms.upload_products_log') }}</li>
        </ol>
    </section>

    <section class="content">
		
		<div class="row margin-bottom">
			<div class="col-xs-12">
				<a href="/admin/catalog/products/create" class="btn btn-primary">
					<i class="fa fa-plus-circle"></i>
					{{ trans('cms.add_product') }}
				</a>
				<a href="/admin/catalog/products/upload" class="btn btn-primary">
					<i class="fa fa-upload"></i>
					{{ trans('cms.upload_products') }}
				</a>
			</div>
		</div>
		
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('cms.upload_files') }}</h3>
                    </div>
                    <div class="box-body">
                        <?php if( $uploads->isEmpty() ): ?>
                        <div class="alert alert-warning"><?= trans('cms.no_uploads_found') ?></div>
                        <?php else: ?>
                            <div class="row">
                                <div class="grid col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>{{ trans('cms.label_user') }}</th>
                                                <th>{{ trans('cms.label_total_success') }}</th>
                                                <th>{{ trans('cms.label_total_error') }}</th>
                                                <th>{{ trans('cms.label_total_warnings') }}</th>
                                                <th>{{ trans('cms.label_status') }}</th>
                                                <th>{{ trans('cms.label_fecha_upload') }}</th>
                                                <th>{{ trans('cms.label_fecha_process_start') }}</th>
                                                <th>{{ trans('cms.label_fecha_process_end') }}</th>
                                                <th>{{ trans('cms.label_download_log') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($uploads as $upload): ?>
                                            <tr>
                                                <td>
													<i class="<?= $upload->fa_icon ?> <?= $upload->text_color ?>"></i>
                                                </td>
                                                <td><?= $upload->user->name ?></td>
                                                <td><?= $upload->total_success ?></td>
                                                <td><?= $upload->total_error ?></td>
                                                <td><?= $upload->total_warnings ?></td>
                                                <td><?= $upload->status ?></td>
                                                <td><?= $upload->formatDate($upload->fecha_upload,'d/m/Y H:i:s') ?></td>
                                                <td><?= $upload->formatDate($upload->fecha_process_start,'d/m/Y H:i:s') ?></td>
                                                <td><?= $upload->formatDate($upload->fecha_process_end,'d/m/Y H:i:s') ?></td>
                                                <td>
                                                    <?php if( $upload->isProcessed() ): ?>
                                                    <a href="<?= $upload->file_url ?>" download class="btn btn-default"><?= trans('cms.label_download_log') ?></a>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection