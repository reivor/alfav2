@extends('admin.layouts.main')

@section('title', trans('cms.products'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-home"></i>
		{{ trans('cms.products') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/catalog/products') }}">
				<i class="fa fa-cubes"></i>
				{{ trans('menu.products') }}
			</a>
		</li>
        <li class="active">
			<?= $product->name ?>
		</li>
	</ol>
</section>

<section class="content">
	
	<div class="box">
		
		<div class="box-header with-border">
			<h3 class="box-title">{{ $product->name }}</h3>
			<div class="box-tools pull-right">
				<a href="/admin/catalog/products/delete-product/<?=$product->id?>" class="btn btn-box-tool"
				   data-confirm="{{ trans('cms.confirm_delete_product_variations') }}">
					<i class="fa fa-trash-o"></i>
				</a>
			</div>
		</div>
		
		<div class="box-body">
			
			<div class="row">
				
				<div class="col-sm-6 col-md-3">
					<img src="{{ $product->imageUrl }}" class="img-thumbnail" />
				</div>

				<div class="col-sm-6 col-md-8">
					<dl class="dl-horizontal">
						<dt>{{ trans('cms.label_product_name') }}</dt>
						<dd>{{ $product->name }}</dd>
						<?php if( $product->description ): ?>
						<dt>{{ trans('cms.label_product_description') }}</dt>
						<dd>{{ $product->description }}</dd>
						<?php endif; ?>
						<?php if( $product->ambience ): ?>
						<dt>{{ trans('cms.label_product_ambiance') }}</dt>
						<dd>{{ $product->ambience }}</dd>
						<?php endif; ?>
						<?php if( $product->enforcement ): ?>
						<dt>{{ trans('cms.label_product_enforcement') }}</dt>
						<dd>{{ $product->enforcement }}</dd>
						<?php endif; ?>
					</dl>
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div class="box">
		
		<div class="box-header with-border">
			<h3 class="box-title">{{ trans('cms.product_variations') }}</h3>
		</div>
		
		<div class="box-body">
			
			<div class="row margin-bottom">
				<div class="col-sm-12">
					<a href="/admin/catalog/products/create/<?=$product->id?>" class="btn btn-success">
						<i class="fa fa-plus-circle"></i>
						{{ trans('cms.add_product_variation') }}
					</a>
				</div>
			</div>
			
			<div class="row">
				<?php foreach($product->variations as $variation): ?>
					<div class="col-sm-6 col-md-3 text-center">
						<a href="/admin/catalog/products/edit/<?=$variation->id?>">
							<img src="<?= $variation->imageUrl ?>" class="img-thumbnail" />
							<?= $variation->name ?>
						</a>
						<a href="/admin/catalog/products/delete-variation/<?=$variation->id?>" class="btn btn-box-tool"
							data-confirm="{{ trans('cms.confirm_delete_product') }}">
							 <i class="fa fa-trash-o"></i>
						 </a>
					</div>
				<?php endforeach; ?>
			</div>
			
		</div>
		
	</div>
	
</section>


@endsection