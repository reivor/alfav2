@extends('admin.layouts.main')


@section('title', trans('cms.products'))


@section('content')


    <section class="content-header">
        <h1>
            <i class="fa fa-cubes"></i>
            {{ trans('cms.products') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/catalog/products/tags') }}">
                    <i class="fa fa-tag"></i>
                    {{ trans('menu.tags') }}
                </a>
            </li>
            <li class="active">{{ trans('menu.products') }}</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title box-title-products">{{ trans('cms.products_list') }}</h3>
                        <div class="box-tools">
                            <div class="btn-group" style="margin-right:5px">
                                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-fw fa-navicon"></i> {{ trans('cms.categories') }} <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" style="height:250px;overflow-x:hidden;overflow-y:scroll;">
                                    <?php $categories = App\Models\Category::buildCategoriesTree() ?>
                                    @foreach ($categories as $categoryLv1)
                                        <li><a href="#" style="font-weight: bold;background: #F5F5F5;">{{ $categoryLv1->name }}</a></li>
                                        @if(isset($categoryLv1->categories))
                                            @foreach ($categoryLv1->categories as $categoryLv2)
                                                <li><a href="#" style="padding-left:30px">{{ $categoryLv2->name }}</a></li>
                                                @if(isset($categoryLv2->categories))
                                                    @foreach ($categoryLv2->categories as $categoryLv3)
                                                        <li><a href="#" style="padding-left:40px">{{ $categoryLv3->name }}</a></li>
                                                        @if(isset($categoryLv3->categories))
                                                            @foreach ($categoryLv3->categories as $categoryLv4)
                                                                <li><a href="#" style="padding-left:50px">{{ $categoryLv4->name }}</a></li>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <form id="form-search-reference" action="/admin/catalog/references" method="get" role="search" data-toggle="validator" class="pull-right">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" id="search" name="search" class="form-control pull-right" placeholder="{{ trans('cms.search') }}">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box-body">
                        <?php if( $products->isEmpty() ): ?>
                        <div class="alert alert-warning">
                            <?= trans('cms.no_products_found') ?>
                        </div>
                        <?php else: ?>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>{{ trans('cms.label_product_name') }}</th>
                                    <th>{{ trans('cms.label_carrusel_description') }}</th>
                                    <th>{{ trans('cms.product_is_new') }}</th>
                                    <th>{{ trans('cms.product_status') }}</th>
                                    <th>&nbsp;</th>
                                </tr>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>
                                            <a href="{{ url('admin/catalog/references/show/' . $product->reference->id ) }}">{{ $product->reference->name }}</a>
                                        </td>
                                        <td>
                                            {{ $short_desc = str_limit($product->reference->description, 100) }}
                                            {{ $short_desc }}
                                        </td>
                                        <td>
                                            @if($product->reference->is_new == 1)
                                                <small class="label bg-yellow">{{ trans('cms.product_is_new') }}</small>
                                            @endif
                                        </td>
                                        <td>
                                            @if($product->reference->status == "active")
                                                <span class="label label-success">{{ $product->reference->status }}</span>
                                            @else
                                                <span class="label label-default">{{ $product->reference->status }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/catalog/references/destroy/' . $product->reference->id ) }}" style="color:red" title="<?= trans('cms.delete') ?>" data-confirm="<?= trans('cms.confirm_delete_product') ?>"><i class="fa fa-fw fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="box-footer clearfix text-right">
                            {!! $products->links() !!}
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
