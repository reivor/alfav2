@extends('admin.layouts.main')


@section('title', trans('cms.products'))


@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-cubes"></i>
        {{ trans('cms.products') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.products') }}</li>
	</ol>
</section>

<section class="content">
	
	<div class="row margin-bottom">
		<div class="col-xs-12">
			<a href="/admin/catalog/products/create" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_product') }}
			</a>
			<a href="/admin/catalog/products/upload" class="btn btn-primary">
				<i class="fa fa-upload"></i>
				{{ trans('cms.upload_products') }}
			</a>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						{{ trans('cms.products_list') }}
						<?php if(isset($catName)){?>
							&nbsp;&nbsp;<i class="fa fa-filter"></i>
							<?=$catName->name?>
						<?php }?>
					</h3>
					<div class="box-tools">
						<div class="btn-group" style="margin-right:5px">
							<form id="form-filter-reference" method="get">
								<select class="form-control" name="category" id="category" style="height: 30px">
									<option value="0">{{ trans('cms.categories') }}</option>
									<?php foreach ($categories as $categoryLv1) {?>
										<option value="<?=$categoryLv1->id?>" <?php if(isset($catName)){ if($categoryLv1->id==$catName->id){?> selected <?php }}?>><?=$categoryLv1->name?></option>
										<?php if(isset($categoryLv1->categories)){ ?>
											<?php foreach ($categoryLv1->categories as $categoryLv2) {?>
												<option value="<?=$categoryLv2->id?>" <?php if(isset($catName)){ if($categoryLv2->id==$catName->id){?> selected <?php }}?>>&nbsp;&nbsp;&nbsp;<?=$categoryLv2->name?></option>
												<?php if(isset($categoryLv2->categories)){ ?>
													<?php foreach ($categoryLv2->categories as $categoryLv3) {?>
														<option value="<?=$categoryLv3->id?>" <?php if(isset($catName)){ if($categoryLv3->id==$catName->id){?> selected <?php }}?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$categoryLv3->name?></option>
														<?php if(isset($categoryLv3->categories)){ ?>
															<?php foreach ($categoryLv3->categories as $categoryLv4) {?>
																<option value="<?=$categoryLv4->id?>" <?php if(isset($catName)){ if($categoryLv4->id==$catName->id){?> selected <?php }}?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$categoryLv4->name?></option>
															<?php }?>

														<?php }?>
													<?php }?>
													
												<?php }?>
											<?php }?>
										<?php }?>
									<?php }?>
								</select>
							</form>
						</div>
						<form id="form-search-reference" action="/admin/catalog/products/search" method="get" role="search" data-toggle="validator" class="pull-right">
							<div class="input-group input-group-sm" style="width: 200px;">
								<input type="search" id="search" name="search" class="form-control pull-right" placeholder="{{ trans('cms.search') }}" value="{{ !empty($search) ? $search : "" }}" required>
								<div class="input-group-btn">
									<button type="submit" class="btn btn-default" style="margin: 0;border-top-left-radius: 0;border-bottom-left-radius: 0;border-left: 0;"><i class="fa fa-search"></i></button>
								</div>
								<a href="/admin/catalog/products" class="btn-reset" id="btn-reset"><i class="fa fa-times"></i></a>
							</div>
						</form>
					</div>
				</div>
				<div class="box-body">
					<?php if( $products->isEmpty() ): ?>
					<div class="alert alert-warning"><?= trans('cms.no_products_found') ?></div>
					<?php else: ?>
						<div class="row">
						<?php foreach($products as $product): ?>
							<div class="col-sm-6 col-md-3 text-center">
								<a href="/admin/catalog/products/view/<?= $product->id ?>">
								<img src="<?= $product->imageUrl ?>" class="img-thumbnail" />
								</a>
								<a href="/admin/catalog/products/view/<?= $product->id ?>">
								<?= $product->name ?>
								</a>
							</div>
						<?php endforeach; ?>
						</div>
						<div class="row">
							<div class="col-sm-6 col-sm-offset-3 text-center">
								{!! $products->appends($pagination)->links() !!}
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('scripts')

<script>
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('[name="_token"]').val()
		}
	});
	
	function showClean(){
		if($('#search').val().length > 0){
			$('#btn-reset').css('display', 'inline-block');
		}else{
			$('#btn-reset').css('display', 'none');
		}
	}
	
	$( document ).ready(function() {
		showClean();
	});
	
	$('#search').on('change', function(e){
		showClean();
	});
	
	$('#category').on('change', function(e){
		if($(this).val()!="-1"){
			var select = $(this), form = select.closest('form');
			form.attr('action', '/admin/catalog/products/category'); //+ select.val());
			form.submit();
		}
	});

</script>

@endpush