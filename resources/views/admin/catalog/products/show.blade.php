<div class="modal-content">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">{{ $products->name }}</h4>
	</div>
	
	<div class="modal-body">
		<div class="row">
			<div class="col-md-8">
				<?php 
				$products_img = DB::table('products_images')
				->select('url')
				->where('product_id', '=', $products->id)
				->first(); 
				?>
				<?php if (!empty($products_img)) { ?>
					<img src="{{ $products_img->url }}" alt="{{ $products->name }}" class="img-responsive img-shadow">
				<?php } ?>
				<?php 
				$products_cat = DB::table('categories')
				->join('products_categories', 'categories.id', '=', 'products_categories.category_id')
				->select('categories.name')
				->where('products_categories.product_id', '=', $products->id)
				->get(); 
				?>
				<?php if (!empty($products_cat)) { ?>
				<ul class="breadcrumb cat-rel">
					@foreach ($products_cat as $category)
					<li class="text-sm">{{ $category->name }}</li>
					@endforeach
				</ul>
				<?php } ?>
			</div>
			<div class="col-md-4">
				<strong>{{ trans('cms.product_code') }}</strong>
				<p>{{ $products->code }}</p>
				<strong>{{ trans('cms.product_dimensions') }}</strong>
				<p>{{ $products->dimensions }}</p>
				<strong>{{ trans('cms.product_color') }}</strong>
				<p>{{ $products->color }}</p>
				<strong>{{ trans('cms.product_color_code') }}</strong>
				<p>{{ $products->color_code }}</p>
			</div>
		</div>

	</div>
</div>