@extends('admin.layouts.main')

@section('title', trans('cms.products'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-home"></i>
		{{ trans('cms.products') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="{{ url('admin/catalog/products') }}">
				<i class="fa fa-cubes"></i>
				{{ trans('menu.products') }}
			</a>
		</li>
        <li class="active">
			{{ trans('cms.create') }}
		</li>
	</ol>
</section>

<section class="content">
	
	<form id="form-create-store" action="/admin/catalog/products/save-product" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
		<?= csrf_field() ?>
		<input type="hidden" name="id_product" value="<?= $product->id ?>" />
		<input type="hidden" name="id_reference" value="<?= $reference ?>" />
		
		<!--PRODUCTO-->
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_data') }}</h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="name_product">{{ trans('cms.label_product_name') }}</label>
							<input type="text" id="name_product" name="name_product" class="form-control" value="<?=$product->reference->name?>" placeholder="" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="quality_seal">{{ trans('cms.label_product_seal_of_quality') }}</label>
							<div class="checkbox">
								<?php for ($i = 1; $i <= 6; $i++) {?>
									<label style="margin-right:20px;"><input type="checkbox" name="quality_seal[]" <?php if(in_array('0'.$i, explode(",",$product->quality_seal))) { ?> checked="checked" <?php } ?> value="<?='0'.$i?>"><?='0'.$i?></label>
								<?php }?>
							</div>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="is_new">{{ trans('cms.product_is_new') }}</label>
							<label><input type="checkbox" name="is_new" value="1" <?php if($product->is_new){?> checked="checked" <?php }?>></label>					
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="ambience">{{ trans('cms.label_product_ambiance') }}</label>
							<input type="text" id="ambience" name="ambience" class="form-control" value="<?=$product->ambience?>" placeholder="">							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="enforcement">{{ trans('cms.label_product_enforcement') }}</label>
							<input type="text" id="enforcement" name="enforcement" class="form-control" value="<?=$product->enforcement?>" placeholder="">							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="use">{{ trans('cms.label_product_use') }}</label>
							<input type="text" id="use" name="use" class="form-control" value="<?=$product->use?>" placeholder="">							
							<div class="help-block with-errors" style="margin-bottom: 22px;"></div>
						</div>
						<div class="form-group">
							<label for="data_sheet">{{ trans('cms.label_product_data_sheet') }}</label>
							<input type="file" id="data_sheet" name="data_sheet" value="">
							<?php if($product->data_sheet) {?><a href="<?=$product->data_sheet?>" target="_blank" class="text-sm"><i class="fa fa-file-pdf-o"></i> <?=$product->data_sheet_name?> </a><?php }?>
							<hr>
						</div>
						<div class="form-group">
							<label for="user_manual">{{ trans('cms.label_product_user_manual') }}</label>
							<input type="file" id="user_manual" name="user_manual">
							<?php if($product->user_manual) {?><a href="<?=$product->user_manual?>" target="_blank" class="text-sm"><i class="fa fa-file-pdf-o"></i> <?=$product->user_manual_name?> </a><?php }?>
							<hr>
						</div>
						<div class="form-group">
							<label for="maintenance_manual">{{ trans('cms.label_product_maintenance_manual') }}</label>
							<input type="file" id="maintenance_manual" name="maintenance_manual">
							<?php if($product->maintenance_manual) {?><a href="<?=$product->maintenance_manual?>" target="_blank" class="text-sm"><i class="fa fa-file-pdf-o"></i> <?=$product->maintenance_manual_name?> </a><?php }?>
							<hr>
						</div>
						<div class="form-group">
							<label for="guarantee">{{ trans('cms.label_product_warranty') }}</label>
							<input type="file" id="guarantee" name="guarantee">
							<?php if($product->guarantee) {?><a href="<?=$product->guarantee?>" target="_blank" class="text-sm"><i class="fa fa-file-pdf-o"></i> <?=$product->guarantee_name?> </a><?php }?>
						</div>
						<div class="form-group">
							<label>{{ trans('cms.label_carrusel_status') }}</label>
							<select class="form-control" name="status" id="status">
								<option <?php if($product->reference_status==$product::STATUS_ACTIVE){?> selected="selected" <?php }?> value="<?=$product::STATUS_ACTIVE?>"><?=$product::STATUS_ACTIVE?></option>
								<option <?php if($product->reference_status==$product::STATUS_DISABLED){?> selected="selected" <?php }?> value="<?=$product::STATUS_DISABLED?>"><?=$product::STATUS_DISABLED?></option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--FIN PRODUCTO-->
		
		<!--VARIACION-->
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.product_variation') }}</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name_variation">{{ trans('cms.label_product_name') }}</label>
									<input type="text" name="name_variation" class="form-control" value="<?=$product->name?>" placeholder="" required>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label for="code_variation">{{ trans('cms.product_code') }}</label>
									<input type="text" id="code_variation" name="code_variation" class="form-control" value="<?=$product->code?>" placeholder="" required>							
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label for="dimensions_variation">{{ trans('cms.product_dimensions') }}</label>
									<input type="text" id="dimensions_variation" name="dimensions_variation" class="form-control" value="<?=$product->dimensions?>" placeholder="">							
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label for="color_variation">{{ trans('cms.product_color') }}</label>
									<input type="text" id="color_variation" name="color_variation" class="form-control" value="<?=$product->color?>" placeholder="">							
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label for="color_code_variation">{{ trans('cms.product_color_code') }}</label>
									<div id="color_code" class="form-group input-group colorpicker-component">
										<input type="text" id="color_code_variation" name="color_code_variation" class="form-control" value="<?=$product->color_code?>" placeholder="">
										<span class="input-group-addon"><i></i></span>
									</div>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label for="description_product">{{ trans('cms.label_carrusel_description') }}</label>
									<textarea id="description_product" name="description_product" class="form-control" rows="6" cols="6"><?=$product->description?></textarea>
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
									<label for="tags_variation">{{ trans('cms.product_tags') }}</label> <span>({{trans('cms.message_enter_tags')}})</span><br>
									<input id="tags_variation" name="tags_variation[]" type="text" class="form-control" value="<?=$product->tags?>" data-provide="typeahead">
								</div>
								<div class="form-group">
									<label>{{ trans('cms.label_carrusel_status') }}</label>
									<select class="form-control" name="status_variation" id="status_variation">
										<option <?php if($product->status==$product::STATUS_ACTIVE){?> selected="selected" <?php }?> value="<?= $product::STATUS_ACTIVE?>"><?= $product::STATUS_ACTIVE?></option>
										<option <?php if($product->status==$product::STATUS_DISABLED){?> selected="selected" <?php }?> value="<?= $product::STATUS_DISABLED?>"><?= $product::STATUS_DISABLED?></option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<!--CATEGORIAS-->
								<div class="row">
									<div class="col-md-12">
										<label>{{ trans('cms.categories') }}</label>
										<div class="box-body content-scrolleable">
											<div class="form-group">
												<?php foreach ($categories as $categoryLv1){ ?>
													<div class="checkbox" style="margin-top:0">
														<label style="font-weight:bold">
															<input type="checkbox" name="categories[]" class="category_variation" value="{{ $categoryLv1->id }}" <?php if($product->categories->contains('id', $categoryLv1->id)) { ?> checked="checked" <?php }?>>
															{{ $categoryLv1->name }}
														</label>
													</div>
													<?php if(isset($categoryLv1->categories)){?>
														<?php foreach ($categoryLv1->categories as $categoryLv2){ ?>
															<div class="checkbox">
																<label style="padding-left: 40px;">
																	<input type="checkbox" name="categories[]" class="category_variation" value="{{ $categoryLv2->id }}" <?php if($product->categories->contains('id', $categoryLv2->id)) { ?> checked="checked" <?php }?>>
																	{{ $categoryLv2->name }}
																</label>
															</div>
															<?php if(isset($categoryLv2->categories)){?>
																<?php foreach ($categoryLv2->categories as $categoryLv3){ ?>
																	<div class="checkbox">
																		<label style="padding-left: 60px;">
																			<input type="checkbox" name="categories[]" class="category_variation" value="{{ $categoryLv3->id }}" <?php if($product->categories->contains('id', $categoryLv3->id)){ ?> checked="checked" <?php }?>>
																			{{ $categoryLv3->name }}
																		</label>
																	</div>
																	<?php if(isset($categoryLv3->categories)){?>
																		<?php foreach ($categoryLv3->categories as $categoryLv4){ ?>
																			<div class="checkbox">
																				<label style="padding-left: 80px;">
																					<input type="checkbox" name="categories[]" class="category_variation" value="{{ $categoryLv4->id }}" <?php if($product->categories->contains('id', $categoryLv4->id)){ ?> checked="checked" <?php }?>>
																					{{ $categoryLv4->name }}
																				</label>
																			</div>
																		<?php } ?>
																	<?php } ?>
																<?php } ?>
															<?php } ?>
														<?php } ?>
													<?php } ?>
												<?php } ?>										
											</div>
										</div>
									</div>
								</div>
								<!--FIN CATEGORIAS-->

								<!--PRODUCTOS RELACIONADOS-->
								<div class="row">
									<div class="col-md-12">
										<label style="margin-top:20px">{{ trans('cms.label_product_related') }}</label>
										<div class="box-body content-scrolleable">
											<div class="form-group">
												<?php foreach ($relatedProducts as $reference){ ?>
													<div class="checkbox" style="margin-top:0">
														<label>
															<input type="checkbox" name="related_product[]" value="{{ $reference->id }}" <?php if($product->relatedProducts->contains('id', $reference->id)){ ?> checked="checked" <?php }?>>
															{{ $reference->name }}
														</label>
													</div>
												<?php }?>
											</div>
										</div>
									</div>
								</div>
								<!--FIN PRODUCTOS RELACIONADOS-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--FIN VARIACION-->
		
		<!--VARIACION-->
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('cms.label_product_images') }}</h3>
					</div>
					<div class="box-body">
						<p>({{trans('cms.suggested_size')}}: 600x467)</p>
						<div class="row">
							<?php foreach($product->images as $image){ ?>
								<div id="img-<?=$image->id?>" class="col-xs-4 col-md-2">
									<div class="img-wrapper">
										<img src="{{ $image->url }}" alt="{{ $product->name }}" class="img-responsive" style="margin-bottom:20px">
										<div class="mask">
											<a href="#" data-id="{{ $image->id }}" data-token="{{ csrf_token() }}" class="delete-img"><i class="fa fa-trash"></i></a>
										</div>
									</div>
								</div>
							<?php }?>
						</div>
						<div class="form-group" id="attach">
							<input type="file" id="images_variation" name="images_variation[]" multiple>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--FIN VARIACION-->

		<div class="box-footer">
			<div class="form-group">
				<input type="submit" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
				<a href="/admin/catalog/products" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
			</div>
		</div>
	</form>
</section>

@endsection

@push('scripts')
<script>
$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('[name="_token"]').val()
	}
});

$('#form-create-store').bootstrapValidator({
	fields: {
		name_product: {
			validators: {
				notEmpty: {message: '<?= trans('auth.form_required_field') ?>'}
			}
		},
		name_variation: {
			validators: {
				notEmpty: {message: '<?= trans('auth.form_required_field') ?>'}
			}
		},	
		code_variation: {
			validators: {
				notEmpty: {message: '<?= trans('auth.form_required_field') ?>'},
				remote: {
					url: '/admin/catalog/check/code/<?=$product->id?>',
					message: '<?= trans('cms.form_duplicated_code') ?>',
					delay: 1000
				}
			}
		}
	}
});

$('#color_code').colorpicker({
	format: 'hex'
});

$('#tags_variation').tagsinput();


$('.delete-img').on('click', function(e) {
	e.preventDefault();
	var id = $(this).attr('data-id');
	
	var params= {
		"id" : id
	};
	
	$.post( "/admin/catalog/productsImages/destroy-image", params, function(data) {
		if(parseInt(data)==1){
			$('#img-'+id).remove();
		}
	});
});

</script>
@endpush