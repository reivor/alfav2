@extends('admin.layouts.main')


@section('title', trans('cms.tags'))


@section('content')

    <section class="content-header">
        <h1>
            <i class="fa fa-tags"></i>
            {{ trans('cms.tags') }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    {{ trans('menu.dashboard') }}
                </a>
            </li>
            <li class="active">{{ trans('menu.tags') }}</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('cms.tags') }}</h3>
                    </div>
                    <div class="box-body">
                        <?php if( $tags->isEmpty() ): ?>
                        <div class="alert alert-warning"><?= trans('cms.no_tags_found') ?></div>
                        <?php else: ?>
                        <div class="row">
                            <div class="grid col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>{{ trans('cms.label_name') }}</th>
                                            <th>{{ trans('cms.details') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($tags as $tag): ?>
                                        <tr>
                                            <td width="75%"><a href="<?= url('admin/catalog/products/productbytags/' . $tag->name) ?>"> <?= $tag->name ?> </a></td>
                                            <td width="25%">
                                                <a href="<?= url('admin/catalog/products/borrartags/' . $tag->name) ?>" class="btn btn-danger" onclick="confirm('{{ trans('cms.confirm_delete_tags') }}');"> {{ trans('cms.delete') }} </a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection