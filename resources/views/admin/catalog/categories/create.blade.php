@extends('admin.layouts.main')


@section('title', trans('cms.categories'))


@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('cms.categories') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/catalog/categories">
				<i class="fa fa-tags"></i> 
				{{ trans('menu.categories') }}
			</a>
		</li>
        <li class="active">{{ trans('cms.create') }}</li>
	</ol>
</section>

<section class="content">
	<form id="form-create-category" action="/admin/catalog/categories/save" method="post" role="form" data-toggle="validator" enctype="multipart/form-data">
		<?= csrf_field() ?>
		<input type="hidden" id="id" name="id" value="<?= $category->id ?>" />
		<input type="hidden" id="level" name="level" value="<?= $category->level ?>" />
		<input type="hidden" id="parent" name="parent" value="{{ isset($category->parentCategory->id) ? $category->parentCategory->id : null }}" />
		
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('cms.add_category') }}</h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label for="parent">{{ trans('cms.label_category_parent') }}</label>
					<div class="form-static">
						{{ isset($category->parentCategory->id) ? $category->parentCategory->name : '-' }}
					</div>
				</div>
				<div class="form-group">
					<label for="name">{{ trans('cms.label_category_name') }}</label>
					<input type="text" id="name" name="name" class="form-control" value="<?= $category->name ?>" placeholder="" maxlength="64" required />
					<div class="help-block with-errors"></div>
				</div>
				<?php if($category->level==1) {?>
					<div class="form-group">
						<label for="display_name_first">{{ trans('cms.label_category_display_name_first') }}</label>
						<input type="text" id="display_name_first" name="display_name_first" class="form-control" value="<?= $category->display_name_first ?>" placeholder="" maxlength="128" required />
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="display_name_second">{{ trans('cms.label_category_display_name_second') }}</label>
						<input type="text" id="display_name_second" name="display_name_second" class="form-control" value="<?= $category->display_name_second ?>" placeholder="" maxlength="128" required />
						<div class="help-block with-errors"></div>
					</div>
				<?php }?>
				<div class="form-group">
					<label for="description">{{ trans('cms.label_category_description') }}</label>
					<textarea id="description" name="description" class="form-control"><?= $category->description ?></textarea>
				</div>
				<div class="form-group">
					<label for="tags">{{ trans('cms.label_category_tags') }}</label>
					<input type="text" id="tags" name="tags" class="form-control" value="<?= $category->tags ?>" placeholder="" maxlength="128" />
				</div>
				<div class="form-group">
					<label for="banner">{{ trans('cms.label_category_banner') }}</label> <span>({{trans('cms.suggested_size')}}:370x325)</span></br>
					<img id="uploadPreview" src="{{$category->image_url}}" width="150"/></br></br>
					<input type="file" id="banner" name="banner" class="form-control"  placeholder=""/>
				</div>
				<div class="form-group">
					<label for="banner">{{ trans('cms.label_category_banner_mobile') }}</label> <span>({{trans('cms.suggested_size')}}:497x360)</span></br>
					<img id="uploadMobilePreview" src="{{$category->image_mobile_url}}" width="150"/></br></br>
					<input type="file" id="banner-mobile" name="banner-mobile" class="form-control"  placeholder=""/>
				</div>
				<div class="form-group">
					<label for="banner">{{ trans('cms.header') }}</label> <span>({{trans('cms.suggested_size')}}:1379x657)</span></br>
					<img id="uploadHeaderPreview" src="{{$category->header_url}}" width="150"/></br></br>
					<input type="file" id="header" name="header" class="form-control"  placeholder=""/>
				</div>
			</div>
			<div class="box-footer">
				
			</div>
		</div>

		<?php if(isset ($categories)){ ?>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.label_banners_detail')}}</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-6 col-sm-6 banners-site" id="banners-detail">
					
						</div>
					</div>
					
				</div>
			</div>
		
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.label_banners_search')}}</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-6 col-sm-6 banners-site" id="banners-search">

						</div>
					</div>
				</div>
			</div>
		<?php }?>
		
		<div class="box">
			<div class="box-body">
				<div class="form-group">
					<input type="submit" id="send-form" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
					<a href="/admin/catalog/categories" class="btn btn-default">{{ trans('cms.btn_cancel') }}</a>
				</div>
			</div>
		</div>
		
	</form>
</section>

<!--MODAL-->
<div class="modal fade" id="modal-form-banner" tabindex="-1" role="dialog" aria-labelledby="modal-good-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="dismiss-modal">×</button>
				<i class="fa fa-image"></i> <span id="title-modal" style="font-size:18px;"></span>
			</div>
			<div class="modal-body">
				<input type="hidden" id="current-index-banner" value="" />
				<input type="hidden" id="current-location" value="" />
				<!--Alertas-->
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="alert alert-dismissable alert-danger" id="alert-error-modal" style="display: none">
							{{trans('cms.form_requierd_field_many')}}:
							<ul id="error-text-modal"class="error-text"></ul>
						</div>
					</div>
				</div>
				<!--Fin alertas-->
				<div class="form-group">
					<label>{{ trans('cms.label_carrusel_linked_to') }}</label>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-category" value="category" checked="" data-index="">
							{{ trans('cms.label_carrusel_category') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-product" value="product" data-index="">
							{{ trans('cms.label_carrusel_product') }}
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" class="type-radio" name="type-radio" id="type-external" value="external" data-index="">
							{{ trans('cms.label_carrusel_external_link') }}
						</label>
					</div>
				</div>
				<div class="form-group option" id="linked-category">
					<label>{{ trans('cms.label_carrusel_choose_category') }}</label>
					<select class="form-control" id="entity-category">
						<option value="0">{{trans('cms.select')}}</option>
						<?php if(isset($categories)){?>
							<?php foreach ($categories as $categoryLv2){?>
								<option value="<?=$categoryLv2->id?>"><?=$categoryLv2->name?></option>
								<?php if(isset($categoryLv2->categories)){?>
									<?php foreach($categoryLv2->categories as $categoryLv3): ?>
										<option value="<?=$categoryLv3->id?>">&nbsp;&nbsp;&nbsp;<?=$categoryLv3->name?></option>
										<?php if(isset($categoryLv3->categories)){?>
											<?php foreach($categoryLv3->categories as $categoryLv4): ?>
												<option value="<?=$categoryLv4->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$categoryLv4->name?></option>
											<?php endforeach; ?>
										<?php }?>
									<?php endforeach; ?>
								<?php }?>
							<?php }?>
						<?php }?>
					</select>
				</div>
				<div class="form-group option" id="linked-product">
					<label>{{ trans('cms.label_carrusel_choose_product') }}</label>
					<select class="form-control" id="entity-product">
						<option value="0">{{trans('cms.select')}}</option>
						<?php if(isset($products)){?>
							<?php foreach ($products as $product) {?>
								<option value="<?= $product->id ?>"><?= $product->name ?></option>
							<?php } ?>
						<?php }?>
					</select>
				</div>
				<div class="form-group option" id="linked-external">
					<label for="url">{{ trans('cms.label_carrusel_url') }}</label>
					<input type="text" id="entity-external" class="form-control" value="" placeholder="" maxlength="100" />
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group image-content">
					<label>{{ trans('cms.label_carrusel_url_image') }}</label> <span id="suggested-size"></span>
					<input type="file" class="image-url" name="image" />
				</div>
				<div class="form-group" id="banner-status">
					<label>{{ trans('cms.label_status') }}</label>
					<select class="form-control" id="status-banner">
						<?php foreach ($status as $st){?>
							<option value="<?=$st?>"><?=$st?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<input type="button" id="send-banner" class="btn btn-primary" value="{{ trans('cms.btn_save') }}" />
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('templates')
<script id="tpl-banner" type="text/x-handlebars-template">
	<a href class="clean-banner clean-banner-site" rel="@{{ index }}" data-location="@{{ location }}" data-sure="<?= trans('cms.confirm_delete_carrusel') ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
	<div class="image-container-thumb" rel="@{{ index }}" data-location="@{{ location }}" id="image-@{{ index }}">
		<img class="image-src-@{{ index }}" src="@{{banner.image_url}}"/>
	</div>
	<input type="hidden" class="image-get-@{{ index }}" name="image-get-@{{ location }}[@{{ index }}]" value="@{{banner.image_url}}" />
	<input type="hidden" class="type type-@{{ index }}" name="type-@{{ location }}[@{{ index }}]" value="@{{banner.type}}" />
	<input type="hidden" class="entity entity-@{{ index }}" name="entity-@{{ location }}[@{{ index }}]" value="@{{banner.entity_id}}" />
	<input type="hidden" class="external-@{{ index }}" name="external-@{{ location }}[@{{ index }}]" value="@{{banner.external_url}}" />
	<input type="hidden" class="status-@{{ index }}" name="status-@{{ location }}[@{{ index }}]" value="@{{banner.status}}" />
</script>

<script id="tpl-layout-search" type="text/x-handlebars-template">
	<div class="grid" id="layout-search">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width4 grid-item--height1 item-1"></div>
		<div class="grid-item grid-item--width2 grid-item--height1 item-2"></div>
	</div>
</script>

<script id="tpl-layout-detail" type="text/x-handlebars-template">
	<div class="grid" id="layout-detail">
		<div class="grid-sizer"></div>
		<div class="grid-item grid-item--width6 grid-item--height1 item-1"></div>
	</div>
</script>
@endpush

@push('plugins')
<link rel="stylesheet" href="/themes/admin/vendor/jstree/dist/themes/default/style.min.css" />
<link rel="stylesheet" href="/themes/admin/css/layout-banners.css" />
<script src="/themes/admin/vendor/packery/dist/packery.pkgd.min.js"></script>
<script src="/themes/admin/vendor/jstree/dist/jstree.min.js"></script>
@endpush

@push('scripts')
<script>
		var modal=$("#modal-form-banner");
		var search='search';
		var detail= 'detail';
		var bannersSearch=<?= json_encode($bannersSearch)?>;
		var bannersDetail=<?= json_encode($bannersDetail)?>;
		var idAmbience=<?= json_encode($idAmbience)?>;
$(function () {
	
    function quitAlerts(){
		$(".has-error" ).each(function() {
			$(this).removeClass('has-error');
		});
		$(".alert-danger" ).css("display", "none");
		$(".error-text").empty();
    }
	
	//-----------------------------------------------
    function initializeEntities(){
         if ($("#type-category").is(':checked')){
            $("#linked-category").show();
            $("#linked-product").hide();
            $("#linked-external").hide();
        }

        if ($("#type-product").is(':checked')){
            $("#linked-product").show();
            $("#linked-category").hide();
            $("#linked-external").hide();
        }

        if ($("#type-external").is(':checked')){
            $("#linked-external").show();
            $("#linked-product").hide();
            $("#linked-category").hide();
        }
    }
	
	//-----------------------------------------------
	function getCurrentBannerArray(layout){
		if(layout == search){
			return bannersSearch;
		}else if(layout == detail){
			return bannersDetail;
		}
	}

	//-----------------------------------------------
	function numberItemsLayout(layout){
		switch(layout){
			case 'search':
				return 2;
			case 'detail':
				return 1;		
		}
	}
	
	//-----------------------------------------------
	function suggestedSize (indexImage, location){
		
		if(location==search){
			if(indexImage==1){
				return "800x200";
			}else{
				return "400x200";
			}
		}else if(location==detail){
			return "1200x200";
		}
		
	}
	
	//-----------------------------------------------
	function cleanSection(){
		$('#entity-category').val(0);
		$('#entity-product').val(0);
		$('#entity-external').val("");
	}
	
	//-----------------------------------------------
	function constructLayout(firstTime, typeLayout){
		var containerLayout = $('#banners-'+typeLayout);
		containerLayout.empty();
		var templateLayout = Handlebars.compile($('#tpl-layout-'+typeLayout).html());
		containerLayout.append(templateLayout);
		
		var numberItems=numberItemsLayout(typeLayout);
		
		$('.grid').packery({
			itemSelector: '.grid-item',
			gutter: 4,
			rowHeight: '.grid-sizer',
			percentPosition: true
		});
		
		var banners=getCurrentBannerArray(typeLayout);

		for ( var i = 1, j = parseInt(numberItems); i <= j; i++ ) {	
			if(firstTime){
				if(banners[i] === undefined || banners[i] === null){
					banners[i]={has_image: 0, image_url: '/images/placeholder.png', has_mobile_image: 0};
				}else{
					banners[i].has_image=1;
					banners[i].has_mobile_image=1;
				}
			}else{
				banners[i]={has_image: 0, image_url: '/images/placeholder.png', has_mobile_image: 0};
			}
			
			var templateBanner = Handlebars.compile($('#tpl-banner').html());
			var htmlLayout = templateBanner({ index: i, banner: banners[i], location : typeLayout});
			$(".item-"+i, "#banners-"+typeLayout).append(htmlLayout);
		}
		
	}
	
	//-----------------------------------------------
	function showModal(content){
		quitAlerts();
		
		var currentBannerIndex=content.attr('rel');
		var location=content.attr('data-location');
		var textLocation="";
		
		if(location==search){
			textLocation="<?= trans('cms.search')?>";
		}else if(location==detail){
			textLocation="<?= trans('cms.detail')?>";
		}
		
		$("#current-index-banner").val(currentBannerIndex);
		$("#current-location").val(location);
		
		var title="<?= trans('cms.data_banner_category')?> "+textLocation+" "+currentBannerIndex;
		$("#title-modal").text(title);
		
		var type=$(".type-"+currentBannerIndex, '#banners-'+location).val();
		var entity=$(".entity-"+currentBannerIndex, '#banners-'+location).val();
		var external=$(".external-"+currentBannerIndex, '#banners-'+location).val();
		var status=$(".status-"+currentBannerIndex, '#banners-' + location).val();

		$('.image-url', modal).val("");

		if(type){
			$('input[name="type-radio"][value="' + type + '"]').prop('checked', true);
			if(type=="external"){
				$('#entity-'+type).val(external);
			}else{
				if(entity!=""){
					$('#entity-'+type).val(entity);
				}
			}
			
			$('#status-banner').val(status);
		}else{
			$('input[name="type-radio"][value="category"]').prop('checked', true);
			cleanSection();
		}

		initializeEntities();
		
		var size=suggestedSize(currentBannerIndex,location);
		$('#suggested-size').text("(<?= trans('cms.suggested_size')?>: "+size+")");
		
		$('#modal-form-banner').modal('show');
	}
	
	//-----------------------------------------------
	function getCurrentSection(layout){
		switch(layout){
			case detail:
				return "<?=trans('label_banners_detail')?>";
			case search:
				return "<?=trans('cms.label_banners_search')?>";
		}
	}
	
	//-----------------------------------------------
    $("#send-banner").click(function() {
        quitAlerts();
		
        var success=true;
        var errorMessage="";
        
		var currentBanner= $("#current-index-banner").val();
		var currentLocation= $("#current-location").val();
        var type=$("input[name=type-radio]:checked").val();
		
		var banners=getCurrentBannerArray(currentLocation);
            
        if(type=="category" && $("#entity-category").val()==0){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.category')?>"+"</li>";
            $("#entity-category").parent().addClass("has-error");
            success=false;
        }else if(type=="product" && $("#entity-product").val()==0){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.product')?>"+"</li>";
            $("#entity-product").parent().addClass("has-error");
            success=false;
        }else if(type=="external" && $("#entity-external").val()==""){
            errorMessage=errorMessage+"<li>"+"<?= trans('cms.url')?>"+"</li>";
            $("#entity-external").parent().addClass("has-error");
            success=false;
        }

		if(banners[currentBanner].has_image==0){
			if($('.image-url', modal).val()==''){
				$('.image-url', modal).parent().addClass("has-error");
				errorMessage=errorMessage+"<li>"+"<?= trans('cms.image')?>"+"</li>";
				success=false;
			}	
		}
		
		if(banners[currentBanner].has_mobile_image==0){
			if($('.image-mobile-url', modal).val()==''){
				$('.image-mobile-url', modal).parent().addClass("has-error");
				errorMessage=errorMessage+"<li>"+"<?= trans('cms.label_image_mobile')?>"+"</li>";
				success=false;
			}	
		}

        if(success){
            var entity="";
			$(".type-"+currentBanner, '#banners-'+currentLocation).val(type);
            entity=$("#entity-"+type).val();
			if(type=="external"){
				$('.external-'+currentBanner, "#banners-"+currentLocation).val(entity);
			}else{
				$('.entity-'+currentBanner, "#banners-"+currentLocation).val(entity);
			}
			
			var status = $("#status-banner").val();
			$(".status-"+currentBanner, "#banners-" + currentLocation).val(status);

			/* FILE IMAGE*/
			var file=$('.image-url', modal);
			
			if(file.val()!=""){
				var imageClone=$('.image-url', modal).clone();
				$("input[name='image-"+currentLocation+"["+currentBanner+"]'").remove();
				file.appendTo("#form-create-category").attr('name', 'image-'+currentLocation+'['+currentBanner+']').css("opacity", 0);
				imageClone.appendTo(".image-content");
				banners[currentBanner].has_image=1;
			}
			
			if(file.val()!=""){//Preview web site
				if (file[0].files && file[0].files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('.image-src-'+currentBanner, '#banners-'+currentLocation).attr('src', e.target.result);
					}
					reader.readAsDataURL(file[0].files[0]);
				}
			}

            $('#modal-form-banner').modal('hide');
        }else{
            $("#error-text-modal").append(errorMessage);
            $("#alert-error-modal" ).slideDown("slow");
        }
    });
	
	//-----------------------------------------------
    $('input[type=radio]').on('change',function() {//Types
        $('#linked-category').hide();
        $('#linked-product').hide();
        $('#linked-external').hide();
        $('#linked-'+$(this).val()).show();
		
		cleanSection();
    });
	
	//-----------------------------------------------
    $('#send-form').click(function(e) {
	
		var success=true;
		var message="<?=trans('cms.message_select_all_banners')?>";
		
		var selectedBanners=$(".type", "#banners-search");

		var numberItems = numberItemsLayout(search);
		var cont=0;
		
		for (j=0; j<numberItems; j++){
			if(selectedBanners[j].value!="" && selectedBanners[j].value!=null){
				cont=cont+1;
			}	
		}	
			
		if(cont > 0 && cont!=numberItems){
			var section=getCurrentSection(search);
			message=message+'<br> -'+section;
			success=false;
		}


		if(!success){
			bootbox.alert(message, function() {  
			});
			
			e.preventDefault();
		}

    });
	
	
	
	//-----------------------------------------------
	$("#header").change(function() {
		if($(this).val()!=""){
			if ($(this)[0].files && $(this)[0].files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#uploadHeaderPreview').attr('src', e.target.result);
				}
				reader.readAsDataURL($(this)[0].files[0]);
			}
		}
	});
	
	//-----------------------------------------------
	$("#banner").change(function() {
		if($(this).val()!=""){
			if ($(this)[0].files && $(this)[0].files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#uploadPreview').attr('src', e.target.result);
				}
				reader.readAsDataURL($(this)[0].files[0]);
			}
		}
	});
	
	//-----------------------------------------------
	$("#banner-mobile").change(function() {
		if($(this).val()!=""){
			if ($(this)[0].files && $(this)[0].files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#uploadMobilePreview').attr('src', e.target.result);
				}
				reader.readAsDataURL($(this)[0].files[0]);
			}
		}
	});

	//-----------------------------------------------
	function assembleLayouts(){
		constructLayout(true, search);
		constructLayout(true, detail);
	}
	
	//-----------------------------------------------
	assembleLayouts();
	
	$(".image-container-thumb").click(function() {
		showModal($(this));
	});
	
	$(".clean-banner").click(function(e) {
		e.preventDefault();
		var index=$(this).attr('rel');
		var location=$(this).attr('data-location');
		var banners=getCurrentBannerArray(location);

		if(parseInt(banners[index].has_image)==parseInt(1)){
			bootbox.confirm({
				message: $(this).data('sure'),
				buttons: {
					'confirm': {
						label: '<?=trans('cms.btn_accept') ?>',
						className: 'btn-default'
					},
					'cancel': {
						label: '<?=trans('cms.btn_cancel') ?>',
						className: 'btn-danger'
					}
				},
				callback: function(result) {
					if (result) {
						$("input[name='image-"+location+"["+index+"]'").remove();
						$('.image-src-'+index,'#banners-'+location).attr('src',"/images/placeholder.png");
						$("input[name='type-"+location+"["+index+"]'").val("");
						$("input[name='entity-"+location+"["+index+"]'").val("");
						$("input[name='external-"+location+"["+index+"]'").val("");
						$("input[name='image-get-"+location+"["+index+"]'").val("");
						banners[index].has_image=0;
					}
				}
			});
		}
	});
	 
	
	//-----------------------------------------------
	$('#form-create-category').bootstrapValidator({
		fields: {
			name: {
				validators: {
					notEmpty: { message: '<?= trans('cms.form_required_field') ?>' },
					remote: {
						url: '/admin/catalog/categories/check?current=<?= $category->id ?>&parent=<?= $category->parentCategory ? $category->parentCategory->id : '' ?>',
						message: '<?= trans('cms.message_category_duplicated') ?>',
						delay: 1000
					}
				}
			}
		}
	});
	
	
	
});
</script>
@endpush