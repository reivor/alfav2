@extends('admin.layouts.main')


@section('title', trans('cms.categories'))


@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-tags"></i>
        {{ trans('cms.categories') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('menu.categories') }}</li>
	</ol>
</section>

<section class="content">

	<div class="row margin-bottom">
		<div class="col-xs-12">
			<a href="/admin/catalog/categories/create" class="btn btn-primary">
				<i class="fa fa-plus-circle"></i>
				{{ trans('cms.add_category') }}
			</a>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6 col-sm-6">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.categories') }}</h3>
				</div>
				<div class="box-body">
					<div id="tree_categories">
						<ul>
							<li class="jstree-open" data-payload='{"root":true, "level":0}'>
								ALFA
								<ul>
								<?php foreach($categories as $categoryLv1): ?>
									<?php $payload = ['root' => false, 'level' => 1, 'category' => $categoryLv1]; ?>
								<li data-payload='<?= json_encode($payload) ?>'><?= $categoryLv1->name." - ".$categoryLv1->code ?>
									<?php if(isset($categoryLv1->categories)){?>
										<ul>
											<?php foreach($categoryLv1->categories as $categoryLv2): ?>
												<?php $payload = ['root' => false, 'level' => 2, 'category' => $categoryLv2]; ?>
												<li data-payload='<?= json_encode($payload) ?>'><?= $categoryLv2->name." - ".$categoryLv2->code ?>
													<?php if(isset($categoryLv2->categories)){?>
														<ul>
															<?php foreach($categoryLv2->categories as $categoryLv3): ?>
																<?php $payload = ['root' => false, 'level' => 3, 'category' => $categoryLv3]; ?>
																<li data-payload='<?= json_encode($payload) ?>'><?= $categoryLv3->name." - ".$categoryLv3->code ?>
																<?php if(isset($categoryLv3->categories)){?>
																	<ul>
																		<?php foreach($categoryLv3->categories as $categoryLv4): ?>
																			<?php $payload = ['root' => false, 'level' => 4, 'category' => $categoryLv4]; ?>
																			<li data-payload='<?= json_encode($payload) ?>'><?= $categoryLv4->name." - ".$categoryLv4->code ?>
																		<?php endforeach; ?>
																	</ul>
																<?php }?>
															<?php endforeach; ?>
														</ul>
													<?php }?>
											<?php endforeach; ?>
										</ul>
									<?php }?>
								</li>
								<?php endforeach; ?>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div id="display-category" class="col-xs-6 col-sm-6 hidden">
			
		</div>
		
</section>

@endsection


@push('plugins')
<link rel="stylesheet" href="/themes/admin/vendor/jstree/dist/themes/default/style.min.css" />
<script src="/themes/admin/vendor/jstree/dist/jstree.min.js"></script>
@endpush


@push('templates')
<script id="tpl-category" type="text/x-handlebars-template">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">@{{ category.name }}</h3>
		</div>
		<div class="box-body">
			<ul class="list-group list-group-unbordered">
				<li class="list-group-item">
					<b><?= trans('cms.label_category_name') ?></b>
					<p>@{{ category.name }}</p>
				</li>
				@{{#ifCond category.level '==' 1 }}
					<li class="list-group-item">
						<b><?= trans('cms.label_category_display_name_first') ?></b>
						<p>@{{ category.display_name_first }}</p>
					</li>
					<li class="list-group-item">
						<b><?= trans('cms.label_category_display_name_second') ?></b>
						<p>@{{ category.display_name_second }}</p>
					</li>
				@{{/ifCond}}
				<li class="list-group-item">
					<b><?= trans('cms.label_category_description') ?></b>
					<p>@{{ category.description }}</p>
				</li>
				<li class="list-group-item">
					<b><?= trans('cms.label_category_tags') ?></b>
					<p>@{{ category.tags }}</p>
				</li>
				<li class="list-group-item">
					<b><?= trans('cms.label_category_banner') ?></b><br>
					<img id="uploadPreview" src="@{{ category.image_url }}" width="150"/>
				</li>
			</ul>
		</div>
		<div class="box-footer">
			<div class="form-group">
				<a href="/admin/catalog/categories/edit/@{{ category.id }}" class="btn btn-default">
					<i class="fa fa-edit"></i>
					<?= trans('cms.edit') ?>
				</a>
				@{{#ifCond category.level '<' 4 }}
					<a href="/admin/catalog/categories/create/@{{ category.id }}" class="btn btn-primary">
						<i class="fa fa-plus-circle"></i>
						<?= trans('cms.add_subcategory') ?>
					</a>
				@{{/ifCond}}
				<a href="/admin/catalog/categories/delete/@{{ category.id }}" class="btn btn-danger" data-confirm="<?= trans('cms.confirm_delete_category') ?>">
					<i class="fa fa-trash-o"></i>
					<?= trans('cms.delete') ?>
				</a>
			</div>
		</div>
	</div>
</script>
@endpush

@push('scripts')
<script>
$(function() {
	
	console.log('ready');
	
	$('#tree_categories')
			.on('changed.jstree', function(event, data) {
				var node = data.instance.get_node(data.selected[0]);
				var payload = node.data.payload;
				console.log(node.data.payload);
				if( payload.level == 0 ) {
					// hide the display box
					$('#display-category').addClass('hidden');
				}
				else {
					displayCategory(payload.category);
				}
			})
			.jstree();
	
	var displayCategory = function(category) {
		var template = Handlebars.compile( $('#tpl-category').html() );
		var html = template({ category: category });
		var $container = $('#display-category');
		$container.html( html );
		$container.removeClass('hidden');
	};
	
	$('#form-category').submit(function(e) {
		
		e.preventDefault();	// submit is via ajax
		
		var $form = $(this);
		
		// show loading state
		$('.overlay', $form).removeClass('hidden');
		
		// submit form
		$.post({
				url: '/admin/catalog/categories/save',
				data: $form.serialize()
			})
				.done(function() {
					console.log('success');	
				})
				.fail(function() {
					console.log('error');
				})
				.always(function() {
					// hide loading state
					$('.overlay', $form).addClass('hidden');
				});
		return false;
	});
	
});
</script>
@endpush