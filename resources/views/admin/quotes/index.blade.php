@extends('admin.layouts.main')


@section('title', trans('cms.slider_home'))


@section('content')

<section class="content-header">
	
	<h1>
		<i class="fa fa-shopping-cart"></i>
        {{ trans('cms.quotes') }}
        <small></small>
	</h1>
	<ol class="breadcrumb">
        <li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
        <li class="active">{{ trans('cms.quotes') }}</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('cms.quotes') }}</h3>
					<a class="btn btn-success pull-right" href="{{ URL::to('admin/quotes/downloadQuotes') }}">Descargar</a>
				</div>
				<div>
					<form class="container fluid">
					<br>
						<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
							<select name="status_filter" class="form-control">
	                    		<option value="">{{ trans('cms.label_carrusel_status') }}</option>
								<option value="open" <?= $status_filter == 'open' ? 'selected' : ''?>>Abierto</option>
								<option value="new" <?= $status_filter == 'new' ? 'selected' : ''?>>Nuevo</option>
								<option value="sent" <?= $status_filter == 'sent' ? 'selected' : ''?>>Enviado</option>
								<option value="received" <?= $status_filter == 'received' ? 'selected' : ''?>>Recibido</option>
								<option value="noinfo" <?= $status_filter == 'noinfo' ? 'selected' : ''?>>No Info</option>
								<option value="done" <?= $status_filter == 'done' ? 'selected' : ''?>>Realizado</option>
							</select>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
							<select name="room_filter" class="form-control">
	                    		<option value="">Sala</option>
	                    		@foreach($stores as $store)
	                    		<option value="{{$store->id}}" <?= $store->id == $room_filter ? 'selected' : ''?>>{{ $store->name }}</option>
	                    		@endforeach
	                    	</select>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
							<input class="btn btn-default" type="submit" name="" value="Filtrar">
						</div>
					</form>
				</div>
				<div class="box-body">
					<?php if( $quotes->isEmpty() ): ?>
					<div class="alert alert-warning"><?= trans('cms.no_quotes_found') ?></div>
					<?php else: ?>
			            <div class="box-body table-responsive no-padding">
			              <table class="table table-hover">
			              	<tr>
			                  <th><a href="{{ URL::to('admin/quotes?id=') }}<?= $id == 'desc' ? 'asc' : 'desc'?>">ID</a></th>
							  <th><a href="{{ URL::to('admin/quotes?name=') }}<?= $name == 'desc' ? 'asc' : 'desc'?>">{{ trans('cms.customer') }}</a></th>
							  <th><a href="{{ URL::to('admin/quotes?email=') }}<?= $email == 'desc' ? 'asc' : 'desc'?>">{{ trans('cms.customer_email') }}</a></th>
							  <th><a href="{{ URL::to('admin/quotes?city=') }}<?= $city == 'desc' ? 'asc' : 'desc'?>">{{ trans('cms.customer_city') }}</a></th>
			                  <th><a href="{{ URL::to('admin/quotes?manager=') }}<?= $manager == 'desc' ? 'asc' : 'desc'?>">{{ trans('cms.manager') }}</a></th>
			                  <th><a href="{{ URL::to('admin/quotes?room=') }}<?= $room == 'desc' ? 'asc' : 'desc'?>">{{ trans('cms.room') }}</a></th>
			                  <th><a href="{{ URL::to('admin/quotes?date=') }}<?= $date == 'desc' ? 'asc' : 'desc'?>">{{ trans('cms.quote_date') }}</a></th>
			                  <th><a href="{{ URL::to('admin/quotes?status=') }}<?= $status == 'desc' ? 'asc' : 'desc'?>">{{ trans('cms.label_carrusel_status') }}</a></th>
			                  <th>{{trans('cms.comment')}}</th>
			                  <th>&nbsp;</th>
			                </tr>
			                @foreach ($quotes as $quote)
	                            <tr>
	                                <td>
	                                    <a href="{{ URL::to('admin/quotes/show/' . $quote->id ) }}">{{ $quote->id }}</a>
	                                </td>
									<td>
										{{ @$quote->customer->fullName }}
									</td>
									<td>
										{{ @$quote->customer->email }}
									</td>
									<td>
										{{ @$quote->customer->getCity['name'] }}
									</td>
	                                <td>
	                                	<input class="manager" type="text" placeholder="Nombre del gerente" data-quote="{{ $quote->id }}" value="{{@$quote->manager}}">
	                                </td>
	                                <td>
	                                	<select class="room" data-quote="{{ @$quote->id }}">
	                                		<option value="">Sala</option>
	                                		@foreach($stores as $store)
	                                		<option value="{{$store->id}}" <?= $store->id == $quote->store_id ? 'selected' : ''?>>{{ @$store->name }}</option>
	                                		@endforeach
	                                	</select>
	                                </td>
	                                <td>
	                                    {{ $quote->date_created->format('d/m/Y') }}
	                                </td>
	                                <td style="text-align: center;">
	                                	<select class="status" value="{{$quote->status}}"  data-quote="{{ $quote->id }}">
											<option value="open" <?= $quote->status == 'open' ? 'selected' : ''?>>Abierto</option>
											<option value="new" <?= $quote->status == 'new' ? 'selected' : ''?>>Nuevo</option>
											<option value="sent" <?= $quote->status == 'sent' ? 'selected' : ''?>>Enviado</option>
											<option value="received" <?= $quote->status == 'received' ? 'selected' : ''?>>Recibido</option>
											<option value="noinfo" <?= $quote->status == 'noinfo' ? 'selected' : ''?>>No Info</option>
											<option value="done" <?= $quote->status == 'done' ? 'selected' : ''?>>Realizado</option>
										</select>
	                                </td>
	                                <td style="text-align: center;">
	                                	@if($quote->comment)
	                                	<a class="comment" style="cursor:pointer" style="color:green" data-comment="{{$quote->comment}}""><i class="fa fa-fw fa-check"></i></a>
	                                	@endif
	                                </td>
	                                <td>
	                                	<a href="<?= url("/admin/quotes/show/{$quote->id}") ?>" style="color:blue" title="<?= trans('cms.edit') ?>"><i class="fa fa-fw fa-pencil"></i></a>
										<a href="<?= url("/admin/quotes/delete/{$quote->id}") ?>" style="color:red" title="<?= trans('cms.delete') ?>" data-confirm="<?= trans('cms.confirm_delete_carrusel') ?>"><i class="fa fa-fw fa-trash"></i></a>
	                                </td>
	                            </tr>
                        	@endforeach
			              </table>
			            </div>
			            <div class="pagination">
							<?php
								$appends = [
									'id' => $id,
					                'date' => $date,
					                'name' => $name,
					                'email' => $email,
					                'city' => $city,
					                'manager' => $manager,
					                'room' => $room,
					                'status' => $status,
					                'status_filter' => $status_filter,
                					'room_filter' => $room_filter
								];
								echo $quotes->appends($appends)->render();
							?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
</section>

<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="modal-good-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<p>¿Está seguro que desea modificar este registro?</p>
			</div>
			<div class="modal-footer">
				<input type="button" id="cancel" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
				<input type="button" id="send" class="btn btn-default" name="" value="Aceptar">
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-comment" tabindex="-1" role="dialog" aria-labelledby="modal-good-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<p id="comment"></p>
			</div>
			<div class="modal-footer">
				<input type="button" id="cancel" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
	var id, data, method, temp, element;

	function change_color(e){
		var color, font;
	        switch( e.val() ){
	        	case "open":
	        		color = "grey";
	        		font = "white";
	        		break;
	        	case "closed":
	        		color = "grey";
	        		font = "white";
	        		break;
	        	case "new":
	        		color = "blue";
	        		font = "white";
	        		break;
	        	case "sent":
	        		color = "yellow";
	        		font = "black";
	        		break;
	        	case "received":
	        		color = "orange";
	        		font = "black";
	        		break;
	        	case "noinfo":
	        		color = "red";
	        		font = "white";
	        		break;
	        	case "done":
	        		color = "green";
	        		font = "white";
	        		break;
	        }
	        e.css('background-color',color);
    		e.css('color',font);
	        e.css('border-radius', '4px');
	        e.css('border', '1px solid #AAAAAA');
	}

	$('.manager, .room, .status').on('focusin', function(){
		temp = $(this).val();
		element = $(this);
	});

	$('.manager').on('change', function(){
		$('#modal-confirm').modal('show');
		id = $(this).attr('data-quote');
		data = $(this).val();
		method = 'setManager';
	});
	
	$('.room').on('change', function(){
		$('#modal-confirm').modal('show');
		id = $(this).attr('data-quote');
		data = $(this).val();
		method = 'setRoom';
	});

	$('#send').on('click', function(){
		$('#modal-confirm').modal('hide');
		$.ajax({
		  url: "{{ URL::to('admin/quotes') }}/"+method,
		  data: {id:id, data: data},
		});
	});

	$('#cancel').on('click', function(){
		element.val(temp);
		if(method == 'setStatus'){
			change_color(element);
		}
	});

	$('.status').change(
	    function (){
	    	change_color($(this));
	    }
	);

	$('.status').trigger('change');

	$('.status').on('change', function(){
		$('#modal-confirm').modal('show');
		id = $(this).attr('data-quote');
		data = $(this).val();
		method = 'setStatus';
	});

	$('.comment').on('click', function(){
		$("#comment").html($(this).attr('data-comment'));
		$('#modal-comment').modal('show');
	});

</script>
@endpush
