@extends('admin.layouts.main')

@section('title', trans('cms.quotes'))

@section('content')

<section class="content-header">
	<h1>
		<i class="fa fa-calculator"></i>
		{{ trans('cms.quote') }}
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('admin/dashboard') }}">
				<i class="fa fa-dashboard"></i> 
				{{ trans('menu.dashboard') }}
			</a>
		</li>
		<li>
			<a href="/admin/quotes">
				<i class="fa fa-calculator"></i> 
				{{ trans('cms.quote') }}
			</a>
		</li>
		<li class="active">{{ $quote->id }}</li>
	</ol> 
</section>

<section class="invoice" style="margin: 10px;">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-header user-block">
				<span class="description" style="margin-left:0">{{ trans('cms.product_created_at') }} {{ $quote->date_created }}</span>
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<strong>{{ trans('cms.quote_customer_name') }}</strong>
			<p>{{ $quote_customer->name }} {{ $quote_customer->lastname }}</p>
			<strong>{{ trans('cms.customer_identification') }}</strong>
			<p>{{ $quote_customer->identification }}</p>
			<strong>{{ trans('cms.label_user_email') }}</strong>
			<p><a href="mailto:{{ $quote_customer->email }}">{{ $quote_customer->email }}</a></p>
			<strong>{{ trans('cms.customer_residence') }}</strong>
			<p>{{ $quote_customer->residence }}</p>
			<strong>{{ trans('cms.customer_cell_phone') }}</strong>
			<p>{{ $quote_customer->cell_phone }}</p>
			<strong>{{ trans('cms.customer_city') }}</strong>
			<p>{{ $quote_customer->getCity['name'] }}</p>
			<strong>{{ trans('cms.customer_job') }}</strong>
			<p>{{ $quote_customer->job }}</p>
			<strong>{{ trans('cms.manager') }}</strong>
			<p>{{ $quote->manager }}</p>
			<strong>{{ trans('cms.room') }}</strong>
			<p>{{ $quote->store['name'] }}</p>
			<strong>{{ trans('cms.label_carrusel_status') }}</strong>
			<p>{{ $quote->status }}</p>
			<strong>{{ trans('cms.comment') }}</strong>
			<br>
			<textarea id="comment" cols="100" rows="5">{{$quote->comment}}</textarea>
			<input id="edit" type="button" class="btn btn-primary" value="Editar Comentario">
		</div>
		<div class="col-md-8">
			<strong>{{ trans('cms.products') }}</strong>
			<ul>
			@foreach($grouped as $grp)
				@foreach($grp as $g)
				<li><p>{{ $g->name }} ({{ $g->color }}) - Cod. {{ $g->code }}</p></li>
				@endforeach
			@endforeach
			</ul>
		</div>
	</div>

	<div class="row">
		<a href="{{ route('admin/quotes') }}" class="btn btn-default pull-right">Volver</a>
	</div>
</section>

<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="modal-good-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<p>¿Está seguro que desea modificar este registro?</p>
			</div>
			<div class="modal-footer">
				<input type="button" id="cancel" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
				<input type="button" id="send" class="btn btn-default" name="" value="Aceptar">
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
	var id, data, method, temp, element;

	$('#comment').on('focusin', function(){
		temp = $(this).val();
		element = $(this);
	});

	$('#edit').on('click', function(){
		$('#modal-confirm').modal('show');
		data = $('#comment').val();
		method = 'setComment';
	});

	$('#send').on('click', function(){
		$('#modal-confirm').modal('hide');
		$.ajax({
		  url: "{{ URL::to('admin/quotes') }}/"+method,
		  data: {id:{{$quote->id}}, data: data},
		});
	});

	$('#cancel').on('click', function(){
		element.val(temp);
	});

</script>
@endpush