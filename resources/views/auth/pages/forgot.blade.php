@extends('auth.layouts.main')

@section('content')

<div class="login-box-body">
	
    <h3 class="login-box-msg">{{ trans('auth.forgot_your_password') }}</h3>

    <form id="form-auth-forgot" method="post">
		{{ csrf_field() }}
		<div class="form-group has-feedback">
			<input type="email" name="email" class="form-control" placeholder="{{ trans('auth.email') }}" maxlength="255">
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('auth.btn_reset_password') }}</button>
		</div>
    </form>

</div>

@endsection

@push('scripts')
<script>
$(function () {
	
	$('#form-auth-forgot').bootstrapValidator({
		fields: {
			email: {
				validators: {
					notEmpty: { message: '<?= trans('auth.form_required_field') ?>' },
					emailAddress: { message: '<?= trans('auth.form_invalid_email') ?>' }
				}
			}
		}
	});
	
});

</script>
@endpush