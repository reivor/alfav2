@extends('auth.layouts.main')

@section('content')

<div class="login-box-body">
	
    <h3 class="login-box-msg">{{ trans('auth.sign_in') }}</h3>

    <form method="post">
		{{ csrf_field() }}
		<div class="form-group has-feedback">
			<input type="email" name="email" class="form-control" placeholder="{{ trans('auth.email') }}">
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password" class="form-control" placeholder="{{ trans('auth.password') }}">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-xs-8">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="remember"> {{ trans('auth.remember_me') }}
					</label>
				</div>
			</div>
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('auth.btn_submit') }}</button>
			</div>
		</div>
    </form>

    <a href="{{ route('auth/forgot') }}">{{ trans('auth.forgot_your_password') }}</a><br>
    <a href="{{ route('auth/register') }}" class="text-center">{{ trans('auth.create_new_account') }}</a>

</div>


@endsection
