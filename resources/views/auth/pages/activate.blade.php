@extends('auth.layouts.main')

@section('content')

    <div class="register-box-body">

        <h3 class="login-box-msg">{{ trans('auth.activated') }}</h3>

        <form method="post" id="activation_password">
            <?= csrf_field() ?>
            <input type="hidden" id="uid" name="uid" value="{{ $user->id }}">
            <div class="form-group form-group-lg">
                <label for="name">{{ trans('cms.label_user_email') }}</label>
                <div class="display-block">{{ $user->email }}</div>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password" class="form-control"  placeholder="{{ trans('auth.password') }}">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password_confirm" id="password_confirm" class="form-control" placeholder="{{ trans('auth.password_confirm') }}">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12 text-right">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('auth.btn_submit') }}</button>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('scripts')
<script>
    $(function () {
        $('#activation_password').bootstrapValidator({
            fields: {
                password: {
                    validators: {
                        notEmpty: { message: '<?= trans('auth.form_required_field') ?>' },
                        stringLength: {
                            min: 6,
                            message: '<?= trans('auth.form_password_too_short') ?>'
                        }
                    }
                },
                password_confirm: {
                    validators: {
                        notEmpty: { message: '<?= trans('auth.form_required_field') ?>' },
                        identical: {
                            field: 'password',
                            message: '<?= trans('auth.form_password_mismatch') ?>'
                        }
                    }
                }
            }
        });
    });
</script>
@endpush
