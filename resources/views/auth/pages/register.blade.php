@extends('auth.layouts.main')

@section('content')

<div class="register-box-body">
    
	<h3 class="login-box-msg">{{ trans('auth.create_account') }}</h3>

    <form id="form-auth-register" method="post">
		{{ csrf_field() }}
		<div class="form-group has-feedback">
			<input type="text" name="name" class="form-control" placeholder="{{ trans('auth.name') }}">
			<span class="glyphicon glyphicon-user form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="email" name="email" class="form-control" placeholder="{{ trans('auth.email') }}">
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password" class="form-control"  placeholder="{{ trans('auth.password') }}">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password_confirm" class="form-control" placeholder="{{ trans('auth.password_confirm') }}">
			<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="terms">
						{{ trans('auth.terms_agree') }} <a href="#">{{ trans('auth.terms_tos') }}</a>
					</label>
				</div>
			</div>
			<div class="col-xs-12 text-right">
				<button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('auth.btn_submit') }}</button>
			</div>
		</div>
    </form>

	<div class="row">
		<div class="col-xs-12">
			<p>
				{{ trans('auth.already_registered') }}
				<a href="{{ route('auth/login') }}" class="text-center">{{ trans('auth.call_sign_in') }}</a>
			</p>
		</div>
	</div>
	
</div>

@endsection

@push('scripts')
<script>
$(function () {
	
	$('#form-auth-register').bootstrapValidator({
		fields: {
			name: {
				validators: {
					notEmpty: { message: '<?= trans('auth.form_required_field') ?>' }
				}
			},
			email: {
				validators: {
					notEmpty: { message: '<?= trans('auth.form_required_field') ?>' },
					emailAddress: { message: '<?= trans('auth.form_invalid_email') ?>' },
					remote: {
						url: '/check/email',
						message: '<?= trans('auth.form_duplicated_email') ?>',
						delay: 1000
					}
				}
			},
			password: {
				validators: {
					notEmpty: { message: '<?= trans('auth.form_required_field') ?>' },
					stringLength: {
						min: 6,
						message: '<?= trans('auth.form_password_too_short') ?>'
					}
				}
			},
			password_confirm: {
				validators: {
					notEmpty: { message: '<?= trans('auth.form_required_field') ?>' },
					identical: {
						field: 'password',
						message: '<?= trans('auth.form_password_mismatch') ?>'
					}
				}
			}
		}
	});
	
});

</script>
@endpush