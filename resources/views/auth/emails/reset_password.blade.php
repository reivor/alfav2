<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Alfa</title>
	</head>
	<body>
		
		<table width="600" align="center" border="0">
			<tr>
				<td>
					<p>
						Click here to reset your password: 
						<a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">
							{{ $link }}
						</a>	
					</p>
				</td>
			</tr>
		</table>
		
	</body>
</html>