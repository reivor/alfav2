<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Alfa CMS</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="icon" type="image/x-icon" href="/favicon.ico" />
		<link rel="stylesheet" href="/themes/admin/vendor/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/fontawesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/ionicons/css/ionicons.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/AdminLTE/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="/themes/admin/vendor/AdminLTE/plugins/iCheck/square/blue.css">
	</head>
	<body class="hold-transition login-page">
		
		<div class="register-box">
			
			<div class="login-logo">
				<a href="{{ route('auth/login') }}">
					<img src="/images/alfa.png" alt="alfa" />
				</a>
			</div>
			@include('auth.partials.flash')
			@yield('content')
		</div>
		
		<script src="/themes/admin/vendor/jquery/dist/jquery.min.js"></script>
		<script src="/themes/admin/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/themes/admin/vendor/bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>
		<script src="/themes/admin/vendor/AdminLTE/plugins/iCheck/icheck.min.js"></script>
		<script>
			$(function () {
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '20%' // optional
				});
			});
		</script>
		@stack('templates')
		@stack('plugins')
		@stack('scripts')
	</body>
</html>