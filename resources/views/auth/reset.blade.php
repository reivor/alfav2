@extends('auth.layouts.main')

@section('content')

<div class="login-box-body">
	
    <p class="login-box-msg">{{ trans('auth.btn_reset_password') }}</p>

    <form method="post">
		{{ csrf_field() }}
		<input type="hidden" name="token" value="{{ $token }}">
		<div class="form-group has-feedback">
			<input type="email" name="email" class="form-control" value="{{ $email or old('email') }}" placeholder="{{ trans('auth.email') }}">
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password" class="form-control" placeholder="{{ trans('auth.password') }}">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password_confirmation" class="form-control" placeholder="{{ trans('auth.password_confirm') }}">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('auth.btn_reset_password') }}</button>
		</div>
    </form>

</div>

@endsection

