@if (session('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<p>{{ session('success') }}</p>
</div>
@endif

@if (session('status'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<p>{{ session('status') }}</p>
</div>
@endif

@if (session('error'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<p>{{ session('error') }}</p>
</div>
@endif

@if (session('errors'))
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<?php foreach( session('errors')->all() as $error ): ?>
	<p><?= $error ?></p>
	<?php endforeach; ?>
</div>
@endif