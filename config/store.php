<?php

return [

	'id' => env('STORE_ID', 'CO'),
	'locale' => env('STORE_LOCALE', 'es'),
	'timezone' => env('STORE_TIMEZONE', 'America/Bogota'),
	
];