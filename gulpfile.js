//var elixir = require('laravel-elixir');
var gulp = require('gulp'),
minifyCss = require("gulp-clean-css"),
concat = require("gulp-concat"),
uglify = require("gulp-uglify");
var sourcemaps = require('gulp-sourcemaps');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

/*elixir(function(mix) {
    mix.sass('app.scss');
});*/

// Minify Css
gulp.task('minify-css', function () {
    gulp.src([
		'./public/css/**/*.css', 
		'!./public/css/main.css', 
		'!./public/css/main-md.css',
		'!./public/css/main-sm.css',
		'!./public/css/overwrite.css'
	])
	.pipe(minifyCss())
	.pipe(concat('vendor.min.css'))
	.pipe(gulp.dest("./public/dist"));
	
	 gulp.src([
		'./public/css/main.css', 
		'./public/css/main-md.css',
		'./public/css/main-sm.css',
		'./public/css/overwrite.css'
	])
	
	.pipe(sourcemaps.init())
	.pipe(minifyCss())
	.pipe(sourcemaps.write())
	.pipe(concat('styles.min.css'))
	
	.pipe(gulp.dest("./public/dist"));
});

// Minify JavaScript
gulp.task('minify-js', function () {
    
    gulp.src([
		'./public/js/vendor/jquery-1.11.2.min.js',
		'./public/js/vendor/bootstrap.min.js', 
        './public/js/select/classie.js', 
        './public/js/select/selectFx.js', 
        './public/js/vendor/mobile-detect.min.js',
        './public/js/jquerybxslider.js',
        './public/js/jquery.mCustomScrollbar.concat.min.js',
		'./public/js/vendor/jquery.mobile.custom.min.js',
		'./public/themes/store/vendor/owl.carousel.2.1.0/owl.carousel.min.js',
		'./public/themes/store/vendor/handlebars/handlebars.min.js'
    ])
        .pipe(uglify())
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest("./public/dist"));

    gulp.src([
        './public/themes/store/js/*.js'
    ])
        //.pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest("./public/dist"));
       
});
